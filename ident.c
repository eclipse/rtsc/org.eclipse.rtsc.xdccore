/*
 *  ======== ident.c ========
 */

#define _VERS_ "@(#)*** xdc-K04"

static char __NAME[] = _NAME_;
static char __DATE[] = _DATE_;
static char __VERS[] = _VERS_;
static char __CSUM[] = _CSUM_;

/* keep compiler from warning about unused variables above */
static int __ISIZ = sizeof(__NAME) + sizeof(__DATE)
                    + sizeof(__VERS) + sizeof(__CSUM) + sizeof(__ISIZ);
