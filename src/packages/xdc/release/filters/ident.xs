/*
 *  ======== getTreeName ========
 */
function getTreeName()
{
    var treeName = environment["xdc.release.filters.tree"];
    
    if ((treeName == undefined) || (treeName.length == 0)) {
        /* look for ident file and read it */
        var ident = getTreeIdent();
        if (ident != null) {
            /* parse ident for the version string */
            var file = new java.io.BufferedReader(
                new java.io.FileReader(ident));
            var pattern = "@(#)*** ";
            var nextLine;
            while ((nextLine = file.readLine()) != null) {
                var position;
                var endpos;
                if ((position = nextLine.indexOf(pattern)) > -1) {
                    endpos = nextLine.lastIndexOf("\"");
                    if (endpos == -1) {
                        endpos = nextLine.length();
                    }
                    treeName = nextLine.substring(
                        position + pattern.length,
                        endpos - 1);
                    break;
                }
            }
            file.close();
        }
    }

    return (treeName);
}

/*
 *  ======== getTreeIdent ========
 */
function getTreeIdent(start)
{
    if (start == null) {
        start = ".";
    }

    var localWorkDir = new java.io.File(start).getCanonicalFile();
    var prefix = "";
    do {
        var ident = new java.io.File(localWorkDir, "ident.c");
        if (ident.exists()) {
	    /* return "portable" path relative to start; although it 
	     * would be simpler to just return an absolute path this
	     * wouldn't allow us to run config on Linux and compile
	     * the result in Windows VM (which is handy for us Linux
	     * users).
	     */
	    check(ident, prefix + "ident.c");
            return (prefix + "ident.c");
        }
        prefix += "../";
    } while ((localWorkDir = localWorkDir.getParentFile()) != null);

    return (null);
}

/*
 *  ======== check ========
 */
function check(file, name)
{
    /* check that file found by looking up 'name' on the package path is
     * the exactly the same as the file specified by 'file'.
     */
    var cp1 = file.getCanonicalPath();
    var cp2 = (new java.io.File(xdc.findFile(name))).getCanonicalPath();
    if (cp1 != cp2) {
	print("warning: ident file '" + name + "' (" + cp2 + ") != " + cp1);
    }
}
