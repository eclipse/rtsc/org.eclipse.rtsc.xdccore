/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
utils.importFile('xdc/xdc.tci');
xdc.$$make_om('bld');	    /* create and name this object model */
xdc.setCurPkgBase(".");

var _bldUtils = xdc.useModule('xdc.bld.Utils');

var N = 20000;

function main() {
    if (false) {
	for (var i = 0; i < 5; i++) {
	    fgrow(N);
	}
    }
    
    print(N);
    for (var i = 0; i < N; i++) {
	expandString2("this ia a test $(rootDir) ok!", {
	    rootDir: "foo",
	    rootDir1: "foo1",
	    rootDir2: "foo2",
	});
    }
}

/*
 *  ======== expandString ========
 */
function expandString(cmd, values)
{
    return (String(Packages.xdc.services.intern.xsr.Utils.expandString(cmd, values)));
}

/*
 *  ======== expandString2 ========
 */
function expandString2(cmd, values)
{
    return (_bldUtils.expandString(cmd, values));
}

main();
