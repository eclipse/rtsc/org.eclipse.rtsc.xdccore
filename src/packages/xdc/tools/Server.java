/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.tools;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import java.net.URLClassLoader;
import java.net.URL;
import java.net.MalformedURLException;
import java.net.URLClassLoader;

import java.io.File;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import java.security.Permissions;
import java.security.AllPermission;
import java.security.CodeSource;
import java.security.PermissionCollection;

/**
 * An XDCscript ICmd-based command run as a headless service to a client.
 * <p/>
 * Each Server instance runs in a private XDCscript environment that
 * emulates stand-alone XDCscript. The client can control which Java
 * classes are shared between the client and Server -- doing so defines
 * a contract between client and server that must be maintained for
 * interoperability between different client and server versions. Best
 * practice is to aggressively minimize the scope of sharing.
 */
public class Server
{
    String xdcRoot = null;
    String XDCPATH = null;
    String xdcPath = null;

    Class<?> shellCls = null;
    Constructor<?> shellCon = null;
    Method execM = null;
    Method exitM = null;
    ClassLoader clsLoader = null;
    boolean debug = false;

    /** Global Server thread identifier. */
    static int threadId = 1;

    /** Map for finding the ServerSocket that belongs to a Server. */
    static HashMap<Integer,ServerSocket> socketMap =
        new HashMap<Integer,ServerSocket>();

    static HashMap<String,URLClassLoader> xdcRootClassLoaders =
        new HashMap<String,URLClassLoader>();

    /**
     * Early check that jar file exists
     */
    private static URL checkJar(String dir, String file) {
        try {
            File f = new File(dir, file).getAbsoluteFile();
            if (!f.exists()) {
                throw new RuntimeException("can't open jar file " + f.getPath());
            }
            return new URL("file:" + f.getPath());
        }
        catch (Exception e) {
            error(e);
            return null;
        }
    }

    /**
     * Get the list of .jar files required to boot XDCscript, that can
     * be shared between independent XDCscript sessions. These are all
     * located in package xdc.shelf.
     */
    private URL[] getShareableJars() throws MalformedURLException
    {
        ArrayList<URL> shareableJars = new ArrayList<URL>();

        /* check that the xdc.shelf/java directory exists */
        File xdcShelfJava = new File(xdcRoot  + "/packages/xdc/shelf/java");
        if (!xdcShelfJava.exists()) {
            throw new RuntimeException(
                "can't load jars from package xdc.shelf at " +
                xdcShelfJava.getPath());
        }

        /* add all the .jar files in that directory */
        for (File jar : xdcShelfJava.listFiles()) {
            if (jar.getName().endsWith(".jar")) {
                URL url = new URL("file:" + jar.getAbsolutePath());
                shareableJars.add(url);
            }
        }

        return shareableJars.toArray(new URL[shareableJars.size()]);
    }

    private void initXdcRoot()
    {
        /* if already initialized, do nothing */
        if (shellCon != null && execM != null) {
            return;
        }

        /* get the xdc.root property */
        xdcRoot = props.getProperty("xdc.root");
        if (xdcRoot == null || (xdcRoot.length() == 0)) {
            xdcRoot = props.getProperty("XDCROOT");
        }
        if (xdcRoot == null || (xdcRoot.length() == 0)) {
            xdcRoot = props.getProperty("config.rootDir");
        }
    }

    private void initShell(String shellClassName)
    {
        try {
            /* load the Shell class using a Firewall instance */
            shellCls = clsLoader.loadClass(shellClassName);

            /* find the constructor that has a single Properties argument */
            Constructor<?> cL[] = shellCls.getConstructors();
            final String cSig = "public " + shellClassName
                + "(java.util.Properties)";
            for (Constructor<?> c: cL) {
                if (c.toString().equals(cSig)) {
                    shellCon = c;
                    break;
                }
            }

            /* find the exec() and exit() methods */
            Method mL[] = shellCls.getMethods();
            final String mSig = "public int " + shellClassName
                + ".exec(java.lang.String[])";
            final String exSig = "public void " + shellClassName + ".exit()";
            int breakCounter = 2;
            for (Method m: mL) {
                if (m.toString().equals(mSig)) {
                    execM = m;
                    if (--breakCounter == 0) {
                        break;
                    }
                }
                if (m.toString().equals(exSig)) {
                    exitM = m;
                    if (--breakCounter == 0) {
                        break;
                    }
                }
            }

            /* if anything is missing, forget all of it */
            if (shellCls == null || shellCon == null || execM == null) {
                shellCls = null;
                execM = null;
                shellCon = null;
            }
        }
        catch (Exception e) {
            shellCls = null;
            execM = null;
            shellCon = null;
        }
    }

    private void initMethods() {
        try {
            // get a classloader to share between Servers with the same xdcRoot
            URLClassLoader xdcRootClassLoader
                = xdcRootClassLoaders.get(xdcRoot);
            if (xdcRootClassLoader == null){
                xdcRootClassLoader = new URLClassLoader(
                    getShareableJars(), Firewall.getJvmClassLoader());
                xdcRootClassLoaders.put(xdcRoot, xdcRootClassLoader);
            }

            /* add non-shareable jars required to bootstrap XDCscript */
            URL jars[] = new URL[]{
                checkJar(xdcRoot, "config.jar")
            };

            /* create a new classloader that adds the required classes */
            clsLoader = new Firewall(jars, apiClasses, apiClassLoader,
                xdcRootClassLoader);

            /* if debugging requested, try to set up the debugger */
            if (debug) {
                initShell("config.ShellGUIDebugger");
            }

            /* if not requested or not found, use the regular shell */
            if (execM == null) {
                initShell("config.Shell");
            }

            /* 'curClsLoader' is a static member of the class Shell of type
             * ShellClassLoader. Its purpose is to load java/package.jar files
             * from packages. We need it here to load xdc.services.global.Err
             * class to invoke its function setToolMode, which disables
             * System.exit().
             * Before we can load the class Err, we need to add its package jar
             * to the classpath for ShellClassLoader.
             */
            ClassLoader shellClsLoader = (ClassLoader)
		(shellCls.getDeclaredField("curClsLoader").get(null));
            Class sclClass = shellClsLoader.getClass();
            
            /* We cannot simply cast shellClsLoader as config.ShellClassLoader
             * because the cast would mean the class is defined by
             * AppClassLoader. shellClsLoader is loaded by Firewall, which 
             * makes it a different class.
             */
            Method addJarMethod = sclClass.getMethod("addJar",
                Class.forName("java.lang.String"));
            addJarMethod.invoke(shellClsLoader, xdcRoot
                + "/packages/xdc/services/global/java");
            Class<?> errCls =
                shellClsLoader.loadClass("xdc.services.global.Err");

            /* find the setToolMode() method */
            Method[] mL = errCls.getMethods();
            Method setToolModeM = null;
            final String errSig =
                "public static void xdc.services.global.Err.setToolMode(boolean)";
            for (Method m: mL) {
                if (m.toString().equals(errSig)) {
                    setToolModeM = m;
                    break;
                }
            }

            /* if found, set tool mode to disallow System.exit() */
            if (setToolModeM != null) {
                setToolModeM.invoke(null, new Object[] {false});
            }
        }
        catch (Exception e) { error(e); }
    }
    
    ServerSocket socket;
    Thread thread;
    int tid;
    Properties props;
    private final String cmdPkg;
    private final String[] cmdArgs;
    private final Class<?>[] apiClasses;
    private final ClassLoader apiClassLoader;
    
    /**
     * Execute an XDCscript command in a client/server context.
     * <p/>
     * The following Java properties are recognized:
     * <p/>
     *  xdc.root -- required. Gives the location of the XDCtools installation.
     *  XDCPATH -- optional. If defined, adds additional repositories to the
     *    package path. May contain the "^" character to indicate the
     *    repository of the command. Only used if xdc.path is not defined
     *    or is null.
     *  xdc.path -- optional. If defined, it overrides the XDCPATH. This
     *    is the full package path, including the XDCtools and current
     *    repositories. Must not include the "^" character.
     */
    public Server( String cmdPkg, String cmdArgs[] )
    {
        this(cmdPkg, cmdArgs, new ServerSocket());
    }

    /**
     * Execute an XDCscript command using a given socket for communication.
     */
    public Server( String cmdPkg, String cmdArgs[], ServerSocket socket ) {
        this(cmdPkg, cmdArgs, socket, System.getProperties());
    }

    /**
     * Execute an XDCscript command using a giving set of Java properties.
     */
    public Server( String cmdPkg, String cmdArgs[], Properties props ) {
        this(cmdPkg, cmdArgs, new ServerSocket(), props);
    }

    /**
     * Execute an XDCscript command using a given socket for communication
     * and a given set of Java properties.
     */
    public Server( String cmdPkg, String cmdArgs[], ServerSocket socket,
        Properties props )
    {
        this(cmdPkg, cmdArgs, socket, props, null, null);
    }

    /**
     * Execute an XDCscript command, sharing only a given set of classes
     * between the client and server.
     *
     * Enforces separation between the client and the server, while still
     * allowing communication based on the named classes. Classes not named in
     * the list are considered part of the implementation of the client and/or
     * server, not the interface. The implementations of the client and server,
     * or even multiple instances of the server, are protected from each other.
     * Implementations may have their own static variables and use different
     * versions of the classes.
     *
     * @param apiClasses a list of classes to share between the client
     *     and server. Classes loaded by the system classloader are always
     *     shared, including all JRE classes.
     */
    public Server( String cmdPkg, String cmdArgs[], ServerSocket socket,
        Properties props, Class<?> apiClasses[] )
    {
        this(cmdPkg, cmdArgs, socket, props, apiClasses, null);
    }

    public Server( String cmdPkg, String cmdArgs[], ServerSocket socket,
        Properties props, ClassLoader apiClassLoader )
    {
        this(cmdPkg, cmdArgs, socket, props, null, apiClassLoader);
    }

    /**
     * Execute an XDCscript command, sharing only the classes visible to
     * a given classloader.
     */
    public Server( String cmdPkg, String cmdArgs[], ServerSocket socket,
        Properties props,
        Class<?> apiClasses[], ClassLoader apiClassLoader)
    {
        this.cmdPkg = cmdPkg;
        this.cmdArgs = cmdArgs;
        this.socket = socket;
        this.props = props;
        this.apiClasses = apiClasses;
        this.apiClassLoader = apiClassLoader;

        /* make sure the XDCscript classes are loaded */
        initXdcRoot();

        this.tid = threadId++;
        socketMap.put(this.tid, this.socket);
    }
    
    private void initProperties() {
        /* Collect the path info. Only look in the given Properties,
         * don't accept values inherited as default properties.
         */
        XDCPATH = (String)props.get("XDCPATH");
        if (XDCPATH != null && XDCPATH.equals("")) {
            XDCPATH = null;
        }
        xdcPath  = (String)props.get("xdc.path");
        if (xdcPath != null && xdcPath.equals("")) {
            xdcPath = null;
        }

        /*
         * For compatibility with clients that are incorrectly setting
         * xdc.path as though it were XDCPATH, make sure that
         * xdc.path, if set, includes the xdc.root repository.
         */
        if (xdcPath != null) {
            String path = ";" + xdcPath.replace("\\", "/") + ";";
            String root = xdcRoot.replace("\\", "/");
            if (!path.contains(";" + root + "/packages;")) {
                /* put xdc.root on the end of the package path -- best effort */
                xdcPath += ";" + xdcRoot + "/packages";
            }
        }

        /*
         * For compatibility with older XDCtools versions, always set
         * the global system properties too. New XDCtools releases that
         * correctly use the local properties will ignore these values.
         */
        System.setProperty("xdc.root", xdcRoot);
        System.setProperty("config.rootDir", xdcRoot);
        set(System.getProperties(), "xdc.path", xdcPath);
        set(System.getProperties(), "XDCPATH", XDCPATH);

        /*
         * Pass xdc.root and XDCPATH to the config.Shell as xdc script
         * requires them. They are local to this session.
         */
        props.setProperty("xdc.root", xdcRoot);
        props.setProperty("config.rootDir", xdcRoot);
        set(props, "XDCPATH", XDCPATH);
        set(props, "xdc.path", xdcPath);

        /* config.scriptName must be set to the empty string to support
         * XDCtools releases (3.24.06 and earlier) whose tconfini.tcf and
         * utils.tci assume environment["config.scriptName"] is non-null.
         */
        props.setProperty("config.scriptName", "");

        if (debug) {
            /* ShellGUIDebugger requires these not be null */
            props.setProperty("config.gopts", "");
        }
    }

    private void initThread() {
        /* cargs must be final because its value is copied as a local variable
         * to run(), so it has to be clear that 'cargs' will not change between
         * the time a new Runnable instance is constructed and the time when 
         * run() is invoked. 'local' in a method means that the reference
         * 'cargs' cannot change, but the object pointed by it obviously can
         * and it does changes below.
         */
        final ArrayList<String> cargs = new ArrayList<String>();
        // --thread# is used by xdc.tools.Cmdr to connect the socket
        cargs.addAll(Arrays.asList(new String[] {
            "-f",
            xdcRoot + "/tconfini.tcf",
            xdcRoot + "/packages/xdc/xs.js",
            cmdPkg,
            "--thread#" + this.tid
        }));
        for (String a: cmdArgs) {
            cargs.add(a);
        }

        final Object[] conArgs = new Object[]{props};
        final ServerSocket sock = socket;
        this.thread = new Thread(new Runnable() {
            public void run()
            {
                Object nInst = null;
                try {
                    if (shellCon != null) {
                        nInst = shellCon.newInstance(conArgs);
                    }
                    execM.invoke(
                        /* if shellCon is null, is a static method */
                        shellCon != null? nInst : null,
                        (Object)cargs.toArray(new String[0])
                    );
                    if (nInst != null) {
                        exitM.invoke(nInst, new Object[0]);
                    }
                }
                catch (Exception e) {
                    error(e);
                }
                finally {
                    sock.giveToClient(null);
                }
            }
        });

        this.thread.setName(cmdPkg + '@' + this.tid);
    }

    private static void set(Properties dst, String key, String value) {
        if (value != null) {
            dst.setProperty(key, value);
        }
        else {
            dst.remove(key);
        }
    }

    // error
    static void error( Exception e )
    {
        System.err.println(e.getClass().getName() + ": " + e.getMessage());
        throw new RuntimeException(e.getMessage(), e);
    }
    
    // findSocket
    public static ServerSocket findSocket( int tid )
    {
        return socketMap.get(tid);
    }
    
    // getSocket
    public  ServerSocket getSocket( )
    {
        return socket;
    }
    
    // getId
    public  int getId( )
    {
        return (this.tid);
    }

    /**
     * Get whether graphical debug is enabled.
     */
    public boolean getDebug() {
        return debug;
    }

    /**
     * Set whether the Rhino graphical debugger will be launched on startup.
     * Cannot be changed once the Server thread has been started.
     */
    public void setDebug(boolean debug) {
        if (this.thread == null) {
            this.debug = debug;
        }
    }

    // read
    public String read()
    {
        return (String)this.socket.takeFromServer();
    }
    
    // start
    public void start()
    {
        initMethods();
        initProperties();
        initThread();
        this.thread.start();
    }

    // stop
    public void stop()
    {
        this.socket.giveToServer(null);
        socketMap.remove(this.tid);
    }

    // write
    public void write( String s )
    {
        this.socket.giveToServer(s);
    }

    /**
     * Classloader that restricts sharing classes between the client
     * and Server. Allows the server to share a specific set of classes
     * with the client, but load others from the package path.
     */
    private static class Firewall extends URLClassLoader {
        /*
         * @param jars jar files needed for booting XDCscript,
         *    not shared between Server instances.
         * @param apiClasses classes shared between the client
         *    and Server, or null.
         * @param apiClassLoader classloader shared between the
         *    client and Server, or null.
         * @param xdcRootClassLoader classloader shared between
         *    all Servers with the same xdcRoot, or null.
         */
        public Firewall(
            URL jars[],
            Class<?> apiClasses[],
            ClassLoader apiClassLoader,
            ClassLoader xdcRootClassLoader
        ) {
            /*
             * Create the URL classloader to load the base jars. Allow
             * only the JVM bootstrap classloader and JVM extension
             * classloader as parents, so that each Server starts with
             * a clean set of classes.
             */ 
            super(jars, Firewall.getJvmClassLoader());
            this.apiClasses = apiClasses;
            this.apiClassLoader = apiClassLoader;
            this.xdcRootClassLoader = xdcRootClassLoader;
        }

        /**
         * Get a classloader that includes only the JVM bootstrap
         * classes and JVM extension classes.
         */
        public static ClassLoader getJvmClassLoader() {
            return ClassLoader.getSystemClassLoader().getParent();
        }

        protected Class<?> findClass(String name)
            throws ClassNotFoundException
        {
            /* first check the Server's own classes */
            for (Class<?> c : serverClasses) {
                if (c.getCanonicalName().equals(name)) {
                    return c;
                }
            }

            /* next check the API classes the user specified */
            if (apiClasses != null) {
                for (Class<?> c : apiClasses) {
                    if (c.getCanonicalName().equals(name)) {
                        return c;
                    }
                }
            }

            /* next check the API class loader */
            try {
                if (apiClassLoader != null) {
                    return apiClassLoader.loadClass(name);
                }
            }
            catch (ClassNotFoundException e) {}

            /* next check shared implementation classes */
            try {
                if (xdcRootClassLoader != null) {
                    return xdcRootClassLoader.loadClass(name);
                }
            }
            catch (ClassNotFoundException e) {}

            /* next check non-shared implementation jars */
            try {
                return super.findClass(name);
            }
            catch (ClassNotFoundException e) {}

            /* otherwise, report that we can't find the class */
            throw new ClassNotFoundException(
                Firewall.class.getName() + ": can't find class " + name
            );
        }

        /* fast, permissive implementation */
        protected PermissionCollection getPermissions(CodeSource cs) {
            Permissions p = new Permissions();
            p.add(new AllPermission());
            return p;
        }

        private Class<?> apiClasses[];
        private ClassLoader apiClassLoader;
        private ClassLoader xdcRootClassLoader;
        private final Class<?> serverClasses[] = new Class<?>[] {
            Server.class,
            ServerSocket.class
        };
    }
}
