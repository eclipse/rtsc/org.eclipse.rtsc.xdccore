include $(XDCROOT)/packages/xdc/bld/xdc_java.mak

CLASSES = $(patsubst %.java,%,$(wildcard *.java))
JARFILE = java/package.jar

CONFIGJAR = $(firstword $(wildcard \
	$(XDCROOT)/config.jar \
	$(CURDIR)/../../../../src/packages/xdc/xs/config.jar \
))

JCPATH := $(PKGROOT)$(PATHCHAR)$(CONFIGJAR)

.libraries: $(JARFILE)
$(JARFILE): $(patsubst %,$(CLASSDIR)/%.class,$(CLASSES))

clean::
	$(RM) $(JARFILE)

