#!/usr/bin/ksh
#
#   usage: xprurl.ksh file ...
#
GRAPHVIZ=/db/ztree/library/trees/gnu-a07/src/graphviz/local/solaris/bin

dot=$GRAPHVIZ/dot
tred=$GRAPHVIZ/tred
gpr=$GRAPHVIZ/gpr

if [ $# -lt 1 ]; then
    echo usage: xprurl file ...
    exit 1
fi
dotfiles=$@

if [ -n "$dotfiles" ]; then
    for d in $dotfiles; do
	echo processing $d ...
	$gpr -c -f xprurl.gpr $d 
    done
fi

