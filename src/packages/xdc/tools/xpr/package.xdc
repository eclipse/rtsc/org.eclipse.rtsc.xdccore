/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*!
 *  ======== xdc.tools.xpr ========
 *  Package print utility
 *
 *  This package provides a simple script that generates HTML pages
 *  for the packages in specified directories.
 *
 * @a(USAGE)
 * @p(code)
 *  xs -f $(XDCROOT)/packages/xdc/tools/xpr/xpr.xs [-od:<dir>] [-title <title_string>] dir ...
 * @p
 * @a(OPTIONS)
 *  @p(dlist)
 *  - `-od:<dir>`
 *      generate all pages in the specified output director `dir`.  If -od:
 *      is not specified, output files are placed in the current working
 *      directory.
 *  - `-title <title_string>
 *      Each page is given the specified title string.  if `-title` is not
 *      specified, the title is set to "XPR".
 * @p
 */
package xdc.tools.xpr [1, 0, 0]
{
}
