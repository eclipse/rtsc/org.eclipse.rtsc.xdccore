/* --COPYRIGHT--,ESD
 *  Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  This program and the accompanying materials are made available under the
 *  terms of the Eclipse Public License v1.0 and Eclipse Distribution License
 *  v. 1.0 which accompanies this distribution. The Eclipse Public License is
 *  available at http://www.eclipse.org/legal/epl-v10.html and the Eclipse
 *  Distribution License is available at
 *  http://www.eclipse.org/org/documents/edl-v10.php.
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 * --/COPYRIGHT--*/
/*
 *  ======== xdc_noinit.c ========
 */

#include <stdint.h>

int_least32_t __xdc__init(void);

/*
 *  ======== __xdc__init ========
 */
int_least32_t  __xdc__init(void)
{
    return (-1);
}
