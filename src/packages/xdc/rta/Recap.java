/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008-2015 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/

/*
 *  ======== Recap.java ========
 *  This class helps ROV and RTA to locate an executable's package basement.
 */
package xdc.rta;

import xdc.services.global.What;
import java.io.File;

/*
 *  ======== Recap ========
 */
public class Recap {

    /*
     *  ======== locateRecap ========
     *  Parses the given 'executable' to find the location of the file within
     *  the package basement with the given 'extension'.
     *
     *  If the recap file can't be located in the exact location specified by
     *  the embedded path, then this function will use the provided
     *  IFileLocator instances to attempt to find it. This is helpful if, for
     *  example, the application was built on Linux and the embedded path is
     *  in Linux format, but we're trying to locate the recap file from
     *  Windows. In this case, the path can be corrected by replacing drive
     *  letter mappings.
     */
    public static String locateRecap(String executable, String extension,
                                     IFileLocator[] locators)
        throws Exception
    {
        /* Verify executable exists. */
        if (!(new File(executable)).exists()) {
            throw new Exception("Recap: can't find " + executable);
        }

        /* Extract the path from the executable. */
        String line = What.getWhatString(executable);

        /* For non-RTSC executable, What returns "" */
        if (line.equals("")) {
            throw (new Exception("Can't extract RTSC configuration package " +
                                 "path from '" + executable + "'."));
        }

        debugPrint("'What' string: \n" + line);

        String asm = "__ASM__ = ";
        int start = line.indexOf(asm);

        if (start == -1) {
            throw (new Exception("Can't extract RTSC configuration package " +
                                 "path from '" + executable + "'."));
        }

        start += asm.length();

        String recapPath = line.substring(start, line.indexOf('\n', start));
        recapPath += extension;

        /* Check if the recap can be found in its original location. */
        debugPrint("Recap: Original path to recap file = " + recapPath);
        if ((new File(recapPath)).exists()) {
            debugPrint("Recap: Found recap file at: " + recapPath);
            return (recapPath);
        }

        /*
         * If the original location doesn't exist, try using the provided
         * IFileLocators.
         */
        if (locators != null) {
            for (int i = 0; i < locators.length; i++) {
                /* See if this locator can find the file. */
                String newPath = locators[i].findFile(recapPath);

                /* If a valid path was found, return it. */
                if ((newPath != null) && (new File(newPath)).exists()) {
                    debugPrint("Recap: Found recap file at: " + newPath);
                    return (newPath);
                }
            }
        }

        String recapBase = recapPath.substring(recapPath.lastIndexOf("/") + 1);

        /* Check for the recap file by finding a relative path */
        /*
        String newRecapPath = checkRelativePath(executable, recapPath);
        debugPrint("Recap: Checking relative path: " + newRecapPath);
        if (java.io.File(newRecapPath).exists()) {
            debugPrint("Recap: Found recap file at: " + newRecapPath);
            return (newRecapPath);
        }
        */
        /* Check for the recap file along a search path */
        if (true) {

            /* get the directory containing the executable */
            int index = executable.replace('\\', '/').lastIndexOf("/") + 1;
            String cwd = executable.substring(0, index);

            String pathTab[] = {
                cwd,
                cwd + "package/cfg/",
                cwd + "../package/cfg/debug/",
                cwd + "../package/cfg/release/",
                cwd + "../package/cfg/whole_program/",
                cwd + "../package/cfg/whole_program_debug/"
            };

            for (int i = 0; i < pathTab.length; i++) {
                String cf = pathTab[i] + recapBase;
                debugPrint("Recap: Checking in " + cf +
                                   " ...");
                if ((new File(cf)).exists()) {
                    return (cf);
                }
            }
            throw (new Exception(cantFind(recapBase, recapPath, cwd)));
        }
        throw new Exception("Can't find recap file.");
    }

    /*
     *  ======== locateRecap ========
     *  Finds the specified recap file for the given executable by running
     *  the What utility on it to extract the path to the configuration.
     */
    public static String locateRecap(String executable, String extension)
        throws Exception
    {
        return (locateRecap(executable, extension, null));
    }
    
    /*
     *  ======== cantFind ========
     *  This helper API forms the error message to report to the user if the
     *  recap file cannot be found.
     *
     *  This API was created simply because the message is so long.
     */
    private static String cantFind(String filename, String origPath, String cwd)
    {
        return ("Cannot find the required file " + filename +  
               ". The original path to the file does not exist:\n"+ 
               "    " + origPath + 
               "\n\nIf the file is missing, please rebuild the associated " +
               "RTSC configuration." +
               "\n\nOtherwise, if the path is no longer valid, please move " +
               "the file to:\n" +
               "    " + cwd + filename);
    }
    
    private static String traceEnable = System.getProperty("xdc.rta.traceEnable");
    
    private static void debugPrint(String msg)
    {
        if ((traceEnable != null) && traceEnable.equals("true")) {
            System.out.println("[Recap] " + msg);
        }
    }
}
