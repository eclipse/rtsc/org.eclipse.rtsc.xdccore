/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
 
/*
 *  ======== IEventMetaData.java ========  
 */
package xdc.rta;

/*
 *  ======== IEventMetaData ========
 *  This interface is used by a HostEvent to retrieve what it needs to decode
 *  the record that it represents.
 *  
 *  The interface provides the following information:
 *    - Object file reader for looking Log_print format strings
 *    - Event id to event name and event message mapping
 *    - Module id to module name mapping
 *    - Target IArg size (for formatting)
 *    - Target record size (to support HostEvent.getRecordSize)
 *  
 *  An interface has been defined here to allow for multiple implementations.
 *  In RTA, the interface is implemented by the xdc.rta.MetaData class which
 *  wraps the RTA XML file. In ROV, the interface is implemented by 
 *  xdc.runtime.Log APIs which reference the ROV recap file. This allows the
 *  HostEvent class to be shared between RTA and any ROV views which deal with
 *  XDC log records.
 */
public interface IEventMetaData {
    
    /*
     *  ======== getOFReader ========
     *  Returns an object file reader instance to use for retrieving Log_print
     *  format strings.
     */
    public IOFReader getOFReader();
	
    /*
     *  ======== lookupEventMessage ========
     *  Returns the unformatted event message for the specified event.
     */
    public String lookupEventMessage(int eventId);

    /*
     *  ======== getTargetArgSize ========
     *  Returns the target size of the IArg type in bytes (not MAUs).
     */
    public int getTargetArgSize();
		
    /*
     *  ======== lookupEventName ========
     *  Retrieve the name of the event with the specified id.
     */
    public String lookupEventName(int eventId);

    /*
     *  ======== lookupModuleName ========
     *  Retrieve the name of the module with the specified id.
     */
    public String lookupModuleName(int modId);

    /*
     *  ======== getTargetEventRecSize ========
     *  Returns the size of a record on the target in bytes (not MAUs).
     */
    public int getTargetEventRecSize();
}
