package xdc.rta;

/*!
 *  ======== IFileLocator ========
 *  Interface for classes which help locate a file at a given path when the
 *  original path is no longer valid.
 *  
 *  The most common example is when an application is built on Linux, but
 *  is being analyzed with a tool on Windows, where the original Linux 
 *  path is no longer valid.
 */
public interface IFileLocator {

    /*
     *  ======== findFile ========
     *  Takes the original path to a file, and uses path mappings or other
     *  techniques to help locate it in the current environment.
     */
    String findFile(String origPath);   
}
