#
#  ======== custom.mak ========
#
#  Revision History
#  ================
#! 25-Aug-2011 sasha	jars are not compressed anymore
#! 21-May-2007 sasha	created from tconf/Makefile
#


JDK ?= $(TOOLS)/default/jdk$(_M)

XDCTOOLS_JVM_TARGET = 1.7
XDCTOOLS_JVM_BOOTCLASSES = $(TOOLS)/vendors/oracle/jre/1.7.0_80/Linux/lib/rt.jar

#
#  P	   - is the package directory path for the configuration classes
#  CLASSES - is the list of a top level classes in the config package.
#	     note that these classes may contain nested classes and, 
#	     thus, the actual classes in the package may be be more than
#	     what is defined by CLASSES

P	= config
CLASSES := $(patsubst %.java,%.class,$(wildcard $P/*.java))

.libraries: config.jar

#
# The config classes require js.jar from the rhino javascript package.
#
CLPATH = $(PKGROOT)/xdc/shelf/java/js.jar

%.class: %.java 
	@$(MSG) javac $< ...
	$(JDK)/bin/javac -source $(XDCTOOLS_JVM_TARGET) -target $(XDCTOOLS_JVM_TARGET) -bootclasspath $(XDCTOOLS_JVM_BOOTCLASSES) -classpath ".:$(CLPATH)" $<

#
# The config jar is built by archiving *all* files that config.jar 
# depends on.
#
config.jar: $(CLASSES)
	@echo "jar cf0 $@ $^ ..."
	@rm -f $@
	@list="";					\
	 for f in $^; do				\
	     if [ "`suffix $$f`" = "class" ]; then	\
	 	p=`prefix $$f`;				\
	 	list="$$list $$p*.class";		\
	     else					\
		list="$$list $$f";			\
	     fi;					\
	 done;						\
	$(JDK)/bin/jar cf0 $@ $$list

#
# Cleanup our non-standard targets
#
.clean: .local-clean
.local-clean:
	@ rm -f config.jar
	@ find ./$P -name "*.class" -exec rm -f {} \; 
