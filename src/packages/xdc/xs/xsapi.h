/* --COPYRIGHT--,EPL
 *  Copyright (c) 2013 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== xsapi.h ========
 *  This module provides APIs to run XDCScript commands.
 *
 *  This module provides API to create/delete session manager, start/stop
 *  sessions, processes XDCScript commands and return the result.
 */
#ifndef XSAPI_H
#define XSAPI_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  ======== XS_Attrs ========
 *  Attributes needed to start a XS session manager
 */
typedef struct XS_Attrs {
    char *xdcroot;      /* XDCtools installation directory */
    char *xdcpath;      /* Path containing RTSC packages */
    char *libpath;      /* DLL and other libs path */
} XS_Attrs;

/*
 *  ======== XS_Handle ========
 *  Handle to XS session manager
 */
typedef struct XS_Obj *XS_Handle;

/*
 *  ======== XS_Session ========
 *  Handle to XS session
 */
typedef struct XS_SesObj *XS_Session;

/*
 *  ======== XS_create ========
 *  Creates a XS session manager
 *
 *  This API sets up JVM, rhino interpretor and XDCScript session environment
 *
 *  Parameters:
 *      attrs     ponter to attributes for XS session manager
 *      errorMsg  pointer to error string. If returned handle is null then
 *                this pointer contains a meaningful error string
 *
 *   Returns:
 *      a handle to the XS session manager.
 */
XS_Handle XS_create(XS_Attrs *attrs, char **errorMsg);

/*
 *  ======== XS_delete ========
 *  Stop (and destory) a previously created XS session manager
 *
 *  Parameters:
 *      hdl     non-NULL handle to the XS session manager
 *
 *  Note:
 *      Server once stopped cannot be again started in the same process. This
 *      is due to a bug in SUN JVM which fails when createVM() is called again.
 *      See http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4712793 for more
 *      info.
 */
void XS_delete(XS_Handle hdl);

/*
 *  ======== XS_getOutput ========
 *  Get the result of XDCScript command processed
 *
 *  This API should be called after the call to XS_process to get the result of
 *  the XDCScript command processed.
 *
 *  `buf` will always be '\0' terminated and no more than `len` bytes will be
 *  transfered to `buf` (including the '\0').
 *
 *  If the return value is equal to `len` bytes, this API should be called
 *  repeatedly till the return value is less than `len` bytes to retrieve
 *  the complete data.
 *
 *  Parameters:
 *      ses     non-NULL handle to a XS session
 *      buf     non-NULL pointer to a buffer of at least `len` bytes
 *      len     length (in bytes) of `buf` (must be >= 2)
 *
 *  Returns:
 *      The number of bytes transfered to `buf`.
 *      < 0 if input is incorrect
 */
int XS_getOutput(XS_Session ses, char *buf, int len);

/*
 *  ======== XS_process ========
 *  Processes the specified XDCScript command
 *
 *  Call to this API should be followed by call to XS_getOutput to
 *  process and get the result.
 *
 *  Parameters:
 *      ses      non-null handle to the XS session
 *      cmd      non-NULL pointer to string containing XDCScript command.
 *
 *  Returns:
 *      0 if successful.
 *      < 0 if input is incorrect
 *
 */
int XS_process(XS_Session ses, char *cmd);

/*
 *  ======== XS_start ========
 *  Start a new XS session for the specified session attributes
 *
 *  Parameters:
 *      hdl       non-NULL handle to the XS session manager
 *      erroMsg   pointer to the error string. If returned handle is null
 *                then this pointer contains a meaningful error string.
 *
 *  Returns:
 *      a handle to the XS session
 */
XS_Session XS_start(XS_Handle hdl, char **errorMsg);

/*
 *  ======== XS_stop ========
 *  Stop (and destory) a previously created XS session
 *
 *  Parameters:
 *      ses     non-NULL handle to the XS session
 *
 */
void XS_stop(XS_Session ses);

#ifdef __cplusplus
}
#endif
#endif
