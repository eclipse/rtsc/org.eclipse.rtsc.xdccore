#!/bin/sh
#
#  xs - XDCscript shell
#
#  usage: xs file
#
ROOT="`dirname $0`"

##  set EXE to select the appropriate xs executable.
EXE=xv7A

## define XDCTOOLS_JAVA_HOME, if it isn't already defined
if [ "$XDCTOOLS_JAVA_HOME" = "" ]; then
    export XDCTOOLS_JAVA_HOME="/usr/lib/jvm/java-6-openjdk"
fi

#
#  run xs executable program and exit with xs' exit status
#
#  Note we use "$@" in order to preserve any white space in individual
#  arguments passed to this script; "$@" is equivalent to "$1" "$2" ...
#
exec ${ROOT}/xs.$EXE "$@"
