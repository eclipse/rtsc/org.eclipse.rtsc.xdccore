/* --COPYRIGHT--,EPL
 *  Copyright (c) 2013-2017 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== xsapi.c ========
 *
 *! Revision History
 *! ================
 *! 22-Jan-2013 vikram created initial version
 */

#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xsapi.h"
#include "tcf.h"

/* Windows specific conventions */
#if defined(xdc_target__os_Windows)
#define DIRSTR      "\\"             /* directory separator character */
#define CALLBACK    __stdcall        /* Win32 callbacks use pascal conv */
#else
#define DIRSTR      "/"
#define CALLBACK
#endif

/* Lengths for arrays */
#define MAXJVMOPTS  8                /* including NULL */
#define MAXPATHTAB  4                /* including NULL */
#define MAXSESARGS  4                /* maximum session arguments */

/* Relative path to xdc shelf */
#define XDCSHELF    "packages" DIRSTR "xdc" DIRSTR "shelf" DIRSTR "java"

/* XS session manager's internal data structure */
typedef struct XS_Obj {
    TCF_Attrs    attrs;            /* tcf attributes */
    TCF_Handle   tcf;              /* handle to tcf session manager */
    int          sesCount;         /* number of opened sessions */
    String       *pathTab;         /* Contains search path for various files */
    String       *jvmOptions;      /* JVM options */
} XS_Obj;

/* XS session internal data structure*/
typedef struct XS_SesObj {
    XS_Handle    hdl;              /* handle to XS session manager */
    TCF_Session  tses;             /* handle to tcf session */
    String       *argv;            /* arguments for the tcf session */
    Int          argc;             /* number of args to tcf session */
} XS_SesObj;

/* Referenced multiple times*/
static String oom = "Out of memory";

/* Internal helper functions */
static Void freeManager(XS_Handle hdl);
static Void freeSession(XS_Session ses);
static Int initAttrs(XS_Handle hdl, String xdcroot, String xdcpath,
                     String libpath, String *errorMsg);
static Int initSesAttrs(XS_Session ses, String *errorMsg);

/* TODO Not sure if I need these? */
static void CALLBACK jvmAbort(void);
static void CALLBACK jvmExit(int status);

/*
 *  ======== freeManager ========
 *  Free up the resources allocated to session manager
 */
static Void freeManager(XS_Handle hdl)
{
    Int i = 0;
    String *tmp = NULL;

    /* free pathTab */
    tmp = hdl->pathTab;
    if (tmp != NULL) {
        while (tmp[i] != NULL) {
            free(tmp[i]);
            i++;
        }
        free(tmp);
    }

    /* free jvmOptions */
    i = 0;
    tmp = hdl->jvmOptions;
    if (tmp != NULL) {
        while (tmp[i] != NULL) {
            free(tmp[i]);
            i++;
        }
        free(tmp);
    }

    /* free hdl */
    free(hdl);
}

/*
 *  ======== freeSession ========
 *  free up the resources allocated to session
 */
static Void freeSession(XS_Session ses)
{
    Int i = 0;
    String *tmp = NULL;

    /* free argv */
    i = 0;
    tmp = ses->argv;
    if (tmp != NULL) {
        while (i < ses->argc) {
            free(tmp[i]);
            i++;
        }
        free(tmp);
    }

    /* free ses */
    free(ses);
}


/*
 *  ======== initAttrs ========
 *  Initilizes the attributes for TCF session manager
 */
static Int initAttrs(XS_Handle hdl, String xdcroot, String xdcpath,
    String libpath, String *errorMsg)
{
    /* JVM options */
    Char xverify[] = "-Xverify:none";           /* Helps speed up JVM start */
    Char dxdcpath[] = "-DXDCPATH=";             /* XDCPATH */
    Char dxdcroot[] = "-Dxdc.root=";            /* XDCROOT */
    Char dlibpath[] = "-Djava.library.path=";   /* libpath */
    Int i = 0;

    /* assign defaults */
    hdl->attrs = TCF_ATTRS;

    if ((xdcroot == NULL) || (xdcpath == NULL) || libpath == NULL) {
        *errorMsg = "XDCROOT or XDCPATH or LIBPATH not provided";
        return (-1);
    }

    /* populate pathTab */
    if ((hdl->pathTab = (String *)malloc(sizeof(String *) * MAXPATHTAB))
        != NULL) {
        i = 0;
        /* 1: path to xdcroot */
        if ((hdl->pathTab[i] = (String)malloc(strlen(xdcroot) + 1)) != NULL) {
            strcpy(hdl->pathTab[i++], xdcroot);
        }
        else {
            *errorMsg = oom;
            return (-1);
        }

        /* 2: path to lib; this path component exists only when xs runs inside
         * the development tree.
         */
        if ((hdl->pathTab[i] = (String)malloc(strlen(xdcroot)
            + strlen(DIRSTR ".." DIRSTR "lib") + 1)) != NULL) {
            strcpy(hdl->pathTab[i], xdcroot);
            strcat(hdl->pathTab[i++], DIRSTR ".." DIRSTR "lib");
        }
        else {
            *errorMsg = oom;
            return (-1);
        }

        /* 3: path to shelf */
        if ((hdl->pathTab[i] = (String)malloc(strlen(xdcroot)
            + strlen(DIRSTR XDCSHELF) + 1)) != NULL) {
            strcpy(hdl->pathTab[i], xdcroot);
            strcat(hdl->pathTab[i++], DIRSTR XDCSHELF);
        }
        else {
            *errorMsg = oom;
            return (-1);
        }

        /* 4: null terminate the pathTab */
        hdl->pathTab[i] = NULL;

        /* Set pathTab and rootDir*/
        hdl->attrs.pathTab = hdl->pathTab;
        hdl->attrs.rootDir = hdl->pathTab[0];

    }
    else {
        *errorMsg = oom;
        return (-1);
    }

    /* populate JVM options */
    if ((hdl->jvmOptions = (String *)malloc(sizeof(String *) * MAXJVMOPTS))
        != NULL) {
        i = 0;
        /* 1: xverify option  */
        /* Disable byte code verification to improve JVM startup time */
        if ((hdl->jvmOptions[i] = (String)malloc(strlen(xverify) + 1))
            != NULL) {
            strcpy(hdl->jvmOptions[i++], xverify);
        }
        else {
            *errorMsg = oom;
            return (-1);
        }

        /* 2: pass xdcpath as a -D option  */
        if ((hdl->jvmOptions[i] = (String)malloc(strlen(dxdcpath)
            + strlen(xdcpath) + 1)) != NULL) {
            strcpy(hdl->jvmOptions[i], dxdcpath);
            strcat(hdl->jvmOptions[i++], xdcpath);
        }
        else {
            *errorMsg = oom;
            return (-1);
        }

        /* 3: pass xdcroot as a -D option  */
        if ((hdl->jvmOptions[i] = (String)malloc(strlen(dxdcroot)
            + strlen(xdcroot) + 1)) != NULL) {
            strcpy(hdl->jvmOptions[i], dxdcroot);
            strcat(hdl->jvmOptions[i++], xdcroot);
        }
        else {
            *errorMsg = oom;
            return (-1);
        }

        /* 4: pass libpath as a -D option  */
        if ((hdl->jvmOptions[i] = (String)malloc(strlen(dlibpath)
            + strlen(libpath) + 1)) != NULL) {
            strcpy(hdl->jvmOptions[i], dlibpath);
            strcat(hdl->jvmOptions[i++], libpath);
        }
        else {
            *errorMsg = oom;
            return (-1);
        }

        /* 5: null terminate  */
        hdl->jvmOptions[i] = NULL;

        /* set jvm options */
        hdl->attrs.jvmOpts = hdl->jvmOptions;

    }
    else {
        *errorMsg = oom;
        return (-1);
    }

    /* Set jvmExit and jvmAbort */
    hdl->attrs.jvmExit = jvmExit;
    hdl->attrs.jvmAbort = jvmAbort;

    return (0);
}

/*
 *  ======== initSesAttrs ========
 *  Initilizes the attributes for TCF session
 */
static Int initSesAttrs(XS_Session ses, String *errorMsg)
{
    Int i = 0;
    /* Session attributes */
    /* This is needed as return value from script will be saved in buffer only
     * in verbose mode.
     */
    Char option[] = "-v";
    Char xsRelativePath[] = "/packages/xdc/xs.js";
    String xdcroot;

    /* Set session attrs */
    if ((ses->argv = (String *)malloc(sizeof(String *) * MAXSESARGS)) != NULL) {
        i = 0;
        /* 1: pass -v option to read script output  */
        if ((ses->argv[i] = (String)malloc(strlen(option) + 1)) != NULL) {
            strcpy(ses->argv[i++], option);
        }
        else {
            *errorMsg = oom;
            return (-1);
        }

        /* 2: pass dir path of xs.js */
        /* ses->hdl->pathTab[0] contains xdcroot */
        xdcroot = ses->hdl->pathTab[0];
        if ((ses->argv[i] = (String)malloc(strlen(xsRelativePath)
            + strlen(xdcroot) + 1)) != NULL) {
            strcpy(ses->argv[i], xdcroot);
            strcat(ses->argv[i++], xsRelativePath);
        }
        else {
            *errorMsg = oom;
            return (-1);
        }

        /* set the argc */
        ses->argc = i;

    }
    else {
        *errorMsg = oom;
        return (-1);
    }

    return (0);
}

/*
 *  ======== XS_getOutput ========
 *  Get the result of the XDCScript command
 *
 *  Should be called repeatedly till return value is less than `len` bytes
 */
int XS_getOutput(XS_Session ses, char *buf, int len)
{
    Int status = -1;

    if (ses == NULL || buf == NULL || len < 2) {
        return (status);
    }

    status = TCF_getOutput(ses->tses, TCF_STDOUT, buf, len);

    return (status);
}
/*
 *  ======== XS_process ========
 *  Process the XDCScript command.
 *
 *  Process it by calling TCF_process
 */
int XS_process(XS_Session ses, char *cmd)
{
    Int status = -1;

    if (ses == NULL || cmd == NULL) {
        return (status);
    }

    /* process command */
    status = TCF_process(ses->tses, cmd);

    return (status);
}

/*
 *  ======== XS_create ========
 *  Creates a XS session manager
 *
 *  Internally creates a TCF session manager.
 */
XS_Handle XS_create(XS_Attrs *attrs, char **errorMsg)
{
    XS_Handle hdl;

    /* Initialize the TCF module */
    TCF_init();

    if (attrs == NULL || attrs->xdcroot == NULL || attrs->xdcpath == NULL
        || attrs->libpath == NULL) {
        *errorMsg = "One or more session manager attributes are NULL";
        return (NULL);
    }

    if ((hdl = (XS_Obj *)malloc(sizeof(XS_Obj))) == NULL) {
        *errorMsg = oom;
        return (NULL);
    }

    /* Initialize the attributes for session manager */
    if (initAttrs(hdl, attrs->xdcroot, attrs->xdcpath, attrs->libpath, errorMsg)
         == -1) {
        freeManager(hdl);
        return (NULL);
    }


    /* create TCF session manager */
    if ((hdl->tcf = TCF_create(NULL, &(hdl->attrs), errorMsg)) == NULL) {
        freeManager(hdl);
        return (NULL);
    }

    return (hdl);
}

/*
 *  ======== XS_start ========
 *  Starts up a XS session
 *
 *  Creates a TCF session
 */
XS_Session XS_start(XS_Handle hdl, char **errorMsg)
{
    XS_Session ses;

    if (hdl == NULL) {
        *errorMsg = "Handle to XS session manager is NULL";
        return (NULL);
    }

    if ((ses = (XS_SesObj *)malloc(sizeof(XS_SesObj))) == NULL) {
        *errorMsg = oom;
        return (NULL);
    }

    /* Save XS session manager handle */
    ses->hdl = hdl;

    /* Init Session Attrs */
    if (initSesAttrs(ses, errorMsg) == -1) {
        freeSession(ses);
        return (NULL);
     }

    /* start a tcf session */
    if ((ses->tses = TCF_start(hdl->tcf, ses->argc, ses->argv, errorMsg))
        == NULL) {
        freeSession(ses);
        return (NULL);
    }

    /* Increment session count */
    hdl->sesCount++;

    return (ses);
}

/*
 *  ======== XS_delete ========
 *  Stop (and destory) the XS session manager.
 *
 *  Requires a non NULL XS handle
 */
void XS_delete(XS_Handle hdl)
{
    if (hdl != NULL) {
        /* if sessions are still open */
        if (hdl->sesCount) {
            printf("Warning: Session manager stopped before stopping sessions\n");
        }

        /* delete interpreter and exit the TCF module */
        TCF_delete(hdl->tcf);

        /* free up allocated resources */
        freeManager(hdl);

        TCF_exit();
    }
}

/*
 *  ======== XS_stop ========
 *  Stop (and destory) XS session
 *
 *  Requires a non NULL XS handle.
 */
void XS_stop(XS_Session ses)
{
    /* stop the session */
    if (ses != NULL) {
        /* Stop tcf session */
        TCF_stop(ses->tses);

        /* decrement session count */
        ses->hdl->sesCount--;

        /* free up allocated resources */
        freeSession(ses);
    }
}

/*
 *  ======== jvmAbort ========
 *  Called by JVM on abort
 */
static void CALLBACK jvmAbort(void)
{
    printf("%s %d: jvm failed and aborted!\n", __FILE__, __LINE__);
}

/*
 *  ======== jvmExit ========
 *  Called by JVM on exit.
 */
static void CALLBACK jvmExit(int status)
{
    printf("%s %d: jvm failed and exited\n", __FILE__, __LINE__);
    TCF_exit();
}
