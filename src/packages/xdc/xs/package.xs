/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2018 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
function getLibs()
{
    var suffix = ".a" + Program.build.target.suffix;

    if (Program.build.target.os == "Windows") {
        var prefix = "lib/" + Program.build.profile;
        return (prefix + "/xs.lib;" + prefix + "/tcf.lib");
    }
    return ("lib/tcf" + suffix);
}
