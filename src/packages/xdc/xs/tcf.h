/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== tcf.h ========
 *
 *  Textual Configuration File Module
 *
 *  This module uses the Rhino JavaScript interpreter and, thus, it
 *  must startup a JVM, pass the JavaScript interpreter Shell class
 *  to the JVM, and get the exit status from the JVM.
 *
 *  Thus, the bulk of this module is involved in finding a JVM to startup,
 *  the Rhino JavaScript interpreter (js.jar), the configuration object
 *  model (config.jar), and the JNI dynamic library for running loading
 *  CDB files (ncdb).
 *
 *  The following is a list of all components loaded:
 *	JVMNAMES[?]		- Java virtual machine DLLs
 *	js.jar			- Rhino JavaScript JAR
 *	config.jar		- configuration object model JAR
 *	tconfini.tcf		- optional startup script
 *
 *  TCF_create uses the search path specified in the TCF_Attrs parameter
 *  when trying to locate files.  The default path is just ".".
 *
 *  Based on where TCF_create() finds config.jar and js.jar, it defines 
 *  the Java CLASSPATH and the Java library path as follows:
 *
 *	java.class.path = 
 *	    full path of js.jar; full path of config.jar; attrs->classPath
 *
 *  The following is a list of were the components must be located
 *	JVMNAMES[?]		- TCF_Attrs.pathTab search path
 *	tconfini.tcf		- TCF_Attrs.pathTab search path
 *	js.jar			- java.class.path
 *	config.jar		- java.class.path
 *
 */
#ifndef TCF_
#define TCF_

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  ======== TCF error codes ========
 */
#define TCF_EOK		0	/* no error */
#define TCF_EFAIL	1	/* unspecified failure */
#define TCF_EUSAGE	2	/* usage error */
#define TCF_EFILE	3	/* file not found */
#define TCF_EIO		4	/* I/O error */
#define TCF_EINCOMPLETE	5	/* incomplete statement */
#define TCF_EFATAL	6	/* fatal error, session is unusable */
#define TCF_EINTERN	7	/* internal runtime error */

/*
 *  ======== TCF_Handle ========
 *  Handle to TCF Manager Object.  A TCF Manager manages one or more
 *  script sessions.
 */
typedef struct TCF_Obj *TCF_Handle;

/*
 *  ======== TCF_Session ========
 *  Handle to TCF Session Object.  A session is a private context for
 *  the execution of configuration commands; i.e., commands in one
 *  session do not affect any other session.
 */
typedef struct TCF_SesObj *TCF_Session;

/*
 *  ======== TCF_Stream ========
 *  Each session has two character output streams that can be read by
 *  the client:
 *	TCF_STDERR  - error messages
 *	TCF_STDOUT  - output from the built-in "print" command
 */
typedef enum {TCF_STDOUT = 1, TCF_STDERR = 2} TCF_Stream;

#if defined(xdc_target__os_Windows)
/* Win32 call backs use Pascal calling conventions!!!! */
typedef Void (__stdcall *TCF_ExitFxn)(Int);
typedef Void (__stdcall *TCF_AbortFxn)(Void);
#else
typedef Void (*TCF_ExitFxn)(Int);
typedef Void (*TCF_AbortFxn)(Void);
#endif

/*
 *  ======== TCF_Attrs ========
 */
typedef struct TCF_Attrs {
    String *pathTab;	    /* NULL terminated array of directories */
    String *jarTab;	    /* NULL terminated array of JAR file names */
    String *jvmOpts;	    /* NULL terminated array of JVM options */
    String rootDir;	    /* root configuration directory */
    String classPath;	    /* classpath to append to computed jar path */
    TCF_ExitFxn jvmExit;    /* JVM exit hook function */
    TCF_AbortFxn jvmAbort;  /* JVM abort hook function */
    Bool gflag;		    /* debug flag */
    Bool nflag;		    /* no-execute flag */
    Bool sflag;		    /* suppress startup file flag */
    String gopts;           /* debugger options passed via -g=xxx */
} TCF_Attrs;

/*
 *  ======== TCF_ATTRS ========
 *  Default attributes for a TCF object
 */
extern TCF_Attrs TCF_ATTRS;

/*
 *  ======== TCF_create ========
 *  Create a textual configuration session manager.
 *
 *  Parameters:
 *	name	name of the session (or script) passed to interpretor in
 *		the runtime environment variable "config.scriptName".  May
 *		be NULL; in this case, config.scriptName is set to "".
 *	attrs	optional attibutes; if equal to NULL, the defaults defined
 *		by TCF_ATTRS are used.
 *
 *	attrs.pathTab	if non-NULL, each String in this array is a directory
 *			searched for the various components required by TCF.
 *			This array must be NULL terminated.  Defaults to the
 *			following array:
 *			    { ".",		// current working directory
 *			      NULL		// requisite NULL termination
 *			    }
 *
 *	attrs.jarTab	if non-NULL, each String in the array is first
 *			located along the path specified via attrs.pathTab
 *			then appended to the computed classpath of the JVM.
 *			This array must be NULL terminated. Defaults to the
 *			following array:
 *			    { "js.jar",		// Rhino jar
 *			      "config.jar",	// Config jar
 *			      NULL		// requisite NULL termination
 *			    }
 *
 *	attrs.jvmOpts	if non-NULL this array of of Strings is passed as
 *			additional arguments to the JVM as part of its
 *			creation.  This array must be NULL terminated.
 *			Defaults to NULL.
 *
 *	attrs.classPath if non-NULL this path is appended to the computed
 *			jar class path; defaults to NULL.
 *
 *	attrs.jvmExit	if non-NULL this function will be called whenever
 *			the JVM exits.  Defaults to NULL.
 *
 *	attrs.jvmAbort	if non-NULL this function will be called whenever
 *			the JVM aborts.  Defaults to NULL.
 *
 *	attrs.gflag	if TRUE, TCF_run() executes the debugger shell
 *			instead of the standard shell; defaults to FALSE.
 *
 *	attrs.nflag	if TRUE, do not execute any methods and output the
 *			equivalent java command (via GT_trace()); defaults
 *			to FALSE.
 *
 *	attrs.sflag	if TRUE, suppress the loading of the startup file
 *			tconfini.tcf; defaults to FALSE.
 *	    
 *	attrs.gopts	the optional debug options string passed via -g=xxx.
 *			Both NULL and "" imply no options.
 *	    
 *	error	pointer to error string (output); may be NULL, if non-NULL
 *		then when an error occurs a meaningful error string is
 *		returned; otherwise it is set to NULL.
 *
 *  Returns:
 *	a non-NULL handle to a TCF object, if successful; otherwise NULL
 *
 *	If NULL is returned and error is non-NULL, error is set to a pointer
 *	to a meaninful error message.
 *
 *  Constraints:
 *	All Strings passed to the TCF_create must be persistent; i.e., they
 *	must not be freed until the TCF object itself is deleted.
 *
 */
extern TCF_Handle /*@null@*/ TCF_create(
    String	/*@null@*/	name,    \
    TCF_Attrs	/*@null@*/	*attrs,  \
    String	/*@null,out@*/	*error   \
);

/*
 *  ======== TCF_delete ========
 *  Delete the specified TCF object.
 *
 *  Parameters:
 *	tcf	non-NULL handle to a successfully created TCF object
 *
 *  Returns:
 *	Void
 *
 *  Preconditions:
 *	all session objects started using this TCF manager have been
 *	stopped (via TCF_stop)
 *
 *  Post-Conditions:
 *	all memory associated with this manager is released.
 *
 *  WARNING:  Due to a bug in SUN's JVM 1.3.1 it is not possible to
 *  call TCF_create() after a call to TCF_delete().  Apparently, all
 *  existing applications of the SUN JVM never try to delete the JVM
 *  until application exit.  This bug was filed and confirmed with
 *  SUN.
 */
extern Void TCF_delete(TCF_Handle tcf);

/*
 *  ======== TCF_exit ========
 *  Exit the TCF module.
 *
 *  This method is called upon application termination after all TCF
 *  objects have been deleted.
 */
extern Void TCF_exit(Void);

/*
 *  ======== TCF_getOutput ========
 *  Get text message corresponding to the last error which occured in
 *  the session object specified by ses.
 *
 *  buf will always be '\0' terminated and no more than len bytes will
 *  be transfered to buf (including the '\0' terminator).
 *
 *  TCF_getOutput() returns the number of bytes transfered *not* including
 *  the '\0' terminator; i.e., the length of the output string.
 *
 *  If the output stream has more the len-1 characters, the unread
 *  characters can be read using another call to TCF_getOutput().  Output
 *  continues to be buffered until it is read.  Thus, it is important to
 *  repeatedly call TCF_getOutput() until it returns a value strictly less
 *  than len; otherwise the output buffer may continue to grow until all
 *  of system memory is consumed.
 *
 *  Parameters:
 *	session	non-NULL handle to a successfully created TCF session
 *	buf	non-NULL pointer to a buffer of at least len bytes
 *	len	length (in bytes) of buf (must be >= 2)
 *
 *  Returns:
 *	number of bytes transfered into buf
 *
 *  Preconditions:
 *	session is non-NULL active handle created via TCF_start() (not
 *      previously stopped via TCF_stop())
 *
 *	buf must be at least len bytes long and len must be >= 2.
 *
 *  Post-Conditions:
 *	the return value is always >= 0.
 *
 *	the number of characters buffered in the specified stream, s,
 *	is reduced by the return value; characters are removed in FIFO
 *	order.
 */
extern Int TCF_getOutput(TCF_Session ses, TCF_Stream s, Char *buf, Int len);

/*
 *  ======== TCF_init ========
 *  Initialize the TCF module.
 *
 *  This method must be called called prior to *any* use of the TCF methods
 *  or variables.
 *
 *  Preconditions:
 *	none
 *
 *  Post-Conditions:
 *	methods and variables in the TCF interface may now be used
 */
extern Void TCF_init(Void);

/*
 *  ======== TCF_process ========
 *  Execute the specified command in a shell previously started via
 *  TCF_start().
 *
 *  Parameters:
 *	session	non-NULL handle to a successfully created TCF session
 *	command	non-NULL command string to execute in the current session
 *
 *  Returns:
 *	TCF_EOK		if line successfully executed
 *	TCF_EFATAL	a fatal error occured; session must be stopped
 *      TCF_E*		other non-fatal error occured; session can continue
 *
 *      Note that if TCF_process() returns TCF_EINCOMPLETE the statement(s)
 *	contained in the string line are do not for a complete compilation
 *	unit; i.e., additional statements are required to complete the
 *	execution of the line.  For example, "if (true) { print('hello')"
 *	requires a closing "}" before it is possible to execute this
 *	statement.
 *
 *  Pre-Conditions:
 *	tcf is non-NULL active handle (not deleted via TCF_delete())
 *
 *	session is non-NULL active handle created via TCF_start() (not
 *      previously stopped via TCF_stop())
 *
 *	string must be a "compilable unit"; e.g., JavaScript "for" loops 
 *	can not span command strings.
 *
 *  Post-Conditions:
 *	any side affects of the execution of command.
 */
extern Int TCF_process(TCF_Session session, String command);

/*
 *  ======== TCF_run ========
 *  Run a script named in the argv[] String array and return the exit
 *  status of the script.  This command implicitly starts, runs, and
 *  stops a shell.
 *
 *  Parameters:
 *	tcf	non-NULL handle to a successfully created TCF object
 *	argc	number of String argument in the argv[] array; may be 0.
 *	argv[]	array of String arguments passed to the Rhino shell.
 *
 *  Returns:
 *	0	if successful
 *	non-0	otherwise
 *
 *  Pre-Conditions:
 *	tcf is non-NULL active handle (not deleted via TCF_delete())
 */
extern Int TCF_run(TCF_Handle tcf, Int argc, String /*@null@*/ argv[]);

/*
 *  ======== TCF_start ========
 *  Start a new configuration session with the specified arguments.
 *  Once started TCF_process() can be successively called to execute
 *  individual configuration commands.
 *
 *  Parameters:
 *	tcf	non-NULL handle to a successfully created TCF object
 *	argc	number of string in the argv[] array (may be 0)
 *	argv	arrays of options to shell (may be NULL)
 *	error	optional output error string
 *
 *  Shell Options:
 *	-v	verbose mode: write value of expressions to stdout
 *	-w	turn on warning reporting
 *	-f file execute contents of file 
 *
 *  Returns:
 *	non-NULL handle to shell session, if successful
 *	NULL	 otherwise
 *
 *	If the error parameter is a non-NULL String pointer, this
 *	output parameter is set as follows:
 *
 *	non-NULL error string, if the handle returned is NULL
 *	NULL	 otherwise
 *
 *	Note that this error string is a static string and must *not*
 *	be freed by the caller.
 *
 *  Pre-Conditions:
 *	tcf is non-NULL active handle (not deleted via TCF_delete())
 */
extern TCF_Session TCF_start(TCF_Handle		    tcf,    \
			     Int		    argc,   \
			     String /*@null@*/	    argv[], \
			     String /*@null,out@*/  *error);

/*
 *  ======== TCF_stop ========
 *  Stop (and destroy) a previously started configuration session
 *  (see TCF_start()).
 *
 *  Parameters:
 *	tcf	non-NULL handle to a successfully created TCF object
 *
 *  Pre-Conditions:
 *	tcf is non-NULL active handle (not deleted via TCF_delete())
 *
 *	session is non-NULL active handle created via TCF_start() (not
 *      previously stopped via TCF_stop())
 */
extern Void TCF_stop(TCF_Session session);

#ifdef __cplusplus
}
#endif

#endif	/* TCF_ */
