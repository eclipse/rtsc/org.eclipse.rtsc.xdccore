/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  config.ShellErrorReporter
 */

package config;

import java.io.*;
import org.mozilla.javascript.*;
import org.mozilla.javascript.tools.ToolErrorReporter;

/*
 *  ======== ShellErrorReporter ========
 *  All Shell errors are passed through this object.
 *  
 *  Note: We extend ToolErrorReporter rather than implement ErrorReporter
 *  and delagate to a ToolErrorReporter because we want clients to be
 *  able to use the "hasReportedError" method when the debugger's shell
 *  is used and when config's Shell is used.
 */
class ShellErrorReporter extends ToolErrorReporter
{
    /*
     *  ======== ShellErrorReporter ========
     */
    public ShellErrorReporter(boolean reportWarnings, ShellOutputStream sos)
    {
	this(reportWarnings, new PrintStream(sos, true));
    }
    
    /*
     *  ======== ShellErrorReporter ========
     *  This constructor is really only for the constructor above;
     *  ShellErrorReporters should be built using ShellOutputStreams (to
     *  allow capturing error output).
     */
    public ShellErrorReporter(boolean reportWarnings, PrintStream ps)
    {
	super(reportWarnings, ps);
	this.ps = ps;
	this.reportWarnings = reportWarnings;
    }
    
    /*
     *  ======== setIsReportingWarnings ========
     */
    public void setIsReportingWarnings(boolean reportWarnings) {
	super.setIsReportingWarnings(reportWarnings);
        this.reportWarnings = reportWarnings;
    }
    
    /*
     *  ======== clearErrorFlag ========
     */
    public void clearErrorFlag()
    {
	errorFlag = false;
    }

    /*
     *  ======== error ========
     */
    public void error(String message, String sourceName, int line,
               String lineSource, int lineOffset)
    {

        Context cx = Context.getCurrentContext();
        ShellDebugContext dc = ShellDebugContext.get(cx);

        /* If we are filtering out the files from xdc.root, we look for a
         * file in the stack frames that is not in xdc.root. That file is
         * promoted as a source file for the error message.
         */
        boolean showAll = false;
        String tg = System.getProperty("xdc.traceGroups");
        if (tg != null && tg.indexOf("xdcerror") != -1) {
            showAll = true;
        }
        
        String root = System.getProperty("xdc.root");
        if (root != null) {
            try {
                root = (new File(root)).getCanonicalPath();
            }
            catch (IOException io) {
                ;
            }
            root = root.replace('\\', '/');
        }

        String src = Shell.getRelativePath(sourceName, ".");
        int ln = line;
        int count = 0;
        while (!showAll && root != null && src.indexOf(root) == 0) {
    	    if (count < dc.getFrameCount()) {
    	        ShellDebugFrame frame = dc.getFrame(count);
    	        src = frame.getSourceName();
    	        ln = frame.getLineNumber();
    	        count++;
    	    }
    	    else {
    	    	/* If we made it here, it means there are no frames with
    	    	 * files outside of xdc.root. Then, we go back to the
    	    	 * original source of the error message because we have to
    	    	 * print something.
    	    	 */
    	    	src = sourceName;
    	    	ln = line;
    	        break;
    	    }
    	}
    	
	super.error(message, src, ln, lineSource, lineOffset);
	if (ln == 0) {
	    /* print everything */
	    ps.print(Shell.getStackTrace("    "));
	}
	else {
	    /* If Rhino already printed out the error message for a specific
	     * file and a line number, or we promoted that file and line
	     * number in the code above, we shouldn't repeat it.
	     */
	    ps.print(Shell.getStackTrace("    ", "", src, ln));
	}

	errorFlag = true;

    }

    /*
     *  ======== getErrorFlag ========
     */
    public boolean getErrorFlag()
    {
	return (errorFlag);
    }

    /*
     *  ======== runtimeError ========
     */
    public EvaluatorException runtimeError(String message, String sourceName, 
                                    int line, String lineSource, 
                                    int lineOffset)
    {
	//System.out.println("ShellErrorReporter.runtimeError: " + message);
        error(message, sourceName, line, lineSource, lineOffset);
	return (new EvaluatorException(message, sourceName, line, lineSource,
	                               lineOffset));
    }

    /*
     *  ======== warning ========
     */
    public void warning(String message, String sourceName, int line,
                 String lineSource, int lineOffset)
    {
//	System.out.println("ShellErrorReporter.warning: " + message);
	super.warning(message, sourceName, line, lineSource, lineOffset);
	if (reportWarnings) {
	    ps.print(Shell.getStackTrace("    ", "  script stack trace:\n"));
	}
    }

    private boolean reportWarnings = false;
    private boolean errorFlag = false;
    private PrintStream ps;
}

