/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== ShellGUIDebugger.java ========
 *  This class allows tconf to run scripts under the control of the
 *  Rhino debugger.
 */
package config;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;
import javax.swing.*;

import org.mozilla.javascript.*;
import org.mozilla.javascript.tools.debugger.Main;

/*
 *  ======== ShellGUIDebugger ========
 */
public class ShellGUIDebugger {

    private Properties props;
    private Shell shell = null;
    private Main sdb = null;
    private InputStream savedSystemIn = null;
    private PrintStream savedSystemOut = null;
    private PrintStream savedSystemErr = null;
    private Runnable exitAction = null;

    /*
     *  ======== swingInvoke ========
     */
    static void swingInvoke(Runnable f) {
        if (SwingUtilities.isEventDispatchThread()) {
            f.run();
            return;
        }
        try {
            SwingUtilities.invokeAndWait(f);
        }
	catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    /**
     * Execute the script in the debugger from the command line.
     * Terminates the JVM when the script completes or the user
     * closes the debugger window.
     */
    public static void main(String[] args)
    {
        ShellGUIDebugger shell = new ShellGUIDebugger(System.getProperties());

        /* exit the JVM when the user closes the window */
        shell.setExitAction(new Runnable() {
            public void run()
            {
                System.exit(0);
            }
        });

        shell.exec(args);
    }

    /**
     * Create a debugger.
     */
    public ShellGUIDebugger() {
        this(System.getProperties());
    }

    /**
     * Create a debugger that uses a private set of Java properties.
     */
    public ShellGUIDebugger(Properties props) {
        this.props = props;
    }

    /**
     * Assign a Runnable object that will be invoked when the user
     * selects "Exit..." or closes the Debugger main window.
     */
    public void setExitAction(Runnable exitAction)
    {
        this.exitAction = exitAction;
    }

    /**
     * Clean up after the session completes. If the script is still
     * running, allow it to continue to completion.
     */
    public void dispose() {
        if (sdb != null) {
            sdb.setBreakOnEnter(false);
            sdb.setBreakOnExceptions(false);
            sdb.setBreakOnReturn(false);
            sdb.setExitAction(null);
            sdb.clearAllBreakpoints();
            sdb.setScopeProvider(null);
            sdb.dispose();
            sdb = null;
        }
        if (savedSystemIn != null) {
            System.setIn(savedSystemIn);
            savedSystemIn = null;
        }
        if (savedSystemOut != null) {
            System.setOut(savedSystemOut);
            savedSystemOut = null;
        }
        if (savedSystemErr != null) {
            System.setErr(savedSystemErr);
            savedSystemErr = null;
        }
    }

    /**
     *  Execute the script with given arguments.
     *  Don't exit the JVM at the end.
     */
    public int exec(String args[]) {
	/*
	 *  If giflag is true then force break on first line of
	 *  initial script and set debug window visible, else delay
	 *  until user script is active.
	 */

	String gopts = props.getProperty("config.gopts");
	final boolean forceInitBreak =
	    (gopts == null || gopts.indexOf('i') == -1) ? false : true;

        try {
	    /* create the script debugger */
            sdb = new Main("tconf Rhino JavaScript Debugger");

	    /* wire it into swig and the JVM's io system */
	    swingInvoke(new Runnable() {
                    public void run() {
                        sdb.pack();
                        sdb.setSize(600, 460);
                        sdb.setVisible(forceInitBreak);
                    }
                });
            /*
             * by default, let the script continue when the user
             * closes the debugger.
             */
            if (exitAction == null) {
                exitAction = new Runnable() {
                    public void run()
                    {
                        dispose();
                    }
                };
            }
            sdb.setExitAction(exitAction);

            /*
             * save the System streams, and redirect them to the
             * debugger console.
             */
            savedSystemIn = System.in;
            System.setIn(sdb.getIn());
            savedSystemOut = System.out;
            System.setOut(sdb.getOut());
            savedSystemErr = System.err;
            System.setErr(sdb.getErr());

	    /* tell Rhino that the debugger wants context notifications */
	    //Context.addContextListener(sdb);
	    ContextFactory global = ContextFactory.getGlobal();
	    sdb.attachTo(global); 

	    /* create a shell that does not override the debugger */
	    if (forceInitBreak) {
                shell = new Shell(true, null, props);
                sdb.doBreak();	/* force break on very first line */
            }
	    else {
                shell = new Shell(true, sdb, props);
	    }
	    
	    /* set the global scope object that scripts run within */
	    sdb.setScopeProvider(shell);

	    /* set to break on exceptions by default; this is done by
	     * modifying the setBreakonxxx functions in  
	     * org.mozilla.javascript.tools.debugger.Main.
	     */
	    sdb.setBreakOnExceptions(true);
	    
	    /* let'r rip ... */
            return shell.exec(args);
	}
	catch (Exception exc) {
            exc.printStackTrace();
            return (Shell.EXITCODE_FAIL);
        }
        finally {
            dispose();
        }
    }
}
