/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  config.ShellExitException
 *
 *  This class defines an exception that is used to exit from a Shell script.
 */
package config;

public class ShellExitException extends RuntimeException {
    private int exitCode;

    ShellExitException() {
	super();
	this.exitCode = 0;
    }

    ShellExitException(String s) {
	super(s);
	this.exitCode = 0;
    }

    ShellExitException(int exitCode) {
	super("Exiting with code " + exitCode);
	this.exitCode = exitCode;
    }

    public int getExitCode() {
	return exitCode;
    }
}

