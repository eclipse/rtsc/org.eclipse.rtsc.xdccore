/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== ShellDebugFrame.java ========
 *
 *! Revision History
 *! ================
 *! 23-May-2012	sasha	created by moving it out of Shell.java
 */

package config;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.debug.DebugFrame;
import org.mozilla.javascript.debug.DebuggableScript;

/*
 *  ======== ShellDebugFrame ========
 */
class ShellDebugFrame implements DebugFrame {

    ShellDebugFrame(Context cx, DebuggableScript fnOrScript)
    {
        this.lineNumber = 1;
        this.sourceFile = fnOrScript.getSourceName();
        this.contextData = ShellDebugContext.get(cx);

        /* if we are entering another frame with exception count > 0, some
         * one must have handled the exception.  So we pop all frames that
         * were not popped in onExit()
         */
        if (contextData.exceptionCount > 0) {
//          System.out.println("new frame push(" + sourceFile
//              + ", line " + lineNumber);
            contextData.popFrame(contextData.exceptionCount);
            contextData.exceptionCount = 0;
        }
        contextData.pushFrame(this);
    }

    /*
     *  ======== DebugFrame.onDebuggerStatement ========
     */
    public void onDebuggerStatement(Context cx)
    {
    }

    /*
     *  ======== DebugFrame.onEnter ========
     */
    public void onEnter(Context cx, Scriptable activation,
                        Scriptable thisObj, Object[] args)
    {
    }

    /*
     *  ======== DebugFrame.onExceptionThrown ========
     */
    public void onExceptionThrown(Context cx, Throwable ex)
    {
    }

    /*
     *  ======== DebugFrame.onExit ========
     */
    public void onExit(Context cx, boolean byThrow, Object resultOrException)
    {
        if (byThrow) {
            /* if we exit via throw, don't pop the frame until later (for
             * complete stack trace back
             */
            //System.err.println("Shell$ onExit thrown for " + sourceFile);
            contextData.exceptionCount++;
//          System.out.println("popping frame because of a throw (" +
//              sourceFile + ", line " + lineNumber);
        }
        else if (contextData.exceptionCount > 0) {
            //System.err.println("Shell$ onExit with exc. for " + sourceFile);
            /* if this is a normal exit *and* there are one or more exception
             * frames below us, then we can dump these because someone must
             * have handled the exception. Must dump the current frame plus
             * the frames skipped during exception handling.
             */
//          System.err.println("popping " + contextData.exceptionCount
//              + " exception frames @ " + sourceFile + ", line "
//              + lineNumber);
            contextData.popFrame(contextData.exceptionCount + 1);
            contextData.exceptionCount = 0;
        }
        else {
            /* if this is a normal exit and there are no exceptions frames
             * below simply to the fastest possible from pop
             */
            contextData.popFrame();
        }
    }

    /*
     *  ======== DebugFrame.onLineChange ========
     */
    public void onLineChange(Context cx, int lineNumber)
    {
        //System.err.println("Shell$ line change to " + lineNumber + " for " +
        //      sourceFile);
        this.lineNumber = lineNumber;
    }

    /*
     *  ======== getLineNumber ========
     */
    public int getLineNumber()
    {
        return (lineNumber);
    }

    /*
     *  ======== getSourceName ========
     */
    public String getSourceName()
    {
        return (sourceFile);
    }

    private int lineNumber = 1;
    private String sourceFile = null;
    private ShellDebugContext contextData;
}
