/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  config.ShellOutputStream
 */
package config;

import java.io.*;

/*
 *  ======== ShellOutputStream ========
 *  All Shell output is passed through instances of this Class.  It's
 *  purpose is to tee output data to both standard output streams
 *  (System.out or System.err) and to an internal buffer that is read
 *  by the client (via a Shell's getOutput method)
 */
class ShellOutputStream extends OutputStream
{
    public ShellOutputStream(PrintStream out)
    {
	this.sw = new StringWriter();
	this.out = out;
    }

    /*
     *  ======== write ========
     */
    public void write(int b)
    {
	if (sw != null) {
	    sw.write(b);
	}
	if (out != null) {
	    out.write(b);
	}
    }
    
    /*
     *  ======== print ========
     */
    public void print(String s)
    {
	if (sw != null) {
	    sw.write(s);
	}
	if (out != null) {
	    out.print(s);
	}
    }
    
    /*
     *  ======== getOutput ========
     */
    public String getOutput()
    {
	return (getOutput(sw.getBuffer().length()));
    }
    
    /*
     *  ======== getOutput ========
     */
    public String getOutput(int n)
    {
	String res = "";
	if (sw != null) {
	    StringBuffer sb = sw.getBuffer();
	    int len = sb.length();
	    
	    if (n > len) {
		n = len;
	    }
	    if (n > 0) {
		res = sw.toString().substring(0, n);
		sb.delete(0, n);
	    }
	}

	return (res);
    }

    /*
     *  ======== reset ========
     */
    public void reset()
    {
	if (sw != null) {
	    StringBuffer sb = sw.getBuffer();
	    sb.delete(0, sb.length());
	}
    }
    
    /*
     *  ======== setCapacity ========
     */
    public void setCapacity(int max)
    {
	if (max == 0) {
	    sw = null;
	}
	else if (sw == null) {
	    sw = new StringWriter();
	}
	capacity = max;
    }
    
    private int capacity = -1;
    private StringWriter sw;
    private PrintStream out;
}
