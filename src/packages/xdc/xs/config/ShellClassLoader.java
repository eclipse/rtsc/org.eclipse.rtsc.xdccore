/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
package config;

import java.net.URLClassLoader;
import java.net.URL;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/*
 *  ======== ShellClassLoader ========
 *  The class loader for the config Shell
 *
 *  This classloader allows one to dynamically add jars to the
 *  "class path".  This is used by RTSC to allow one to deliver jars
 *  in a package without any "installation steps"; e.g., setting class paths,
 *  copying jars to special directories, etc.  When a package is
 *  loaded that contains an appropriately named jar, this jar is dynamically
 *  added to the class path.
 */
public class ShellClassLoader extends URLClassLoader {
	
    HashMap loadedJars = new HashMap(); /*  keep track of loaded jars */
    HashSet jarDirs = new HashSet();

    /*
     *  ======== findClass ========
     */
    protected java.lang.Class findClass(String name)
        throws ClassNotFoundException
    {
        return super.findClass(name);
    }
    
    /*
     *  ======== ShellClassLoader ========
     */
    public ShellClassLoader()
    {
        this(new URL [0]);
    }

    public ShellClassLoader(URL [] urls)
    {
        this(urls, ClassLoader.getSystemClassLoader());
    }

    public ShellClassLoader(URL [] urls, ClassLoader parent)
    {
        super(urls, parent);
    }

    /*
     *  ======== addJar ========
     */
    public void addJar(String fileName)
    {
	File file = new java.io.File(fileName);
	if (file.exists()) {
	    if (file.isDirectory()) {
		String prefix = file + "/";
                this.jarDirs.add(prefix);
		String [] flist = file.list();
		for (int i = 0; i < flist.length; i++) {
		    if (flist[i].endsWith(".jar")) {
			addFile(new File(prefix + flist[i]));
		    }
		}
	    }
	    else {
		addFile(file);
	    }
	}
    }
    
    /*
     * ======== findLibrary ========
     */
    protected String findLibrary(String libname)
    {
        for (Iterator it = this.jarDirs.iterator(); it.hasNext(); ) {
            String fn = (String)it.next() + System.mapLibraryName(libname);
            if (new File(fn).exists()) {
                return fn;
            }
        }
        
        return super.findLibrary(libname);
    }
    
    /*
     *  ======== getFiles ========
     */
    public String [] getFiles()
    {
        Object [] keys = loadedJars.keySet().toArray();
        String [] result = new String[keys.length];
        System.arraycopy(keys, 0, result, 0, keys.length);
        return (result);
    }

    /*
     *  ======== print ========
     */
    public void print()
    {
	URL [] urls = getURLs();
	for (int i = 0; i < urls.length; i++) {
	    System.out.println(urls[i].toString());
	}
    }

    /*
     *  ======== addFile ========
     */
    private void addFile(File file)
    {
	String cp;
	try {
	    cp = file.getCanonicalPath();
	}
	catch (java.io.IOException e) {
	    cp = file.getAbsolutePath();
	}
	
	/* don't add cp to class path if it already exists */
	Object val = loadedJars.get(cp);
	if (val != null) {
	    return;
	}

	/* add cp to class path */
	try {
	    URL url = new URL("file:" + cp);
	    addURL(url);
	    /* make sure we add it just once */
	    loadedJars.put(cp, cp);
	}
	catch (java.net.MalformedURLException e)
	{
	}
    }
}
