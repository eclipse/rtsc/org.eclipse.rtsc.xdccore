/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/* -*- Mode: java; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Rhino code, released
 * May 6, 1998.
 *
 * The Initial Developer of the Original Code is
 * Netscape Communications Corporation.
 * Portions created by the Initial Developer are Copyright (C) 1997-1999
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the terms of
 * the GNU General Public License Version 2 or later (the "GPL"), in which
 * case the provisions of the GPL are applicable instead of those above. If
 * you wish to allow use of your version of this file only under the terms of
 * the GPL and not to allow others to use your version of this file under the
 * MPL, indicate your decision by deleting the provisions above and replacing
 * them with the notice and other provisions required by the GPL. If you do
 * not delete the provisions above, a recipient may use your version of this
 * file under either the MPL or the GPL.
 *
 * ***** END LICENSE BLOCK ***** */

/*
        Environment.java

        Wraps java.lang.System properties.

        by Patrick C. Beard <beard@netscape.com>
 */

package config;

import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptRuntime;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.Wrapper;

import java.util.Vector;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Map;
import java.util.HashMap;

/**
 * Environment, intended to be instantiated at global scope, provides
 * a natural way to access System properties from JavaScript.
 *
 * @author Patrick C. Beard
 */
public class Environment extends ScriptableObject implements Wrapper
{
    static final long serialVersionUID = -430727378460177065L;

    /* support for local Java properties */
    private Properties props;

    public static void defineClass(ScriptableObject scope) {
        try {
            ScriptableObject.defineClass(scope, Environment.class);
        } catch (Exception e) {
            throw new Error(e.getMessage());
        }
    }

    public String getClassName() {
        return "Environment";
    }

    /**
     * No-arg constructor is used by defineClass to make the prototype
     *
     * The prototype has no Java Properties associated with it.
     */
    public Environment() {
        super();
    }

    public Environment(ScriptableObject scope) {
        super(scope, null);

        /* default to using the global Java properties */
        props = System.getProperties();
    }

    /**
     * Return the native Java Properties object
     *
     * When passing an Environment object to a Java class, it will
     * be passed as the underlying Java Properties object instead of
     * as the this wrapper.
     */
    public Object unwrap() {
        return props;
    }

    public boolean has(String name, Scriptable start) {
        /* the prototype object */
        if (props == null)
            return super.has(name, start);

        return (props.getProperty(name) != null);
    }

    public Object get(String name, Scriptable start) {
        /* the prototype object */
        if (props == null)
            return super.get(name, start);

        String result = props.getProperty(name);
        if (result != null)
            return ScriptRuntime.toObject(getParentScope(), result);
        else
            return Scriptable.NOT_FOUND;
    }

    public void put(String name, Scriptable start, Object value) {
        if (props == null)
            super.put(name, start, value);
        else
            props.put(name, ScriptRuntime.toString(value));
    }

    private Object[] collectIds() {
        Enumeration names = props.propertyNames();
        Vector keys = new Vector();
        while (names.hasMoreElements())
            keys.addElement(names.nextElement());
        Object[] ids = new Object[keys.size()];
        keys.copyInto(ids);
        return ids;
    }

    public Object[] getIds() {
        /* the prototype instance */
        if (props == null)
            return super.getIds();
        return collectIds();
    }

    public Object[] getAllIds() {
        /* the prototype instance */
        if (props == null)
            return super.getAllIds();
        return collectIds();
    }

    public Scriptable getPrototype() {
        if (super.getPrototype() == null) {
            super.setPrototype(
                ScriptableObject.getClassPrototype(this.getParentScope(),
                                                   getClassName())
            );
        }
        return super.getPrototype();
    }

    /****************************************************/

    /**
     * Set the Java Properties for this environment
     */
    public void setProperties(Properties props) {
        this.props = props;
    }

    /**
     * Get the Java Properties for this environment
     */
    public Properties getProperties() {
        return props;
    }
}
