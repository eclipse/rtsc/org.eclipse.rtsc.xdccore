/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== Shell.java ========
 *
 *! Revision History
 *! ================
 *! 23-May-2012 sasha	moved ShellDebugContext to a separate file
 *! 12-Jan-2012	sasha	added trace support
 *! 11-Sep-2009 sasha   removed deprecation warning for XDC_TRACE_ENABLE
 *! 10-Aug-2005 sasha   unwrapping exceptions in doJavaScriptException
 */

/*
 *  config.Shell
 *
 *  This collection of classes implements the Shell used by the tconf
 *  utility and more generally by any client of the TCF module.  This
 *  shell accepts lines of java script, executes them, and return
 *  results (as strings).
 *
 *  TCF supports several different "modes" of operation:
 *      1. batch mode: execute a script named in a file
 *      2. interactive mode: execute one line at a time
 *      3. debug mode: execute within a GUI debugger
 *
 *  Batch mode is achieved by invoking Shell.main(String args[]).
 *
 *  Interactive mode is achived by constructing a Shell object, repeatedly
 *  invoking the processLine and getOutput methods, and deleting the shell
 *  object via exit.
 *
 *  Debug mode is achieved by running a separate "debug Shell" rather than
 *  this Shell; see org.mozilla.javascript.tools.debugger.JSDebugger.
 *
 */
package config;

import java.io.*;
import java.lang.reflect.*;
import java.net.URL;
import java.util.*;
import java.text.MessageFormat;
import java.text.NumberFormat;

import org.mozilla.javascript.*;
import org.mozilla.javascript.debug.*;
import org.mozilla.javascript.tools.ToolErrorReporter;

import org.mozilla.javascript.tools.debugger.ScopeProvider;
import org.mozilla.javascript.tools.debugger.Main;

/*
 *  ======== Shell ========
 */
public class Shell extends ScriptableObject
    implements ScopeProvider
{

    /* the active Rhino debugger Main object; allows Shell to control GUI */
    org.mozilla.javascript.tools.debugger.Main rhinoDbgMain = null;

    /* These exit codes should match TCF error codes in tcf.h: TCF_E[A-Z]+ */
    static final public int EXITCODE_OK = 0;
    static final public int EXITCODE_FAIL = 1;
    static final public int EXITCODE_USAGE_ERROR = 2;
    static final public int EXITCODE_FILE_NOT_FOUND = 3;
    static final public int EXITCODE_IO_ERROR = 4;
    static final public int EXITCODE_INCOMPLETE = 5;
    static final public int EXITCODE_FATAL = 6;
    static final public int EXITCODE_INTERN_ERROR = 7;

    static final public int STDOUT = 1;
    static final public int STDERR = 2;
    static final private String usageString = "Invalid or incomplete option \"{0}\". \n    Options:\n        -w                    // turn-on warning reporting\n        -v                    // verbose (display value of expressions)\n        -f script-filename    // execute script contained in file\n        -e script-source      // execute the statements in the string\n";

    static public ShellClassLoader curClsLoader = new ShellClassLoader(
            new URL [0],
            /* parent is whichever classloader loaded me */
            Shell.class.getClassLoader()
        );

    /*
     *  ======== Shell ========
     *  Create a Configuration Shell Session.
     *
     *  Params:
     *      externalDebug   - if true then an external debugger has been setup
     *                        and Shell should not override the debugger;
     *                        the external debugger presumably provides
     *                        enough information about locations of exceptions
     *
     *            dbgMain   - if non-null the GUI Debugger is active. Use the
     *                        object to set the GUI visible and break on entry
     *                        of the user's script
     *
     *  The no-arg constructor is called by main() below (because it needs to
     *  set the mode to non-interactive).
     *
     *  The constructor with an argument array is called via JNI to create an
     *  interactive session.
     */
    public Shell()
    {
        this(false);
    }

    public Shell(Properties props)
    {
        this(false, null, props);
    }

    public Shell(boolean externalDebug)
    {
        this(externalDebug, null);
    }

    public Shell(boolean externalDebug,
        org.mozilla.javascript.tools.debugger.Main dbgMain)
    {
        this(externalDebug, dbgMain, System.getProperties());
    }

    public Shell(boolean externalDebug,
        org.mozilla.javascript.tools.debugger.Main dbgMain, Properties props)
    {
        String traceMsg = "TIME=" + fmt.format(new java.util.Date());
        this.rhinoDbgMain = dbgMain;

        this.profileThreshold =
            Integer.getInteger("tconf.shell.options.profile", 0).intValue();

        /* TODO: default should be 0 not -1.  However, 0 causes error
         *  messages generated by cx.reportError() to not contain line
         *  number information!
         */
        this.optimizationLevel =
            Integer.getInteger("tconf.shell.options.opt", -1).intValue();

        if (this.profileThreshold > 0) {
            /* attach instruction observer to monitor performance */
            this.cx = new ShellContext();
            this.cx.enter(this.cx);
            this.cx.setInstructionObserverThreshold(this.profileThreshold);
            this.optimizationLevel = -1;    /* can't profile while optimized */
        }
        else {
            this.cx = Context.enter();
        }

        this.stdout = new ShellOutputStream(System.out);
        this.stderr = new ShellOutputStream(System.err);
        this.errorReporter = new ShellErrorReporter(false, this.stderr);

        this.cx.setErrorReporter(this.errorReporter);
        this.cx.setApplicationClassLoader(curClsLoader);

        /* attach "debugger" so we can get JS stack trace from JS exceptions */
        if (!externalDebug) {
            this.cx.setDebugger(new ShellDebugger(), new ShellDebugContext());
        }

        /* Initialize the standard objects (Object, Function, etc.)
         * This must be done before scripts can be executed.
         */
        this.cx.initStandardObjects(this, false);

        /*
         *  Define some global functions particular to this shell. Note
         *  that these functions are not part of ECMA.  However, to ensure
         *  that all scripts can be run inside the Rhino debugger (with the
         *  same results), *any* function added here must be 100% compatible
         *  with the Rhino shell; the Rhino debugger uses the Rhino shell
         *  to run scripts.
         *
         *  These functions *may* be overridden in the tconfini.tcf,
         *  however.  Since the Rhino debugger will also execute the
         *  tconfini.tcf script any overrides will be seen in the Rhino
         *  debugger.  Thus, we only define *only* those functions that we
         *  need to bootstrap the configuration world; all other global
         *  functions should be defined in tconfini.tcf.
         */
        String [] names = {"print", "load", "defineClass", "addJars",
                           "loadClass"};
        String [] pnames = {"print", "load", "defineClass", "addJars",
                            "loadClass", "_profile" };
        if (this.profileThreshold > 0) {
            names = pnames;
        }
        this.defineFunctionProperties(names, Shell.class,
                                     ScriptableObject.DONTENUM);

        /* Set the package name of the generated classes in compiled mode.
         * (Perhaps we should use a package name?)
         */
//      ClassNameHelper.get(this.cx).setTargetPackage("");

        /* Don't Wrap Java primitives so that
         *      (java.lang.Boolean.FALSE) ? foo() : bar()
         * evaluates to bar() rather than foo().
         *
         * See also rhino/examples/PrimitiveWrapFactory.java
         */
//      this.cx.getWrapFactory().setJavaPrimitiveWrap(false)

        /* add trace enable and warning strings from the environment to
         * the system properties, unless already set on command line
         */
        Map<String, String> traceEnv = new HashMap<String, String>() {
            {
                put("xdc.traceLevel",    "XDC_TRACE_LEVEL");
                put("xdc.traceCapsules", "XDC_TRACE_CAPSULES");
                put("xdc.tracePackages", "XDC_TRACE_PACKAGES");
                put("xdc.traceGroups",   "XDC_TRACE_GROUPS");
                put("xdc.traceEnable",   "XDC_TRACE_ENABLE");
                put("xdc.warningsSuppress", "XDC_WARNINGS_SUPPRESS");
            }
        };
        Iterator it = traceEnv.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry =
                (Map.Entry<String, String>)it.next();
            String envTrace = entry.getKey();
            String varTrace = entry.getValue();
            if (System.getProperty(envTrace) == null
                && System.getenv(varTrace) != null) {
                System.setProperty(envTrace, System.getenv(varTrace));
            }
        }

        if (System.getProperty("xdc.traceGroups") != null
            && System.getProperty("xdc.traceGroups").contains("bench")) {
            trace = true;
            System.out.println(traceMsg + " Shell.constructor starts");
        }


        /* Set up "environment" in the global scope to provide access to the
         * System environment variables.
         */
        Environment.defineClass(this);
        Environment environment = new Environment(this);
        environment.setProperties(props);
        this.defineProperty("environment", environment,
                              ScriptableObject.DONTENUM);


        /* Default to interactive mode */
        this.setInteractive(true);
    }

    /*
     *  ======== Shell ========
     *  This constructor with arguments is called via JNI to create an
     *  interactive session.
     */
    public Shell(String args[])
    {
        this(false);

        exec(args);     /* BUG: we should fail if the return is non-zero */
    }

    /*
     *  ======== exit ========
     *  This is called by JNI to exit an interactive Shell session.
     *  Failure to call this will result in a memory leak because the
     *  Context object maintains a "back" reference to the Shell.
     */
    public void exit()
    {
        if (profileThreshold > 0) {
            ShellDebugContext dc = ShellDebugContext.get(cx);
            if (dc != null) {
                dc.dumpProfile();
            }
        }

        cx.exit();
        if (trace) {
            String msg = "TIME=" + fmt.format(new java.util.Date());
            System.out.println(msg + " Shell.exit() exits");
        }
    }

    /*
     *  ======== getOutput ========
     */
    public String getOutput(int sid, int n)
    {
        ShellOutputStream sos = (sid == STDERR) ? stderr : stdout;
        return (sos.getOutput(n));
    }

    /*
     *  ======== getOutput ========
     */
    public String getOutput(int sid)
    {
        ShellOutputStream sos = (sid == STDERR) ? stderr : stdout;
        return (sos.getOutput());
    }

    /*
     *  ======== getRelativePath ========
     */
    public static String getRelativePath(String path, String base)
    {
        String cwd, src;
        String result;

        /* need to handle case where exceptions have no source file and
         * we blindly pass null for path
         */
        if (path == null) {
           return (base);
        }

        try {
            cwd = (new File(base)).getCanonicalPath();
            src = (new File(path)).getCanonicalPath();
        }
        catch (IOException io) {
            return (path);
        }

        if (src.indexOf(cwd + File.separator) == 0) {
            result = "." + src.substring(cwd.length()).replace('\\', '/');
        }
        else {
            /*
             * BUG: we really should compute a path relative to base, but
             * for now simply return a full path.
             */
            result = src.replace('\\', '/');
        }

        return (result);
    }

    /*
     *  ======== ScopeProvider.getScope ========
     *  This method is called by Rhino's debugger to get a script's
     *  global scope object.
     */
    public Scriptable getScope()
    {
        return (this);
    }

    /*
     *  ======== getStackTrace ========
     */
    public static String getStackTrace(String prefix)
    {
        return (getStackTrace(prefix, "", null, 0));
    }

    public static String getStackTrace(String prefix, String banner)
    {
        return (getStackTrace(prefix, banner, null, 0));
    }

    /*
     *  ======== getStackTrace ========
     *  This function generates a stack trace output with a possibility to
     *  skip a frame coming from a certain file name and a line number. That
     *  capability is used in some cases where Rhino already printed info
     *  from that frame.
     *
     *  @param prefix   beginning of a line for one frame, usually TAB
     *  @param banner   beginning of the whole printout
     *  @param skipFile file name from a frame to be excluded from printout
     *  @param skipLine line number from a frame to be excluded from printout
     *  @return         string displaying frames from the top frame to the
     *                  bottom frame, possibly excluding one of them
     */
    public static String getStackTrace(String prefix, String banner,
                                       String skipFile, int skipLine)
    {
        String stackTrace = "";

        Context cx = Context.getCurrentContext();
        ShellDebugContext dc = ShellDebugContext.get(cx);

        if (dc != null) {
            boolean showAll = false;
            String tg = System.getProperty("xdc.traceGroups");
            if (tg != null && tg.indexOf("xdcerror") != -1) {
                showAll = true;
            }

            String root = System.getProperty("xdc.root");
            if (root != null) {
                try {
                    root = (new File(root)).getCanonicalPath();
                }
                catch (IOException io) {
                    ;
                }
                root = root.replace('\\', '/');
            }

            /* Create a number formatter that does *not* use commas; although
             * commas make it easier for the human to read, it's more difficult
             * to get tools like CCS, emacs, etc. to parse error messages to
             * take you to the specified line number.
             */
            NumberFormat nf = NumberFormat.getNumberInstance();
            nf.setGroupingUsed(false);

            int count, i;
            if ((count = dc.getFrameCount()) > 0) {
                stackTrace += banner;
                int last = dc.exceptionCount <= count ?
                    dc.exceptionCount : count;
                for (i = 0; i < last; i++) {
                    ShellDebugFrame frame = dc.getFrame(i);
                    String src = getRelativePath(frame.getSourceName(), ".");
                    /* There are cases when Rhino already prints out the
                     * original error message, but that call frame is still
                     * on stack. We don't want to print it out twice. Since
                     * frame.getSourceName() returns canonical path, we need
                     * to do the same with skipFile.
                     */
                    String absSkipFile;
                    try {
                        absSkipFile =
                            (new java.io.File(skipFile).getCanonicalPath());
                    }
                    catch (Exception e) {
                        absSkipFile = skipFile;
                    }
                    if (skipLine != frame.getLineNumber()
                        || (!absSkipFile.equals(frame.getSourceName())
                        && !skipFile.equals(frame.getSourceName()))) {
//                        System.out.println("src = '" + src + "', root = '" + root + "'");
                        /* optionally show source files in the xdc root */
                        if ((root != null && src.indexOf(root) != 0
                            && src.indexOf("$internal") == -1)
                            || showAll) {
                            stackTrace = stackTrace + prefix + "\"" + src
                                + "\", line "
                                + nf.format(frame.getLineNumber())
                                + "\n";
                        }
                    }
                }
            }
        }

        return (stackTrace);
    }

    /*
     *  ======== getFrame ========
     *  Get a selected frame for the call stack. The returned value is a string
     *  that includes a file name and a line number.
     *
     *  @param index    index of the selected frame; 0 is the current (top)
     *                  frame, 1 is the caller to 0, and so on
     *  @return         string displaying file name and a line number
     */
    public static String getFrame(int index)
    {
        String stackTrace = "";

        Context cx = Context.getCurrentContext();
        ShellDebugContext dc = ShellDebugContext.get(cx);
        if (dc == null) {
            return "";
        }
        ShellDebugFrame frame = dc.getFrame(index);
        stackTrace = "\"" + frame.getSourceName() + "\", line "
             + frame.getLineNumber();

        return stackTrace;
    }

    /*
     *  ======== processReader ========
     *  This function is called only from processFile at the beginning of
     *  running in script mode. It calls evaluateReader(), where all
     *  JavaScript exception handling is done.
     *
     *  @param in       script to be processed
     *  @param filename name of the script
     *  @return         EXITCODE_FATAL or EXITCODE_OK
     */
    public int processReader(Reader in, String filename) {
        if (fatalError) {
            return (EXITCODE_FATAL);
        }

        if (trace) {
            String msg = "TIME=" + fmt.format(new java.util.Date());
            System.out.println(msg + " Rhino evaluating " + filename);
        }

        // Here we evalute the entire contents of the file as
        // a script. Text is printed only if the print() function
        // is called.
        value = evaluateReader(cx, this, in, filename, 1);
        if (value == null) {
            fatalError = true;
            return (EXITCODE_FATAL);
        }

        return (errorReporter.getErrorFlag() ? EXITCODE_FAIL : EXITCODE_OK);
    }

    /*
     *  ======== processFile ========
     *  Evaluate the script contained in the file named by filename
     *  within  the current scope and context.  Note this method is
     *  called from within Shell creation (via processOptions()).
     */
    public int processFile(String filename)
    {
//        System.out.println("processFile:filename = "+filename);
        if (fatalError) {
            return (EXITCODE_FATAL);
        }

        Reader in = null;
        try {
            in = new PushbackReader(new FileReader(filename));
            int c = in.read();
            // Support the executable script #! syntax:  If
            // the first line begins with a '#', treat the whole
            // line as a comment.
            if (c == '#') {
                while ((c = in.read()) != -1) {
                    if (c == '\n' || c == '\r') {
                        break;
                    }
                }
                ((PushbackReader) in).unread(c);
            }
            else {
                // No '#' line, just reopen the file and forget it
                // ever happened.  OPT closing and reopening
                // undoubtedly carries some cost.  Is this faster
                // or slower than leaving the PushbackReader
                // around?
                in.close();
		/* This stream is closed in evaluateReader. */
                in = new FileReader(filename);
            }
        }
        catch (FileNotFoundException ex) {
            Context.reportError(ToolErrorReporter.getMessage(
                "msg.couldnt.open",
                filename));
            return (EXITCODE_FILE_NOT_FOUND);
        }
        catch (IOException ioe) {
            stderr.print(ioe.toString());
            return (EXITCODE_IO_ERROR);
        }

        return processReader(in, filename);
    }

    /**
     * ======== processLine ========
     * Evaluate JavaScript source.
     *
     * @param line the next line to interpret
     */
    public int processLine(String line) {

        if (fatalError) {
            return (EXITCODE_FATAL);
        }

        // clear output stream buffers and previous error status
        stderr.reset();
        stdout.reset();
        errorReporter.clearErrorFlag();

        // check that string is a complete statement
        if (!cx.stringIsCompilableUnit(line)) {
            return (EXITCODE_INCOMPLETE);
        }

        // use the interpreter for interactive input
        cx.setOptimizationLevel(-1);

        // set error reporter to allow multiple shells per thread
        cx.setErrorReporter(errorReporter);

        // process the line
        Reader reader = new StringReader(line);
        value = evaluateReader(cx, this, reader, "<stdin>", lineno++);
        if (verbose && value != null && value != cx.getUndefinedValue()) {
            stdout.print(cx.toString(value) + "\n");
        }

        // force all output to the output streams
        System.out.flush();
        System.err.flush();

        if (value == null) {
            fatalError = true;
            return (EXITCODE_FATAL);
        }
        return (errorReporter.getErrorFlag() ? EXITCODE_FAIL : EXITCODE_OK);
    }

    /*
     *  ======== processLoad ========
     *  Evaluate the loaded script contained in the file named by filename
     *  within  the current scope and context.  Note this method is
     *  called only from within scripts (via "load").
     *
     *  This function does not do any exception handling because it would have
     *  to catch exceptions, which would remove stack frames that let the
     *  exception go through. All exception handling is done in
     *  evaluateReader.
     */
    public int processLoad(String filename)
    {
        if (fatalError) {
            return (EXITCODE_FATAL);
        }

//        System.out.println("processLoad:filename = "+filename);

        Reader in = null;
        try {
            in = new PushbackReader(new FileReader(filename));
            int c = in.read();
            // Support the executable script #! syntax:  If
            // the first line begins with a '#', treat the whole
            // line as a comment.
            if (c == '#') {
                while ((c = in.read()) != -1) {
                    if (c == '\n' || c == '\r') {
                        break;
                    }
                }
                ((PushbackReader) in).unread(c);
            }
            else {
                // No '#' line, just reopen the file and forget it
                // ever happened.  OPT closing and reopening
                // undoubtedly carries some cost.  Is this faster
                // or slower than leaving the PushbackReader
                // around?
                in.close();
		/* This stream is closed in evaluateLoad. */
                in = new FileReader(filename);
            }
        }
        catch (FileNotFoundException ex) {
            Context.reportError(ToolErrorReporter.getMessage(
                "msg.couldnt.open",
                filename));
            return (EXITCODE_FILE_NOT_FOUND);
        }
        catch (IOException ioe) {
            stderr.print(ioe.toString());
            return (EXITCODE_IO_ERROR);
        }

        // Here we evalute the entire contents of the file as
        // a script. Text is printed only if the print() function
        // is called.
        value = evaluateLoad(cx, this, in, filename, 1);
        if (value == null) {
            fatalError = true;
            //System.err.println("Shell$ returning error code to load");
            return (EXITCODE_FATAL);
        }

        return (errorReporter.getErrorFlag() ? EXITCODE_FAIL : EXITCODE_OK);
    }

    /*
     *  ======== setInteractive ========
     */
    public void setInteractive(boolean interactive)
    {
        if (!interactive) {
            cx.setOptimizationLevel(
                optimizationLevel < 0 ? -1 : optimizationLevel);
            if (optimizationLevel != -1) {
                /* can only debug using the interpreted mode of rhino */
                cx.setDebugger(null, null);
            }
            stdout.setCapacity(0);
            stderr.setCapacity(0);
        }
        else {
            cx.setOptimizationLevel(-1);
            stdout.setCapacity(-1);
            stderr.setCapacity(-1);
        }
    }

    /*
     *  ======== finalize ========
     *  This method is called when a Shell object is being reclaimed by
     *  the garbage collector.  Note we have *no* control over the order
     *  in which items are removed; although it is possible to force
     *  garbage collection synchroneously.  Moreover, finalize() methods
     *  may be invoked when the application exits.  Applications can
     *  request this via System.runFinalizersOnExit(true).
     */
    protected void finalize() throws Throwable
    {
        try {
            cx.exit();  // unnecessary because a Shell can not be GC'd
                        // until cx.exit() is called (Context puts
                        // Context objects in a hashtable until exit()
                        // is called).
                        //
                        // safe because Context reference counts enter's.
                        //
                        // Leave it in because changing the implementation
                        // of Context may allow GC of Shell objects that
                        // have not exited.
        }
        finally {
            super.finalize();
        }
    }

    /**
     *  ======== exec ========
     *  Execute the given arguments, but don't exit at the end.
     */
    public int exec(String args[])
    {
        /* parse options  */
        if ((args = processOptions(args)) == null) {
            return (EXITCODE_USAGE_ERROR);
        }

        if (trace) {
            String msg = "TIME=" + fmt.format(new java.util.Date());
            System.out.println(msg + " Shell.exec() starts");
        }
        
        /* Set up "arguments" in the global scope to contain the command
         * line arguments after the name of the script to execute
         */
        Object[] array = {};
        if (args.length > 0) {
            int length = args.length - 1;
            array = new Object[length];
            System.arraycopy(args, 1, array, 0, length);
        }
        Scriptable argsObj = cx.newArray(this, array);
        this.defineProperty("arguments", argsObj, ScriptableObject.DONTENUM);

        /* execute any '-f' files specified in options */
        for (int i = 0; i < fileList.size(); i++) {
            processFile((String)fileList.get(i));
        }

        /* execute script specified on command line (after any options) */
        if (args.length > 0) {

            /* if debug object is non-null then break on user script */
            if (rhinoDbgMain != null) {
                rhinoDbgMain.setVisible(true);
                rhinoDbgMain.setBreakOnEnter(true);
            }

            return (processFile(args[0]));
        }

        return (EXITCODE_OK);
    }

    /*
     *  ======== evaluateLoad ========
     *  This function is called only from processLoad() and
     *  utils.loadCapsule().
     */
    public static Object evaluateLoad(Context cx, Scriptable scope, Reader in,
                                      String sourceName, int lineno)
    {
        Object result = cx.getUndefinedValue();
        try {
            String rpath = getRelativePath(sourceName, ".");
            result = cx.evaluateReader(scope, in, rpath, lineno, null);
            if (result == null) {
                result = cx.getUndefinedValue();
            }

            /* This is for the cases when there are reported errors, but there
             * is no exception thrown. It only happens if in Err.exit(),
             * we invoke reportRuntimeError() instead of
             * throwAsScriptRuntimeEx(), or if ShellErrorReporter.error() was
             * called directly.
             * An example of it is config/Program.gen() used in Tconf scripts,
             * where error() is called, but then the execution continues
             * without throwing an exception. Without an exception, the stack
             * trace is not printed.
             * In such cases, we throw an exception here that will be caught
             * in evaluateReader and the stack trace will be printed.
             */
            if (((ToolErrorReporter)(cx.getErrorReporter())).
                 hasReportedError()) {
                 Context.throwAsScriptRuntimeEx(
                     new EvaluatorException("Error"));
            }
            in.close();
            return (result);
        }
        catch (IOException ioe) {
	    if (in != null) {
	        try {
		    in.close();
		}
		catch (IOException ioe2) {}
            }
            return (null);
        }
    }

    /*
     *  ======== evaluateReader ========
     *  This function is being called from processFile() and procesLine() and
     *  represents the entry into Rhino for both script processing and Tconf
     *  interactive shell. All exception handling is done here.
     */
    private static Object evaluateReader(Context cx, Scriptable scope,
                                         Reader in, String sourceName,
                                         int lineno)
    {
        Object result = cx.getUndefinedValue();
        String error;

        try {
            String rpath = getRelativePath(sourceName, ".");
            result = cx.evaluateReader(scope, in, rpath, lineno, null);
            if (result == null) {
                result = cx.getUndefinedValue();
            }
        }
        catch (EcmaError ee) {
            /* EcmaErrors are generated by Rhino, most frequently when an
             * undefined variable or property is referenced. These errors are
             * defined in ECMA specification.
             */
            error = ToolErrorReporter.getMessage("msg.format1", ee.details());
            Context.reportError(error, ee.sourceName(), ee.lineNumber(),
                                ee.lineSource(), ee.columnNumber());
        }
        catch (WrappedException we) {
            /* WrappedExceptions are EvaluatorExceptions that have a more
             * interesting exception "inside" that needs to be unwrapped.
             * Any Java exception thrown in Java, and not caught in JavaScript
             * ends up here as EvaluatorException.
             */
            //System.err.println("#Shell.evaluateReader WrappedEvaluatorException:");
            Throwable ex = ((WrappedException)we).getWrappedException();
            if (ex instanceof JavaScriptException) {
                handleJavaScriptException((JavaScriptException)ex);
            }
            else {
                /* This is a standard Java exception, or an object other
                 * than EcmaError or EvaluatorException supplied as an
                 * argument to Context.throwAsScriptRuntimeEx().
                 */
                //System.err.println("#WrappedException: Java exception");
                error = ex.getMessage();
                Context.reportError(error, we.sourceName(), we.lineNumber(),
                                    we.lineSource(), we.columnNumber());
                we.printStackTrace();
            }
        }
        catch (EvaluatorException ee) {
            /*
             * EvaluatorExceptions are created for the most usual syntax errors:
             * e.g., "new Array(-1)", "2++", or "print(;".
             *
             * EvaluatorExceptions are created by Context.reportRuntimeError
             * which in turn creates them by calling the ErrorReporter's
             * runtimeError() method. This exception is thrown by the
             * rhino runtime.
             *
             * EvaluatorExceptions are also thrown from Err.java in
             * xdc.services.intern.xsr, which is a class used by Java code
             * in xdc.services packages. Such errors include the errors that
             * are not recognized by JavaScript, because they are related to
             * XDCscript extensions (maps, types, function signatures).
             *
            System.err.println("#Shell.evaluateReader EvaluatorException:");
            System.err.println("message: " + ee.getMessage());
            System.err.println("file: " + ee.sourceName());
            System.err.println("line: " + ee.lineNumber());
             */

            boolean hasReported = false;
            ErrorReporter err = cx.getErrorReporter();
            if (err instanceof ToolErrorReporter) {
                hasReported = ((ToolErrorReporter)err).hasReportedError();
            }

            if (!hasReported) {
                /* This is an XDCscript exception thrown from Global.java by
                 * calling Context.throwAsScriptRuntimeEx() or from Err.java by
                 * calling Context.reportRuntimeError(). Usually, it's a
                 * reference error specific for XDCscript (an incorrect type,
                 * or a reference to a non-existent property for a typed
                 * object).
                 * The file name and the line number weren't added at the time
                 * of these calls, so we get the file name and the line from
                 * the top of the stack.
                 */
                //System.err.println("XDCscript reference error");
                if (ee.sourceName() == null) {
                    ShellDebugContext dc = ShellDebugContext.get(cx);
                    ShellDebugFrame frame = dc.getFrame(0);
                    int line = frame.getLineNumber();
                    String filename = frame.getSourceName();
                    ee.initSourceName(filename);
                    ee.initLineNumber(line);
                }
                Context.reportError(ee.details(), ee.sourceName(),
                    ee.lineNumber(), ee.lineSource(), ee.columnNumber());
            }
            else {
                /* This is a JavaScript syntax error, already reported by Rhino
                 * by calling ShellErrorReporter.error() for each encountered
                 * error. However, at that time the stack trace contained only
                 * the frame for the loaded script. At the end of the script
                 * evaluation, Rhino calls ShellErrorReporter.runtimeError(),
                 * which then throws EvaluationException caught here. Only now
                 * we can print the whole stack trace.
                 */
                System.err.print(Shell.getStackTrace("    "));
            }
        }
        catch (JavaScriptException jse) {
            /* This is the common case, when a JavaScript throws something
            System.err.println("JavaScriptException caught in evaluateReader");
            System.err.println("message: " + jse.getMessage());
            System.err.println("file: " + jse.sourceName());
            System.err.println("line: " + jse.lineNumber());
            System.err.println("value: " + jse.getValue());
             */
            handleJavaScriptException(jse);
        }
        catch (Exception e) {
            /*
             * Catch all "checked" exceptions (exceptions that require
             * declaration in the method signature if they can come up)
             * and "unchecked" RuntimeExceptions.
             *
             * The only exceptions not caught are Errors which we should
             * not (according to Java conventions) try to catch.
             */
            //System.err.println("Other exceptions: " + e.getClass().getName());
            error = e.toString();
            Context.reportError(error);
            if (e instanceof RuntimeException) {
                //System.out.println("Runtime Exception:");
                /* TODO: "filter" stack traces */
                e.printStackTrace();    /* assume its a Java Host obj excep */
            }
            result = null;
        }

        finally {
            try {
                in.close();
            }
            catch (IOException ioe) {
                error = ioe.toString();
                Context.reportError(error);
            }
        }
        return (result);
    }

    /*
     *  ======== handleJavaScriptException ========
     *  This function processes a throw command from JavaScript
     *
     *  @param jse      object thrown from JavaScript
     */
    private static void handleJavaScriptException(JavaScriptException jse)
    {
        /*
        System.out.println("Shell JavaScriptException: " + jse.getClass().getName());
        System.out.println("Shell JavaScriptException: details " + jse.details());
        */

        String rhName = "rhinoException";
        Object unwrappedException = null;
        Object o = jse.getValue();

        /*
         * JavaScriptException's are created as a result of JavaScript's
         * throw keyword, or Java throwing a JavaScriptException.
         * The argument to JavaScript throw or to JavaScriptException
         * constructor is extracted by calling getValue().
         */

        if (o instanceof Scriptable) {
            //System.err.println("o is Scriptable " + o.getClass().getName());
            if (ScriptableObject.hasProperty((Scriptable)o, rhName)) {
                /* This is a JavaScriptException with RhinoException inside.
                 * An example of this case is when a Java exception is caught
                 * in a script and then rethrown.
                 */
                //System.err.println("Java exception caught and rethrown");
                NativeJavaObject rhEx = (NativeJavaObject)
                    ScriptableObject.getProperty((Scriptable)o, rhName);
                RhinoException unwrapped = (RhinoException)rhEx.unwrap();

                if (unwrapped instanceof WrappedException) {
                    //System.err.println("rhino Exception unwrapped more");
                    Object wrEx =
                        ((WrappedException)unwrapped).getWrappedException();
                    unwrappedException = wrEx;
                }
                else {
                    unwrappedException = unwrapped.getMessage();
                }
            }
            else {
                if (o instanceof NativeJavaObject) {
                    //System.err.println("NativeJavaObject");
                    /* JavaScript threw a Java Object, possibly XDCException.
                     * Rhino wrapped it up in a scriptable object of type
                     * NativeJavaObject.
                     */
                    unwrappedException = ((NativeJavaObject)o).unwrap();
                    //System.err.println("Unwrapped: " + unwrappedException);
                }
                else {
                    //System.err.println("Something scriptable thrown");
                    /* JavaScript threw a JavaScript Error or some other
                     * JavaScript object, or Java threw JavaScriptException,
                     * with some scriptable object as the constructor's
                     * argument.
                     * The caught object was the argument for JavaScript throw
                     * or for JavaScriptException constructor.
                     */
                    unwrappedException = o;
                }
            }
        }
        else {
            /* JavaScriptException was thrown from Java with a value of a
             * primitive type (a string or a number) passed to the constructor,
             * or JavaScript threw a primitive type value.
             */
            //System.err.println("Primitive type thrown");
            unwrappedException = o;
        }

        /* A more specific error in some cases can be found in the
         * unwrapped object.
         */
        String msg = unwrappedException != null ?
            unwrappedException.toString() : jse.getMessage();
        String error = ToolErrorReporter.getMessage("msg.format1", msg);
        String fileName = jse.sourceName();
        int lineNumber = jse.lineNumber();

        Context.reportError(error, fileName, lineNumber, jse.lineSource(),
                            jse.columnNumber());
    }

    /**
     *  ======== processOptions ========
     *  Parse arguments.
     */
    private String[] processOptions(String args[]) {
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];

            /* if not an option then we've found a script with args */
            if (!arg.startsWith("-")) {
                String[] result = new String[args.length - i];
                System.arraycopy(args, i, result, 0, args.length - i);
                return result;
            }

            if (arg.equals("-e")) {
                if (++i == args.length) {
                    usage(arg);
                    return (null);
                }
                Reader reader = new StringReader(args[i]);
                value = evaluateReader(cx, this, reader, "<command>", 1);
                if (value == null) {
                    fatalError = true;
                    return (null);
                }
                continue;
            }
            if (arg.equals("-w")) {
                errorReporter.setIsReportingWarnings(true);
                continue;
            }
            if (arg.equals("-f")) {
                if (++i == args.length) {
                    usage(arg);
                    return (null);
                }
                fileList.addElement(args[i]);
                continue;
            }
            if (arg.equals("-v")) {
                verbose = true;
                continue;
            }

            usage(arg);
            return (null);
        }

        return new String[0];
    }

    /**
     *  ======== usage ========
     *  Generate a usage message.
     */
    private void usage(String s) {
        Object[] args = {s};
        MessageFormat formatter = new MessageFormat(usageString);
        stderr.print(formatter.format(args));
    }

    /**
     * Return name of this class, the global object.
     *
     * This method must be implemented in all concrete classes
     * extending ScriptableObject.
     *
     * @see org.mozilla.javascript.Scriptable#getClassName
     */
    public String getClassName() {
        return "global";
    }

    /**
     *  ======== print ========
     * Print the string values of its arguments.
     *
     * This method is defined as a JavaScript function.
     * Note that its arguments are of the "varargs" form, which
     * allows it to handle an arbitrary number of arguments
     * supplied to the JavaScript function.
     *
     */
    public static Object print(Context cx, Scriptable thisObj,
                               Object[] args, Function funObj)
    {
        /*
         *  Get the shell object from thisObj;  it may be a NativeCall
         *  in the case that we are called in a nested funtion.  For
         *  example, the following script fails without getTopLevelScope():
         *
         *      function cfg () {
         *          function mprint(msg)
         *          {
         *              _print(msg);
         *          }
         *
         *          var _print = print;
         *          mprint("hello world!");
         *      }
         *      cfg()
         */
        Shell shell = (Shell)ScriptableObject.getTopLevelScope(thisObj);
        ShellOutputStream stdout = shell.stdout;

        for (int i = 0; i < args.length; i++) {
            if (i > 0) {
                stdout.print(" ");
            }

            /* Convert the arbitrary JavaScript value into a string form. */
            String s = Context.toString(args[i]);

            stdout.print(s);
        }
        stdout.print("\n");
        return Context.getUndefinedValue();
    }

    /**
     *  ======== load ========
     * Load and execute a set of JavaScript source files.
     *
     * This method is defined as a JavaScript function.
     *
     */
    public static void load(Context cx, Scriptable thisObj,
                            Object[] args, Function funObj)
    {
        /* Get the shell object from thisObj; see print above */
        Shell shell = (Shell)ScriptableObject.getTopLevelScope(thisObj);
        //System.err.println("Shell$load " + cx.toString(args[0]));

        for (int i = 0; i < args.length; i++) {
            shell.processLoad(cx.toString(args[i]));
        }
    }

    /**
     *  ======== addJars ========
     *  Add specified Jars to class path
     *
     * This method is defined as a JavaScript function.
     *
     */
    public static Scriptable addJars(Context cx, Scriptable thisObj,
                            Object[] args, Function funObj)
    {
        Scriptable result = null;

        /* Get the shell object from thisObj; see print above */
        Shell shell = (Shell)ScriptableObject.getTopLevelScope(thisObj);

        ClassLoader cl = cx.getApplicationClassLoader();
        if (cl != null && cl instanceof ShellClassLoader) {
            for (int i = 0; i < args.length; i++) {
                ((ShellClassLoader)cl).addJar(cx.toString(args[i]));
            }
            if (args.length <= 0) {
                String [] jars = ((ShellClassLoader)cl).getFiles();
                result = cx.newObject(shell, "Array", jars);
            }
        }
        else {
            Context.reportError("addJars: class loader ("
                + cl + ") doesn't support runtime extensions");
        }

        return (result);
    }

    /**
     *  ======== _profile ========
     *  Save profile data to specified file
     *
     *  This method is defined as a JavaScript function in tconfini.tcf.
     */
    public static void _profile(Context cx, Scriptable thisObj,
                            Object[] args, Function funObj)
    {
        ShellDebugContext dc = ShellDebugContext.get(cx);
        if (dc != null) {
            String prefix = args.length > 0 ? cx.toString(args[0]) : "";

            if (args.length > 1) {
                String fileName = cx.toString(args[1]);
                BufferedWriter out;
                try {
                    out = new BufferedWriter(new FileWriter(fileName));
                }
                catch (IOException e) {
                    Context.reportError("_profile: " + e);
                    return;
                }
                dc.dumpProfile(prefix, out);
            }
            else {
                dc.dumpProfile(prefix);
            }
        }
    }

    /**
     *  ======== report ========
     *  Trigger warning or an error from within JavaScript.
     *
     *  This method is defined as a JavaScript function in tconfini.tcf.
     */
    public static void report(String type, String mesg)
    {
        if (type.equals("warning")) {
            Context.reportWarning(mesg);
        }
        else if (type.equals("error")) {
            Context.reportError(mesg);
        }
        else {
            Context.reportError("invalid report type: " + type);
        }
    }

    /**
     *  ======== defineClass ========
     * Load a Java class that defines a JavaScript object using the
     * conventions outlined in ScriptableObject.defineClass.
     * <p>
     * This method is defined as a JavaScript function.
     * @exception IllegalAccessException if access is not available
     *            to a reflected class member
     * @exception InstantiationException if unable to instantiate
     *            the named class
     * @exception InvocationTargetException if an exception is thrown
     *            during execution of methods of the named class
     * @exception ClassDefinitionException if the format of the
     *            class causes this exception in ScriptableObject.defineClass
     * @exception PropertyException if the format of the
     *            class causes this exception in ScriptableObject.defineClass
     * @see org.mozilla.javascript.ScriptableObject#defineClass
     */
    public static void defineClass(Context cx, Scriptable thisObj,
                                   Object[] args, Function funObj)
        throws IllegalAccessException, InstantiationException,
               InvocationTargetException
    {
        Class clazz;

        if (args.length == 0) {
            throw Context.reportRuntimeError(ToolErrorReporter.getMessage(
                "msg.expected.string.arg"));
        }

        String className = Context.toString(args[0]);
        try {
            clazz = Class.forName(className);
        }
        catch (ClassNotFoundException cnfe) {
            throw Context.reportRuntimeError(ToolErrorReporter.getMessage(
                "msg.class.not.found",
                className));
        }

        ScriptableObject.defineClass(thisObj, clazz);
    }

    /**
     * Load and execute a script compiled to a class file.
     *
     * This method is defined as a JavaScript function.
     *
     * When called as a JavaScript function, a single argument is
     * expected. This argument should be the name of a class that
     * implements the Script interface, as will any script
     * compiled by jsc.
     *
     * @exception IllegalAccessException if access is not available
     *            to the class
     * @exception InstantiationException if unable to instantiate
     *            the named class
     */
    public static void loadClass(Context cx, Scriptable thisObj,
                                 Object[] args, Function funObj)
        throws IllegalAccessException, InstantiationException
    {
        Class<?> clazz = getClass(args);
        if (!Script.class.isAssignableFrom(clazz)) {
            String className = Context.toString(args[0]);
            throw Context.reportRuntimeError(ToolErrorReporter.getMessage(
                "msg.must.implement.Script", className));
        }
        Script script = (Script) clazz.newInstance();
        script.exec(cx, thisObj);
    }

    private static Class<?> getClass(Object[] args) {
        if (args.length == 0) {
            throw Context.reportRuntimeError(ToolErrorReporter.getMessage(
                "msg.expected.string.arg"));
        }
        Object arg0 = args[0];
        if (arg0 instanceof Wrapper) {
            Object wrapped = ((Wrapper)arg0).unwrap();
            if (wrapped instanceof Class)
                return (Class<?>)wrapped;
        }
        String className = Context.toString(args[0]);
        try {
            return Class.forName(className);
        }
        catch (ClassNotFoundException cnfe) {
            throw Context.reportRuntimeError(ToolErrorReporter.getMessage(
                "msg.class.not.found", className));
        }
    }

    /*
     *  ======== quit ========
     */
     public static final void quit(int exitCode) {
        throw new ShellExitException(exitCode);
     }

    /*
     *  ======== main ========
     *  Rhino Shell compatible shell (except for interactive mode)
     */
    public static int main(String args[])
    {
        String msg = "TIME=" + fmt.format(new java.util.Date());

        Shell shell = new Shell();

        shell.setInteractive(false);

        int status = shell.exec(args);
        if (shell.trace) {
            System.out.println(msg + " Shell.main() starts");
        }

        shell.exit();
        if (shell.trace) {
            msg = "TIME=" + fmt.format(new java.util.Date());
            System.out.println(msg + " Shell.main() exits");
        }
        return (status);
    }

    static private final java.text.SimpleDateFormat fmt =
        new java.text.SimpleDateFormat("hh:mm:ss.SSS a");

    private int optimizationLevel = -1;
    private int profileThreshold = 0;
    private ShellOutputStream stderr;
    private ShellOutputStream stdout;
    private ShellErrorReporter errorReporter;
    private Context cx;
    private int lineno = 1;
    private Object value;
    private boolean fatalError = false;
    private boolean verbose = false;
    private boolean trace = false;
    private Vector fileList = new Vector(5);
}

/*
 *  ======== ShellContext ========
 */
class ShellContext extends Context {

    /*
     *  ======== observeInstructionCount ========
     * You can throw an instance of Error subclass and put try/catch for it
     * around code that executes JavaScript. With EvaluatorException each
     * finally block in a script will still be executed but with Error
     * instances the script never get the control back.
     */
    protected void observeInstructionCount(int instructionCount)
    {
        Context cx = ShellContext.getCurrentContext();
        ShellDebugContext dc = ShellDebugContext.get(cx);

        if (dc != null) {
            int count;
            if ((count = dc.getFrameCount()) > 0) {
                ShellDebugFrame frame = dc.getFrame(0);
                String src = Shell.getRelativePath(frame.getSourceName(), ".");
                String key = src + ":" + frame.getLineNumber();
                Integer hits = (Integer)dc.profile.get(key);
                dc.profile.put(key, hits == null
                    ? new Integer(1) : new Integer(hits.intValue() + 1));
            }
            dc.instructionCount += instructionCount;
        }
    }
}

/*
 *  ======== ShellDebugger ========
 *  This class exists to force Rhino to keep track of the call stack; if
 *  a debugger is bound to the current DebuggableEngine, Rhino maintains
 *  the call stack in the DebuggableEngine.
 */
class ShellDebugger implements Debugger {

    /*
     *  ======== Debugger.handleCompilationDone ========
     */
    public void handleCompilationDone(Context cx, DebuggableScript fnOrScript,
                               String source)
    {
    }

    /*
     *  ======== Debugger.getFrame ========
     */
    public DebugFrame getFrame(Context cx, DebuggableScript fnOrScript)
    {
//      System.out.println("newFrame()");
        //System.err.println("Shell$ new ShellDebugFrame for " +
        //    fnOrScript.getSourceName());
        return (new ShellDebugFrame(cx, fnOrScript));
    }
}

