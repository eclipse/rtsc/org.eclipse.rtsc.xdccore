/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== XsShell.java ========
 */
package config;

import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.Scriptable;

public class XsShell extends Shell
{
    /*
     *  ======== initXsObjects ========
     */
    public void initXsObjects()
    {
        String importPath = xdcPath + java.io.File.pathSeparator + xdcRoot;     
           
        System.setProperty("config.rootDir", xdcRoot);
        System.setProperty("config.importPath", importPath);

        processLine("environment['XDCPATH'] = \'" + xdcPath + "\';");
        processLine("environment['config.importPath'] = \'" + importPath + "\';");
        
        put("arguments", this, arguments);
        processFile(xdcRoot + "/tconfini.tcf");
        processFile(xdcRoot + "/packages/xdc/xs.js");
    }

    /*
     *  ======== loadModule ========
     *  Execute the named XDC module, found along the package path.
     */
    public Scriptable loadModule(String moduleName)
    {
        processLine("var $$mymod = xdc.module('" + moduleName + "');");
        Scriptable mod = (Scriptable) get("$$mymod", this);
        return mod;
    }
    
    /*
     *  ======== getArguments ========
     */
    public NativeArray getArguments()
    {
        return arguments;
    }

    /*
     *  ======== setArguments ========
     */
    public void setArguments(NativeArray arguments)
    {
        this.arguments = arguments;
    }

    /*
     *  ======== getScriptFilename ========
     */
    public String getScriptFilename()
    {
        return scriptFilename;
    }

    /*
     *  ======== setScriptFilename ========
     */
    public void setScriptFilename(String scriptFilename)
    {
        this.scriptFilename = scriptFilename;
    }

    /*
     *  ======== getXdcPath ========
     */
    public String getXdcPath()
    {
        return xdcPath;
    }

    /*
     *  ======== setXdcPath ========
     */
    public void setXdcPath(String xdcPath)
    {
        this.xdcPath = xdcPath;
    }
    
    /*
     *  ======== getXdcRoot ========
     */
    public String getXdcRoot()
    {
        return xdcRoot;
    }

    /*
     *  ======== setXdcRoot ========
     */
    public void setXdcRoot(String xdcRoot)
    {
        this.xdcRoot = xdcRoot;
    }

    private NativeArray arguments = new NativeArray(0);
    private String scriptFilename = "";
    private String xdcPath = System.getProperty("xdc.path");
    private String xdcRoot = System.getProperty("xdc.root");
    private static final long serialVersionUID = 5841577280374670415L;
}
