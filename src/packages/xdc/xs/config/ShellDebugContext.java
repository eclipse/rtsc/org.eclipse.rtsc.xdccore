/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== ShellDebugContext.java ========
 *
 *! Revision History
 *! ================
 *! 23-May-2012	sasha	created by moving it out of Shell.java
 */

package config;

import java.io.IOException;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.util.Enumeration;
import java.util.Vector;
import java.util.Hashtable;
import org.mozilla.javascript.Context;
/*
 *  ======== ShellDebugContext ========
 *  This class implements the debugger's context data; e.g., the call
 *  stack frames.  An instance of this class is created along with a
 *  ShellDebugger instance for each Shell created.  These objects are
 *  "registered" with Rhino via Context.setDebugger() and can be retrieved
 *  via Context.get*() methods.
 *
 *  The call stack frames implement DebugFrame and are created by
 *  ShellDebugger above.
 *
 *  The exceptionStack frames are saved to allow one to get complete
 *  traceback for uncaught exceptions.  DebugFrames are poped as an
 *  exception travels up the stack.  So, by the time we look for the stack
 *  traceback (in Shell) the only frames left on the stack are those that
 *  did *not* see an exception.
 */
class ShellDebugContext {

    /*
     *  ======== get ========
     */
    static ShellDebugContext get(Context cx)
    {
        Object cd = cx.getDebuggerContextData();
        if (cd instanceof ShellDebugContext) {
            return ((ShellDebugContext)cd);
        }
        return (null);
    }

    /*
     *  ======== getFrameCount ========
     */
    int getFrameCount()
    {
        return (frameStack.size());
    }

    /*
     *  ======== getFrame ========
     */
    ShellDebugFrame getFrame(int frameNumber)
    {
//      System.out.println("getFrame(" + frameNumber + ")");

        return (
            (ShellDebugFrame)frameStack.get(frameStack.size()-frameNumber-1)
        );
    }

    /*
     *  ======== pushFrame ========
     */
    void pushFrame(ShellDebugFrame frame)
    {
        frameStack.add(frame);
        //System.err.println("Shell$ pushFrame(), count = " + getFrameCount() +
        //    " file: " + frame.getSourceName());
    }

    /*
     *  ======== popFrame ========
     */
    void popFrame()
    {
//      System.out.println("popFrame(), count = " + getFrameCount());

        frameStack.remove(frameStack.size() - 1);
        //System.err.println("Shell$ popFrame(), count = " + getFrameCount());
    }

    void popFrame(int count)
    {
//      System.out.println("popFrame(), count = " + getFrameCount());

        for (int end = frameStack.size() - 1; count > 0; count--) {
            frameStack.remove(end--);
        }
        //System.err.println("Shell$ popFrames(), count = " + getFrameCount());
    }

    /*
     *  ======== dumpProfile ========
     */
    void dumpProfile()
    {
        dumpProfile("");
    }

    void dumpProfile(String prefix)
    {
        dumpProfile(prefix, new OutputStreamWriter(System.out));
    }

    void dumpProfile(String prefix, Writer out)
    {
        try {
            out.write(prefix + "profile dump (" + instructionCount
                + " monitored instructions):\n");
            for (Enumeration keys = profile.keys(); keys.hasMoreElements();) {
                String key = (String)keys.nextElement();
                Integer hits = (Integer)profile.get(key);
                out.write(hits + ":" + key + "\n");
            }
            out.flush();
            instructionCount = 0;
            profile = new Hashtable();
        }
        catch (IOException e) {
            ;
        }
    }

    private Vector frameStack = new Vector();
    Hashtable profile = new Hashtable();
    int instructionCount = 0;
    int exceptionCount = 0;
}

