package config;

import java.util.Stack;
import java.util.HashMap;

/**
 * This class is used to measure the time spent in different segments of code.
 * It can be invoked from Java as well as JavaScript code. The function start()
 * expects a label as a first parameter. The start() calls are kept on a stack,
 * while a call to stop() pops an entry off the stack. The prupose of labels is
 * to allow recursive calls into a section of code, in which case the times
 * spend under the same label on the top of the stack are added together.
 *
 * If a label is provided to stop(), the call to stop is conditional and
 * executed only if the same label is on the top of the stack.
 *
 * The results are printed when getReport() is called.
 */


public class Stats {

    /* This structure corresponds to one instance of a start()-stop() pair. If
     * during the execution, the same start()-stop() pair is entered multiple
     * times, one such structure is created for each entry and their execution
     * times are added.
     */
    static class StackEntry {
        protected String name;
        protected long offset; // time spent in embedded start-stop pairs
        protected long startTime;
        protected float startMem;
        
        StackEntry(String name) {
            this.name = name;
            this.offset = 0;
        }
    }

    enum State {DISABLED, ENABLED, TERMINATED};

    private static State state = State.DISABLED;

    /* stack contains embedded Stats calls. The offset field for a stack entry
     * captures the time spent in the calls whose stack entries were above that
     * stack entry. The offset time is then subtracted from the difference
     * between the stop and the start time for that entry.
     */
    private static Stack<StackEntry> stack = new Stack<StackEntry>();

    /* A map of code units for which we measure the time spent in them. A code
     * unit may have several stack entries even at the same time. The times for
     * all entries are accumulated.
     */
    private static HashMap<String, Stats> statsMap =
        new HashMap<String, Stats>();

    /* 'allTime' measures the cumulative time spent in all statsMap entries.
     * For validation purposes, that time is not calculated by simply adding
     * accumulators for all entries at the end, but rather by calculating that
     * time along the way. At the end, this value should be close or equal to 
     * the sum of all entries.
     */
    static private long allTime = 0;
    
    static private long firstStart = 0;
    static final float MEGA = 1048576f; // it's float to force float results
    
    /* the actual content of a Stats instance */

    private long accumulator;
    /* difference between memory used for the current objects at the start and
     * at the end of the code marked by this instance, in MB.
     */
    private float memAcc;
    private int invocations;

    Stats() {
        accumulator = 0;
        memAcc = 0;
        invocations = 0;
    }

    public static void start(String name) {
        Runtime r = Runtime.getRuntime();
        Stats currInst;
        StackEntry se = new StackEntry(name);
        stack.push(se);
        if ((currInst = statsMap.get(name)) == null) {
            currInst = new Stats();
            statsMap.put(name, currInst);
        }

        /* If time tracking is disabled, we still push entries on the stack to
         * preserve normal function of stack in cases where time tracking gets
         * enabled between start() and stop(). The main issue here is that we
         * don't want stop() to try to pop from the empty stack.
         */        
        if (state == State.ENABLED) {
            currInst.invocations++;
            se.startTime = System.currentTimeMillis();
            if (Stats.firstStart == 0) {
                Stats.firstStart = se.startTime;
            }
        }
        else {
            se.startTime = 0;
        }

        /* Always keep track of memory, even if the time tracking is disabled.
         */
        se.startMem = (r.totalMemory() - r.freeMemory())/MEGA;
        //System.out.println(se.startTime);
    }

    public static void stop(String name) {
        if (!stack.empty()) {
            if (stack.peek().name.equals(name)) {
                stop();
            }
        }
    }
    
    public static void stop() {
        if (state == State.TERMINATED) return;
        Runtime r = Runtime.getRuntime();
        long stopTime = System.currentTimeMillis();
        float stopMem = (r.totalMemory() - r.freeMemory())/MEGA;
        //System.out.println(stopTime);
        StackEntry se = stack.pop();
        Stats currInst = statsMap.get(se.name);

        if (state == State.ENABLED) {
            /* This new offset is only stop/start difference because that's
             * time during which the current top stack entry (after pop) was not
             * executing.
             */ 
            long newOffset = stopTime - se.startTime;
            currInst.accumulator += newOffset - se.offset;
            currInst.memAcc += stopMem - se.startMem;
            
            /* Now we have to update the offset in the current top entry on
             * the stack.
             */
            if (!stack.empty()) {
                stack.peek().offset += newOffset;
            }
            else {
                allTime += stopTime - Stats.firstStart;
                Stats.firstStart = 0;
            }
        }
    }
    
    public static long getTime(String name) {
        return (statsMap.get(name).accumulator);
    }
    
    public static void enable() {
        if (state == State.ENABLED) return;
        state = State.ENABLED;
        /* Go through existing stack entries and mark them as if they started
         * right now.
         */
        long startTime = System.currentTimeMillis();
        java.util.Iterator i = stack.iterator();
        while (i.hasNext()) {
            StackEntry se = (StackEntry)(i.next());
            se.startTime = startTime;
            if (Stats.firstStart == 0) {
                Stats.firstStart = se.startTime;
            }
            statsMap.get(se.name).invocations++;
        }
    }

    /* When the Stats functionality is disabled, we go and call stop on
     * everything on stack to stop every measurement right now.
     */
    public static void disable() {
        while (!stack.empty()) {
            stop();
        }
        state = State.TERMINATED;
    }

    /* At the beginning of getReport we disable() because there is no point in
     * reporting while some calls are still on the stack. They may consume a
     * lot of time already that the report would miss.
     */
    public static void getReport() {
        disable();
        System.out.println("=== cumulative ===");
        System.out.println("time: " + allTime/1000f);

        for (String name: statsMap.keySet()) {
            System.out.println("=== " + name + " ===");
            System.out.println("time: " + statsMap.get(name).accumulator/1000f
                + " s");
            System.out.println("mem:  "
                + statsMap.get(name).memAcc/statsMap.get(name).invocations
                + " MB");
            System.out.println("calls: " + statsMap.get(name).invocations);
        }
    }
}