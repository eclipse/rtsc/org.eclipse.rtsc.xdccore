/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== ShellTest ========
 *
 *  This is a simple soak test of the Shell class.
 *
 *  To run this test:
 *	    set debug to true (see below)
 *	    java -cp "js.jar:config.jar" config.ShellTest
 */
package config;

import java.io.*;

public class ShellTest {
    public ShellTest()
    {
	System.out.println("hello from ShellTest(): foo = " + foo);
	foo = 1;
    }
    
    static public void main(String args[])
    {
	Shell shell;
	
	if (args.length == 0) {
	    args = new String[3];
	    args[0] = "-f";
	    args[1] = "tconfini.tcf";
	    args[2] = "-i";
	}
	
	for (int i = 0; i < 1000 || true; i++) {
	    shell = new Shell(args);
	    int status = shell.processLine("print('" + i + ": hello')");
	    if (status != Shell.EXITCODE_OK) {
		System.out.println("return status = " + status);
		System.out.println("error msg = '" +
		    shell.getOutput(Shell.STDERR) + "'");
	    }
	    shell.exit();
	}

	/* try to force the finalize method to run */
	shell = null;
	System.gc();		    /* first try garbage collection */
	System.runFinalization();   /* now explicitly request finalization */
    }

    private int foo = 0;
}
