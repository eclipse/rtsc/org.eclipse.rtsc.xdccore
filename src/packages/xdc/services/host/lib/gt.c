/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 * ======== gt.c ========
 *
 *! Revision History
 *! ================
 *! 17-Jan-2006 sasha	updated per new RTSC types and headers
 *! 16-May-1997 dr      Changed GT_Config member names to conform to coding
 *!                     standards.
 *! 23-Apr-1997 ge      Check for GT->TIDFXN for NULL before calling it.
 *! 03-Jan-1997 ge      Changed GT_Config structure member names to eliminate
 *!                     preprocessor confusion with other macros.
 */

#include <xdc/std.h>

#include <stdarg.h>

#ifdef _LINT_
#undef _LINT_
#endif
#include <gt.h>

#define GT_WILD '*'

#define GT_CLEAR        '='
#define GT_ON           '+'
#define GT_OFF          '-'

typedef enum {
    GT_SEP,
    GT_FIRST,
    GT_SECOND,
    GT_OP,
    GT_DIGITS
} GT_State;

String GT_1format = "%s - %d: ";
String GT_2format = "%s - %d(%d): ";

UChar * GT_tMask[GT_BOUND];

static Bool curInit = FALSE;
static String separator;
static UChar tabMem[GT_BOUND][sizeof (UChar) * GT_BOUND];

static Void error( String string );
static Void localPrint(String format, ...);
static Void setMask(Short index1, Short index2, Char op, UChar mask);

/*
 *  ======== _GT_create ========
 *  mask is an output parameter.
 */
Void _GT_create( GT_Mask *mask, String modName )
{
    mask->modName = modName;
    mask->flags = &(GT_tMask[modName[0] - 'A'][modName[1] - 'A']);
}

/*
 *  ======== GT_init ========
 */
#ifdef GT_init
#undef GT_init
#endif
Void GT_init(Void)
{
    register UChar index1;
    register UChar index2;

    if (!curInit) {
        curInit = TRUE;

        separator = " ,;/";

        for (index1 = 0; index1 < GT_BOUND; index1++) {
            GT_tMask[index1] = tabMem[index1];
            for (index2 = 0; index2 < GT_BOUND; index2++) {
                GT_tMask[index1][index2] = 0x00;        /* no tracing */
            }
        }
    }
}


/*
 *  ======== _GT_set ========
 */

Void _GT_set( String str )
{
    GT_State            state;
    String              sep;
    Short               index1 = GT_BOUND;      /* indicates all values */
    Short               index2 = GT_BOUND;      /* indicates all values */
    Char                op = GT_CLEAR;
    Bool                maskValid;
    Short               digit;
    register UChar      mask = 0x0;             /* no tracing */

    if (str == NULL) {
        return;
    }

    maskValid = FALSE;
    state = GT_SEP;
    while (*str != '\0') {
        switch ((Int)state) {
            case (Int)GT_SEP:
                maskValid = FALSE;
                sep = separator;
                while (*sep != '\0') {
                    if (*str == *sep) {
                        str++;
                        break;
                    }
                    else {
                        sep++;
                    }
                }
                if (*sep == '\0') {
                    state = GT_FIRST;
                }
                break;
            case (Int)GT_FIRST:
                if (*str == GT_WILD) {
                    index1 = GT_BOUND;  /* indicates all values */
                    index2 = GT_BOUND;  /* indicates all values */
                    state = GT_OP;
                }
                else {
                    if (*str >= 'a') {
                        index1 = (Short)(*str - 'a');
                    }
                    else {
                        index1 = (Short)(*str - 'A');
                    }
                    if ((index1 >= 0) && (index1 < GT_BOUND)) {
                        state = GT_SECOND;
                    }
                    else {
                        state = GT_SEP;
                    }
                }
                str++;
                break;
            case (Int)GT_SECOND:
                if (*str == GT_WILD) {
                    index2 = GT_BOUND;  /* indicates all values */
                    state = GT_OP;
                    str++;
                }
                else {
                    if (*str >= 'a') {
                        index2 = (Short)(*str - 'a');
                    }
                    else {
                        index2 = (Short)(*str - 'A');
                    }
                    if ((index2 >= 0) && (index2 < GT_BOUND)) {
                        state = GT_OP;
                        str++;
                    }
                    else {
                        state = GT_SEP;
                    }
                }
                break;
            case (Int)GT_OP:
                op = *str;
                mask = 0x0;     /* no tracing */
                switch (op) {
                    case (Int)GT_CLEAR:
                        maskValid = TRUE;
                    case (Int)GT_ON:
                    case (Int)GT_OFF:
                        state = GT_DIGITS;
                        str++;
                        break;
                    default :
                        state = GT_SEP;
                        break;
                }
                break;
            case (Int)GT_DIGITS:
                digit = (Short)(*str - '0');
                if ((digit >= 0) && (digit <= 7)) {
                    mask        |= (0x01 << digit);
                    maskValid = TRUE;
                    str++;
                }
                else {
                    if (maskValid == TRUE) {
                        setMask(index1, index2, op, mask);
                        maskValid = FALSE;
                    }
                    state = GT_SEP;
                }
                break;
            default :
                error("illegal trace mask");
                break;
        }
    }

    if (maskValid) {
        setMask(index1, index2, op, mask);
    }
}


/*
 *  ======== _GT_trace ========
 */

Int _GT_trace( GT_Mask *mask, String format, ...)
{
    va_list va;
    va_start(va, format);

    if (GT->PIDFXN == NULL) {
        localPrint(GT_1format, mask->modName,
            GT->TIDFXN ? (*GT->TIDFXN)() : 0);
    }
    else {
        localPrint(GT_2format,
            mask->modName, (*GT->PIDFXN)(), GT->TIDFXN ? (*GT->TIDFXN)() : 0);
    }

    (*GT->VPRINTFXN)(format, va);
    va_end(va);

    return (0);
}


/*
 *  ======== error ========
 */
static Void error( String string )
{
    localPrint("GT: %s", string);
}


/*
 *  ======== localPrint ========
 */
static Void localPrint(String format, ...)
{
    va_list va;
    va_start(va, format);
    (*GT->VPRINTFXN)(format, va);
    va_end(va);
}


/*
 *  ======== setmask ========
 */

static Void setMask( Short index1, Short index2, Char op, UChar mask )
{
    register Short      index;

    if (index1 < GT_BOUND) {
        if (index2 < GT_BOUND) {
            switch (op) {
                case (Int)GT_CLEAR:
                    GT_tMask[index1][index2] = mask;
                    break;
                case (Int)GT_ON:
                    GT_tMask[index1][index2] |= mask;
                    break;
                case (Int)GT_OFF:
                    GT_tMask[index1][index2] &= ~mask;
                    break;
                default :
                    error("illegal trace mask");
                    break;
            }
        }
        else {
            for (index2--; index2 >= 0; index2--) {
                switch (op) {
                    case (Int)GT_CLEAR:
                        GT_tMask[index1][index2] = mask;
                        break;
                    case (Int)GT_ON:
                        GT_tMask[index1][index2] |= mask;
                        break;
                    case (Int)GT_OFF:
                        GT_tMask[index1][index2] &= ~mask;
                        break;
                    default :
                        error("illegal trace mask");
                        break;
                }
            }
        }
    }
    else {
        for (index1--; index1 >= 0; index1--) {
            if (index2 < GT_BOUND) {
                switch (op) {
                    case (Int)GT_CLEAR:
                        GT_tMask[index1][index2] = mask;
                        break;
                    case (Int)GT_ON:
                        GT_tMask[index1][index2] |= mask;
                        break;
                    case (Int)GT_OFF:
                        GT_tMask[index1][index2] &= ~mask;
                        break;
                    default :
                        error("illegal trace mask");
                        break;
                }
            }
            else {
                index = GT_BOUND;
                for (index--; index >= 0; index--) {
                    switch (op) {
                        case (Int)GT_CLEAR:
                            GT_tMask[index1][index] = mask;
                            break;
                        case (Int)GT_ON:
                            GT_tMask[index1][index] |= mask;
                            break;
                        case (Int)GT_OFF:
                            GT_tMask[index1][index] &= ~mask;
                            break;
                        default :
                            error("illegal trace mask");
                            break;
                    }
                }
            }
        }
    }
}
