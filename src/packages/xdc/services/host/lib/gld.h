/*
 *  ======== gld.h ========
 */
#ifndef GLD_
#define GLD_

/*
 *  ======== GLD_create ========
 *  Load specified shared object and return its handle.  The search path
 *  used to locate the object 'name' can be affected by GLD_setpath().
 *  If GLD_setpath() is not used or GLD_setpath(NULL) is called, the search
 *  path used is host OS dependent; i.e., normal host dependent search is
 *  the only mechanism employed.
 */
extern Ptr GLD_create(String name);

/*
 *  ======== GLD_delete ========
 *  Unload specified shared object.
 */
extern Void GLD_delete(Ptr lib);

/*
 *  ======== GLD_exit ========
 *  Exit this module
 */
extern Void GLD_exit(Void);

/*
 *  ======== GLD_getFxn ========
 *  Get function named "name" from the shared object specified by lib
 */
extern Fxn GLD_getFxn(Ptr lib, String name);

/*
 *  ======== GLD_init ========
 *  Initialize this module
 */
extern Void GLD_init(Void);

/*
 *  ======== GLD_setpath ========
 *  Set search path used to locate shared objects.  If path is NULL
 *  GLD will not search for objects during GLD_create(); default host
 *  OS search path will, however, be applied.  If path is not NULL,
 *  GLD_create() will try to locate the shared object along the specified
 *  path.
 *
 *  Path strings may name multiple directories by separating each with ';'
 *  (SUN hosts can use either ':' or ';').
 *
 *  The path string argument is *not* copied by GLD; therfore this string
 *  must persist in the client during any GLD operations.  If path is an
 *  automatic variable, be sure to call GLD_setpath(NULL) prior to exiting
 *  the automatic's context.
 */
extern Void GLD_setpath(String path);

#endif
