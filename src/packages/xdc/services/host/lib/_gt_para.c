/*
 *  ======== _gt_para.c ========
 *  Default configuration parameters for GT.  This file is separated from
 *  gt.c so that GT_assert() can reference the error function without forcing
 *  the linker to include all the code for GT_set(), GT_init(), etc. into
 *  a fully bound image.  Thus, GT_assert() can be retained in a program for
 *  which GT_?trace() has been compiled out.
 *
 *! Revision History
 *! ================
 *! 17-Jan-2006 sasa	updated per new RTSC types and headers
 */

#define HAVESTDIO   1

#if HAVESTDIO
#  if defined(_VXWORKS_)
#    include <stdioLib.h>
#  else
#    include <stdio.h>
#  endif
#endif

#if HAVESTDIO
#  include <xdc/std.h>	/* This has to be after <stdioLib.h for VxWorks */
#  include "gt.h"

static Int error( String msg, ... );

#  include <stdlib.h>
#  include <stdarg.h>

#else
#  define	vprintf	nop
#  define	error	nop
#endif

static Int nop(Void);

GT_Config _GT_params = {
    (Fxn)vprintf,			/* vprintf */
    (Fxn)NULL,				/* procid */
    (Fxn)nop,				/* taskid */
    (Fxn)error,				/* error */
};

GT_Config *GT = &_GT_params;

/*
 *  ======== nop ========
 */
static Int nop(Void)
{
    return(0);
}

#if HAVESTDIO
/*
 * ======== error ========
 */
static Int error( String msg, ... )
{
    va_list vargs;

    va_start(vargs, msg);
    
    vfprintf(stderr, msg, vargs);

    va_end(vargs);

    fputc('\n', stderr);

    return 0;
}
#endif
