/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== lst.h ========
 */

#ifndef LST_
#define LST_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct LST_TSeq *LST_Seq;

/*
 *  ======== LST_append ========
 *  Append item pointed to by val to list
 *
 *  For example, to append a String to list:
 *
 *    String tmp = "this is an example";
 *    LST_append(list, &tmp);
 *
 *  Returns FALSE in the event that there is not enough memory
 *  to add to the list;
 */
extern Bool LST_append(LST_Seq seq, Ptr val);

/*
 *  ======== LST_create ========
 *  Create a list of items of size valSize.
 *
 *  For example, to create a list of Strings:
 *      LST_Seq list = LST_create(sizeof(String));
 *
 *  Returns NULL in the event that there is not enough memory
 *  to create the list;
 */
extern LST_Seq LST_create(UShort valSize);

/*
 *  ======== LST_cursor ========
 */
extern Ptr LST_cursor(LST_Seq seq, Ptr newcurs);

/*
 *  ======== LST_delete ========
 */
extern Void LST_delete(LST_Seq seq);

/*
 *  ======== LST_init ========
 */
extern Void LST_init(Void);

/*
 *  ======== LST_length ========
 *  Return the current length of the list
 */
extern UShort LST_length(LST_Seq seq);

/*
 *  ======== LST_nconc ========
 */
extern Void LST_nconc(LST_Seq seq1, LST_Seq seq2);

/*
 *  ======== LST_next ========
 */
extern Bool LST_next(LST_Seq seq);

/*
 *  ======== LST_scan ========
 *  Interate over a list
 *
 *  For example, to loop over a list of Strings:
 *
 *    String name;
 *    for (LST_scan(list, &name); LST_next(list);) {
 *       printf("%s\n", name);
 *   }
 */
extern Void LST_scan(LST_Seq seq, Ptr valBuf);

/*
 *  ======== LST_valbuf ========
 */
extern Ptr LST_valbuf(LST_Seq seq, Ptr valBuf);

#ifdef __cplusplus
}
#endif

#endif
