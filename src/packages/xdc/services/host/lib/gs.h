/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== gs.h ========
 *  Memory allocation/release wrappers.  This module allows clients to
 *  avoid OS spacific issues related to memory allocation.  It also provides
 *  simple diagnostic capabilities to assist in the detection of memory
 *  leaks.
 *
 *! Revision History
 *! ================
 *! 17-Jan-2006 sasa	updated per new RTSC types and headers
 */

#ifndef GS_
#define GS_

/*
 *  ======== GS_alloc ========
 *  Alloc size bytes of space.  Returns pointer to space
 *  allocated, otherwise NULL.
 */
extern Ptr GS_alloc(UInt size);

/*
 *  ======== GS_calloc ========
 *  Alloc size bytes of space and initialize the space to 0.
 *  Returns pointer to space allocated, otherwise NULL.
 */
#define GS_calloc(s)	GS_alloc(s)

/*
 *  ======== GS_clearerr ========
 *  Clear error latches in GS module
 */
extern Void GS_clearerr(Void);

/*
 *  ======== GS_err ========
 *  Return TRUE if an error occured; i.e., is any allocation failed
 *  or GS_frees() mismatch occured.
 */
extern Bool GS_err(Void);

/*
 *  ======== GS_exit ========
 *  Module exit.  Do not change to "#define GS_init()"; in
 *  some environments this operation must actually do some work!
 */
extern Void GS_exit(Void);

/*
 *  ======== GS_free ========
 *  Free space allocated by GS_alloc() or GS_calloc().
 */
extern Void GS_free(Ptr ptr);

/*
 *  ======== GS_frees ========
 *  Free space allocated by GS_alloc() or GS_calloc() and assert that
 *  the size of the allocation is size bytes.
 */
extern Void GS_frees(Ptr ptr, UInt size);

/*
 *  ======== GS_init ========
 *  Module initialization.  Do not change to "#define GS_init()"; in
 *  some environments this operation must actually do some work!
 */
extern Void GS_init(Void);

/*
 *  ======== GS_mark ========
 */
extern Ptr GS_mark(Void);

/*
 *  ======== GS_realloc ========
 *  Resize a currently allocated buffer and preserve the
 *  current buffers contents.  Returns NULL of reallocation is not
 *  possible, otherwise a pointer to the new buffer is returned.
 */
extern Ptr GS_realloc(Ptr ptr, UInt size);

/*
 *  ======== GS_release ========
 */
extern Void GS_release(Ptr mark);

/*
 *  ======== GS_size ========
 *  Return the total size (in bytes) of all outstanding
 *  allocations.
 */
extern UInt GS_size(Void);

#endif /*GS_*/
