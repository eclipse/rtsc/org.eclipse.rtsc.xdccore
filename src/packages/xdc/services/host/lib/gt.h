/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2018 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== gt.h ========
 *  There are two definitions that affect which portions of trace are acutally
 *  compiled into the client: GT_TRACE and GT_ASSERT.  If GT_TRACE is set to
 *  0 then all trace statements (except for assertions) will be compiled out
 *  of the client, if GT_ASSERT is set to 0 then assertions will be compiled
 *  out of the client.  GT_ASSERT can not be set to 0 unless GT_TRACE is also
 *  set to 0; i.e., GT_TRACE == 1 implies GT_ASSERT == 1.
 *
 *  Revision History
 *  ================
 *! 17-Jan-2006 sasa	updated per new RTSC types and headers
 *  03-Jan-1997	ge	Replaced "GT_" prefix to GT_Config structure members
 *			to eliminate preprocessor confusion with other macros.
 */
#ifndef GT_
#define GT_

#ifndef GT_TRACE
/* 0 implies trace is compiled out, 1 -> in */
#define GT_TRACE 0
#endif

#if !defined(GT_ASSERT) || GT_TRACE
#define GT_ASSERT 1
#endif

typedef struct {
    Fxn		VPRINTFXN;
    Fxn		PIDFXN;
    Fxn		TIDFXN;
    Fxn		ERRORFXN;
} GT_Config;

extern GT_Config *GT;

typedef struct {
    String	modName;
    UChar	*flags;
} GT_Mask;

#define GT_ENTER	((UChar)0x01)
#define GT_1CLASS	((UChar)0x02)
#define GT_2CLASS	((UChar)0x04)
#define GT_3CLASS	((UChar)0x08)
#define GT_4CLASS	((UChar)0x10)
#define GT_5CLASS	((UChar)0x20)
#define GT_6CLASS	((UChar)0x40)
#define GT_7CLASS	((UChar)0x80)


#ifdef _LINT_

/* LINTLIBRARY */

/*
 *  ======== GT_assert ========
 */
/* ARGSUSED */
Void GT_assert( GT_Mask mask, Int expr )
{
}

/*
 *  ======== GT_config ========
 */
/* ARGSUSED */
Void GT_config(GT_Config config)
{
}

/*
 *  ======== GT_create ========
 */
/* ARGSUSED */
Void GT_create( GT_Mask *mask /* OUT */, String modName )
{
}

/*
 *  ======== GT_curLine ========
 *
 *  Returns the current source code line number.  Is useful for performing
 *  branch testing using trace.  For example,
 *
 *      GT_1trace(curTrace, GT_1CLASS, 
 *          "in module XX_mod, executing line %u\n", GT_curLine());
 */
/* ARGSUSED */
UShort GT_curLine (Void)
{
    return ((UShort)NULL);
}


/*
 *  ======== GT_exit ========
 */
/* ARGSUSED */
Void GT_exit(Void)
{
}

/*
 *  ======== GT_init ========
 */
/* ARGSUSED */
Void GT_init(Void)
{
}

/*
 *  ======== GT_query ========
 */
/* ARGSUSED */
Bool GT_query( GT_Mask mask, UChar class )
{
    return (FALSE);
}

/*
 *  ======== GT_set ========
 *  sets trace mask according to settings
 */

/* ARGSUSED */
Void GT_set( String settings )
{
}

/*
 *  ======== GT_setprintf ========
 *  sets printf function
 */

/* ARGSUSED */
Void GT_setprintf( Fxn fxn )
{
}

/* ARGSUSED */
Void GT_0trace( GT_Mask mask, UChar class, String format )
{
}

/* ARGSUSED */
Void GT_1trace( GT_Mask mask, UChar class, String format, ... )
{
}

/* ARGSUSED */
Void GT_2trace( GT_Mask mask, UChar class, String format, ... )
{
}

/* ARGSUSED */
Void GT_3trace( GT_Mask mask, UChar class, String format, ... )
{
}

/* ARGSUSED */
Void GT_4trace( GT_Mask mask, UChar class, String format, ... )
{
}

/* ARGSUSED */
Void GT_5trace( GT_Mask mask, UChar class, String format, ... )
{
}

/* ARGSUSED */
Void GT_6trace( GT_Mask mask, UChar class, String format, ... )
{
}

#else

#define	GT_BOUND	26	/* 26 letters in alphabet */

extern Void _GT_create(GT_Mask *mask, String modName);
#define GT_exit()
extern Void GT_init(Void);
extern Void _GT_set(String str);
extern Int _GT_trace(GT_Mask *mask, String format, ...);

#if GT_ASSERT == 0
#define GT_assert( mask, expr )
#define GT_config( config )
#define GT_configInit( config )
#define GT_seterror( fxn )

#else

extern GT_Config _GT_params;
#define GT_assert( mask, expr ) \
	(!(expr) ? \
	    (*GT->ERRORFXN)("assertion violation: %s, line %d\n", \
			    __FILE__, __LINE__), NULL \
	    : NULL)
#define GT_config( config )		(_GT_params = *(config))
#define GT_configInit( config )		(*(config) = _GT_params)
#define GT_seterror( fxn )		(_GT_params.ERRORFXN = (Fxn)(fxn))

#endif


#if GT_TRACE == 0
#define GT_curLine()			((UShort)__LINE__)
#define GT_create(mask, modName)
#define GT_exit()
#define GT_init()
#define GT_set( settings )
#define GT_setprintf( fxn )

#define GT_query( mask, class )		FALSE

#define GT_0trace( mask, class, format ) 
#define GT_1trace( mask, class, format, arg1 ) 
#define GT_2trace( mask, class, format, arg1, arg2 ) 
#define GT_3trace( mask, class, format, arg1, arg2, arg3 ) 
#define GT_4trace( mask, class, format, arg1, arg2, arg3, arg4 ) 
#define GT_5trace( mask, class, format, arg1, arg2, arg3, arg4, arg5 ) 
#define GT_6trace( mask, class, format, arg1, arg2, arg3, arg4, arg5, arg6 ) 

#else	/* GT_TRACE == 1 */

extern String GT_format;
extern UChar * GT_tMask[GT_BOUND];

#define GT_create(mask, modName)	_GT_create((mask), (modName))
#define GT_curLine()			((UShort)__LINE__)
#define GT_set( settings )		_GT_set( settings )
#define GT_setprintf( fxn )		(_GT_params.VPRINTFXN = (Fxn)(fxn))

#define GT_query( mask, class ) ((*(mask).flags & (class)))

#define GT_0trace( mask, class, format ) \
    ((*(mask).flags & (class)) ? \
    _GT_trace(&(mask), (format)) : 0)

#define GT_1trace( mask, class, format, arg1 ) \
    ((*(mask).flags & (class)) ? \
    _GT_trace(&(mask), (format), (arg1)) : 0)

#define GT_2trace( mask, class, format, arg1, arg2 ) \
    ((*(mask).flags & (class)) ? \
    _GT_trace(&(mask), (format), (arg1), (arg2)) : 0)

#define GT_3trace( mask, class, format, arg1, arg2, arg3 ) \
    ((*(mask).flags & (class)) ? \
    _GT_trace(&(mask), (format), (arg1), (arg2), (arg3)) : 0)

#define GT_4trace( mask, class, format, arg1, arg2, arg3, arg4 ) \
    ((*(mask).flags & (class)) ? \
    _GT_trace(&(mask), (format), (arg1), (arg2), (arg3), (arg4)) : 0)

#define GT_5trace( mask, class, format, arg1, arg2, arg3, arg4, arg5 ) \
    ((*(mask).flags & (class)) ? \
    _GT_trace(&(mask), (format), (arg1), (arg2), (arg3), (arg4), (arg5)) : 0)

#define GT_6trace( mask, class, format, arg1, arg2, arg3, arg4, arg5, arg6 ) \
    ((*(mask).flags & (class)) ? \
    _GT_trace(&(mask), (format), (arg1), (arg2), (arg3), (arg4), (arg5), \
	(arg6)) : 0)

#endif /* GT_TRACE */

#endif /* _LINT_ */

#endif /* GT_ */
