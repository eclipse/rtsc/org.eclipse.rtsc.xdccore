/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== gs.c ========
 *
 *! Revision History
 *! ================
 *! 31-Oct-2012 sasha	removed Solaris support
 *! 17-Jan-2006 sasha	updated per new RTSC types and headers
 */

#if defined(_VXWORKS_)
#  include <stdioLib.h>
#else
#  include <stdio.h>
#endif

#include <xdc/std.h>
#include <stdlib.h>
#include <string.h>

#include "gs.h"

typedef struct Hdr Hdr;
struct Hdr {
    UInt	    size;
    Hdr	    *next;
    Hdr	    *prev;
};

static Hdr root = {0, &root, &root};
static Bool full = FALSE;
static Bool leak = FALSE;

/*
 *  ======== GS_alloc ========
 */

Ptr GS_alloc( UInt size )
{
    Hdr *hdr;

    /* allocate user's buffer *plus* a header */
    if ((hdr = (Hdr *)malloc(sizeof (Hdr) + size)) == NULL) {
	full = TRUE;
	return (NULL);
    }
    
    /* zero out user's buffer so he/she/it can avoid some initializers */
    memset((Ptr)hdr, '\0', sizeof(Hdr) + size);

    /* update total memory allocation count */
    root.size += hdr->size = size;

    /* insert hdr at front of circular list (root is both beginning and end) */
    hdr->next = root.next;
    hdr->prev = &root;
    root.next = root.next->prev = hdr;

    /* return pointer to user's buffer (after header) */
    return ((Ptr)(hdr + 1));
}


/*
 * ======== GS_clearerr ========
 */

Void GS_clearerr(Void)
{
    full = FALSE;
    leak = FALSE;
}

/*
 * ======== GS_err ========
 */

Bool GS_err(Void)
{
    return (full | leak);
}


/*
 *  ======== GS_exit ========
 */
Void GS_exit(Void)
{
}

/*
 *  ======== GS_free ========
 */

Void GS_free( Ptr ptr )
{
    Hdr *hdr;
    
    hdr = ((Hdr *)ptr) - 1;
    
    root.size -= hdr->size;
    
    hdr->prev->next = hdr->next;
    hdr->next->prev = hdr->prev;

    free((Ptr)hdr);
}


/*
 *  ======== GS_frees ========
 */

Void GS_frees( Ptr ptr, UInt size )
{
    Hdr *hdr;

    hdr = ((Hdr *)ptr) - 1;

    if (size != hdr->size) {
	leak = TRUE;
    }

    GS_free(ptr);
}

/*
 *  ======== GS_init ========
 */
Void GS_init(Void)
{
}

/*
 * ======== GS_mark ========
 *  Return a marker that can be used to free all memory allocated after
 *  this call (via GS_release()).
 */

Ptr GS_mark(Void)
{
    return ((Ptr)root.next);
}


/*
 *  ======== GS_realloc ========
 */

Ptr GS_realloc( Ptr ptr, UInt size )
{
    Hdr *hdr;

    hdr = ((Hdr *)ptr) - 1;

    if (hdr->size >= size) {
	return (NULL);
    }

    if ((hdr = (Hdr *)realloc((Ptr)hdr, sizeof (Hdr) + size)) == NULL) {
	full = TRUE;
	return (NULL);
    }

    root.size += size - hdr->size;
    hdr->size = size;
    hdr->next->prev = hdr;
    hdr->prev->next = hdr;

    return ((Ptr)(hdr + 1));
}

/*
 * ======== GS_release ========
 */

Void GS_release( Ptr mark )
{
    Hdr *hdr;
    Ptr p;

    for (hdr = root.next; hdr != &root; ) {

	if (hdr == (Hdr *)mark) {
	    break;
	}
	
	root.size -= hdr->size;
	p = (Ptr)hdr;
	hdr = hdr->next;
	free(p);
    }

    hdr->prev = &root;
    root.next = hdr;
}

/*
 *  ======== GS_size ========
 */

UInt GS_size( Void )
{
    return (root.size);
}
