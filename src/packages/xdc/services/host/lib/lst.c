/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== lst.c ========
 */

#include <xdc/std.h>

#include <xdc/services/host/lib/gs.h>

#undef _LINT_
#include <lst.h>

typedef struct Elem Elem;
struct Elem {		    /* the elem that can be in a list */
    Elem	*next;
    UChar	data[1];
};

struct LST_TSeq {
    Elem	*head;	    /* pointer to first elem in the list */
    Elem	*tail;	    /* pointer to last elem in the list */
    Elem	*cursor;    /* pointer to an elem in the list (may be NULL) */
    Ptr		valBuf;	    /* pointer to where data is copied in scan/next  */
    UShort	valSize;    /* size of data in elems */
    UShort	length;	    /* number of elems in the list */
};

static Bool curInit = FALSE;


/*
 *  ======== LST_append ========
 */

Bool LST_append(LST_Seq seq, Ptr val)
{
    Elem *elem;
    UShort i;
    Char *p;
    Char *pv = (Char *)val;

    elem = (Elem *)GS_alloc(sizeof (Elem) - 1 + seq->valSize);
    if (elem != NULL) {
        /* copy value into elem->data */
        p = (Char *)elem->data;
        for (i = 0; i < seq->valSize; i++) {
            *p++ = *pv++;
        }
    
        if (seq->length == 0) {
            seq->head = elem;
        }
        else {
            seq->tail->next = elem;
        }
    
        seq->tail = elem;
        elem->next = NULL;
        
        /* if cursor is NULL, then a scan is at the endo of the list  */
        if (seq->cursor == NULL) {
            seq->cursor = elem; /* we just extended the list, so point next to
                                 * new elem
                                 */
        }
    
        seq->length++;
        return (TRUE);
    }

    return (FALSE);
}


/*
 *  ======== LST_create ========
 */

LST_Seq LST_create(UShort valSize)
{
    LST_Seq seq;

    seq = (LST_Seq)GS_alloc(sizeof (struct LST_TSeq));
    if (seq != NULL) {
        seq->head = seq->tail = seq->cursor = NULL;
        seq->valSize = valSize;
        seq->length = 0;
    }

    return (seq);
}


/*
 *  ======== LST_cursor ========
 *  Set the cursor and return previous cursor 
 */

Ptr LST_cursor(LST_Seq seq, Ptr newcurs)
{
    Ptr		oldcurs;

    oldcurs = (Ptr)seq->cursor;
    seq->cursor = (Elem *)newcurs;

    return (oldcurs);
}


/*
 *  ======== LST_delete ========
 */
Void LST_delete(LST_Seq seq)
{
    Elem *elem, *next;
    
    for (elem = seq->head; elem != NULL; elem = next) {
	next = elem->next;
	GS_free(elem);
    }
    GS_free(seq);
}


/*
 *  ======== LST_init ========
 */

Void LST_init(void)
{
    if (!curInit) {
	curInit = TRUE;
	GS_init();
    }
}


/*
 *  ======== LST_length ========
 */

UShort LST_length(LST_Seq seq)
{
    return (seq->length);
}


/*
 *  ======== LST_nconc ========
 */

Void LST_nconc(LST_Seq seq1, LST_Seq seq2)
{
    seq1->cursor = NULL;

    if (seq2->length == 0) {
	return;
    }
    else if (seq1->length == 0) {
	seq1->head = seq2->head;
    }
    else {
	seq1->tail->next = seq2->head;
    }

    seq1->tail = seq2->tail;
    seq1->length += seq2->length;

    return;
}


/*
 *  ======== LST_next ========
 */

Bool LST_next(LST_Seq seq)
{
    UShort i;
    Char *p;
    Char *v;

    if (seq->cursor == NULL) {
	return (FALSE);
    }

    p = (Char *)(seq->cursor->data);
    v = (Char *)(seq->valBuf);

    for (i = 0; i < seq->valSize; i++) {
	*v++ = *p++;
    }

    seq->cursor = seq->cursor->next;
    
    return (TRUE);
}


/*
 *  ======== LST_scan ========
 */

Void LST_scan(LST_Seq seq, Ptr valBuf)
{
    seq->cursor = seq->head;
    seq->valBuf = valBuf;
}

/*
 *  ======== LST_valbuf ========
 *  set the valBuf and return the previous valBuf 
 */
Ptr LST_valbuf(LST_Seq seq, Ptr valBuf)
{
   Ptr oldValBuf;

   oldValBuf = seq->valBuf;
   seq->valBuf = valBuf;

   return (oldValBuf);
}
