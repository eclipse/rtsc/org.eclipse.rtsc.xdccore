/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== xutl.h ========
 */
#ifndef XUTL_
#define XUTL_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>

/*
 *  ======== XUTL_PkgDesc ========
 */
typedef struct XUTL_PkgDesc {
    String name;        /* the package name */
    String date;        /* release date */
    String label;       /* release label */
    String compatKey;   /* package compatibility key */
    String buildCount;  /* build count of package */
    String releaseName; /* release name */
} XUTL_PkgDesc;

/*
 *  ======== XUTL_FSFlags ========
 *  Flags to control what's visited by XUTL_FileScanFxn
 */
typedef enum {
    XUTL_FSNORM  = 0x0, /* visit only normal files or empty dirs */
    XUTL_FSALL   = 0x1, /* visit all files (even hidden files) */
    XUTL_FSNEDIR = 0x2  /* visit non-empty dirs with no visited files */
} XUTL_FSFlags;

/*
 *  ======== XUTL_FileScanFxn ========
 *  Function applied to ordinary files during file system scan via XUTL_scanFS
 *
 *  Parameters:
 *      arg    - uninterpreted value from XUTL_scanFS arg
 *      cookie - directory specific state argument; this single argument may
 *               be modified by FileScanFxn, is initialized to 0 when dir is
 *               first encountered, and persists until all files in dir are
 *               scanned.
 *      base -   base name of file; i.e., the name of the file in the
 *               directory dir
 *      dir  -   directory path containing base.  This directory path starts
 *               with  the root originally passed to XUTL_scanFS and is,
 *               therefore, an absolute or a relative (to the current working
 *               directory) path.
 *
 *  Returns: 0 if successful non-zero on any failure.  The return value is or'd
 *  with other return values during a scan.  A non-zero return will cause the
 *  scan of the current directory and any sub-directories to terminate.
 */
typedef Int (*XUTL_FileScanFxn)(IArg arg, IArg *cookie, String base, String dir);

/*
 *  ======== XUTL_FileFilterFxn ========
 *  Function applied to determine if the FileScanFxn should be called
 *
 *  Parameters:
 *      arg -    uninterpreted value from XUTL_scanFS arg
 *      base -   base name of file; i.e., the name of the file in the
 *               directory dir
 *      dir  -   directory path containing base.  This directory path starts
 *               with the root originally passed to XUTL_scanFS and is,
 *               therefore, an absolute or a relative (to the current working
 *               directory) path.
 *
 *  Returns:  TRUE iff the file (or directory and it contents) should be
 *            "scanned".
 */
typedef Bool (*XUTL_FileFilterFxn)(IArg arg, String base, String dir, Char *end);

/*
 *  ======== XUTL_cp ========
 *  Copy file from src to dst
 */
extern Bool XUTL_cp(String src, String dst);

/*
 *  ======== XUTL_expandArgv ========
 *  Expand an argv array that may contain @file options
 *
 *  Returns new argv array, sets outArgc to the new argv array length.
 *
 *  Retuns NULL in the event of an error.  If the return value is NULL, err
 *  is set to a printable string that indicates the cause of the error.
 */
extern String *XUTL_expandArgv(Int argc, String *argv,
    Int *outArgc, String *err);

/*
 *  ======== XUTL_insertArgv ========
 *  Insert arguments into an argv array
 *
 *  Returns new argv array, sets outArgc to the new argv array length.
 *
 *  Retuns NULL in the event of an error.  If the return value is NULL, err
 *  is set to a printable string that indicates the cause of the error.
 */
extern String *XUTL_insertArgv(Int insertArgc, String *insertArgv, Int insertBefore,
    Int argc, String *argv, Int *outArgc, String *err);

/*
 *  ======== XUTL_expandPath ========
 *  Return the string str with all '^' characters replaced with the
 *  absolute (or relative) path of the specified package's repository; if
 *  the absolute parameter is TRUE, the path is absolute otherwise it
 *  is relative.
 *
 *  If there is no current package, the ^ and all characters up to the
 *  next ';' are replaced with "".
 *
 *  The string returned may be freed via free().
 *
 *  If the current working directory can not be determined or insufficient
 *  memory exists in the application to allocate space for the new string,
 *  XUTL_expandPath may return NULL.
 *
 *  Params:
 *	str	    the string to expand
 *	pdir	    the base directory of the package (the directory 
 *		    containing package.xdc)
 *	absolute    flag to select absolute path expansion (or not)
 */
extern String XUTL_expandPath(String str, String pdir, Bool absolute);

/*
 *  ======== XUTL_findFile ========
 *  Find file along specified path.  Returned string must be freed (via
 *  free()) by caller.
 */
extern String XUTL_findFile(String file, String path, String pathSep);

/*
 *  ======== XUTL_findPackage ========
 *  Find package base directory from specified package name and package
 *  path.
 *
 *  The string returned may be freed via free().
 */
extern String XUTL_findPackage(String pname, String ppath);

/*
 *  ======== XUTL_findPackages ========
 *  Return array of all package base directories starting from (and including)
 *  startDir.
 *
 *  Every string in the returned array begins with startDir followed by
 *  '/'.  Thus, relative startDir's result in relative paths to packages
 *  and an absolute startDir results in absolute paths to packages.
 *
 *  Returns NULL on error or when no packages are found.
 *
 *  len is negative on error; otherwise len is number of packages found.
 *
 *  The strings returned must be freed via free() before freeing the table
 *  itself (also via free()).
 */
extern String *XUTL_findPackages(String startDir, Bool buildable, Int *len);

/*
 *  ======== XUTL_freePackageDesc ========
 *  Free the strings allocated in a previously obtained package descriptor
 *
 *  All descriptor strings are reset to "".
 */
Void XUTL_freePackageDesc(XUTL_PkgDesc *desc);

/*
 *  ======== XUTL_getCanonicalPath ========
 *  Returns the canonocal path name corresponding to the path argument.
 *
 *  The string returned may be freed via free().
 */
extern String XUTL_getCanonicalPath(String path);

/*
 *  ======== XUTL_getLastErrorString ========
 *  Return meaningful error string for the last error that occured.
 *
 *  The string returned MUST NOT be freed via free().
 */
extern String XUTL_getLastErrorString(Void);

/*
 *  ======== XUTL_getPackageDesc ========
 *  Get package description
 *
 *  Returns TRUE iff descriptor has been initialized with package information
 */
extern Bool XUTL_getPackageDesc(String dname, XUTL_PkgDesc *desc);

/*
 *  ======== XUTL_getPackageName ========
 *  Return the name of a package (or it's repository expressed as ../^n,
 *  where n is the number of components in the package's name) specified by 
 *  the directory containing the package's specification file (package.xdc).
 *
 *  Params:
 *	dname	directory containing package.xdc
 *	rflag	if TRUE, return the package's repository path (relative to
 *		dname); otherwise return the "dot separated" package name.
 *
 *  Returned string must be freed (via free()) by caller.
 */
extern String XUTL_getPackageName(String dname, Bool rflag);

/*
 *  ======== XUTL_getPackageRep ========
 *  Return canonical name of a package's repository
 *
 *  The package is specified by the directory containing the package's
 *  specification file (package.xdc).
 *
 *  Params:
 *	dname	directory containing package.xdc; must be a non-empty
 *              string
 *
 *  Returns:
 *      absolute canonical path to package's repository.  In the event of
 *      an error it returns NULL
 *
 *  Returned string must be freed (via free()) by caller.
 */
extern String XUTL_getPackageRep(String dname);

/*
 *  ======== XUTL_getPackageReqs ========
 *  Return an array strings (package names) that the specified package
 *  requires.  The package is specified using the name of the directory
 *  containing the package's specification file (package.xdc).
 *
 *  Params:
 *	dname	directory containing package.xdc
 *	len	output number of strings in the returned array
 *
 *  Returns NULL in the event of an error; otherwise it returns an array
 *  of strings naming required packages (possibly of length 0).
 *
 *  Note that the caller is responsible for freeing the storage associated
 *  with the returned array.  If the array length is zero, no storage needs
 *  to be freed.
 */
extern String *XUTL_getPackageReqs(String dname, Int *len);

/*
 *  ======== XUTL_getProgPath ========
 *  Return the absolute path to the current executable; progName must be set
 *  to the value of argv[0] passed to main().
 *
 *  Returned string must be freed (via free()) by caller.
 */
extern String XUTL_getProgPath(String progName);

/*
 *  ======== XUTL_init ========
 *  Initialize this module.
 */
extern Void XUTL_init(Void);

/*
 *  ======== XUTL_isPackageBase ========
 *  Is the specified directory a package.
 *
 *  If buildFlag is FALSE, return TRUE iff pbase is a package base directory.
 *
 *  If buildFlag is TRUE, return TRUE iff pbase is a package base directory 
 *  and it is a buildable package.
 */
extern Bool XUTL_isPackageBase(String pbase, Bool buildFlag);

/*
 *  ======== XUTL_isDir ========
 *  Return TRUE iff name exists and is a directory
 */
extern Bool XUTL_isDir(String name);

/*
 *  ======== XUTL_isFile ========
 *  Return TRUE iff name exists and is not a directory
 */
extern Bool XUTL_isFile(String name);

/*
 *  ======== XUTL_mkdir ========
 *  Create the specified directory if it does not already exist
 *
 *  The specified directory may contain one or more sub-directores that
 *  do not exist.  All intermediate directores will be created as
 *  necessary.
 *  
 *  If the specified directory is created or already exists, XUTL_mkdir
 *  will return TRUE; otherwise it will return FALSE.
 */
extern Bool XUTL_mkdir(String name);
    
/*
 *  ======== XUTL_reducePath ========
 *  Remove redundent '/' and '\' characters and embedded "/../" sequences
 *  from path. If trim is not TRUE, don't remove leading "./".
 *
 *  Always returns path.
 */
extern String XUTL_reducePath(String path, Bool trim);

/*
 *  ======== XUTL_rm ========
 *  Forcefully (and quietly) remove fileName
 *
 *  This function will remove both file and directories.  Directories can
 *  only be removed if they are empty.
 */
extern Bool XUTL_rm(String fileName);

/*
 *  ======== XUTL_run ========
 *  Run the specified external command
 *
 *  The command specified by cmdname is searched for along the PATH
 *  environment variable.
 *
 *  This function only returns to the caller if it is not possible to run
 *  the command; otherwise the current process is replaced with the specified
 *  command and the exit status is that of the command.
 */
extern Int XUTL_run(const char *cmdname, String argv[], String envp[]);

/*
 *  ======== XUTL_scanFS ========
 *  Recursively scan a file system and apply a function to each ordinary file
 *
 *  Parameters:
 *      root        starting directory path
 *      scanFxn     function to run on each ordinary file
 *      filterFxn   function called to determine if scanFxn should be called 
 *                  on the specified file or it's decendents (if its a
 *                  directory)
 *      arg         uninterpreted first argument passed to each of scanFxn and
 *                  filterFxn
 *
 *      aflag       if TRUE visit all files (including hidden files and
 *                  files/directories that begin with '.'); otherwie,
 *                  skip any file/directory that begins with '.' or is
 *                  marked "hidden" by the underlying filesystem.
 *
 *  Returns: 0 on success and non-zero for any failure.
 */
extern Int XUTL_scanFS(String root, XUTL_FileScanFxn scanFxn,
    XUTL_FileFilterFxn filterFxn, IArg arg, XUTL_FSFlags flags);

/*
 *  ======== XUTL_tmpFile ========
 *  Create a temporary output file stream.
 *
 *  This function is necessary because Windows creates temp files at the
 *  root of a drive; if this drive is a network mounted filesystem, the
 *  caller may not have access rights to create a file here!!!!
 */
extern FILE *XUTL_tmpFile(Void);

/*
 *  ======== XUTL_setDate ========
 *  Update the file times of the dst file to the times of the ref file
 *
 *  If ref is NULL, the modification time of dst is updated to the current
 *  local time.
 *
 *  If ref is non-NULL, cmp is used to select one of three options:
 *      o if cmp == 0, the date of dst is set equal to ref dates 
 *      o if cmp >  0, the date of dst is set to the max of dst and ref
 *      o if cmp <  0, the date of dst is set to the min of dst and ref
 *
 *  If dst does not exist, it will be created (as a zero-length ordinary
 *  file).
 *  
 *  Returns TRUE if the times of dst were successfully set; otherwise,
 *  it returns FALSE.
 */
extern Bool XUTL_setDate(String dst, String ref, Int cmp);

/*
 *  ======== XUTL_getVersion ========
 *  Return a version string that uniquely identifies the version of the
 *  package containing this (the XUTL) module.
 */
extern String XUTL_getVersion(Void);

/*
 *  ======== XUTL_getXDCRoot ========
 *  Return the installation directory of the XDCtools
 *
 *  Executables that use this function can be run from one of two places:
 *      1. XDCROOT
 *      2. XDCROOT/<some_repository>/<some_package>
 *
 *  To determine XDCROOT we first locate the directory containing
 *  progName.  If this directory is the package providing this
 *  progName (e.g., xdc.services.host.bin), XDCROOT is the directory
 *  containing this package's repository; otherwise, XDCROOT is the
 *  directory containing this executable.
 *
 *  Returns NULL is root could not be determined.
 *
 *  Warning: progName should be argv[0] on Unix platforms and is ignored on
 *  Windows platforms.  This function determines the location of the
 *  executable that calls this function.  You can't simply pass arbitrary
 *  progName vaues and expect this function to determine the XDCROOT for
 *  the specified progName.
 */
extern String XUTL_getXDCRoot(String progName);

#ifdef __cplusplus
}
#endif

#endif	/* XUTL_ */
