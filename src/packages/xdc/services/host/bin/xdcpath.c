/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== xdcpath.c ========
 *  Print the full package path or path of named file found along the
 *  package path.
 *
 *	usage: xdcpath [-b package-base]
 *
 *  Options:
 *	-b dir	    base directory of a package; used to form complete
 *		    package path.
 *
 *  Exit status:
 *	0 if successful; otherwise non-zero
 *
 *  Error messages are output to stderr.
 *
 */

#include <xdc/std.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xdc/services/host/lib/xutl.h>

#if defined(xdc_target__os_Windows)
#include <io.h>	    /* access() */
#endif

#define MAXPATHLEN  1024

static Char *pathBuf = NULL;
static SizeT pathBufLen = 0;
static SizeT pathLen = 0;

static String progName = "";

static Void append(String new);
static String usage = "usage: %s [-b dir] [file ...]\n";

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    String tmp;
    Char pkgsBuf[MAXPATHLEN + 1] = {'\0'};
    String base = NULL;
    Int status = 0;

    XUTL_init();

    progName = argv[0];

    while (argc > 1 && argv[1][0] == '-') {
	switch (argv[1][1]) {
	    case 'b': {
		if (argc < 3) {
		    fprintf(stderr, usage, progName);
		    exit(1);
		}
		base = argv[2];
		argv++;
		argc--;
		break;
	    }
	    default: {
		fprintf(stderr, usage, progName);
		exit(1);
	    }
	}
	argv++;
	argc--;
    }

    if ((pathBuf = malloc(sizeof (Char) * MAXPATHLEN)) == NULL) {
	fprintf(stderr, "%s: out of memory\n", progName);
	exit(1);
    }
    pathBufLen = MAXPATHLEN;
    pathBuf[0] = '\0';
    pathLen = 0;

    /* start with XDCPATH (if its defined) */
    if ((tmp = getenv("XDCPATH")) != NULL) {
	append(tmp);
    }

    /* add XDCROOT/packages */
    if ((tmp = getenv("XDCROOT")) != NULL) {
	if ((80 + strlen(tmp)) >= MAXPATHLEN) {
	    fprintf(stderr, "%s: error: path too long: %s\n", progName, tmp);
	    exit (1);
	}
	sprintf(pkgsBuf, "%s/packages", tmp);
    }
    else if ((tmp = XUTL_getProgPath(progName)) != NULL) {
	/* XDCROOT = "tmp/.." */
	SizeT len = strlen(tmp);
	if ((80 + len) >= MAXPATHLEN) {
	    fprintf(stderr, "%s: error: path too long: %s\n", progName, tmp);
	    exit (1);
	}
	if (len > 0) {
	    len--;
	    if (len > 0 && (tmp[len] == '/' || tmp[len] == '\\')) {
		len--;
	    }
	    while (len > 0 && tmp[len] != '/' && tmp[len] != '\\') {
		len--;
	    }
	    tmp[len] = '\0';
	}
	sprintf(pkgsBuf, "%s/packages", tmp);
    }
    else {
	fprintf(stderr, "%s: error: can't determine XDCROOT: %s\n",
	    progName, XUTL_getLastErrorString());
	exit (1);
    }
    if (access(pkgsBuf, 0) != 0) {
	fprintf(stderr, "%s: error: can't access $XDCROOT/packages (%s): ",
	    progName, pkgsBuf);
	perror("");
	exit (1);
    }
    append(pkgsBuf);

    /* add repository of any specified base */
    if (base != NULL) {
	if ((tmp = XUTL_getPackageName(base, TRUE)) == NULL) {
	    fprintf(stderr, "%s: error: %s\n",
		progName, XUTL_getLastErrorString());
	    exit(1);
	}
	append(tmp);
    }

    if (argc > 1) {
	Int i;
	for (i = 1; i < argc; i++) {
	    String fname = XUTL_findFile(argv[i], pathBuf, ";");
	    if (fname != NULL) {
		printf("%s\n", fname);
		free(fname);
	    }
	    else {
		status = 1;
	    }
	}
    }
    else {
	printf("%s\n", pathBuf);
    }

    return (status);
}

/*
 *  ======== append ========
 */
static Void append(String new)
{
    SizeT len = strlen(new) + 1;

    /* make sure pathBuf has sufficient space to hold new string + ';' */
    if ((len + pathLen) >= pathBufLen) {
	pathBufLen += len;
	if ((pathBuf = realloc(pathBuf, sizeof (Char) * pathBufLen)) == NULL) {
	    fprintf(stderr, "%s: error: ", progName);
	    perror("");
	    exit (1);
	}
    }

    if (pathBuf[0] != '\0' && pathBuf[strlen(pathBuf) - 1] != ';') {
        strcat(pathBuf, ";");
    }
    strcat(pathBuf, new);
    pathLen = strlen(pathBuf);
}
