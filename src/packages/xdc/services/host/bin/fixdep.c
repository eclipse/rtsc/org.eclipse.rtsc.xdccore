/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== fixdep.c ========
 *  Fix generated dependency files to always use '/' instead of '\' and
 *  add empty rules for prerequisites.
 *
 *  usage: fixdep inc-file [output-file]
 *
 *  where inc-file is simple makefile typically generated from a compiler's
 *  "generate dependencies" option.  If output-file is not specified the
 *  processed output is written to stdout.
 *
 *  Note: output-file may be the same as inc-file.  In this case, the
 *  input file will be overwritten with the appropriate results.
 */
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xdc/services/host/lib/xutl.h>

#define LINEQUANTUM	128

static char *usage = "usage: %s dep-file [output-file]\n";

static char *progName = "";
static char *lineBuf;
static int lineBufLen;

static void processFile(String file, FILE *out);
static void processLine(String line, FILE *out);

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    FILE *tmpOut;
    FILE *out = stdout;

    if (argc != 2 && argc != 3) {
	fprintf(stderr, usage, argv[0]);
	exit(1);
    }

    XUTL_init();

    progName = argv[0];

    if ((lineBuf = (char *)malloc(LINEQUANTUM)) == NULL) {
	perror("out of memory");
	exit(1);
    }
    lineBufLen = LINEQUANTUM;

    if ((tmpOut = XUTL_tmpFile()) == NULL) {
	perror("can't create temporary file");
	exit(1);
    }

    processFile(argv[1], tmpOut);

    if (argc == 3) {
	/* remove destination file */
	XUTL_rm(argv[2]);

	/* open destination for writing */
	if ((out = fopen(argv[2], "w")) == NULL) {
	    fprintf(stderr, "%s: can't open '%s' ", progName, argv[2]);
	    perror("because");
	    exit(1);
	}
    }

    /* write final file with generated output contents */
    rewind(tmpOut);
    while (fgets(lineBuf, lineBufLen, tmpOut) != NULL) {
	fputs(lineBuf, out);
    }
    fclose(out);
    fclose(tmpOut);
    
    return (0);
}

/*
 *  ======== processFile ========
 *  Read one line at a time (combining lines ending with '\') and call
 *  processLine() until EOF.
 */
static void processFile(String file, FILE *out)
{
    char *done = NULL;
    String line;
    SizeT lineLen;
    FILE *in;

    if ((in = fopen(file, "r")) == NULL) {
        return;
    }

    do {
	lineBuf[0] = '\0';
	line = lineBuf;
	lineLen = lineBufLen;
	while ((done = fgets(line, (UInt)lineLen, in)) != NULL) {
	    SizeT len = strlen(lineBuf);
	    if (len < (lineBufLen - 1) || lineBuf[len - 1] == '\n') {
		/* concatenate lines that end with \ */
		if (len >= 2
		    && lineBuf[len - 2] == '\\'
		    && lineBuf[len - 1] == '\n') {
		    line = lineBuf + len - 2;
		}
		else {
		    break;  /* got a full line; process it now */
		}
	    }
	    else {
		lineBufLen += LINEQUANTUM;
		if ((lineBuf = (char *)realloc(lineBuf, lineBufLen)) == NULL) {
		    perror("out of memory");
		    exit(1);
		}
		line = lineBuf + len;
	    }
	    lineLen = lineBufLen - (line - lineBuf);
	}
        if (lineBuf[0] != '\0') {
            processLine(lineBuf, out);
        }
    } while (done != NULL);

    fclose(in);
}

/*
 *  ======== processLine ========
 *  Given a line of a generated makefile, convert it to a form necessary
 *  for RTSC generated makefiles.
 *
 *  Processing:
 *      Convert all '\'s to '/'s to "normalize" the output
 *
 *      Output an empty rule for every prerequisite; this tells gmake 
 *      that if the file does not exist then it should rebuild any goals 
 *      that depend on the file.
 */
static void processLine(char *line, FILE *out)
{
    char *cp;
    char *token;
    char *pre = NULL;
    
    /* convert all '\' to '/' */
    for (cp = line; *cp != '\0'; cp++) {
	/* don't remove "\ " sequences because these exist to allow spaces
	 * in absolute paths
	 */
	if (*cp == '\\' && cp[1] != ' ') {  
	    *cp = '/';
	}
	else if (pre == NULL && *cp == ':') {
	    pre = cp + 1;
	}
    }

    /* output converted line */
    fputs(line, out);
    if (cp[-1] != '\n' && cp[-1] != '\r') {
	fputs("\n", out);   /* add new line if it's missing from the input */
    }
    
    /* output empty rule line for every prerequisite */
    if (pre != NULL) {
	for (token = strtok(pre, " \t\r\n"); token != NULL;) {

	    if (token[strlen(token) - 1] == '\\') {
		/* found '\' in the name, so its a name with embedded space */
		fprintf(out, "%s ", token); /* restore space */
	    }
	    else {
		/* no '\' in the name, so its a complete file name */
		fprintf(out, "%s:\n", token);
	    }

	    /* advance to next token */
	    token = strtok(NULL, " \t\r\n");
	}
    }
}
