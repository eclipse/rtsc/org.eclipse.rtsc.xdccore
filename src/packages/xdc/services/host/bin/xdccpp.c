/* --COPYRIGHT--,EPL
 *Copyright (c) 2012-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== xdccpp.c ========
 *  Copy packages to specified repository
 *
 *  usage: xdccpp [-help] [-n] [base-directory ...] dest-repo
 *
 *  This utility copies all entire packages, specified by their base
 *  directory, to the specified repository.  Unlike a normal "cp -rp":
 *      1. nested packages are not copied (unless explicitly specified), and
 *      2. each package's full package path name is copied; e.g., the
 *         xdc.runtime package would be copied to <dest-repo>/xdc/runtime
 *
 *  These differences make xdccpp useful in shell scripts that need to copy
 *  packages from one location to another.  
 */
#include <xdc/std.h>
#include <xdc/services/host/lib/xutl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXPATHLEN  1024

static Char pathBuf[MAXPATHLEN + 1];
static Char dstBuf[MAXPATHLEN + 1];

static String usage = "%s: [-help] [-n] [package-base-directory ...] dest-repo\n"
    "    -help    display this message\n"
    "    -n       don't actually copy any files; just show the commands\n"
    "             that would be executed.\n";

static String progName = NULL;

static String dstRepo = NULL;   /* path prefix of dst repo */
static String curRepo = NULL;   /* path prefix of src repo */
static String curBase = NULL;   /* path prefix of src package base */
static String curName = NULL;   /* package name with '/'s */
static SizeT curBaseLen = 0;    /* strlen(curBase) */

static Bool nflag = FALSE;

static Bool filter(IArg arg, String baseName, String dirName, Char *end);
static Int scanFile(IArg arg, IArg *cookie, String baseName, String dirName);

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    Int i;
    Int status;

    XUTL_init();

    progName = argv[0];

    for (i = 1; i < argc; i++) {
        if (argv[i][0] == '-') {
            switch (argv[i][1]) {
                case 'n': {
                    nflag = TRUE;
                    break;
                }
                case 'h': {
                    printf(usage, progName);
                    return (0);
                }
                default: {
                    fprintf(stderr, usage, progName);
                    return (1);
                }
            }
        }
        else {
            break;
        }
    }

    if ((argc - i) <= 1) {
        fprintf(stderr, usage, progName);
        return (1);
    }

    dstRepo = argv[--argc];
    if (XUTL_isDir(dstRepo) != TRUE) {
        fprintf(stderr, "%s: %s is not a directory\n",
                progName, dstRepo);
        fprintf(stderr, usage, progName);
        return (1);
    }

    for (status = 0; status == 0 && i < argc; i++) {
        if (XUTL_isPackageBase(argv[i], FALSE)) {

            curRepo = NULL;
            curBase = argv[i];
            curBaseLen = strlen(curBase);

            /* if argv[i] is a package base, find it's files */
            if ((curName = XUTL_getPackageName(curBase, FALSE)) != NULL) {
                Char *cp;
                /* replace '.'s with '/'s in curName */
                for (cp = curName; *cp != '\0'; cp++) {
                    if (*cp == '.') {
                        *cp = '/';
                    }
                }

                /* compute source package repo */
                curRepo = XUTL_getPackageRep(curBase);
                if (curRepo == NULL) {
                    fprintf(stderr, "%s: can't find repository of '%s'\n",
                            progName, curBase);
                    exit(1);
                }

                /* now scan the file system copying pkg files */
                status = XUTL_scanFS(curBase, scanFile, filter, 0, XUTL_FSALL);

                /* free up temporary allocated strings */
                free(curName);
                free(curRepo);
            }
        }
        else {
            fprintf(stderr, "%s: %s is not a package base directory\n",
                progName, argv[i]);
            status = 1;
        }
    }

    return (status);
}

/*
 *  ======== filter ========
 *  skip nested package directories
 *
 *  Return TRUE to skip dirName/baseName (and not copy it), FALSE to
 *  scan it (i.e, copy it).
 */
static Bool filter(IArg arg, String baseName, String dirName, Char *end)
{
    SizeT len = end - dirName;
    SizeT baseLen = strlen(baseName);
    Bool result = TRUE;

    /* append baseName and dirName to determine if it's a package */
    if ((len + 1 + baseLen + 1) < MAXPATHLEN) {
        sprintf(pathBuf, "%s/%s", dirName, baseName);
        result = XUTL_isPackageBase(pathBuf, FALSE);

        /* check curRepo against pathBuf pkg's repository */
        if (result) {
            String rname = XUTL_getPackageRep(pathBuf);
            result = FALSE; /* only skip if curRepo matches pathBuf's repo */
            if (rname != NULL) {
                if (strcmp(rname, curRepo) == 0) {
                    result = TRUE;
                }
                free(rname);
            }
        }
    }
    else {
        fprintf(stderr, "%s: error: path too long (%s/%s)\n",
            progName, dirName, baseName);
    }

#if 0
    printf("%s/%s: %s\n", dirName, baseName,
           result == TRUE ? "TRUE" : "FALSE");
#endif

    return (result);
}

/*
 *  ======== scanFile ========
 *  copy file visited
 */
static Int scanFile(IArg arg, IArg *cookie, String baseName, String dirName)
{
    String pkgDir = dirName + curBaseLen;

    /* check that path lengths are no longer than buffers used to hold them */
    if ((strlen(dstRepo) + strlen(curName)
         + strlen(pkgDir) + strlen(baseName) + 2) >= MAXPATHLEN) {
        fprintf(stderr, "%s: path name too long: copy to %s/%s%s/%s failed",
                progName, dstRepo, curName, pkgDir, baseName);
        return (1);
    }
    if ((strlen(dirName) + strlen(baseName) + 1) >= MAXPATHLEN) {
        fprintf(stderr, "%s: path name too long: copy of %s/%s failed",
                progName, dirName, baseName);
        return (1);
    }

    /* if this is first time visiting this directory, make destination dir */
    if (*cookie == 0) {
        *cookie = 1;
        if (nflag == TRUE) {
            printf("mkdir -p %s/%s%s\n", dstRepo, curName, pkgDir);
        }
        else {
            sprintf(pathBuf, "%s/%s%s",  dstRepo, curName, pkgDir);
            if (!XUTL_mkdir(pathBuf)) {
                fprintf(stderr, "mkdir -p %s failed.\n", pathBuf);
                return (1);
            }
        }
    }

    /* if baseName == "", we're visiting a dir and there's nothing to do */
    if (baseName[0] == '\0') {
        return (0);
    }
    
    /* copy source path into pathBuf */
    sprintf(pathBuf, "%s/%s", dirName, baseName);

    /* copy destination path into dstBuf */
    sprintf(dstBuf, "%s/%s%s/%s", dstRepo, curName, pkgDir, baseName);

    /* do (or display) the file copy operation */
    if (nflag == TRUE) {
        printf("cp -p %s %s\n", pathBuf, dstBuf);
    }
    else if (!XUTL_cp(pathBuf, dstBuf)) {
        fprintf(stderr, "%s: cp -p %s %s failed.\n", progName,
                pathBuf, dstBuf);
        return (1);
        /* TODO: is it better to copy as much as possible returning 0?
         *       we need to ensure that we return non-zero for the command
         *       as a whole if we do this, however.
         */
    }

    return (0);
}

