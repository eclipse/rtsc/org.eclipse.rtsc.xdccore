/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== cmp.c ========
 *  Compare two files
 *
 *  Usage: cmp file1 file2
 *
 *  Options:
 *      -b      binary compare flag; if set, treat '\r' at the end of a line
 *              as a difference, otherwise treat the files as text files and
 *              ignore this difference.
 *      -d      debug flag
 *      -s      silent flag
 *
 *  Examples:
 *    The following command compares tmp.mak to new.mak
 *
 *          cmp tmp.mak new.mak
 */

#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(xdc_target__os_Windows)
#define access _access
#include <io.h>
#else
#include <unistd.h>
#include <sys/types.h>
#endif

#define BUFSIZE     512

static char *usage = "usage: %s [-s] [-b] file1 file2\n";

static int bcompare(char *new, char *old);
static int tcompare(char *new, char *old);
static int trim(char *buf, FILE *fs);

static int dflag = 0;
static int sflag = 0;
static int bflag = 0;

/*
 *  ======== compare ========
 */
#define compare(n, o) (bflag ? bcompare((n), (o)) : tcompare((n), (o)))

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    int status = 2;
    char *progName = argv[0];
    int i;

    for (i = 1; i < argc && (argv[i][0] == '-'); i++) {
        switch (argv[i][1]) {
            case 'd': {
                dflag++;
                break;
            }
            case 's': {
                sflag++;
                break;
            }
            case 'b': {
                bflag++;
                break;
            }
            default: {
                fprintf(stderr, usage, progName);
                exit(2);
                break;
            }
        }
    }
    argv += i;
    argc -= i;

    if (argc != 2) {
        fprintf(stderr, usage, progName);
        exit(2);
    }

    /* if a file does not exist, treat this as a difference */
    if (access(argv[0], 00) != 0) {
        if (!sflag) {
            fprintf(stderr, "%s: %s does not exist\n", progName, argv[0]);
            exit(2);
        }
    }
    else if (access(argv[1], 00) != 0) {
        if (!sflag) {
            fprintf(stderr, "%s: %s does not exist\n", progName, argv[1]);
            exit(2);
        }
    }
    else if ((status = compare(argv[0], argv[1])) == 1) {
        /* both source and destination files exist and differ */
    }
    else if (status == 0) {
        /* source and destination files both exist and are identical */
        if (dflag) {
            printf("files identical '%s' ...\n", argv[1]);
        }
    }
    else {
        if (!sflag) {
            fprintf(stderr, "%s: can't compare '%s' to '%s' ",
                progName, argv[0], argv[1]);
            perror("because");
        }
    }

    /* if the command failed, return its error status */
    return (status);
}

/*
 *  ======== bcompare ========
 */
static int bcompare(char *new, char *old)
{
    FILE *ns = NULL, *os = NULL;
    char nbuf[BUFSIZE];
    char obuf[BUFSIZE];
    SizeT nn, on;
    int status = 0;
    long lineNum = 1;
    long charNum = 1;

    /* how do we decide to use "rb" mode for windows?  "rb" is necessary for
     * binary files but this causes a problem for text files which are the
     * usual case
     */
    if ((ns = fopen(new, "rb")) == NULL  || (os = fopen(old, "rb")) == NULL) {
        status = 2;
        goto exit;
    }

    do {
        SizeT min, i;

        nn = fread(nbuf, 1, sizeof (nbuf), ns);
        on = fread(obuf, 1, sizeof (obuf), os);

        min = nn > on ? on : nn;

        if (min > 0) {
            for (i = 0; i < min; i++) {
                if (nbuf[i] != obuf[i]) {
                    status = 1;
                    goto exit;
                }
                if (nbuf[i] == '\n') {
                    lineNum++;
                }
                charNum++;
            }
        }

        if (nn != on) {
            status = 1;
            goto exit;
        }
    } while (nn > 0 && on > 0);

exit:
    if (ns) {
        fclose(ns);
    }
    if (os) {
        fclose(os);
    }
    if (!sflag && status == 1) {
        printf("%s %s differ: char %ld, line %ld\n",
            new, old, charNum, lineNum);
    }
    return (status);
}

/*
 *  ======== tcompare ========
 */
static int tcompare(char *new, char *old)
{
    FILE *ns = NULL, *os = NULL;
    char nbuf[BUFSIZE];
    char obuf[BUFSIZE];
    char *nl, *ol;
    int status = 0;
    long lineNum = 1;

    /* how do we decide to use "rb" mode for windows?  "rb" is necessary for
     * binary files but this causes a problem for text files which are the
     * usual case
     */
    if ((ns = fopen(new, "r")) == NULL  || (os = fopen(old, "r")) == NULL) {
        status = 2;
        goto exit;
    }

    for (;;) {
        nl = fgets(nbuf, sizeof (nbuf), ns);
        ol = fgets(obuf, sizeof (obuf), os);

        if (nl != NULL && ol != NULL) {
            int inc = trim(ol, os);
            trim(nl, ns);
            if (strcmp(nl, ol) == 0) {
                lineNum += inc;
                continue;
            }
        }
        status = (nl == NULL && ol == NULL) ? 0 : 1;
        break;
    }

exit:
    if (ns) {
        fclose(ns);
    }
    if (os) {
        fclose(os);
    }
    if (!sflag && status == 1) {
        printf("%s %s differ: line %ld\n",
            new, old, lineNum);
    }
    return (status);
}

/*
 *  ======== trim ========
 *  Remove trailing new lines (or DOS '\r's)
 *  return 1 if new line detected; otherwise 0
 */
static int trim(char *buf, FILE *fs)
{
    SizeT index = strlen(buf) - 1;
    if (index >= 0) {
        if (buf[index] == '\n') {
            buf[index] = '\0';
            if (index-- > 0 && buf[index] == '\r') {
                buf[index] = '\0';
            }
            return (1);
        }
        else if (buf[index] == '\r') {
            char c = fgetc(fs);
            if (c == '\n') {
                buf[index] = '\0';
            }
            else {
                ungetc(c, fs);
            }
        }
    }
    return (0);
}
