/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== tee.c ========
 *	Splits standard input stream to standard output stream and
 *	one or more output files.
 *
 */

#include <xdc/std.h>
#include <stdlib.h>
#include <stdio.h>

#include <fcntl.h>

#if defined(xdc_target__os_Windows)
#include <io.h>
#else
#include <unistd.h>
#endif

#define MAXFILE		30		/* Maximum output files		*/

static String progName;
static Bool aflag = FALSE;
static void tee(int numfiles, char *filename[]);
static String usage = "usage:  %s [-a] file... < input\n";

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    progName = argv[0];
    
    /* parse command line args */
    argv += 1;
    argc -= 1;
    while (argc > 0 && argv[0][0] == '-') {
	switch (argv[0][1]) {
	    case 'a': {
		aflag = TRUE;
		argv += 1;
		argc -= 1;
		break;
	    }
        }
    }

    /* Check command line args */
    if (argc < 1 || isatty(fileno(stdin))) {
        fprintf(stderr, usage, progName);
        return (EXIT_FAILURE);
    }

#ifdef O_BINARY
    /* Open stdin in binary mode */
    if (setmode(fileno(stdin), O_BINARY) == -1) {
        fprintf(stderr, "%s: can't set mode on standard input\n",
            progName);
        return (EXIT_FAILURE);
    }

    /* Open stdout in binary mode */
    if (setmode(fileno(stdout), O_BINARY) == -1) {
        fprintf(stderr, "%s: can't set mode on standard output\n",
            progName);
        return (EXIT_FAILURE);
    }
#endif

    /* Split output into one or more file streams */
    tee(argc, argv);

    return (EXIT_SUCCESS);
}


/*
 *  ======== tee ========
 *	Split stdin input stream into stdout stream and one or more
 *	output files.
 */
static void tee(int numfiles, char *filename[])
{
    int		i;		/* File counter			*/
    FILE *	fp[MAXFILE];	/* Output file pointers		*/
    int		c;		/* Input/output char		*/

    /* Open the files */
    for (i = 0; i < numfiles && i < MAXFILE; ++i) {
        fp[i] = fopen(filename[i], aflag ? "ab" : "wb");
        if (fp[i] == NULL) {
            fprintf(stderr, "%s: can't write to %s\n", progName, filename[i]);
        }
    }

    /* Split the input to several output files */
    while ((c = getchar()) != EOF) {
        /* send char to output files */
        for (i = 0;  i < numfiles && i < MAXFILE; ++i) {
            if (fp[i] != NULL) {
                putc(c, fp[i]);
            }
        }

        /* send char to stdout */
        putchar(c);
    }

    /* Close the files */
    for (i = 0;  i < numfiles  &&  i < MAXFILE;  ++i) {
        if (fp[i] == NULL) {
            fclose(fp[i]);
        }
    }
}
