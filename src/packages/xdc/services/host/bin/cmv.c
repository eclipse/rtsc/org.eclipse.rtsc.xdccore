/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== cmv.c ========
 *  Overwrite destination with source only if destination and source differ.
 *
 *  Usage: cmv source destination
 *
 *  Options:
 *      -d	debug flag
 *	-v	verbose flag
 *
 *  Examples:
 *    The following command copies tmp.mak to new.mak only if they differ
 *
 *	    cmv tmp.mak new.mak
 */
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <xdc/services/host/lib/xutl.h>

#if defined(xdc_target__os_Windows)
#include <io.h>
#define access _access
#else
#include <unistd.h>
#include <sys/types.h>
#endif

#define BUFSIZE	    512

static char *usage = "usage: %s source destination\n";

static void cat(char *file);
static int compare(char *new, char *old);

static int dflag = 0;
static int vflag = 0;

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    int status = 0;
    char *progName = argv[0];
    int i;

    XUTL_init();

    for (i = 1; i < argc && (argv[i][0] == '-'); i++) {
	switch (argv[i][1]) {
	    case 'd': {
		dflag++;
		break;
	    }
	    case 'v': {
		vflag++;
		break;
	    }
	    default: {
		fprintf(stderr, usage, progName);
		exit(1);
		break;
	    }
	}
    }
    argv += i;
    argc -= i;

    if (argc != 2) {
        fprintf(stderr, usage, progName);
        exit(1);
    }

    /* if destination file does not exist, treat this as a difference */
    if (access(argv[1], 00) != 0) {
	if (dflag) {
	    printf("creating %s ...\n", argv[1]);
	}

	/* rename source file */
	if (status = rename(argv[0], argv[1])) {
	    fprintf(stderr, "%s: can't rename '%s' (exit status = %d) ",
		progName, argv[0], status);
	    perror("because");
	}
    }
    else if ((status = compare(argv[0], argv[1])) > 0) {
	/* both source and destination files exist and differ */
	if (dflag) {
	    printf("updating %s from:\n", argv[1]);
	    cat(argv[1]);
	}

	/* remove destination file */
	if (XUTL_rm(argv[1]) != TRUE) {
	    fprintf(stderr, "%s: can't delete '%s' (exit status = %d) ",
		progName, argv[1], status);
	    perror("because");
	}
	else {
	    /* rename source file */
	    if (status = rename(argv[0], argv[1])) {
		fprintf(stderr, "%s: can't rename '%s' (exit status = %d) ",
		    progName, argv[0], status);
		perror("because");
	    }
	}
    }
    else if (status == 0) {
	/* source and destination files both exist and are identical */
	if (dflag) {
	    printf("files identical (%s) ...\n", argv[1]);
	}

	/* remove source file */
	if (XUTL_rm(argv[0]) != TRUE) {
	    fprintf(stderr, "%s: can't delete '%s' (exit status = %d) ",
		progName, argv[0], status);
	    perror("because");
	}
    }
    else {
	fprintf(stderr, "%s: can't compare '%s' to '%s' ",
	    progName, argv[0], argv[1]);
	perror("because");
	status = -1;
    }

    /* if the command failed, return its error status */
    return (status);
}

/*
 *  ======== cat ========
 */
static void cat(char *file)
{
    FILE *fs;
    SizeT n;
    char buf[BUFSIZE];

    if ((fs = fopen(file, "rb")) != NULL) {
        while ((n = fread(buf, 1, sizeof (buf), fs)) > 0) {
            fwrite(buf, 1, n, stdout);
        }
        fclose(fs);
    }
}

/*
 *  ======== compare ========
 */
static int compare(char *new, char *old)
{
    FILE *ns = NULL, *os = NULL;
    char nbuf[BUFSIZE];
    char obuf[BUFSIZE];
    SizeT nn, on;
    int status = 0;

    if ((ns = fopen(new, "rb")) == NULL	|| (os = fopen(old, "rb")) == NULL) {
	status = -1;
	goto exit;
    }

    while ((nn = fread(nbuf, 1, sizeof (nbuf), ns)) > 0
	&& (on = fread(obuf, 1, sizeof (obuf), os)) > 0) {
	if (nn != on) {
	    status = 1;
	    goto exit;
	}
	for (--nn; nn >= 0; nn--) {
	    if (nbuf[nn] != obuf[nn]) {
		status = 1;
		goto exit;
	    }
	}
    }

exit:
    if (ns) {
	fclose(ns);
    }
    if (os) {
	fclose(os);
    }
    return (status);
}
