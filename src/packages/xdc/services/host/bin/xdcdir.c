/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== xdcdir.c ========
 *  Print XDC package name (in dot format; e.g., ti.platforms.sim62xx) or
 *  relative repository path (e.g., ../../../) for the specified
 *  directories.
 *
 *      usage: xdcdir [-t[a]] dir ...
 *
 *  where dir is a package's base directory (the directory containing
 *  the package.xdc and package.bld files).
 *
 *  Options:
 *	-t[a]   output the repository name for the specified package.  
 *
 *              If a is not specified, the repository is output in the 
 *              form (../)^N where N is the number of components in the
 *              packages's name; thus, the package's repository is the
 *              concatenation of dir and the output of this command.
 *
 *              If a is specified, the package's repository is output
 *              as a canonical path.
 *
 *  Exit status:
 *	0 if successful; otherwise non-zero
 *
 *  Error messages are output to stderr.
 */
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <xdc/services/host/lib/xutl.h>

static String usage = "%s: [-t] dir ...\n";
static Bool tflag = FALSE;
static Bool aflag = FALSE;

static String progName = "";

static void clean(String path);

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    Int i;
    
    XUTL_init();
    
    progName = argv[0];

    while (argc > 1 && argv[1][0] == '-') {
	switch (argv[1][1]) {
	    case 't': {
                if (argv[1][2] == 'a') {
                    aflag = TRUE;
                }
		tflag = TRUE;
		break;
	    }
	    default: {
		fprintf(stderr, usage, progName);
		exit(1);
	    }
	}
	argv++;
	argc--;
    }
    if (argc <= 1) {
	fprintf(stderr, usage, progName);
	exit(1);
    }
    
    for (i = 1; i < argc; i++) {
	String name;
        if (aflag) {
            name = XUTL_getPackageRep(argv[i]);
            clean(name);
        }
        else {
            name = XUTL_getPackageName(argv[i], tflag);
        }

        if (name != NULL) {
	    printf("%s\n", name);
	}
	else {
	    fprintf(stderr, "%s: error : %s\n",
		progName, XUTL_getLastErrorString());
	    return (1);
	}
    }

    return (0);
}

/*
 *  ======== clean ========
 *  Convert '\'s to '/'s unless we are quoting a space
 *  or we are a UNC path
 */
static void clean(String path)
{
    if (path != NULL) {
        Char *cp = path;
        for (cp = path; cp[0] != '\0'; cp++) {
            if (cp[0] == '\\' && cp[1] != ' '
                && (cp != path || cp[1] != '\\')) {
                cp[0] = '/';
            }
        }
    }
}
