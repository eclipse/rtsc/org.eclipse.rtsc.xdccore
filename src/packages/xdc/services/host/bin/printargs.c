/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== printargs.c ========
 */
#include <xdc/std.h>

#include <stdio.h>

/*
 *  ======== main ========
 */
int main(int argc, char * argv[], char *envp[])
{
    int i;
    
    for (i = 0; i < argc; i++) {
	printf("%s\n", argv[i]);
    }

    while (*envp) {
	printf( "%s\n", *envp++);
    }

    return (0);
}
