/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== xdcpkg.c ========
 *  Recursively descend into a directory tree looking for (buildable) RTSC
 *  packages; i.e., packages that have package.xdc (and package.bld) files.
 *  Each package directory found is output (to stdout) on a separate line.
 *
 *      usage: xdcpkg [-v] [-a] [-d] [-l] [-m[c]] [-p path] [-r] [-s] [dir ...]
 *
 *  Exit status:
 *	0 if successful; otherwise non-zero
 *
 *  Errors are output to stderr.
 *
 *  Options:
 *	-a  find all packages (not just the buildable ones)
 *
 *	-d  treat each directory as a package directory and find the
 *	    package's pre-requisites and output in an order that ensures that
 *	    prerequisites are output before any package that requires
 *	    them.
 *
 *	-l[:fmt]  long output; print package names followed by it's base
 *          directory, release name, release label, version, and release date.
 *          If the optional':' is used, the remainder of the option is taken to
 *          be a "format string" that controls the contents and format of the
 *          output.  The format string is similar to a printf format string:
 *          each character (except %) in the string is written to stdout,
 *          "output specifiers" are introduced by the % character, the special
 *          characters '%', '\', tab, new line, and carrige return are output
 *          by the character pairs '\%', '\\', '\t', '\n', or '\r'
 *          respectivly.  The following "output specifiers" are supported:
 *              %b - base directory of package
 *              %c - release build counter
 *              %d - release date in milliseconds since midnight (GMT) 1/1/1970
 *              %k - package's compatibility key (as specifed by package.xdc)
 *              %l - package's release label
 *              %n - package's name
 *              %r - package's release name
 *              %t - top directory containing package (i.e., the package's
 *                   repository)
 *              %v - a "version" number created by appending the package's
 *                   compatibility key with the build counter
 *              %V - a "version" number created by appending the package's
 *                   compatibility key with the release date in milliseconds
 *
 *      -m[c] output make dependencies in the form a:b where a and b are
 *            package base directories
 *
 *      -p path the path to use as the *complete* package path (not just
 *              $XDCPATH)
 *
 *      -r  treat each directory as a repository and only find packages that
 *          are "rooted" in this repository.  Without this option, each
 *          directory is recursively searched for any package in any
 *          repository at or below the starting directory
 *
 *      -s  shallow package search; don't recurse when looking for packages.
 *          When -d is also specified only display immediate requirements.
 *          This option can't be used with -r.
 *
 *      -v  verbose output
 *
 *  To deal with files that are too big (>2GB):
 *      http://ftp.sas.com/standards/large.file/x_open.20Mar96.html states
 *	that "Applications which define the macro _LARGEFILE_SOURCE to 1
 *	before inclusion of any header will enable at least the functionality
 *      described in 2.0 Changes to the Single UNIX Specification on
 *      implementations that support these features. "
 */
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <xdc/services/host/lib/xutl.h>

#define TABQUANTUM  16

typedef struct NodeRec {
    String  name;
    String  base;
    String *reqs;
    Int     rlen;
    Int	    index;
} NodeRec;
typedef struct NodeRec *Node;

static String usage = "usage: %s [-a] [-d] [-l[:fmt]] [-m[c]] [-p path] [-r] dir ...\n" \
    "   -a       display all packages (not just buildable packages)\n"    \
    "   -d       follow the package requires declarations\n"    \
    "   -l[:fmt] long form of output. If the optional format string fmt is\n"\
    "            specified this controls the information output.  The\n" \
    "            following output specifiers are supported:\n" \
    "               %%b - base directory of package\n" \
    "               %%c - release build counter\n" \
    "               %%d - release date in milliseconds since midnight (GMT)\n"\
    "                    1/1/1970\n" \
    "               %%D - calendar time representation of the release date\n"\
    "               %%k - package's compatibility key (as specifed by the\n" \
    "                    package's package.xdc file)\n" \
    "               %%l - package's release label\n" \
    "               %%n - package's name\n" \
    "               %%r - package's release name\n" \
    "               %%t - top directory containing the package (i.e., the\n" \
    "                    absolute path to the package's repository)\n" \
    "               %%v - a 'version' number created by appending the \n" \
    "                    package's compatibility key with the build\n" \
    "                    counter\n" \
    "               %%V - a 'version' number created by appending the\n" \
    "                    package's compatibility key with the release date\n"\
    "                    in milliseconds\n"\
    "            If fmt is not specified, the following format is used:\n" \
    "               %%n;%%b;%%r;%%l;%%v;%%d\n\n" \
    "   -m[c]    output make dependencies in the form a:b, where\n"    \
    "            a and b are package base directories; if the character c\n" \
    "            is specified, the ':' character is replaced by c.\n" \
    "   -p path  use path as the *entire* package path\n" \
    "   -r       only display packages rooted at specified repositories\n" \
    "   -s       shallow package search; don't recurse when looking for\n" \
    "            packages.  If -d is also specified only display immediate\n" \
    "            requirements. This option can't be used with -r.\n" \
    "   -v       verbose output; display error messages for any failure\n";
    
static Bool aflag = FALSE;
static Bool dflag = FALSE;
static Bool lflag = FALSE;
static Bool mflag = FALSE;
static Char mchar = ':';
static Bool rflag = FALSE;
static Bool sflag = FALSE;
static Bool vflag = FALSE;
static String ppath = NULL;
static String longFormat = NULL;
static String progName = "";
static String xppath = ".";
static Int visitIndex = 0;

static Node *addNode(Node *nt, Int *ntLen, Int *ntMax, Node node);
static Int find(String start);
static Node findNode(String name, Node nodeTab[], Int nodeTabLen);
static String getPackagePath(String pdir, String ppath);
static Void printfPkg(String format, String base, XUTL_PkgDesc *desc);
static Void printPkg(String pdir);
static Void printPkgReqs(String pdir);
static Node mkNode(String pname);
static Void rmNode(Node node);
static Void rmNodeTab(Node *nodeTab, Int nodeTabLen);

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    Int i;
    Int status = 0;

    XUTL_init();

    progName = argv[0];

    while (argc > 1 && argv[1][0] == '-') {
	switch (argv[1][1]) {
	    case 'p': {
		if (argc > 2) {
		    argv++;
		    argc--;
		    ppath = argv[1];
		    break;
		}
	    }
	    case 'd': {
		dflag = TRUE;
		break;
	    }
	    case 'a': {
		aflag = TRUE;
		break;
	    }
	    case 'l': {
		lflag = TRUE;
                if (argv[1][2] == ':') {
                    longFormat = argv[1] + 3;
                }
		break;
	    }
	    case 'm': {
		mflag = TRUE;
		if (argv[1][2] != '\0') {
		    mchar = argv[1][2];
		}
		break;
	    }
            case 'r': {
                rflag = TRUE;
                break;
            }
            case 's': {
                sflag = TRUE;
            }
            case 'v': {
                vflag = TRUE;
                break;
            }
	    default: {
		fprintf(stderr, usage, progName);
		exit(1);
	    }
	}
	argv++;
	argc--;
    }
    if (argc <= 1) {
	fprintf(stderr, usage, progName);
	exit(1);
    }
    if (rflag && sflag) {
        fprintf(stderr, "%s: -r and -s can not both be specified\n", progName);
        exit(1);
    }

    for (i = 1; i < argc; i++) {
	if (dflag) {
	    String name;
	    if ((name = XUTL_getPackageName(argv[i], FALSE)) == NULL) {
		fprintf(stderr, "%s: error : %s\n",
		    progName, XUTL_getLastErrorString());
		exit(1);
	    }
	    if (name[0] == '\0') {  /* unnamed package */
		SizeT j;

                /* re-define name */
                free(name);
		name = (String)strdup(argv[i]);

		/* trim trailing '/'s */
		for (j = strlen(name) - 1; j >= 0; j--) {
		    if (name[j] != '/' && name[j] != '\\') {
			break;
		    }
		    name[j] = '\0';
		}
		/* find next directory separator */
		for (; j >= 0; j--) {
		    if (name[j] == '/' || name[j] == '\\') {
			name = name + j + 1;
			break;
		    }
		}
	    }

	    xppath = getPackagePath(argv[i], ppath);
	    XUTL_reducePath(xppath, FALSE);

	    printPkgReqs(name);

            /* free package path (if it's newly allocated) and package name */
            if (ppath != xppath) {
                free(xppath);
            }
            free(name);
	}
        else if (sflag) {
            if (XUTL_isPackageBase(argv[i], (Bool)(aflag ? FALSE : TRUE))) {
                printPkg(argv[i]);
            }
        }
	else {
	    status |= find(argv[i]);
	}
    }

    if (vflag && status != 0) {
        String err = XUTL_getLastErrorString();
        if (err[0] == '\0') {
            err = "unspecified error";
        }
        fprintf(stderr, "%s: %s\n", progName, err);
    }

    return (status);
}

/*
 *  ======== addNode ========
 *  Add specified node to the nodeTab if it does not already exist in the
 *  table *and* recursively add any requirements that node has.
 */
static Node *addNode(Node *nodeTab, Int *nodeTabLen, Int *nodeTabMax,Node node)
{
    Int i, j;

    /* if node is already in table, return */
    for (j = 0; j < *nodeTabLen; j++) {
	if (strcmp(nodeTab[j]->name, node->name) == 0) {
	    rmNode(node);
	    return (nodeTab);
	}
    }

    /* if node is not buildable and we aren't putting all nodes in, return */
    if (!aflag && !XUTL_isPackageBase(node->base, TRUE)) {
	rmNode(node);
	return (nodeTab);
    }

    /* add node to table (expanding it if necessary) */
    if (*nodeTabLen >= *nodeTabMax) {
	nodeTab = (Node *)realloc(nodeTab,
	    (*nodeTabMax + TABQUANTUM) * sizeof(Node));
	if (nodeTab == NULL) {
            fprintf(stderr, "%s: error : out of memory\n", progName);
            exit(1);
	}
	*nodeTabMax += TABQUANTUM;
    }
    nodeTab[*nodeTabLen] = node;
    *nodeTabLen += 1;
    
    /* for each package referenced by node, add to table */
    if (sflag == FALSE) {
        for (i = 0; i < node->rlen; i++) {
            if (findNode(node->reqs[i], nodeTab, *nodeTabLen) == NULL) {
                Node tmp;
                if ((tmp = mkNode(node->reqs[i])) != NULL) {
                    nodeTab = addNode(nodeTab, nodeTabLen, nodeTabMax, tmp);
                }
            }
        }
    }

    /* increment node's index *after* decendents have been added; this
     * ensures that sorting by index will result in a topological sort
     * of the nodes (relative to the requires relation).
     */
    node->index = visitIndex++;
    
    return (nodeTab);
}

/*
 *  ======== compare ========
 *  The compare function to be used by qsort() on nodeTab
 */
static Int compare(const Void *ptr1, const Void *ptr2)
{
    return ((*(Node *)ptr1)->index - (*(Node *)ptr2)->index);
}

/*
 *  ======== find ========
 */
static Int find(String root)
{
    String *pkgs;
    String croot = NULL;
    Int npkgs;
    
    pkgs = XUTL_findPackages(root, (Bool)(aflag ? FALSE : TRUE), &npkgs);
    if (pkgs != NULL) {
	Int i;
        if (npkgs < 0) {
            npkgs = -1 * npkgs; /* ignore errors and process all packages */
        }
	for (i = 0; i < npkgs; i++) {
            if (rflag == FALSE) {
                printPkg(pkgs[i]);
            }
            else {
                String rname = XUTL_getPackageRep(pkgs[i]);
                if (rname != NULL) {
                    if (croot == NULL) {
                        croot = XUTL_getCanonicalPath(root);
                    }
                    if (croot != NULL) {
                        if (strcmp(rname, croot) == 0) {
                            printPkg(pkgs[i]);
                        }
                    }
                    free(rname);
                }
            }
	    free(pkgs[i]);
	}
	free(pkgs);
        if (croot != NULL) {
            free(croot);
        }
	return (0);
    }

    return (npkgs == 0 ? 0 : 1);
}

/*
 *  ======== findNode ========
 *  Return node named name from nodeTab (if it exists, otherwise NULL)
 */
static Node findNode(String name, Node nodeTab[], Int nodeTabLen)
{
    Int i;
    
    for (i = 0; i < nodeTabLen; i++) {
	if (strcmp(nodeTab[i]->name, name) == 0) {
	    return (nodeTab[i]);
	}
    }

    return (NULL);
}

/*
 *  ======== getPackagePath ========
 */
static String getPackagePath(String pdir, String ppath)
{
    String xpath;
    
    if (ppath == NULL) {
	String tmp, penv;
	static String root = NULL;
	static String pkgs = NULL;
        
	/* get user's XDCPATH setting */
	penv = getenv("XDCPATH");
	if (penv == NULL) {
	    penv = "";
	}

	/* determine XDCROOT/packages directory */
        if (root == NULL) {
            root = getenv("XDCROOT");
            pkgs = "/packages;";
        }
	if (root == NULL) {
	    root = XUTL_getProgPath(progName);
	    pkgs = "/../packages;";
	}
	if (root == NULL) {
	    root = "";
	    pkgs = "";
	}

	/* allocate enough space for "penv;root/packages;^" */
	if ((tmp = (String)malloc(strlen(root) + strlen(penv) + 32)) == NULL) {
	    fprintf(stderr, "%s: error : out of memory\n", progName);
	    exit(1);
	}
	sprintf(tmp, "%s%s%s%s^",
	    penv, penv[0] != '\0' ? ";" : "",
	    root, pkgs);
	xpath = XUTL_expandPath(tmp, pdir, TRUE);

	free(tmp);
    }
    else {
	xpath = XUTL_expandPath(ppath, pdir, TRUE);
    }

    if (xpath == NULL) {
        fprintf(stderr, "%s: error : %s\n",
            progName, XUTL_getLastErrorString());
        exit(1);
    }

    return (xpath);
}

/*
 *  ======== mkNode ========
 */
static Node mkNode(String pname)
{
    Node node;

    if (pname == NULL || pname[0] == '\0') {
	fprintf(stderr, "%s: assertion violation: empty package name\n",
	    progName);
	exit(2);
    }

    node = (Node)malloc(sizeof(struct NodeRec));
    if (node == NULL) {
        fprintf(stderr, "%s: error : out of memory\n", progName);
        exit(1);
    }

    node->rlen = 0;
    node->index = 0;
    node->name = pname;
    if ((node->base = XUTL_findPackage(pname, xppath)) != NULL) {
	node->reqs = XUTL_getPackageReqs(node->base, &node->rlen);
	if (node->reqs != NULL) {
	    return (node);
	}
    }

    fprintf(stderr, "%s: Warning: can't find package '%s' along '%s'\n",
	progName, pname, xppath);

    rmNode(node);
    return (NULL);
}

/*
 *  ======== printPkg ========
 */
static Void printPkg(String base)
{
    if (lflag) {
        XUTL_PkgDesc desc;
        if (XUTL_getPackageDesc(base, &desc)) {
            printfPkg(longFormat, base, &desc);
            XUTL_freePackageDesc(&desc);
        }
        else {
            fprintf(stderr, "%s: error : %s\n",
                progName, XUTL_getLastErrorString());
        }
    }
    else {
        printf("%s\n", base);
    }
}

/*
 *  ======== printPkgReqs ========
 */
static Void printPkgReqs(String pname)
{
    Node node;
    Node *nodeTab = NULL;
    Int nodeTabLen = 0;
    Int nodeTabMax = 0;
    
    /* reset global visitIndex for clean topological sort of nodeTab */
    visitIndex = 0;

    if ((node = mkNode(pname)) != NULL) {
	nodeTab = addNode(nodeTab, &nodeTabLen, &nodeTabMax, node);

	if (nodeTab != NULL) {
	    Int i;

	    /* sort nodeTab in dependency order */
	    qsort(nodeTab, nodeTabLen, sizeof(Node), compare);

	    for (i = 0; i < nodeTabLen; i++) {
		String base = XUTL_reducePath(nodeTab[i]->base, FALSE);
		if (mflag) {
		    Int j;
                    Bool basePrinted = FALSE;
		    Node node = nodeTab[i];
		    for (j = 0; j < node->rlen; j++) {
			Node r = findNode(node->reqs[j], nodeTab, nodeTabLen);
			if (r != NULL) {
			    printf("%s%c%s\n", node->base, mchar, r->base);
                            basePrinted = TRUE;
			}
		    }
                    if (basePrinted == FALSE) {
                        printf("%s%c%s\n", node->base, mchar, node->base);
                    }
		}
		else {
                    printPkg(base);
		}
	    }

            rmNodeTab(nodeTab, nodeTabLen);
	}
    }
}

/*
 *  ======== printfPkg ========
 */
static Void printfPkg(String format, String base, XUTL_PkgDesc *desc)
{
    String top = NULL;
    enum Mode {RAW, CHAR, SELECT};
    enum Mode mode;
    Char *cp;
    
    if (format == NULL || format[0] == '\0') {
        format = "%n;%b;%r;%l;%v;%d";
    }

    for (mode = RAW, cp = format; *cp != '\0'; cp++) {
        switch (mode) {
            case RAW: {
                if (cp[0] == '%') {
                    mode = SELECT;
                }
                else if (cp[0] == '\\') {
                    mode = CHAR;
                }
                else {
                    putchar(cp[0]);
                }
                break;
            }
            case CHAR: {
                switch (cp[0]) {
                    case 'r': {
                        putchar('\r');
                        break;
                    }
                    case 't': {
                        putchar('\t');
                        break;
                    }
                    case 'n': {
                        putchar('\n');
                        break;
                    }
                    default: {
                        putchar(cp[0]);
                        break;
                    }
                }
                mode = RAW;
                break;
            }
            case SELECT: {
                switch (cp[0]) {
                    case 'b': {
                        printf("%s", base);
                        break;
                    }
                    case 'c': {
                        printf("%s", desc->buildCount);
                        break;
                    }
                    case 'd': {
                        printf("%s", desc->date);
                        break;
                    }
                    case 'D': {
                        /* convert from milliseconds to seconds */
                        double date = atof(desc->date);
                        time_t time = (time_t)(date / 1000);

                        /* convert to a GMT struct tm and then to ascii */
                        struct tm *tp;
                        if ((tp = gmtime(&time)) != NULL) {
                            printf("%s", asctime(tp));
                        }
                        else {
                            printf("Thu Jan  1 00:00:00 1970");
                        }
                        break;
                    }
                    case 'k': {
                        printf("%s", desc->compatKey);
                        break;
                    }
                    case 'l': {
                        printf("%s", desc->label);
                        break;
                    }
                    case 'n': {
                        printf("%s", desc->name);
                        break;
                    }
                    case 'r': {
                        printf("%s", desc->releaseName);
                        break;
                    }
                    case 't': {
                        if (top == NULL) {
                            top = XUTL_getPackageRep(base);
                        }
                        printf("%s", top == NULL ? "" : top);
                        break;
                    }
                    case 'v': {
                        printf("%s%s%s", desc->compatKey,
                            (desc->compatKey[0] == '\0' ? "" : ", "),
                            desc->buildCount);
                        break;
                    }
                    case 'V': {
                        printf("%s%s%s", desc->compatKey,
                            (desc->compatKey[0] == '\0' ? "" : ", "),
                            desc->date);
                        break;
                    }
                    default: {
                        putchar(cp[0]);
                    }
                }
                mode = RAW;
            }
        }
    }
    putchar('\n');
    if (top != NULL) {
        free(top);
    }
}

/*
 *  ======== rmNode ========
 */
static Void rmNode(Node node)
{
    if (node != NULL) {
	if (node->rlen != 0) {
	    Int i;
	    for (i = 0; i < node->rlen; i++) {
		free(node->reqs[i]);
	    }
	    free(node->reqs);
	}
	if (node->base != NULL) {
	    free(node->base);
	}
	free(node);
    }
}

/*
 *  ======== rmNodeTab ========
 */
static Void rmNodeTab(Node *nodeTab, Int nodeTabLen)
{
    if (nodeTab != NULL) {
        Int i;
        
        for (i = 0; i < nodeTabLen; i++) {
            Node node = nodeTab[i];
            if (node != NULL) {
                rmNode(node);
            }
        }

        free(nodeTab);
    }
}
