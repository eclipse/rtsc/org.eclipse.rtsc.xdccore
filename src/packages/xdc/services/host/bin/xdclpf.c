/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== xdclpf.c ========
 *  List all files in the specified base directories _except_ any "nested
 *  packages".
 *
 *  usage: xdclpf [-a] [base-directory ...]
 */
#include <xdc/std.h>
#include <xdc/services/host/lib/xutl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXPATHLEN  1024

static Char pathBuf[MAXPATHLEN + 1];

static String usage = "%s: [-help] [-a] [package-base-directory ...]\n"
    "    -help    display this message\n"
    "    -a       exclude all packages under the specified package base.\n"
    "             Without this flag only packages with the same repository\n"
    "             are excluded\n";
    
static String progName = NULL;

static String curRepo = NULL;
static String curBase = NULL;
static String curName = NULL;
static SizeT curBaseLen = 0;
static Bool aflag = FALSE;

static Bool filter(IArg arg, String baseName, String dirName, Char *end);
static Int scanFile(IArg arg, IArg *cookie, String baseName, String dirName);

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    Int i;

    progName = argv[0];

    for (i = 1; i < argc; i++) {
        if (argv[i][0] == '-') {
            switch (argv[i][1]) {
                case 'a': {
                    aflag = TRUE;
                    break;
                }
                case 'h': {
                    printf(usage, progName);
                    return (0);
                }
                default: {
                    fprintf(stderr, usage, progName);
                    return (1);
                }
            }
        }
        else {
            break;
        }
    }

    if (i >= argc) {
        fprintf(stderr, usage, progName);
        return (1);
    }

    for (; i < argc; i++) {
        if (XUTL_isPackageBase(argv[i], FALSE)) {
            curRepo = NULL;
            curBase = argv[i];
            curBaseLen = strlen(curBase);

            /* if argv[i] is a package base, find it's files */
            if ((curName = XUTL_getPackageName(curBase, FALSE)) != NULL) {
                Char *cp;
                /* replace '.'s with '/'s in curName */
                for (cp = curName; *cp != '\0'; cp++) {
                    if (*cp == '.') {
                        *cp = '/';
                    }
                }
                /* if we only filter pkgs with the same repo, compute it now */
                if (aflag == FALSE) {
                    curRepo = XUTL_getPackageRep(curBase);
                }

                /* now scan the file system looking for pkg files */
                XUTL_scanFS(curBase, scanFile, filter, 0, XUTL_FSALL);

                free(curName);
                if (curRepo != NULL) {
                    free(curRepo);
                }
            }
        }
        else {
            fprintf(stderr, "%s: %s is not a package base directory\n",
                progName, argv[i]);
        }
    }

    return (0);
}

/*
 *  ======== filter ========
 *  skip nested package directories
 *
 *  Return TRUE to skip dirName/baseName, FALSE to include it.
 */
static Bool filter(IArg arg, String baseName, String dirName, Char *end)
{
    SizeT len = end - dirName;
    SizeT baseLen = strlen(baseName);
    Bool result = TRUE;

    /* append baseName and dirName to determine if it's a package */
    if ((len + 1 + baseLen + 1) < MAXPATHLEN) {
        sprintf(pathBuf, "%s/%s", dirName, baseName);
        result = XUTL_isPackageBase(pathBuf, FALSE);

        /* if curRepo exists, make sure it matches pathBuf pkg's repository */
        if (result && curRepo != NULL) {
            String rname = XUTL_getPackageRep(pathBuf);
            result = FALSE;
            if (rname != NULL) {
                if (strcmp(rname, curRepo) == 0) {
                    result = TRUE;
                }
                free(rname);
            }
        }
    }
    else {
        fprintf(stderr, "%s: error: path too long (%s/%s)\n",
            progName, dirName, baseName);
    }

    return (result);
}

/*
 *  ======== scanFile ========
 *  print file visited
 */
static Int scanFile(IArg arg, IArg *cookie, String baseName, String dirName)
{
    printf("%s%s/%s\n", curName, dirName + curBaseLen, baseName);
    return (0);
}
