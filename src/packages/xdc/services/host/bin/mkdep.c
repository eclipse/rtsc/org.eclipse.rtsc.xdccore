/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== mkdep.c ========
 *  Generate make include file dependencies
 *
 *  usage: mkdep [-w] [-v] [-g] [-s target-suffix] [-p target-prefix]
 *               [-i include-env-variable] [-e exclude-env-variable]
 *               [-{o|a} output-file] file ... [-C compiler-options ...]
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <xdc/std.h>

#if defined(xdc_target__os_Windows)
#include <io.h>         /* access() declaration */
#include <direct.h>     /* getcwd() declaration */
#else
#include <unistd.h>     /* access(), getcwd() declaration */
#endif

#include <xdc/services/host/lib/lst.h>
#include <xdc/services/host/lib/gs.h>
#include <xdc/services/host/lib/xutl.h>

#define QUANTUM	    32

#define LINELEN	    1024
#define NAMELEN	    1024
#define MAXARGV	    256
#define MAXPATH     (10 * NAMELEN)

#define PATHSEPS    "\r\n;"
#define PREFIX	    ""

#if defined(xdc_target__os_Windows)
#define INCLUDE	    ".;..\\..\\include"
#define SUFFIX	    "obj"
#define DIRCHAR	    "\\"
#else
#define INCLUDE	    ".;../../include"
#define SUFFIX	    "o"
#define DIRCHAR	    "/"
#endif

static Bool addName(LST_Seq headers, String name);
static Void doFile(String fileName, FILE *file, Char mode);
static Void doGen(String prefix, String suffix, Int argc, String argv[]);
static Void doName(String name, String dotDir, LST_Seq incList);
static Bool exists(String dir, String name, String filename);
static Bool findFile(String name, String dotDir, String pathTab[], String filename);
static Void genmake(String targ, Char mode, String filename);
static String getDef(String token);
static String getIncPath(Int argc, String argv[]);
static String *getOptionsFile(Int *argc, String argv[]);
static Bool inList(LST_Seq headers, String name);
static String *mkPathTab(String path);
static String *myRealloc(String *array, SizeT len, Int inc);
static Void parseC(String linebuf, String dotDir, LST_Seq incList);
static Void parseS(String linebuf, String dotDir, LST_Seq incList);
static Void printFile(FILE *out, String format, String filename);
static Void printFile2(FILE *out, String format, String f1, String f2);
static Void printStringList(LST_Seq list);
static Void prunePathTab(String pathTab[], String ignoreTab[]);
static String quoteString(String filename);
static Void rmPrefix(String path);
static Void setSeparator(String path, Char from, Char to);
static Bool skipLine(String linebuf);

static String usage = "usage: %s [-w] [-v] [-g] [-s target-suffix] [-p target-prefix] [-i include-env-variable] [-e exclude-env-variable] [-{o|a} output-file] file ... [-C compiler-options ...]\n";

static Bool unknownDefs = FALSE; /* TRUE => we may have new defines */

static LST_Seq list;		/* list of all included files */
static String *incPathTab;	/* table of include paths to search */
static String *excPathTab;	/* table of include paths to ignore */
static String  progName;
static FILE *outFile;
static String *cOpts;
static Int cOptsLen = 0;
static Char dirChar[2] = {'\0', '\0'};
static Char cwd[MAXPATH + 1];
static SizeT cwdLen = 0;
static Bool dflag = FALSE;
static Bool gflag = FALSE;
static Bool nflag = FALSE;
static Bool uflag = FALSE;
static Bool wflag = FALSE;
static Int  vflag = 0;

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    Char incBuf[sizeof(INCLUDE) + 1];
    Char prefixBuf[sizeof(PREFIX) + 1];
    String suffix = SUFFIX;
    String prefix = prefixBuf;
    String incPath = incBuf;
    String excPath = "";
    Int i;

    outFile = stdout;

    strcpy(incBuf, INCLUDE);
    strcpy(prefixBuf, PREFIX);

    GS_init();
    LST_init();
    XUTL_init();

    progName = argv[0];
    if (argc < 2) {
	fprintf(stderr, usage, progName);
	return (1);
    }

    /* parse command line args */
    argv += 1;
    argc -= 1;
    while (argc > 0 && argv[0][0] == '-') {
	if (argc < 2) {
	    fprintf(stderr, usage, progName);
	    return (1);
	}

	switch (argv[0][1]) {
	    case 'd': {
		if (uflag == TRUE) {
		    fprintf(stderr,
			"%s: error: can't use both -d and -u\n", progName);
		    return (1);
		}
		dflag = TRUE;
		argv -= 1;
		argc += 1;
		break;
	    }

	    case 'u': {
		if (dflag == TRUE) {
		    fprintf(stderr,
			"%s: error: can't use both -d and -u\n", progName);
		    return (1);
		}
		uflag = TRUE;
		argv -= 1;
		argc += 1;
		break;
	    }

	    case 'I': {
		incPath = argv[1];
		break;
	    }

	    case 'i': {
		if ((incPath = getenv(argv[1])) == NULL) {
		    fprintf(stderr,
			"%s: error: can't find environment variable %s\n",
			progName, argv[1]);
		    return (1);
		}
		break;
	    }

	    case 'E': {
		excPath = argv[1];
		break;
	    }

	    case 'e': {
		if ((excPath = getenv(argv[1])) == NULL) {
		    fprintf(stderr,
			"%s: error: can't find environment variable %s\n",
			progName, argv[1]);
		    return (1);
		}
		break;
	    }

	    case 's': {
		suffix = argv[1];
		break;
	    }

	    case 'p': {
		prefix = argv[1];
		break;
	    }

            case 'o': {
		if ((outFile = fopen(argv[1], "w")) == NULL) {
		    fprintf(stderr, "%s: can't open %s for output\n",
			progName, argv[1]);
		    return (1);
		}
                break;
            }

            case 'a': {
		if ((outFile = fopen(argv[1], "a+")) == NULL) {
		    fprintf(stderr, "%s: can't open %s for output\n",
			progName, argv[1]);
		    return (1);
		}
		break;
	    }

	    case 'n': {
		nflag = TRUE;
		argv -= 1;
		argc += 1;
		break;
	    }

	    case 'v': {
		vflag++;
		argv -= 1;
		argc += 1;
		break;
	    }

	    case 'w': {
		wflag = TRUE;
		argv -= 1;
		argc += 1;
		break;
	    }
	    
	    case 'g': {
		gflag = TRUE;
		argv -= 1;
		argc += 1;
		break;
	    }
	    
	    default: {
		fprintf(stderr, usage, progName);
		return (1);
	    }
	}

	argv += 2;
	argc -= 2;
    }
    if (argc <= 0) {
	fprintf(stderr, usage, progName);
	return (1);
    }

    /* set path separation charactor used in dependency output */
    if (dflag == TRUE) {
	setSeparator(prefix, DIRCHAR[0], '\\');
	setSeparator(incPath, DIRCHAR[0], '\\');
	setSeparator(excPath, DIRCHAR[0], '\\');
	dirChar[0] = '\\';
    }
    else if (uflag == TRUE) {
	setSeparator(prefix, DIRCHAR[0], '/');
	setSeparator(incPath, DIRCHAR[0], '/');
	setSeparator(excPath, DIRCHAR[0], '/');
	dirChar[0] = '/';
    }
    else {
	dirChar[0] = DIRCHAR[0]; /* use local machine's char (i.e. DIRCHAR) */
    }

    if (getcwd(cwd, MAXPATH) == NULL) {
        cwd[0] = '\0';
    }
    cwdLen = strlen(cwd);
    setSeparator(cwd, DIRCHAR[0], dirChar[0]);

    /* locate compiler options *after* file names */
    for (i = 0; i < argc; i++) {
	if (argv[i][0] == '-' && argv[i][1] == 'C') {
	    if ((incPath = getIncPath(argc - i - 1, argv + i + 1)) == NULL) {
		fprintf(stderr, "%s: out of memory\n", progName);
		return (1);
	    }
            cOpts = argv + i + 1;
            cOptsLen = argc - i - 1;
	    break;
	}
    }
    argc = i;

    incPathTab = mkPathTab(incPath);
    excPathTab = mkPathTab(excPath);
    if (incPathTab == NULL || excPathTab == NULL) {
	fprintf(stderr, "%s: can't create include path table\n", progName);
	return (1);
    }
    prunePathTab(incPathTab, excPathTab);

    doGen(prefix, suffix, argc, argv);

    return (0);
}

/*
 *  ======== addName ========
 */
static Bool addName(LST_Seq headers, String name)
{
    String tmp;
    Ptr cursor;
    Ptr valbuf;
    
    /* printf("addName(%s, ...)\n", name); */
    
    cursor = LST_cursor(headers, (Ptr)NULL);
    valbuf = LST_valbuf(headers, (Ptr)NULL);

    if (nflag == FALSE) {
	for (LST_scan(headers, &tmp); LST_next(headers);) {
	    /* printf("\n\tscanning %s ... ", tmp); */
	    if (strcmp(name, tmp) == 0) {
		LST_cursor(headers, cursor);
		LST_valbuf(headers, valbuf);
		return (FALSE);
	    }
	}
    }
    
    if ((tmp = (String)GS_alloc((UInt)(strlen(name) + 1))) == NULL) {
        fprintf(stderr, "%s: warning: out of memory\n", progName);
        return (FALSE);
    }
    strcpy(tmp, name);
    LST_append(headers, &tmp);

    if (cursor != NULL) {
	LST_cursor(headers, cursor);
    }
    LST_valbuf(headers, valbuf);
    /* printf("added '%s'\n", tmp); */
    return (TRUE);
}

/*
 *  ======== doFile ========
 *  Parse contents of file according to mode (.c, .m, .s*), looking
 *  for dependencies due to include files.  As each dependency is
 *  found write it to stdout; the target has already been output.
 */
static Void doFile(String fileName, FILE *file, Char mode)
{
    Char linebuf[LINELEN];
    Char dotDir[NAMELEN + 1];
    Char *cp;
    
    if (vflag > 1) {
        printf("\ndoFile %s ...\n", fileName);
    }
    
    /* initialize ifdef state variables */
    skipLine(NULL);
    
    /* set dotDir to any directory prefix of fileName */
    if ((cp = strrchr(fileName, dirChar[0])) != NULL) {
	strcpy(dotDir, fileName);
	dotDir[cp - fileName] = '\0';
    }

    while (fgets(linebuf, LINELEN, file)) {
	switch (mode) {
	    case 'c': {
		parseC(linebuf, (cp == NULL ? "." : dotDir), list);
		break;
	    }

	    case 's': {
		parseS(linebuf, (cp == NULL ? "." : dotDir), list);
		break;
	    }

	    default: {
		break;
	    }
	}
    }

    if (vflag > 1) {
        printf("    includes: ");
        printStringList(list);
    }
    return;
}

/*
 *  ======== doGen ========
 */
static Void doGen(String prefix, String suffix, Int argc, String argv[])
{
    Char targName[NAMELEN + 1];
    Char *tmp, *base;
    Char mode = 'c';
    Int i;

    if (argc == 1 && argv[0][0] == '@') {
	argv = getOptionsFile(&argc, argv);
    }

    for (i = 0; i < argc; i++, argv++) {
	
	if (vflag) {
	    printf("scanning '%s' ...\n", argv[0]);
	}
	
	if ((strlen(prefix) + 1 + strlen(suffix) + strlen(argv[0]) + 1) >= NAMELEN) {
	    fprintf(stderr, "%s: error path name %s too long\n",
		progName, argv[0]);
	    exit(1);
	}

        /* set targName = <prefix>`basename argv[0]`<suffix> */
	strcpy(targName, prefix);
	if (prefix[0] != '\0' && prefix[strlen(prefix) - 1] != dirChar[0]) {
	    strcat(targName, dirChar);
	}
        /* the "type mismatch" warning from GCC 4.1 for the line below is
         * bogus; if you #define _HAVE_STRING_ARCH_strpbrk 1, the warning
         * disapears.  Also, there is no warning from GCC 4.3.
         */
	for (base = argv[0]; (tmp = strpbrk(base, "/\\")) != NULL; ) {
	    base = tmp + 1;
	}
	strcat(targName, base);
	if ((tmp = strrchr(targName, '.')) != NULL) {
	    if (strcmp(".asm", tmp) == 0 || tmp[1] == 's') {
		mode = 's';
	    }
	    *tmp = '\0';
	}
	strcat(targName, ".");
	strcat(targName, suffix);

        /* generate header dependencies for targName referenced via argv[0] */
	genmake(targName, mode, argv[0]);
    }
}

/*
 *  ======== doName ========
 *  name appears in the input file and represents a dependency that must
 *  be output.
 */

static Void doName(String name, String dotDir, LST_Seq incList)
{
    Char fileName[NAMELEN + 2];

#if 0
    printf("doName(%s, %s, ...)\n",
	name == NULL ? "<null>" : name,
	dotDir == NULL ? "<null>" : dotDir);
#endif

    /* if name is already in the incList, no need to look for it again */
    if (nflag == TRUE && inList(incList, name)) {
	/* printf("doName: %s already in list.\n", name);*/
	return;
    }
    
    if (findFile(name, dotDir, incPathTab, fileName)) {
        /* "canonicalize" fileName before adding it to prevent the possibility
         * of an infinite loop caused by circular header file references(!) 
         * and the use of "../" in #include's; we need to recognize that
         * "foo/bar/../hal.h" is the same as "foo/hal.h".
         */
        XUTL_reducePath(fileName, FALSE);
	if (addName(incList, fileName)) {
	    printFile(outFile, "%s ", fileName);
	}
    }
}

/*
 *  ======== exists ========
 */
static Bool exists(String dir, String name, String filename)
{
    sprintf(filename, "%s%s%s", dir, DIRCHAR, name);
    setSeparator(filename, dirChar[0], DIRCHAR[0]);
    if (access(filename, 0) == 0) {
	setSeparator(filename, '/', dirChar[0]);
	setSeparator(filename, DIRCHAR[0], dirChar[0]);
        rmPrefix(filename);
	return (TRUE);
    }

    return (FALSE);
}

/*
 *  ======== findFile ========
 */
static Bool findFile(String name, String dotDir, String pathTab[], String filename)
{
    String *prefix;
    
    /* check for file in dotDir first */
    if (dotDir != NULL && exists(dotDir, name, filename)) {
	return (TRUE);
    }

    /* now look in -I paths */
    for (prefix = pathTab; *prefix != NULL; prefix++) {
	if ((*prefix)[0] != '\0' && exists(*prefix, name, filename)) {
	    return (TRUE);
	}
    }

    /* if we get here we can't find it but the compiler did?!?! */
    if (wflag) {
	printf("Warning: can't find '%s' in: (%s)\n", name,
	    dotDir == NULL ? "" : dotDir);
	for (prefix = pathTab; *prefix != NULL; prefix++) {
	    if ((*prefix)[0] != '\0') {
		printf("    %s\n", *prefix);
	    }
	}
    }

    return (FALSE);
}

/*
 *  ======== genmake ========
 *  Generate dependencies for the file filename.  target is the target
 *  file resulting from filename (e.g. boot_RTK.o40 is target of
 *  boot.s40)
 *
 *  The mode character identifies this file as being a C or assembly
 *  language source file ('c', or 's', respectively.)
 */

static Void genmake(String target, Char mode, String filename)
{
    FILE	*file;
    String	name;

    /* reset state associated with separate compilation units */
    unknownDefs = FALSE;
    
    setSeparator(filename, '/', DIRCHAR[0]);
    setSeparator(filename, dirChar[0], DIRCHAR[0]);
    if ((file = fopen(filename, "r")) != NULL) {
	setSeparator(filename, DIRCHAR[0], dirChar[0]);

	/* output dependency on source file */
	printFile2(outFile, "%s: %s ", target, filename);

	/* parse source file for "#include" files and output these as well */
	if ((list = LST_create(sizeof(String))) != NULL) {
	
	    doFile(filename, file, mode);
	    fclose(file);
	
	    /* scan included files for more dependencies */
	    for (LST_scan(list, &name); LST_next(list);) {
		/* printf("scanning %s ...\n", name);*/
		setSeparator(name, dirChar[0], DIRCHAR[0]);
		if (file = fopen(name, "r")) {
		    setSeparator(name, DIRCHAR[0], dirChar[0]);
		    doFile(name, file, mode);
		    fclose(file);
		}
		else {
		    fprintf(stderr, "%s: can't open '%s'\n", progName, name);
		    exit(1);
		}
	    }
	
	    /* free dependency list (doFile already output the file names) */
	    for (LST_scan(list, &name); LST_next(list);) {
		if (gflag == TRUE) {
		    /* output empty rule for GNU make */
		    printFile(outFile, "\n%s:", name);
		}
		GS_free(name);
	    }
	    LST_delete(list);
	}
        else {
            fclose(file);
        }
    
	/* terminate dependency line */
	fprintf(outFile, "\n");
    }
}

/*
 *  ======== getDef ========
 */
static String getDef(String token)
{
    Int i;
    static Char value[MAXPATH + 3]; /* + 3 for '\0' and two "'s */

    if (token == NULL) {
        return (NULL);
    }

    for (i = 0; i < cOptsLen; i++) {
        if (cOpts[i][0] == '-' && (cOpts[i][1] == 'D' || cOpts[i][1] == 'd')) {
            SizeT len = strlen(token);
            String def = cOpts[i] + 2;
            if (strncmp(def, token, len) == 0) {
               String val = def + len;
               if (val[0] == '=') {
                   return (val + 1);
               }
               else if (val[0] == '\0') {
                   return (val);
               }
            }
        }
    }

    /* HACK: special hack for xdc !!!!! */
    if (strcmp(token, "xdc_cfg__xheader__") == 0) {
        String result = getDef("xdc_cfg__header__");
        if (result != NULL && strlen(result) < MAXPATH) {
            sprintf(value, "<%s>", result);
            return (value);
        }
    }
    else if (strcmp(token, "xdc_target__") == 0) {
        String result = getDef("xdc_target_types__");
        if (result != NULL && strlen(result) < MAXPATH) {
            sprintf(value, "<%s>", result);
            return (value);
        }
    }
    else if (strcmp(token, "__local_include(__xdc_bld_pkg_c__)") == 0) {
        String result = getDef("__xdc_bld_pkg_c__");
        if (result != NULL && strlen(result) < MAXPATH) {
            sprintf(value, "\"%s\"", result);
            return (value);
        }
    }
    
    return (NULL);
}

/*
 *  ======== getIncPath ========
 */
static String getIncPath(Int argc, String argv[])
{
    Int i;
    SizeT len;
    String incPath;

    for (len = i = 0; i < argc; i++) {
	len += strlen(argv[i]);
    }

    if ((incPath = (String)GS_alloc((UInt)len + 1)) != NULL) {
	Char *cp = incPath;

	/* look for "-I" or "-i" options and copy paths into incPath */
	for (i = 0; i < argc; i++) {
	    if (argv[i][0] == '-'
		&& (argv[i][1] == 'i' || argv[i][1] == 'I')) {
		String incDir = "";
		Bool quoted = FALSE;

		if (argv[i][2] == '\0') {
		    if ((i + 1) < argc) {
			incDir = argv[i + 1];
		    }
		}
		else {
		    incDir = &argv[i][2];
		}
		
		/* copy path to incPath */
		if (incDir[0] == '"') {
		    quoted = TRUE;
		    incDir++;		/* don't copy the '"' */
		}
		while (incDir[0] != '\0'
			&& (!quoted || incDir[0] != '"')) {
		    *cp++ = *incDir++;	
		}

		*cp++ = ';';		/* terminate with ';' in incPath */
		*cp = '\0';
	    }
        }

	*cp = '\0';			/* '\0' terminate incPath */
    }

    return (incPath);
}

/*
 *  ======== getOptionsFile ========
 */
static String *getOptionsFile(Int *argc, String argv[])
{
    FILE *file;
    Int i;
    SizeT len;
    String tmp;
    static Char line[LINELEN];
    static String fileArgv[MAXARGV];

    if ((file = fopen(argv[0] + 1, "rt")) == NULL) {
	fprintf(stderr, "%s: can't open options file %s\n", progName, argv[0]);
	exit(1);
    }

    for (i = 0; fgets(line, sizeof(line), file) != NULL; ) {
	len = strlen(line);
	if (len > 0 && line[len - 1] == '\n') {
	    line[--len] = '\0';
	}
	if ((tmp = GS_alloc((UInt)len + 1)) == NULL) {
	    fprintf(stderr, "%s: out of memory\n", progName);
	    exit(1);
	}
	strcpy(tmp, line);

	for (tmp = strtok(tmp, " \t"); tmp != NULL; tmp = strtok(NULL, " \t")){
	    fileArgv[i++] = tmp;
	    if (i >= MAXARGV) {
		fprintf(stderr, "%s: too many files listed in %s\n",
		    progName, argv[0]);
		exit(1);
	    }
	}
    }

    fclose(file);

    *argc = i;
    fileArgv[i] = NULL;
    return (fileArgv);
}

/*
 *  ======== inList ========
 */
static Bool inList(LST_Seq headers, String name)
{
    String tmp;
    SizeT nameLen;
    SizeT index;
    Ptr cursor;
    Ptr valbuf;
    Bool status = FALSE;

    /* save LST_Seq state */
    cursor = LST_cursor(headers, (Ptr)NULL);
    valbuf = LST_valbuf(headers, (Ptr)NULL);

    /* search list for name */
    nameLen = strlen(name);
    for (LST_scan(headers, &tmp); LST_next(headers);) {
	index = strlen(tmp) - nameLen;
	if (index >= 0 && (strcmp(name, &tmp[index]) == 0)) {
	    if (index == 0 || (index != 0 && tmp[index - 1] == dirChar[0])) {
		status = TRUE;
		break;
	    }
	}
    }

    /* restore LST_Seq state */
    LST_cursor(headers, cursor);
    LST_valbuf(headers, valbuf);

    return (status);
}

/*
 *  ======== mkPathTab ========
 */
static String *mkPathTab(String path)
{
    String  *pathTab;
    String  pathCopy;
    Int	    numPaths = 0;
    SizeT   pathTabLen;
    SizeT   len;

    /* allocate initial pathTab; +1 to allow for NULL termination */
    pathTab = (String *)GS_alloc((UInt)(sizeof(String) * (QUANTUM + 1)));
    pathTabLen = QUANTUM + 1;

    if (pathTab != NULL) {
	pathTab[0] = NULL;
	
	if ((len = strlen(path)) != 0) {

	    /* create a copy of path so it isn't modified by strtok() */
	    pathCopy = (Char *)GS_alloc((UInt)((len + 1) * sizeof(Char)));
	    if (pathCopy == NULL) {
		GS_free(pathTab);
		return (NULL);
	    }
	    strcpy(pathCopy, path);

	    /* fill table with paths listed in pathCopy */
	    pathTab[numPaths++] = strtok(pathCopy, PATHSEPS);
	    while ((pathTab[numPaths++] = strtok(NULL, PATHSEPS)) != NULL) {
		if (numPaths >= pathTabLen) {
		    pathTab = myRealloc(pathTab, pathTabLen, QUANTUM);
		    pathTabLen += QUANTUM;
		    if (pathTab == NULL) {
			return (NULL);
		    }
		}
	    }
	}
    }

    return (pathTab);
}

/*
 *  ======== myRealloc ========
 */
static String *myRealloc(String *array, SizeT len, Int inc)
{
    String *new;
    SizeT   nbytes;

    nbytes = sizeof(String) * (len + inc);
    if ((new = (String *)GS_alloc((UInt)nbytes)) != NULL) {
	memcpy((char *)new, (char *)array, len * sizeof(String));
	GS_free(array);
    }

    return (new);
}

/*
 *  ======== parseC ========
 */
static Void parseC(String linebuf, String dotDir, LST_Seq incList)
{
    String s;
    Bool searchDot = TRUE;

    /*printf("parseC line: %s\n", linebuf); */

    /* skip leading white space */
    for (; *linebuf == ' ' || *linebuf == '\t'; linebuf++) {
        ;
    }


    /* if this is a C pre-processor directive line, look for include keyword */
    if (linebuf[0] == '#' && skipLine(linebuf) == FALSE) {

	/* skip any existing white space */
	for (++linebuf; *linebuf == ' ' || *linebuf == '\t'; linebuf++) {
	    ;
	}

	/* if this is an include statement, get the included file name */
	if (strncmp(linebuf, "include", 7) == 0) {
	    Char *cp;

	    /* if this is not a "..." include, set searchDot to FALSE */
	    for (cp = linebuf + 7; *cp == ' ' || *cp == '\t'; cp++) {
		;
	    }
	    if (*cp != '"') {
		searchDot = FALSE;
	    }

	    /* get the included file name and pass this to doName() */
	    strtok(linebuf, "<\"");
	    if ((s = strtok(NULL, ">\"")) != NULL) {
		doName(s, searchDot ? dotDir : NULL, incList);
	    }
            else {  /* include file may be a macro */
                strtok(linebuf, " \t");
                if ((s = strtok(NULL, " \t\n\r")) != NULL) {
                    /* lookup macro definition */
                    if ((s = getDef(s)) != NULL) {
                        /* found it, copy into linebuf so we can remove <'s */
                        strcpy(linebuf, s);
                        searchDot = s[0] != '"' ? FALSE : TRUE;
                        if ((s = strtok(linebuf, "<>\"")) != NULL) {
                            doName(s, searchDot ? dotDir : NULL, incList);
                        }
                    }
                }
            }
	}
    }
}


/*
 *  ======== parseS ========
 */

static Void parseS(String linebuf, String dotDir, LST_Seq incList)
{
    String	s;
    
    /* printf("parseS line: %s\n", linebuf);*/
    
    /* skip leading white space */
    for (; *linebuf == ' ' || *linebuf == '\t'; linebuf++) {
	;
    }

    /* is this a C preprocessor line? */
    if (linebuf[0] == '#') {
	Char *cp = linebuf + 1;

	while (*cp == '\t' || *cp == ' ') { /* eat white space */
	    cp++;
	}

	if (strncmp("include", cp, 7) == 0) {   /* is this an include line? */
	    parseC(linebuf, dotDir, incList);	/* yes, parse it as a C line */
	}
	else {
	    return;				/* no, ignore it */
	}
    }
    
    s = strtok(linebuf, "<>\" \t");
    if (s == NULL
	|| ((strcmp(".copy", s) != 0)
	    && (strcmp(".mlib", s) != 0)
	    && (strcmp(".cdecls", s) != 0)
	    && (strcmp(".include", s) != 0 && strcmp("include", s) != 0)) ) {
	return;
    }
    
    /* .cdecls is not like other includes (ack!) */
    if (strcmp(".cdecls", s) == 0) {
        Char *cp = (Char *)s + 8; /* skip over .cdecls */
        /* if there is a ',' before a '"', then remove the options */
        while (*cp != '\0' && *cp != '\n' && *cp != '\"') {
            if (*cp++ == ',') {
                strtok(NULL, "\"");  /* skip over options */
                break;
            }
        }
        while ((s = strtok(NULL, ",\" \t\n")) != NULL) {
            doName(s, dotDir, incList);
        }

        return;
    }

    if ((s = strtok(NULL, "<>\" \t\n")) == NULL) {
	return;
    }

    doName(s, dotDir, incList);

    return;
}

/*
 *  ======== printFile ========
 *  Output a file name being careful to backquote any embedded spaces.
 */
static Void printFile(FILE *out, String format, String filename)
{
    if (strchr(filename, ' ') == NULL) {
	fprintf(out, format, filename);
    }
    else {
	String tmp = quoteString(filename);
	fprintf(out, format, tmp);
	GS_free(tmp);
    }
}

/*
 *  ======== printFile2 ========
 *  Output file names being careful to backquote any embedded spaces.
 */
static Void printFile2(FILE *out, String format, String f1, String f2)
{
    String tmp1 = NULL;
    String tmp2 = NULL;

    if (strchr(f1, ' ') != NULL) {
	tmp1 = quoteString(f1);
	f1 = tmp1;
    }
    if (strchr(f2, ' ') != NULL) {
	tmp2 = quoteString(f2);
	f2 = tmp2;
    }

    fprintf(out, format, f1, f2);

    if (tmp1 != NULL) {
	GS_free(tmp1);
    }
    if (tmp2 != NULL) {
	GS_free(tmp2);
    }
}

/*
 *  ======== printStringList ========
 *  For debugging only.
 */
static Void printStringList(LST_Seq list)
{
    String tmp;
    Ptr cursor;
    Ptr valbuf;

    /* save cursor state */
    cursor = LST_cursor(list, (Ptr)NULL);
    valbuf = LST_valbuf(list, (Ptr)NULL);

    for (LST_scan(list, &tmp); LST_next(list);) {
	printFile(stdout, "%s ", tmp);
    }
    printf("\n");

    /* restore cursor state */
    LST_cursor(list, cursor);
    LST_valbuf(list, valbuf);
}

/*
 *  ======== prunePathTab ========
 */
static Void prunePathTab(String pathTab[], String ignoreTab[])
{
    String *ignorePathPtr;
    String *curPathPtr;
    SizeT len;

    for (ignorePathPtr = ignoreTab; *ignorePathPtr != NULL; ignorePathPtr++) {
	for (curPathPtr = pathTab; *curPathPtr != NULL; curPathPtr++) {
	    len = strlen(*ignorePathPtr);
	    if ((Int)strlen(*curPathPtr) >= len) {
		if ((*curPathPtr)[len] == '\0'
		    || (*curPathPtr)[len] == dirChar[0]) {
		    if (strncmp(*curPathPtr, *ignorePathPtr, len) == 0) {
			*curPathPtr = "";
		    }
		}
	    }
	}
    }
}

/*
 *  ======== quoteString ========
 *  Insert a '\' before every ' ' in the specified filename and return a
 *  new string that should be freed.
 */
static String quoteString(String filename)
{
    String tmp = (String)GS_alloc((UInt)(2 * strlen(filename) + 1));

    if (tmp != NULL) {
	Char *src, *dst;
	for (dst = tmp, src = filename; *src != '\0'; src++, dst++) {
	    if (*src == ' ') {
		*dst++ = '\\';
	    }
	    *dst = *src;
	}
	*dst = '\0';
    }

    return (tmp);
}

/*
 *  ======== setSeparator ========
 */
static Void setSeparator(String path, Char from, Char to)
{
    Char *cp = path;

    if (from != to) {
	while ((cp = strchr(cp, from)) != NULL) {
	    *cp = to;
	}
    }
}

/*
 *  ======== rmPrefix ========
 *  Remove unnecessary "../" prefixes.
 *
 *  This function removes "../" prefixes from path that simply cancel later
 *  portions of path; e.g., "../foo/bar.c" becomes "bar.c" iff the cwd ends
 *  with "/foo".
 *
 *  We do this to ensure that output dependency names exactly match relative
 *  file names specified in make pattern rules; otherwise make does not
 *  recognize that the two names reference the same file.
 *
 *  A more robust (but slower) implementation would be to canonicalize path
 *  and compare to cwd; if cwd is a prefix of the canonical path, remove it
 *  otherwise simply return the full path.
 */
static Void rmPrefix(String path)
{
    Char *cp;
    Char *new;
    Char up[] = {'.', '.', dirChar[0], '\0'};
    Int i, j;
    SizeT len;
    
    /* if it's an absolute path simply return */
    if (path[0] == dirChar[0] || (path[1] == ':' && path[2] == '\\')) {
        return;
    }

    /* count the number of "../"s */
    for (i = 0, new = path; strncmp(new, up, 3) == 0; i++) {
        new += 3;
        while (new[0] == dirChar[0]) {
            new++;          /* remove redundent /'s */
        }
    }
    if (i == 0) {           /* no "../" prefixes detected */
        return;
    }

    /* skip over i directories starting from after the last "../" */
    for (cp = new, j = 0; j < i && cp[0] != '\0'; cp++) {
        if (cp[0] == dirChar[0]) {
            j++;
        }
    }
    if (cp[0] == '\0') {    /* too many "../"s */
        return;
    }

    /* if we get here, cp may be the name of a file in the cwd */
    
    /* compare (new -> cp) to the tail of cwd */
    len = (cp - 1) - new;
    if (strncmp(new, cwd + cwdLen - len, len) == 0) {
        /* prefix simply returns us to cwd, so we strip it off */
        Char *src = cp;
        Char *dst = path;
        
        while (src[0] == dirChar[0]) {
            src++;          /* remove redundent /'s */
        }
        while (*src != '\0') {
            *dst++ = *src++;
        }
        *dst = '\0';
    }
}

typedef enum State {
    SREAD,          /* read lines until next #if or #el */
    SSKIP,          /* skip lines until next #el or #endif */
    SSKIPTOEND,     /* skip lines until next #endif */
    SUNKNOWN        /* read lines until next #if */
} State;

typedef enum PPId {
    PINCLUDE, PIFDEF, PIFNDEF, PIF, PELSE, PELIF, PENDIF, PDEFINE, PUNDEF,
    PUNKNOWN
} PPId;

typedef struct PPTypeDesc {
    String name;
    Int    len;
    PPId   id;
} PPTypeDesc;

static PPTypeDesc ppTypeMap[] = {
    {"include", sizeof("include") - 1,  PINCLUDE},
    {"ifdef",   sizeof("ifdef") - 1,    PIFDEF},
    {"ifndef",  sizeof("ifndef") - 1,   PIFNDEF},
    {"if",      sizeof("if") - 1,       PIF},
    {"else",    sizeof("else") - 1,     PELSE},
    {"elif",    sizeof("elif") - 1,     PELIF},
    {"endif",   sizeof("endif") - 1,    PENDIF},
    {"define",  sizeof("define") - 1,   PDEFINE},
    {"undef",   sizeof("undef") - 1,    PUNDEF},
};
#define PPTYPELEN (sizeof(ppTypeMap) / sizeof(PPTypeDesc))
#define MAXNEST 1024

/*
 *  ======== ppType ========
 *  lexical parse of C-preprocessor line
 *
 *  Returns type of pre-processor line and the first token after
 *  the pre-processors directive (if it exists).
 */
static PPId ppType(String linebuf, String *token)
{
    Int i;
    
    *token = NULL;
    
    /* skip '#' and any intervening white space */
    for (++linebuf; *linebuf == ' ' || *linebuf == '\t'; linebuf++) {
        ;
    }

    /* lookup pre-processor token */
    for (i = 0; i < PPTYPELEN; i++) {
        PPTypeDesc *tp = ppTypeMap + i;
        if (strncmp(linebuf, tp->name, tp->len) == 0) {
            *token = strtok(linebuf + tp->len, " \r\n\t");
            return (tp->id);
        }
    }

    return (PUNKNOWN);
}

/*
 *  ======== skipLine ========
 *  Determine if a line should be read by the C pre-processor
 *
 *  This function eliminates some (but not all) #include lines
 *  that are conditionally eliminated by the C pre-processor.  It should
 *  never skip a line that is read by the pre-processor, but may include
 *  some that aren't
 *
 *  Returns TRUE if linebuf should be skipped; otherwise it return FALSE.
 */
static Bool skipLine(String linebuf)
{
    static State stateStack[MAXNEST + 1] = {SREAD};
    static Int   levelStack[MAXNEST + 1] = {0};
    static int   stackIndex = 0;

    static State curState = SREAD;
    static Int nestingLevel = 0;
    static Int skipLevel = 0;
    
    Bool result = FALSE;
    PPId type;
    String token;
    
    /* linebug == NULL => reset state related to file-scope */
    if (linebuf == NULL) {
        stateStack[0] = SREAD;  /* stack of state values */
        levelStack[0] = 0;      /* stack of skipIndex values */
        stackIndex = 0;         /* next slot to write into */
        curState = SREAD;
        nestingLevel = 0;       /* current nesting level of #if statements */
        skipLevel = 0;          /* level to skip to */

        return (FALSE);
    }

    /* if this is a pre-processor line we need to update the current state */
    if (linebuf[0] == '#') {
        result = TRUE;
        type = ppType(linebuf, &token);

        switch (type) {
            case PINCLUDE:
            case PUNDEF:
            case PDEFINE: {
                if (curState != SSKIP && curState != SSKIPTOEND) {
                    unknownDefs = TRUE; /* don't know value of defs any more */
                    result = type == PINCLUDE ? FALSE : TRUE;
                }
                else {
/*                    printf("skipping: %s\n", token); */
                }
                break;
            }

            case PIF:
            case PIFDEF:
            case PIFNDEF: {
                nestingLevel++;
                if (curState != SSKIP && curState != SSKIPTOEND) {
                    if (stackIndex < MAXNEST) {
                        levelStack[stackIndex] = skipLevel;
                        stateStack[stackIndex++] = curState;
                    }

                    if (type == PIF || unknownDefs || stackIndex >= MAXNEST) {
                        curState = SUNKNOWN;
                    }
                    else {
                        Bool defined = getDef(token) != NULL;
                        if ((type == PIFDEF && defined)
                            || (type == PIFNDEF && !defined)) {
                            curState = SREAD;
                        }
                        else {
                            curState = SSKIP;
                            skipLevel = nestingLevel;
                        }
                    }

                }
                break;
            }

            case PELSE:
            case PELIF: {
                switch (curState) {
                    case SREAD: {
                        curState = SSKIPTOEND;
                        skipLevel = nestingLevel;
                        break;
                    }
                    case SSKIP: {
                        if (skipLevel == nestingLevel) {
                            curState = type == PELIF ? SUNKNOWN : SREAD;
                        }
                        break;
                    }
                }
                break;
            }
            
            case PENDIF: {
                if (( (curState == SSKIP || curState == SSKIPTOEND)
                      && skipLevel == nestingLevel)
                    || curState == SREAD
                    || curState == SUNKNOWN) {

                    if (stackIndex > 0 && stackIndex < MAXNEST) {
                        stackIndex--;
                        curState = stateStack[stackIndex];
                        skipLevel = levelStack[stackIndex];
                    }
                }
                nestingLevel--;
                break;
            }

            default: {
                result = FALSE;
                break;
            }
        }
    }
    else {
        /* this is not a pre-processor line, use current state */
        if (curState == SSKIP || curState == SSKIPTOEND) {
            result = TRUE;
        }
    }

    return (result);
}
