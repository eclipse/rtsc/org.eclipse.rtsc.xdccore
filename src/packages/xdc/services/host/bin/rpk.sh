#! /bin/sh
#
#  redefine package of a Java source file
#
#  usage: rpk [-f] package-name java-source ...
#	       -f - overwrite file (otherwise create file,rpk)
#
#

status=1
trap 'rm -f /tmp/$$* ; exit $status' 0 2 3

usage="usage: $0 [-f] new-package-name java-source-file ..."
script=/tmp/$$,rpk
fflag="no"

while test `expr "$1" : "-.*"` != "0"; do
    case $1 in
	-f )  fflag="yes"; shift;;
	* )   echo "$usage"
              exit 1;;
    esac
done

if [ "$#" = "0" -o "$#" = "1" ]; then
    echo "$usage"
    exit 1
fi

#
#  set name to package name and shift arguments so all remaining
#  arguments are Java source files
#
name="$1"
shift

#
#  create appropriate sed script
#
if [ "$name" = "" ]; then
    #
    # create sed script which does the following:
    #    1. comment out the first occurance of package statement
    #
    cat > $script <<EOF
	s/^package\([ \t][ \t]*\).*$/\/\/package default;/
EOF
else
    #
    # create sed script which does the following:
    #    1. replace first occurance of package statement with new statement
    #    2. replace first "//package" line with new package statement
    #
    cat > $script <<EOF
	s/^package\([ \t][ \t]*\).*$/package\1$name;/
	s/^\/\/package\([ \t][ \t]*\).*$/package\1$name;/
EOF
fi

#
# apply sed script to all files and compare results for differences
#
for f do
    rm -f $f,rpk
    sed -f $script $f > $f,rpk
    if cmp -s $f $f,rpk ; then
	echo "$f: unchanged"
	rm -f $f,rpk
    else
	if test "$fflag" = "yes"; then
	    mv -f $f,rpk $f
	    echo "$f: modified"
	else
	    echo "$f,rpk: created"
	fi
    fi
done

status=0
