/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== rxgen.c ========
 *  Create script for remote execution of a specified command
 *
 *  Usage: rxgen [-o out-file] [-e env] [-a from=to] [cmd [arg ...]]
 *
 *  Output a Bourne shell script that runs the specified command (and
 *  arguments) with the environment variables specified via the -e
 *  options.
 *
 *  The command, its arguments, and the environment variable values are
 *  filtered to replace local file names with remote file names.
 *
 *  This command is used, for example, by tisb.gnurx.targets to run (from
 *  a Windows host) compiler/linker/archive commands that must run on a *nix
 *  host.
 *
 *  If no command is specified on the command line, rxgen filters stdin
 *  and writes the result to stdout.  In this case, -e options are
 *  ignored.
 *
 *  If the environment variable RX_MAPOPTIONS is set, it is parsed for
 *  additional -a options.  Command line -a options override any -a options
 *  specified by RX_MAPOPTIONS.
 *
 *  When run from Windows all network drives are implicitly mapped to
 *  their UNC names.  If the UNC name is mapped, the drive is mapped to the
 *  UNC's mapping.
 *
 *  Options:
 *	-o out-file	write the script to out-file *and* write the filtered
 *			pwd to stdout.  This allows clients to get both the
 *			script and the remote directory name without any
 *			additional commands in a shell script.
 *
 *	-e env		if env has an '=' character, then copy this definition
 *			into the output script.  Otherwise, get the value of
 *			this variable from the local environment, filter it
 *			and output the new definition; if the variable is not
 *			defined, output an "unset" command.
 *
 *	-a from=to	add mapping of the string from to the string to
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <xdc/std.h>

#if defined(xdc_target__os_Windows)
#include <direct.h>
#include <io.h>
#include <windows.h>
#include <process.h>
#else
#include <unistd.h>
#endif

#ifndef FILENAME_MAX
#define FILENAME_MAX    512
#endif

#define TOOLSENVNAME	"TOOLS"
#define UROOTENVNAME	"UNIXROOT"

#define MAXMAPLEN	128
#define MAXENVLEN	128
#define MAXLINE		(2 * FILENAME_MAX + 1)

#define BEGIN	    0
#define SEARCHING   1

typedef struct Map {
    String from;
    String to;
    SizeT  len;
} Map;

static Void addAlias(String from, String to);
static Void addEnv(String env);
static Void b2f(String path);
static String filterArg(String arg);
static String filterPath(String path);
static String getUnixCurDir(Void);
static String map(String path, SizeT *offset);
static Void parseEnv(Void);

#if defined(xdc_target__os_Windows)
static Int isLocalFile(const String pathToTest);
static Bool initDriveMap(Void);
#endif

static Map mapTab[MAXMAPLEN] = {
    {"//Sambasb/", "/db/", 10},
    {"//sambasb/", "/db/", 10}
};
static Int mapTabLen = 2;

static String envTab[MAXENVLEN];
static envTabLen = 0;

static String progName;	    /* name of program for error messages */

static int dflag = FALSE;   /* -d option flag */
static String outFile = NULL;

static Char lineBuf[MAXLINE + 1];

static String usage = "usage: %s [-d] [-e env] [-a from=to] [cmd [arg ...]]\n";

/*
 *  ======== main ========
 */
int main(int argc, String argv[])
{
    int	 i;
    FILE *out;
    String rwd;

    /* save our name */
    progName = *argv++;
    argc--;

    /* parse environment variable options first (to allow command line
     * options to override)
     */
    parseEnv();

    /*
     * Parse command line options and set argv and argc to reflect the
     * UNIX command's argument list
     */
    while ((argc > 0) && (argv[0][0] == '-')) {
	switch (argv[0][1]) {
	    case 'd':
		dflag = TRUE;
		break;

	    case 'a': {
		String from = argv[1];
		String to = strchr(from, '=');
		if (to != NULL) {
		    *to = '\0';	    /* null terminate from */
		    to++;	    /* point to the rval */
		}

		addAlias(from, to);
		argv++;
		argc--;
		
		break;
	    }

	    case 'e': {
		addEnv(argv[1]);
		argv++;
		argc--;
		
		break;
	    }

	    case 'o': {
		outFile = argv[1];
		argv++;
		argc--;
		break;
	    }
	    default: 
		fprintf(stderr, usage, progName);
		exit(1);
		break;
	}
	argv += 1;
	argc -= 1;
    }
	
#if defined(xdc_target__os_Windows)
    initDriveMap();
#endif

    /* otherwise generate script equivalent of the specified command */
    rwd = getUnixCurDir();

    /* open output stream, if necessary */
    if (outFile != NULL) {
	remove(outFile);
	out = fopen(outFile, "wb");
	if (out == NULL) {
	    fprintf(stderr, "%s: can't open %s for writing\n",
		progName, outFile);
            exit(1);
	}
	if (argc >= 1) {    /* only output if a cmd and -o is specified */
	    printf("%s\n", rwd);
	}
    }
    else {
	out = stdout;
    }

    /* if no command is specified, simply filter stdin */
    if (argc < 1) {
	/* filter stdin */
	String line;
	while ((line = fgets(lineBuf, sizeof(lineBuf), stdin)) != NULL) {
	    line = filterArg(line);
	    if (line != NULL) {
		fprintf(out, "%s", line);
		free(line);
	    }
	}
	exit(0);
    }

    /* cd to appropriate working directory */
    fprintf(out, "cd %s\n", rwd);
    
    /* set the environment */
    for (i = 0; i < envTabLen; i++) {
	if (strchr(envTab[i], '=') != NULL) {
	    fprintf(out, "export %s\n", envTab[i]);
	}
	else {
	    String val;
	    if ((val = getenv(envTab[i])) != NULL) {
		fprintf(out, "export %s=\"%s\"\n", envTab[i], filterArg(val));
	    }
	    else {
		fprintf(out, "unset %s\n", envTab[i]);
	    }
	}
    }
    fprintf(out, "export RX_MAPOPTIONS=\"");
    for (i = 0; i < mapTabLen; i++) {
	fprintf(out, "-a %s=%s ", mapTab[i].from, mapTab[i].to);
    }
    fprintf(out, "\"\n");
    
    /* run command with arguments */
    for (i = 0; i < argc; i++) {
	fprintf(out, "'%s' ",
	    i == 0 ? filterPath(argv[i]) : filterArg(argv[i]));
    }
    fputc('\n', out);

    return (0);
}

/*
 *  ======== addAlias ========
 */
static Void addAlias(String from, String to)
{
    Map *map = NULL;
    Int i;
    
    if (mapTabLen >= MAXMAPLEN) {
	fprintf(stderr, "%s: too many aliases\n", progName);
	exit(1);
    }

    /* redefine any existing map for from string */
    for (i = 0; i < mapTabLen; i++) {
#if defined(xdc_target__os_Windows)
	if (stricmp(mapTab[i].from, from) == 0) {
#else
	if (strcmp(mapTab[i].from, from) == 0) {
#endif
	    map = mapTab + i;
	    break;
	}
    }

    /* otherwise allocate a new mapping */
    if (map == NULL) {
	map = &mapTab[mapTabLen++];
    }
    
    /* initialize specified mapping */
    map->from = from;
    map->to = to;
    if (map->to == NULL) {
	map->to = "";
    }
    map->len = strlen(map->from);
}

/*
 *  ======== addEnv ========
 */
static Void addEnv(String env)
{
    if (envTabLen >= MAXENVLEN) {
	fprintf(stderr, "%s: too many environment variables\n", progName);
	exit(1);
    }

    envTab[envTabLen++] = env;
}

/*
 *  ======== append ========
 */
static Void append(String *dst, SizeT *dlen, String suffix, SizeT slen)
{
    SizeT plen = strlen(*dst);

    if ((plen + slen) >= *dlen) {
	if (*dlen == 0) {
	    *dst = (String)malloc(slen + 128);
            if (*dst == NULL) {
                fprintf(stderr, "%s: out of memory\n", progName);
                exit(1);
            }
	    (*dst)[0] = '\0';
	}
	else {
	    *dst = (String)realloc(*dst, *dlen + slen + 128);
            if (*dst == NULL) {
                fprintf(stderr, "%s: out of memory\n", progName);
                exit(1);
            }
	}
	*dlen += slen + 128;
    }
    strncat(*dst, suffix, slen);
    (*dst)[plen + slen] = '\0';

/*    printf("#append: '%s'\n", *dst); */
}

/*
 *  ======== b2f ========
 */
static Void b2f(String path)
{
    Char ch;
    Char *cp = path;
    
    /* replace \ with / */
    while ((ch = *cp) != '\0') {
	if (ch == '\\') {
	    ch = '/';
	}
	*cp++ = ch;
    }
}

#if 0
/*
 *  ======== cat ========
 *  Dump file contents to stdout 
 */
static Void cat(FILE *file)
{
    char c;
    
    fflush(file);
    rewind(file);
    while ((c = getc(file)) != EOF) {
	putc(c, stdout);
    }
    putc('\n', stdout);
}
#endif

/*
 *  ======== filterArg ========
 *
 *  Convert DOS path to UNIX path. Consists of replacing '\' with '/'.
 *
 *  Assumes input string is null-terminated.
 */
static String filterArg(String arg)
{
    SizeT offset;
    String alias;
    Int state = BEGIN;
    Char *src, *cp;
    SizeT len = 0;
    String result = "";
    
    /* replace \ with / */
    b2f(arg);
    
    for (cp = src = arg; cp[0] != '\0';) {
	if (state == BEGIN) {
	    /* look for a replacement */
	    alias = map(cp, &offset);
	    if (alias == NULL) {
		if (cp[0] == '/' && cp[1] != '/') {
		    /* assume we are in the middle of a path */
		    state = SEARCHING;
		}
		cp++;
	    }
	    else {
		/* append src -> cp to dst followed by alias */
		append(&result, &len, src, cp - src);
		append(&result, &len, alias, strlen(alias));

		/* update src and cp pointers */
		state = SEARCHING;
		cp = src = cp + offset;
	    }
	}
	else if (state == SEARCHING) {
	    /* look for a path termination character */
	    if (isspace(cp[0])
		|| (!isalnum(cp[0]) && cp[0] != '/' && cp[0] != '_')) {
		state = BEGIN;
	    }
	    cp++;
	}
    }

    /* append any remainder (stuff between src and cp) */
    append(&result, &len, src, cp - src);

    return (result);
}


/*
 *  ======== filterPath ========
 *
 *  Convert DOS path to UNIX path. Consists of replacing '\' with '/'.
 *
 *  Assumes input string is null-terminated.
 */
static String filterPath(String path)
{
    SizeT offset;
    String result, alias;

    /* replace \ with / */
    b2f(path);

    /* map prefix as specified in mapTab */
    alias = map(path, &offset);
    if (alias == NULL) {
        alias = "";
    }

    /* allocate a string buffer large enough to hold new string */
    result = (String)malloc(strlen(path) + strlen(alias) + 1);
    if (result == NULL) {
	fprintf(stderr, "out of memory\n");
	exit(1);
    }
    result[0] = '\0';

    strcat(result, alias);
    strcat(result, path + offset);

    return (result);
}

#if 0
/*
 *  ======== getTmpFile ========
 */
static FILE *getTmpFile(String name)
{
    FILE *tmpFile = NULL;
    char tmpName[13];
    int	 cnt = 0;
    
    do {
	sprintf(tmpName, "uexec%03d.@@@", cnt++);
    } while ((access(tmpName, 0) == 0) && cnt < 100);

    if (cnt < 100) {
	strcpy(name, tmpName);
	tmpFile = fopen(tmpName, "wb");
    }

    return (tmpFile);
}
#endif

/*
 *  ======== getUnixCurDir ========
 */
static String getUnixCurDir()
{
    static char curDir[FILENAME_MAX + 2];

    if (getcwd(curDir, sizeof(curDir)) == NULL) {
	fprintf(stderr, "%s: could not get current working directory\n",
	    progName);
	perror(progName);
	exit(1);
    }

    return (filterPath(curDir));
}

#if defined(xdc_target__os_Windows)
/*
 *  ======== isLocalFile ========
 */
static int isLocalFile(const String pathToTest)
{
    char path[256];

    if (pathToTest == NULL) {
	getcwd(path, sizeof(path));
	path[3] = '\0';
    }
    else {
	if (pathToTest[1] == ':') {
	    path[0] = pathToTest[0];
	    path[1] = ':';
	    path[2] = '/';
	    path[3] = '\0';
	}
	else {
	    path[0] = '/';
	    path[1] = '\0';
	}
    }

    return (GetDriveType(path) != DRIVE_REMOTE);
}
#endif

#if 0
/*
 *  ======== drive2tree ========
 */
static String drive2tree(String path)
{
    static Char tree[] = {'a', 't', 'r', 'e', 'e', '\0'};
    
    if (path[1] == ':') {
	Char drive = path[0];
	switch (drive) {
	    case 'h':
	    case 'H': {
		return ("home");
	    }

	    case 'i':
	    case 'I': {
		return ("atree");
	    }

	    case 'q':
	    case 'Q': {
		return ("public");
	    }

	    case 't':
	    case 'T': {
		return ("toolsrc");
	    }

	    /* handle gprsuvwxz */
	    default: {
		tree[0] = tolower(drive);
		return (tree);
	    }
	}
    }
    return ("");
}
#endif

#if defined(xdc_target__os_Windows)
/*
 *  ======== initDriveMap ========
 */
static Bool initDriveMap(Void)
{ 
    DWORD dwResult, dwResultEnum;
    HANDLE hEnum;
    DWORD cbBuffer = 16384; /* 16K is a good size */
    DWORD cEntries = -1;    /* enumerate all possible entries */
    LPNETRESOURCE nrTab;    /* pointer to enumerated structures */
    UInt i;
    

    /* Call the WNetOpenEnum function to begin the enumeration. */
    dwResult = WNetOpenEnum(RESOURCE_CONNECTED, /* local resources */
			  RESOURCETYPE_ANY,	/* all resources */
			  0,        /* enumerate all resources */
			  NULL,     /* NULL first time this fxn is called */
			  &hEnum);  /* handle to the resource */
    
    if (dwResult != NO_ERROR) {  
	return (FALSE);
    }
    
    /* allocate resource table */
    nrTab = (LPNETRESOURCE)malloc(cbBuffer);
    if (nrTab == NULL) {
	WNetCloseEnum(hEnum);
	return (FALSE);
    }
    
    do {  
	/* Initialize the buffer */
	memset(nrTab, '\0', cbBuffer);
	
	/* get enumeration of net resources */
	dwResultEnum = WNetEnumResource(hEnum,      /* resource handle */
					&cEntries,  /* defined locally as -1 */
					nrTab,	    /* LPNETRESOURCE */
					&cbBuffer); /* buffer size */

	/* If the call succeeds, loop through the structures. */
	if (dwResultEnum == NO_ERROR) {
	    for (i = 0; i < cEntries; i++) {
		String lname = nrTab[i].lpLocalName;
		if (lname != NULL && lname[1] == ':') {
		    String from = strdup(lname);
		    String to = filterPath(nrTab[i].lpRemoteName);
		    if (from == NULL || to == NULL) {
			fprintf(stderr, "%s: output of memory\n", progName);
			exit(1);
		    }
		    b2f(from);
		    addAlias(from, to);
		}
	    }
	}
	else if (dwResultEnum != ERROR_NO_MORE_ITEMS) {
	    break;	/* process errors */
	}
    } while(dwResultEnum != ERROR_NO_MORE_ITEMS);
    
    /* free resource table */
    free(nrTab);
    
    /* Call WNetCloseEnum to end the enumeration. */
    dwResult = WNetCloseEnum(hEnum);
    if (dwResult != NO_ERROR) { 
	return (FALSE);
    }
    
    return (TRUE);
}
#endif

/*
 *  ======== map ========
 */
static String map(String path, SizeT *offset)
{
    Int i;

    for (i = 0; i < mapTabLen; i++) {
	Map *map = mapTab + i;
#if defined(xdc_target__os_Windows)
	if (strnicmp(map->from, path, map->len) == 0) {
#else
	if (strncmp(map->from, path, map->len) == 0) {
#endif
	    *offset = map->len;
	    return (map->to);
	}
    }
    *offset = 0;
    return (NULL);
}    

/*
 *  ======== parseEnv ========
 */
static Void parseEnv(Void)
{
    String token;
    String opts = getenv("RX_MAPOPTIONS");
    if (opts == NULL) {
	return;
    }

    if ((opts = (String)strdup(opts)) == NULL) {
	fprintf(stderr, "%s: strdup failed\n", progName);
	exit(1);
    }
    
    token = strtok(opts, " \t\n\r");
    for (; token != NULL; token = strtok(NULL, " \t\n\r")) {
	if (strcmp("-a", token) == 0) {
	    String from = strtok(NULL, " \t\n\r");
            if (from != NULL) {
                String to = strchr(from, '=');
                if (to != NULL) {
                    *to = '\0';	    /* null terminate from */
                    to++;	    /* point to the rval */
                }
                addAlias(from, to);
            }
	}
    }
}
