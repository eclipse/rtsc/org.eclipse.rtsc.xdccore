#!/bin/sh
if [ $# -lt 1 ]; then
    echo usage $0 dir
    exit 1
fi

tmpd=.$$tmpd
tmpd=tmp

rm -rf $tmpd
mkdir $tmpd
fin=$tmpd/fin.inc
tin=$tmpd/tin.inc

echo $1 > $fin
/db/ztree/dr/xdcutils/src/xdc/services/host/bin/fixlist.x86U $fin "" $tin toto $tmpd/tin.dep

echo fixlist done ...

tar cf $tmpd/tin.tar -T $tin
echo tar done ...

tar cf $tmpd/fin.tar -T $fin
echo old tar done ...

if false; then
    tar tf $tmpd/fin.tar > $tmpd/fin.lst
    tar tf $tmpd/tin.tar > $tmpd/tin.lst
    cat $tmpd/tin.lst $tmpd/fin.lst | sort | uniq -u | grep -v '/$'
else
    mkdir $tmpd/tdir $tmpd/fdir
    tar xf $tmpd/tin.tar -C $tmpd/tdir
    tar xf $tmpd/fin.tar -C $tmpd/fdir
    tdiff $tmpd/tdir $tmpd/fdir | grep -v "fe "
fi
