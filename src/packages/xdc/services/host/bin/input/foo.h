#ifdef __config__

#include "bar1.h"

#else

#    ifndef __config__
#    include "bar2.h"
#    elif defined(foo)
#    include "bar3.h"
#    else
#    include "bar4.h"
#    endif

#include "bar5.h"
#endif
