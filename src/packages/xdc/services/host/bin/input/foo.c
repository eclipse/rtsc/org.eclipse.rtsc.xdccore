#ifndef __config__
char __dummy__;
#define __xdc_PKGVERS 1, 0, 0, 0
#define __xdc_PKGNAME ti.sysbios.knl
#define __xdc_PKGPREFIX ti_sysbios_knl_
#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

#else
#include <string.h>

#ifndef xdc_runtime_System__include
#ifndef __nested__
#define __nested__
#include <xdc/runtime/System.h>
#undef __nested__
#else
#include <xdc/runtime/System.h>
#endif
#endif

#ifndef xdc_runtime_Memory__include
#ifndef __nested__
#define __nested__
#include <xdc/runtime/Memory.h>
#undef __nested__
#else
#include <xdc/runtime/Memory.h>
#endif
#endif

#ifndef xdc_runtime_Error__include
#ifndef __nested__
#define __nested__
#include <xdc/runtime/Error.h>
#undef __nested__
#else
#include <xdc/runtime/Error.h>
#endif
#endif

#ifdef ti_sysbios_knl_Idle___used
/*
 *  ======== module Idle ========
 *  Do not modify this file; it is generated from the specification Idle.xdc
 *  and any modifications risk being overwritten.
 */

#ifndef ti_sysbios_knl_Idle__include
#ifndef __nested__
#define __nested__
#include <ti/sysbios/knl/Idle.h>
#undef __nested__
#else
#include <ti/sysbios/knl/Idle.h>
#endif
#endif

#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Idle___LOGOBJ) && ti_sysbios_knl_Idle___DGSINCL & 0x1
#define ti_sysbios_knl_Idle___L_ENTRY 1
#else
#define ti_sysbios_knl_Idle___L_ENTRY 0
#endif
#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Idle___LOGOBJ) && ti_sysbios_knl_Idle___DGSINCL & 0x2
#define ti_sysbios_knl_Idle___L_EXIT 1
#else
#define ti_sysbios_knl_Idle___L_EXIT 0
#endif
#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Idle___LOGOBJ) && ti_sysbios_knl_Idle___DGSINCL & 0x4
#define ti_sysbios_knl_Idle___L_LIFECYCLE 1
#else
#define ti_sysbios_knl_Idle___L_LIFECYCLE 0
#endif
#ifndef __isrom__

#undef Module__MID
#define Module__MID ti_sysbios_knl_Idle_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Idle_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Idle_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Idle_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Idle_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Idle_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Idle_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Idle_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Idle_Module__gateObj__S()
#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Idle_Module__gatePrms__S()
#undef Module__GF_alloc
#define Module__GF_alloc ti_sysbios_knl_Idle_Module__G_alloc__S
#undef Module__GF_enter
#define Module__GF_enter ti_sysbios_knl_Idle_Module__G_enter__S
#undef Module__GF_free
#define Module__GF_free ti_sysbios_knl_Idle_Module__G_free__S
#undef Module__GF_leave
#define Module__GF_leave ti_sysbios_knl_Idle_Module__G_leave__S
#undef Module__GF_query
#define Module__GF_query ti_sysbios_knl_Idle_Module__G_query__S
#undef __FN__
#if ti_sysbios_knl_Idle___scope == -1
#define __FN__ ti_sysbios_knl_Idle_loop__R
#else
#define __FN__ ti_sysbios_knl_Idle_loop__F
#endif
xdc_Void ti_sysbios_knl_Idle_loop__E( xdc_UArg arg1, xdc_UArg arg2 ) {
#if ti_sysbios_knl_Idle___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Idle_loop__ENTRY_EVT, (xdc_IArg)arg1, (xdc_IArg)arg2);
#endif
#if ti_sysbios_knl_Idle___L_EXIT
    __FN__(arg1, arg2);
    xdc_runtime_Log_write1(ti_sysbios_knl_Idle_loop__EXIT_EVT, 0);
#else
    __FN__(arg1, arg2);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Idle___scope == -1
#define __FN__ ti_sysbios_knl_Idle_run__R
#else
#define __FN__ ti_sysbios_knl_Idle_run__F
#endif
xdc_Void ti_sysbios_knl_Idle_run__E( void ) {
#if ti_sysbios_knl_Idle___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Idle_run__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Idle___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Idle_run__EXIT_EVT, 0);
#else
    __FN__();
#endif
}

#if defined(ti_sysbios_knl_Idle___EXPORT) && defined(__ti__)
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Idle_loop__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Idle_run__E);
#endif
__FAR__ ti_sysbios_knl_Idle_Module__MTAB__C__qual ti_sysbios_knl_Idle_MTab__ ti_sysbios_knl_Idle_Module__MTAB__C = {
#if ti_sysbios_knl_Idle___scope == -1
#if ti_sysbios_knl_Idle___L_ENTRY || ti_sysbios_knl_Idle___L_EXIT
    ti_sysbios_knl_Idle_loop__E,
#else
    ti_sysbios_knl_Idle_loop__R,
#endif
#if ti_sysbios_knl_Idle___L_ENTRY || ti_sysbios_knl_Idle___L_EXIT
    ti_sysbios_knl_Idle_run__E,
#else
    ti_sysbios_knl_Idle_run__R,
#endif
    0,  /* Handle__label */
    0,  /* Module__gateObj */
    0,  /* Module__gatePrms */
    0,  /* Module__G_alloc */
    0,  /* Module__G_enter */
    0,  /* Module__G_free */
    0,  /* Module__G_leave */
    0,  /* Module__G_query */
    ti_sysbios_knl_Idle_Module__startupDone__S,
    0,  /* Object__create */
    0,  /* Object__delete */
    0,  /* Object__destruct */
    0,  /* Object__get */
    0,  /* Params__init */
    0,  /* Proxy__abstract */
    0,  /* Proxy__delegate */
#else
0
#endif
};
xdc_Bool ti_sysbios_knl_Idle_Module__startupDone__S() { return 1; }

#else /* __isrom__ */

#ifndef ti_sysbios_knl_Idle___ROMPATCH
#undef Module__MID
#define Module__MID ti_sysbios_knl_Idle_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Idle_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Idle_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Idle_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Idle_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Idle_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Idle_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Idle_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Idle_Module__gateObj__S()
#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Idle_Module__gatePrms__S()
#undef Module__GF_alloc
#define Module__GF_alloc ti_sysbios_knl_Idle_Module__G_alloc__S
#undef Module__GF_enter
#define Module__GF_enter ti_sysbios_knl_Idle_Module__G_enter__S
#undef Module__GF_free
#define Module__GF_free ti_sysbios_knl_Idle_Module__G_free__S
#undef Module__GF_leave
#define Module__GF_leave ti_sysbios_knl_Idle_Module__G_leave__S
#undef Module__GF_query
#define Module__GF_query ti_sysbios_knl_Idle_Module__G_query__S
#undef __FN__
#if ti_sysbios_knl_Idle___scope == -1
#define __FN__ ti_sysbios_knl_Idle_loop__R
#else
#define __FN__ ti_sysbios_knl_Idle_loop__F
#endif
xdc_Void ti_sysbios_knl_Idle_loop__E( xdc_UArg arg1, xdc_UArg arg2 ) {
#if ti_sysbios_knl_Idle___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Idle_loop__ENTRY_EVT, (xdc_IArg)arg1, (xdc_IArg)arg2);
#endif
#if ti_sysbios_knl_Idle___L_EXIT
    __FN__(arg1, arg2);
    xdc_runtime_Log_write1(ti_sysbios_knl_Idle_loop__EXIT_EVT, 0);
#else
    __FN__(arg1, arg2);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Idle___scope == -1
#define __FN__ ti_sysbios_knl_Idle_run__R
#else
#define __FN__ ti_sysbios_knl_Idle_run__F
#endif
xdc_Void ti_sysbios_knl_Idle_run__E( void ) {
#if ti_sysbios_knl_Idle___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Idle_run__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Idle___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Idle_run__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#endif /* ti_sysbios_knl_Idle___ROMPATCH */
#ifdef ti_sysbios_knl_Idle___ROMPATCH
xdc_Void ti_sysbios_knl_Idle_loop__E( xdc_UArg arg1, xdc_UArg arg2 ) {
    ti_sysbios_knl_Idle_Module__MTAB__C.loop(arg1, arg2);
}
xdc_Void ti_sysbios_knl_Idle_run__E( void ) {
    ti_sysbios_knl_Idle_Module__MTAB__C.run();
}
#endif /* ti_sysbios_knl_Idle___ROMPATCH */
xdc_Void ti_sysbios_knl_Idle_loop__R( xdc_UArg arg1, xdc_UArg arg2 ) {
    ti_sysbios_knl_Idle_loop__F(arg1, arg2);
}
xdc_Void ti_sysbios_knl_Idle_run__R( void ) {
    ti_sysbios_knl_Idle_run__F();
}
xdc_runtime_Types_Label* ti_sysbios_knl_Idle_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Handle__label(obj, lab);}
xdc_Ptr ti_sysbios_knl_Idle_Module__gateObj__S( void ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Module__gateObj();}
xdc_Ptr ti_sysbios_knl_Idle_Module__gatePrms__S( void ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Module__gatePrms();}
xdc_runtime_Types_GateRef ti_sysbios_knl_Idle_Module__G_alloc__S( xdc_Ptr prms, xdc_runtime_Error_Block* eb ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Module__G_alloc(prms, eb);}
xdc_IArg ti_sysbios_knl_Idle_Module__G_enter__S( xdc_runtime_Types_GateRef ref ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Module__G_enter(ref);}
xdc_Void ti_sysbios_knl_Idle_Module__G_free__S( xdc_runtime_Types_GateRef ref ) {
    ti_sysbios_knl_Idle_Module__MTAB__C.Module__G_free(ref);}
xdc_Void ti_sysbios_knl_Idle_Module__G_leave__S( xdc_runtime_Types_GateRef ref, xdc_IArg key ) {
    ti_sysbios_knl_Idle_Module__MTAB__C.Module__G_leave(ref, key);}
xdc_Bool ti_sysbios_knl_Idle_Module__G_query__S( xdc_Int qual ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Module__G_query(qual);}
xdc_Bool ti_sysbios_knl_Idle_Module__startupDone__S( void ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Module__startupDone();}
xdc_Ptr ti_sysbios_knl_Idle_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Object__create(__oa, __aa, __pa, __sz, __eb);}
xdc_Void ti_sysbios_knl_Idle_Object__delete__S( xdc_Ptr instp ) {
    ti_sysbios_knl_Idle_Module__MTAB__C.Object__delete(instp);}
xdc_Void ti_sysbios_knl_Idle_Object__destruct__S( xdc_Ptr objp ) {
    ti_sysbios_knl_Idle_Module__MTAB__C.Object__destruct(objp);}
xdc_Ptr ti_sysbios_knl_Idle_Object__get__S( xdc_Ptr oarr, xdc_Int i ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Object__get(oarr, i);}
xdc_Void ti_sysbios_knl_Idle_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) {
    ti_sysbios_knl_Idle_Module__MTAB__C.Params__init(dst, src, psz, isz);}
xdc_Bool ti_sysbios_knl_Idle_Proxy__abstract__S( void ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Proxy__abstract();}
xdc_Ptr ti_sysbios_knl_Idle_Proxy__delegate__S( void ) {
    return ti_sysbios_knl_Idle_Module__MTAB__C.Proxy__delegate();}

#if defined(ti_sysbios_knl_Idle___EXPORT) && defined(__ti__)
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Idle_loop__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Idle_run__R);
#endif
#endif /* __isrom__ */
#endif /* $__used__ */

#ifdef ti_sysbios_knl_Swi___used
/*
 *  ======== module Swi ========
 *  Do not modify this file; it is generated from the specification Swi.xdc
 *  and any modifications risk being overwritten.
 */

#ifndef ti_sysbios_knl_Swi__include
#ifndef __nested__
#define __nested__
#include <ti/sysbios/knl/Swi.h>
#undef __nested__
#else
#include <ti/sysbios/knl/Swi.h>
#endif
#endif

#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Swi___LOGOBJ) && ti_sysbios_knl_Swi___DGSINCL & 0x1
#define ti_sysbios_knl_Swi___L_ENTRY 1
#else
#define ti_sysbios_knl_Swi___L_ENTRY 0
#endif
#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Swi___LOGOBJ) && ti_sysbios_knl_Swi___DGSINCL & 0x2
#define ti_sysbios_knl_Swi___L_EXIT 1
#else
#define ti_sysbios_knl_Swi___L_EXIT 0
#endif
#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Swi___LOGOBJ) && ti_sysbios_knl_Swi___DGSINCL & 0x4
#define ti_sysbios_knl_Swi___L_LIFECYCLE 1
#else
#define ti_sysbios_knl_Swi___L_LIFECYCLE 0
#endif
#ifndef __isrom__

#undef Module__MID
#define Module__MID ti_sysbios_knl_Swi_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Swi_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Swi_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Swi_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Swi_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Swi_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Swi_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Swi_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Swi_Module__gateObj__S()
#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Swi_Module__gatePrms__S()
#undef Module__GF_alloc
#define Module__GF_alloc ti_sysbios_knl_Swi_Module__G_alloc__S
#undef Module__GF_enter
#define Module__GF_enter ti_sysbios_knl_Swi_Module__G_enter__S
#undef Module__GF_free
#define Module__GF_free ti_sysbios_knl_Swi_Module__G_free__S
#undef Module__GF_leave
#define Module__GF_leave ti_sysbios_knl_Swi_Module__G_leave__S
#undef Module__GF_query
#define Module__GF_query ti_sysbios_knl_Swi_Module__G_query__S
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_disable__R
#else
#define __FN__ ti_sysbios_knl_Swi_disable__F
#endif
xdc_UInt ti_sysbios_knl_Swi_disable__E( void ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Swi_disable__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_disable__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_enable__R
#else
#define __FN__ ti_sysbios_knl_Swi_enable__F
#endif
xdc_Void ti_sysbios_knl_Swi_enable__E( void ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Swi_enable__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_enable__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_restore__R
#else
#define __FN__ ti_sysbios_knl_Swi_restore__F
#endif
xdc_Void ti_sysbios_knl_Swi_restore__E( xdc_UInt key ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restore__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restore__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_restoreHwi__R
#else
#define __FN__ ti_sysbios_knl_Swi_restoreHwi__F
#endif
xdc_Void ti_sysbios_knl_Swi_restoreHwi__E( xdc_UInt key ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restoreHwi__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restoreHwi__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_self__R
#else
#define __FN__ ti_sysbios_knl_Swi_self__F
#endif
ti_sysbios_knl_Swi_Handle ti_sysbios_knl_Swi_self__E( void ) {
#if ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_Handle __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Swi_self__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_self__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_getTrigger__R
#else
#define __FN__ ti_sysbios_knl_Swi_getTrigger__F
#endif
xdc_UInt ti_sysbios_knl_Swi_getTrigger__E( void ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Swi_getTrigger__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_getTrigger__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_raisePri__R
#else
#define __FN__ ti_sysbios_knl_Swi_raisePri__F
#endif
xdc_UInt ti_sysbios_knl_Swi_raisePri__E( xdc_UInt priority ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_raisePri__ENTRY_EVT, (xdc_IArg)priority);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__(priority);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_raisePri__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__(priority);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_restorePri__R
#else
#define __FN__ ti_sysbios_knl_Swi_restorePri__F
#endif
xdc_Void ti_sysbios_knl_Swi_restorePri__E( xdc_UInt key ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restorePri__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restorePri__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_andn__R
#else
#define __FN__ ti_sysbios_knl_Swi_andn__F
#endif
xdc_Void ti_sysbios_knl_Swi_andn__E( ti_sysbios_knl_Swi_Handle _this, xdc_UInt mask ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Swi_andn__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)mask);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this, mask);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_andn__EXIT_EVT, 0);
#else
    __FN__((void*)_this, mask);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_dec__R
#else
#define __FN__ ti_sysbios_knl_Swi_dec__F
#endif
xdc_Void ti_sysbios_knl_Swi_dec__E( ti_sysbios_knl_Swi_Handle _this ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_dec__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_dec__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_getHookContext__R
#else
#define __FN__ ti_sysbios_knl_Swi_getHookContext__F
#endif
xdc_Ptr ti_sysbios_knl_Swi_getHookContext__E( ti_sysbios_knl_Swi_Handle _this, xdc_Int id ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_Ptr __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Swi_getHookContext__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)id);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__((void*)_this, id);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_getHookContext__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this, id);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_setHookContext__R
#else
#define __FN__ ti_sysbios_knl_Swi_setHookContext__F
#endif
xdc_Void ti_sysbios_knl_Swi_setHookContext__E( ti_sysbios_knl_Swi_Handle _this, xdc_Int id, xdc_Ptr hookContext ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write3(ti_sysbios_knl_Swi_setHookContext__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)id, (xdc_IArg)hookContext);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this, id, hookContext);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_setHookContext__EXIT_EVT, 0);
#else
    __FN__((void*)_this, id, hookContext);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_getPri__R
#else
#define __FN__ ti_sysbios_knl_Swi_getPri__F
#endif
xdc_UInt ti_sysbios_knl_Swi_getPri__E( ti_sysbios_knl_Swi_Handle _this ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_getPri__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_getPri__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_inc__R
#else
#define __FN__ ti_sysbios_knl_Swi_inc__F
#endif
xdc_Void ti_sysbios_knl_Swi_inc__E( ti_sysbios_knl_Swi_Handle _this ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_inc__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_inc__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_or__R
#else
#define __FN__ ti_sysbios_knl_Swi_or__F
#endif
xdc_Void ti_sysbios_knl_Swi_or__E( ti_sysbios_knl_Swi_Handle _this, xdc_UInt mask ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Swi_or__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)mask);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this, mask);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_or__EXIT_EVT, 0);
#else
    __FN__((void*)_this, mask);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_post__R
#else
#define __FN__ ti_sysbios_knl_Swi_post__F
#endif
xdc_Void ti_sysbios_knl_Swi_post__E( ti_sysbios_knl_Swi_Handle _this ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_post__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_post__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#if ti_sysbios_knl_Swi___scope == -1
xdc_Int ti_sysbios_knl_Swi_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Swi_Module_startup__R(state); }
#else
xdc_Int ti_sysbios_knl_Swi_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Swi_Module_startup__F(state); }
#endif
#ifdef ti_sysbios_knl_Swi___OBJHEAP
#undef ti_sysbios_knl_Swi_Instance_init
#if ti_sysbios_knl_Swi___scope == -1
#define ti_sysbios_knl_Swi_Instance_init ti_sysbios_knl_Swi_Instance_init__R
#else
#define ti_sysbios_knl_Swi_Instance_init ti_sysbios_knl_Swi_Instance_init__F
#endif
#undef ti_sysbios_knl_Swi_Instance_finalize
#if ti_sysbios_knl_Swi___scope == -1
#define ti_sysbios_knl_Swi_Instance_finalize ti_sysbios_knl_Swi_Instance_finalize__R
#else
#define ti_sysbios_knl_Swi_Instance_finalize ti_sysbios_knl_Swi_Instance_finalize__F
#endif
#endif

#if defined(ti_sysbios_knl_Swi___EXPORT) && defined(__ti__)
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Handle__label__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Object__create__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Object__delete__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Object__destruct__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Object__get__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Params__init__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Module_startup__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_disable__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_enable__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_restore__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_restoreHwi__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_self__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_getTrigger__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_raisePri__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_restorePri__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_andn__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_dec__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_getHookContext__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_setHookContext__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_getPri__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_inc__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_or__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_post__E);
#endif
__FAR__ ti_sysbios_knl_Swi_Module__MTAB__C__qual ti_sysbios_knl_Swi_MTab__ ti_sysbios_knl_Swi_Module__MTAB__C = {
#if ti_sysbios_knl_Swi___scope == -1
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_disable__E,
#else
    ti_sysbios_knl_Swi_disable__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_enable__E,
#else
    ti_sysbios_knl_Swi_enable__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_restore__E,
#else
    ti_sysbios_knl_Swi_restore__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_restoreHwi__E,
#else
    ti_sysbios_knl_Swi_restoreHwi__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_self__E,
#else
    ti_sysbios_knl_Swi_self__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_getTrigger__E,
#else
    ti_sysbios_knl_Swi_getTrigger__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_raisePri__E,
#else
    ti_sysbios_knl_Swi_raisePri__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_restorePri__E,
#else
    ti_sysbios_knl_Swi_restorePri__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_andn__E,
#else
    ti_sysbios_knl_Swi_andn__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_dec__E,
#else
    ti_sysbios_knl_Swi_dec__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_getHookContext__E,
#else
    ti_sysbios_knl_Swi_getHookContext__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_setHookContext__E,
#else
    ti_sysbios_knl_Swi_setHookContext__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_getPri__E,
#else
    ti_sysbios_knl_Swi_getPri__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_inc__E,
#else
    ti_sysbios_knl_Swi_inc__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_or__E,
#else
    ti_sysbios_knl_Swi_or__R,
#endif
#if ti_sysbios_knl_Swi___L_ENTRY || ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_post__E,
#else
    ti_sysbios_knl_Swi_post__R,
#endif
    ti_sysbios_knl_Swi_Handle__label__S,
    0,  /* Module__gateObj */
    0,  /* Module__gatePrms */
    0,  /* Module__G_alloc */
    0,  /* Module__G_enter */
    0,  /* Module__G_free */
    0,  /* Module__G_leave */
    0,  /* Module__G_query */
    ti_sysbios_knl_Swi_Module__startupDone__S,
#ifdef ti_sysbios_knl_Swi___OBJHEAP
    ti_sysbios_knl_Swi_Object__create__S,
#else
    0,  /* Object__create */
#endif
#ifdef ti_sysbios_knl_Swi___DELETE
    ti_sysbios_knl_Swi_Object__destruct__S,
    ti_sysbios_knl_Swi_Object__delete__S,
#else
    0,  /* Object__destruct */
    0,  /* Object__delete */
#endif
    ti_sysbios_knl_Swi_Object__get__S,
#ifdef ti_sysbios_knl_Swi___OBJHEAP
    ti_sysbios_knl_Swi_Params__init__S,
#else
    0,  /* Params__init */
#endif
    0,  /* Proxy__abstract */
    0,  /* Proxy__delegate */
#else
0
#endif
};
xdc_runtime_Types_Label* ti_sysbios_knl_Swi_Handle__label__S( Ptr obj, xdc_runtime_Types_Label* lab ) {
    lab->oaddr = obj;
    lab->modid = ti_sysbios_knl_Swi_Module__id__D;
#ifdef ti_sysbios_knl_Swi___NAMEDINST
    xdc_runtime_Core_assignLabel(lab, ((ti_sysbios_knl_Swi_Object__*)obj)->__name, 1);
#else
    xdc_runtime_Core_assignLabel(lab, 0, 0);
#endif
    return lab;
}
xdc_Void ti_sysbios_knl_Swi_Params__init__S( xdc_Ptr prms, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) {
#ifdef ti_sysbios_knl_Swi___OBJHEAP
    xdc_runtime_Core_assignParams__I(prms, (xdc_Ptr)(src ? src : &ti_sysbios_knl_Swi_Object__PARAMS__C), psz, isz);
#endif
}
xdc_Ptr ti_sysbios_knl_Swi_Object__get__S( xdc_Ptr oa, xdc_Int i ) {
    if (oa) return ((ti_sysbios_knl_Swi_Object*)oa) + i;
#if ti_sysbios_knl_Swi_Object__count__D != 0
    return ti_sysbios_knl_Swi_Object__table__V + i;
#else
    return 0;
#endif
}

#ifdef ti_sysbios_knl_Swi___OBJHEAP
typedef struct { ti_sysbios_knl_Swi_Object2__ s0; char c; } ti_sysbios_knl_Swi___S1;
#ifdef __ti__
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Object__DESC__C, ".const:ti_sysbios_knl_Swi_Object__DESC__C");
asm("	.sect \".const:ti_sysbios_knl_Swi_Object__DESC__C\"");
asm("	 .clink");
asm("	.sect \"[0].const:ti_sysbios_knl_Swi_Object__DESC__C\"");
asm("	 .clink");
asm("	.sect \"[1].const:ti_sysbios_knl_Swi_Object__DESC__C\"");
asm("	 .clink");
#endif
const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Swi_Object__DESC__C = {
    (Fxn)ti_sysbios_knl_Swi_Instance_finalize, /* finalFxn */
    (Ptr)-1, /* fxnTab */
    &ti_sysbios_knl_Swi_Module__root__V.hdr.link, /* modLink */
    sizeof(ti_sysbios_knl_Swi___S1) - sizeof(ti_sysbios_knl_Swi_Object2__), /* objAlign */
    ti_sysbios_knl_Swi___OBJHEAP, /* objHeap */
#ifdef ti_sysbios_knl_Swi___NAMEDINST
    offsetof(ti_sysbios_knl_Swi_Object__, __name), /* objName */
#else
    0, /* objName */
#endif
    sizeof(ti_sysbios_knl_Swi_Object2__), /* objSize */
    (Ptr)&ti_sysbios_knl_Swi_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_knl_Swi_Params), /* prmsSize */
};
extern xdc_Ptr ti_sysbios_knl_Swi_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    ti_sysbios_knl_Swi_Params prms;
    ti_sysbios_knl_Swi_Object* obj;
#ifdef ti_sysbios_knl_Swi___DELETE
    int iStat;
#endif
    ti_sysbios_knl_Swi_Args__create* args = __aa;
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, __oa, &prms, __pa, __sz, __eb);
    if (!obj) return 0;
#ifdef ti_sysbios_knl_Swi___DELETE
#define ti_sysbios_knl_Swi___ISTAT iStat = 
#else
#define ti_sysbios_knl_Swi___ISTAT
#endif
    ti_sysbios_knl_Swi___ISTAT ti_sysbios_knl_Swi_Instance_init(obj, args->fxn, &prms, __eb);
    if (xdc_runtime_Error_check(__eb)) {
#ifdef ti_sysbios_knl_Swi___DELETE
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, obj, iStat, __oa == 0);
#endif
        return 0;
    }
#if ti_sysbios_knl_Swi___L_LIFECYCLE
#ifdef ti_sysbios_knl_Swi___NAMEDINST
    xdc_runtime_Log_write2(__oa ? xdc_runtime_Log_L_construct : xdc_runtime_Log_L_create, (xdc_IArg)obj, (xdc_IArg)(((ti_sysbios_knl_Swi_Object__*)obj)->__name));
#else
    xdc_runtime_Log_write2(__oa ? xdc_runtime_Log_L_construct : xdc_runtime_Log_L_create, (xdc_IArg)obj, 0);
#endif
#endif
    return obj;
}
#else
extern xdc_Ptr ti_sysbios_knl_Swi_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    xdc_runtime_Error_raiseX(NULL, ti_sysbios_knl_Swi_Module__id__C, NULL, 0, xdc_runtime_Error_E_generic, (xdc_IArg)"create policy error", 0);
    return 0;
}
#endif

#ifdef ti_sysbios_knl_Swi___DELETE
extern xdc_Void ti_sysbios_knl_Swi_Object__destruct__S( xdc_Ptr obj ) {
#if ti_sysbios_knl_Swi___L_LIFECYCLE
    xdc_runtime_Log_write1(xdc_runtime_Log_L_destruct, (xdc_IArg)obj);
#endif
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, obj, 0, TRUE);
}
extern xdc_Void ti_sysbios_knl_Swi_Object__delete__S( xdc_Ptr instp ) {
#if ti_sysbios_knl_Swi___L_LIFECYCLE
    xdc_runtime_Log_write1(xdc_runtime_Log_L_delete, (xdc_IArg)(*((ti_sysbios_knl_Swi_Object**)instp)));
#endif
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, *((ti_sysbios_knl_Swi_Object**)instp), 0, FALSE);
    *((ti_sysbios_knl_Swi_Handle*)instp) = 0;
}
#else
extern xdc_Void ti_sysbios_knl_Swi_Object__destruct__S( xdc_Ptr obj ) {
    xdc_runtime_Error_raiseX(NULL, ti_sysbios_knl_Swi_Module__id__C, NULL, 0, xdc_runtime_Error_E_generic, (xdc_IArg)"delete policy error", 0);
}
extern xdc_Void ti_sysbios_knl_Swi_Object__delete__S( xdc_Ptr instp ) {
    xdc_runtime_Error_raiseX(NULL, ti_sysbios_knl_Swi_Module__id__C, NULL, 0, xdc_runtime_Error_E_generic, (xdc_IArg)"delete policy error", 0);
}
#endif

#else /* __isrom__ */

#ifndef ti_sysbios_knl_Swi___ROMPATCH
#undef Module__MID
#define Module__MID ti_sysbios_knl_Swi_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Swi_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Swi_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Swi_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Swi_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Swi_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Swi_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Swi_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Swi_Module__gateObj__S()
#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Swi_Module__gatePrms__S()
#undef Module__GF_alloc
#define Module__GF_alloc ti_sysbios_knl_Swi_Module__G_alloc__S
#undef Module__GF_enter
#define Module__GF_enter ti_sysbios_knl_Swi_Module__G_enter__S
#undef Module__GF_free
#define Module__GF_free ti_sysbios_knl_Swi_Module__G_free__S
#undef Module__GF_leave
#define Module__GF_leave ti_sysbios_knl_Swi_Module__G_leave__S
#undef Module__GF_query
#define Module__GF_query ti_sysbios_knl_Swi_Module__G_query__S
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_disable__R
#else
#define __FN__ ti_sysbios_knl_Swi_disable__F
#endif
xdc_UInt ti_sysbios_knl_Swi_disable__E( void ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Swi_disable__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_disable__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_enable__R
#else
#define __FN__ ti_sysbios_knl_Swi_enable__F
#endif
xdc_Void ti_sysbios_knl_Swi_enable__E( void ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Swi_enable__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_enable__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_restore__R
#else
#define __FN__ ti_sysbios_knl_Swi_restore__F
#endif
xdc_Void ti_sysbios_knl_Swi_restore__E( xdc_UInt key ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restore__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restore__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_restoreHwi__R
#else
#define __FN__ ti_sysbios_knl_Swi_restoreHwi__F
#endif
xdc_Void ti_sysbios_knl_Swi_restoreHwi__E( xdc_UInt key ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restoreHwi__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restoreHwi__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_self__R
#else
#define __FN__ ti_sysbios_knl_Swi_self__F
#endif
ti_sysbios_knl_Swi_Handle ti_sysbios_knl_Swi_self__E( void ) {
#if ti_sysbios_knl_Swi___L_EXIT
    ti_sysbios_knl_Swi_Handle __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Swi_self__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_self__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_getTrigger__R
#else
#define __FN__ ti_sysbios_knl_Swi_getTrigger__F
#endif
xdc_UInt ti_sysbios_knl_Swi_getTrigger__E( void ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Swi_getTrigger__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_getTrigger__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_raisePri__R
#else
#define __FN__ ti_sysbios_knl_Swi_raisePri__F
#endif
xdc_UInt ti_sysbios_knl_Swi_raisePri__E( xdc_UInt priority ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_raisePri__ENTRY_EVT, (xdc_IArg)priority);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__(priority);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_raisePri__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__(priority);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_restorePri__R
#else
#define __FN__ ti_sysbios_knl_Swi_restorePri__F
#endif
xdc_Void ti_sysbios_knl_Swi_restorePri__E( xdc_UInt key ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restorePri__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_restorePri__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_andn__R
#else
#define __FN__ ti_sysbios_knl_Swi_andn__F
#endif
xdc_Void ti_sysbios_knl_Swi_andn__E( ti_sysbios_knl_Swi_Handle _this, xdc_UInt mask ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Swi_andn__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)mask);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this, mask);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_andn__EXIT_EVT, 0);
#else
    __FN__((void*)_this, mask);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_dec__R
#else
#define __FN__ ti_sysbios_knl_Swi_dec__F
#endif
xdc_Void ti_sysbios_knl_Swi_dec__E( ti_sysbios_knl_Swi_Handle _this ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_dec__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_dec__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_getHookContext__R
#else
#define __FN__ ti_sysbios_knl_Swi_getHookContext__F
#endif
xdc_Ptr ti_sysbios_knl_Swi_getHookContext__E( ti_sysbios_knl_Swi_Handle _this, xdc_Int id ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_Ptr __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Swi_getHookContext__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)id);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__((void*)_this, id);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_getHookContext__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this, id);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_setHookContext__R
#else
#define __FN__ ti_sysbios_knl_Swi_setHookContext__F
#endif
xdc_Void ti_sysbios_knl_Swi_setHookContext__E( ti_sysbios_knl_Swi_Handle _this, xdc_Int id, xdc_Ptr hookContext ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write3(ti_sysbios_knl_Swi_setHookContext__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)id, (xdc_IArg)hookContext);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this, id, hookContext);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_setHookContext__EXIT_EVT, 0);
#else
    __FN__((void*)_this, id, hookContext);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_getPri__R
#else
#define __FN__ ti_sysbios_knl_Swi_getPri__F
#endif
xdc_UInt ti_sysbios_knl_Swi_getPri__E( ti_sysbios_knl_Swi_Handle _this ) {
#if ti_sysbios_knl_Swi___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_getPri__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_getPri__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_inc__R
#else
#define __FN__ ti_sysbios_knl_Swi_inc__F
#endif
xdc_Void ti_sysbios_knl_Swi_inc__E( ti_sysbios_knl_Swi_Handle _this ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_inc__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_inc__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_or__R
#else
#define __FN__ ti_sysbios_knl_Swi_or__F
#endif
xdc_Void ti_sysbios_knl_Swi_or__E( ti_sysbios_knl_Swi_Handle _this, xdc_UInt mask ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Swi_or__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)mask);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this, mask);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_or__EXIT_EVT, 0);
#else
    __FN__((void*)_this, mask);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Swi___scope == -1
#define __FN__ ti_sysbios_knl_Swi_post__R
#else
#define __FN__ ti_sysbios_knl_Swi_post__F
#endif
xdc_Void ti_sysbios_knl_Swi_post__E( ti_sysbios_knl_Swi_Handle _this ) {
#if ti_sysbios_knl_Swi___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_post__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Swi___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Swi_post__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#if ti_sysbios_knl_Swi___scope == -1
xdc_Int ti_sysbios_knl_Swi_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Swi_Module_startup__R(state); }
#else
xdc_Int ti_sysbios_knl_Swi_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Swi_Module_startup__F(state); }
#endif
#endif /* ti_sysbios_knl_Swi___ROMPATCH */
#ifdef ti_sysbios_knl_Swi___ROMPATCH
xdc_UInt ti_sysbios_knl_Swi_disable__E( void ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.disable();
}
xdc_Void ti_sysbios_knl_Swi_enable__E( void ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.enable();
}
xdc_Void ti_sysbios_knl_Swi_restore__E( xdc_UInt key ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.restore(key);
}
xdc_Void ti_sysbios_knl_Swi_restoreHwi__E( xdc_UInt key ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.restoreHwi(key);
}
ti_sysbios_knl_Swi_Handle ti_sysbios_knl_Swi_self__E( void ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.self();
}
xdc_UInt ti_sysbios_knl_Swi_getTrigger__E( void ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.getTrigger();
}
xdc_UInt ti_sysbios_knl_Swi_raisePri__E( xdc_UInt priority ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.raisePri(priority);
}
xdc_Void ti_sysbios_knl_Swi_restorePri__E( xdc_UInt key ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.restorePri(key);
}
xdc_Void ti_sysbios_knl_Swi_andn__E( ti_sysbios_knl_Swi_Handle _this, xdc_UInt mask ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.andn((void*)_this, mask);
}
xdc_Void ti_sysbios_knl_Swi_dec__E( ti_sysbios_knl_Swi_Handle _this ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.dec((void*)_this);
}
xdc_Ptr ti_sysbios_knl_Swi_getHookContext__E( ti_sysbios_knl_Swi_Handle _this, xdc_Int id ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.getHookContext((void*)_this, id);
}
xdc_Void ti_sysbios_knl_Swi_setHookContext__E( ti_sysbios_knl_Swi_Handle _this, xdc_Int id, xdc_Ptr hookContext ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.setHookContext((void*)_this, id, hookContext);
}
xdc_UInt ti_sysbios_knl_Swi_getPri__E( ti_sysbios_knl_Swi_Handle _this ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.getPri((void*)_this);
}
xdc_Void ti_sysbios_knl_Swi_inc__E( ti_sysbios_knl_Swi_Handle _this ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.inc((void*)_this);
}
xdc_Void ti_sysbios_knl_Swi_or__E( ti_sysbios_knl_Swi_Handle _this, xdc_UInt mask ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.or((void*)_this, mask);
}
xdc_Void ti_sysbios_knl_Swi_post__E( ti_sysbios_knl_Swi_Handle _this ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.post((void*)_this);
}
#if ti_sysbios_knl_Swi___scope == -1
xdc_Int ti_sysbios_knl_Swi_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Swi_Module_startup__R(state); }
#else
xdc_Int ti_sysbios_knl_Swi_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Swi_Module_startup__F(state); }
#endif
#endif /* ti_sysbios_knl_Swi___ROMPATCH */
xdc_UInt ti_sysbios_knl_Swi_disable__R( void ) {
    return ti_sysbios_knl_Swi_disable__F();
}
xdc_Void ti_sysbios_knl_Swi_enable__R( void ) {
    ti_sysbios_knl_Swi_enable__F();
}
xdc_Void ti_sysbios_knl_Swi_restore__R( xdc_UInt key ) {
    ti_sysbios_knl_Swi_restore__F(key);
}
xdc_Void ti_sysbios_knl_Swi_restoreHwi__R( xdc_UInt key ) {
    ti_sysbios_knl_Swi_restoreHwi__F(key);
}
ti_sysbios_knl_Swi_Handle ti_sysbios_knl_Swi_self__R( void ) {
    return ti_sysbios_knl_Swi_self__F();
}
xdc_UInt ti_sysbios_knl_Swi_getTrigger__R( void ) {
    return ti_sysbios_knl_Swi_getTrigger__F();
}
xdc_UInt ti_sysbios_knl_Swi_raisePri__R( xdc_UInt priority ) {
    return ti_sysbios_knl_Swi_raisePri__F(priority);
}
xdc_Void ti_sysbios_knl_Swi_restorePri__R( xdc_UInt key ) {
    ti_sysbios_knl_Swi_restorePri__F(key);
}
xdc_Void ti_sysbios_knl_Swi_andn__R( ti_sysbios_knl_Swi_Handle _this, xdc_UInt mask ) {
    ti_sysbios_knl_Swi_andn__F((void*)_this, mask);
}
xdc_Void ti_sysbios_knl_Swi_dec__R( ti_sysbios_knl_Swi_Handle _this ) {
    ti_sysbios_knl_Swi_dec__F((void*)_this);
}
xdc_Ptr ti_sysbios_knl_Swi_getHookContext__R( ti_sysbios_knl_Swi_Handle _this, xdc_Int id ) {
    return ti_sysbios_knl_Swi_getHookContext__F((void*)_this, id);
}
xdc_Void ti_sysbios_knl_Swi_setHookContext__R( ti_sysbios_knl_Swi_Handle _this, xdc_Int id, xdc_Ptr hookContext ) {
    ti_sysbios_knl_Swi_setHookContext__F((void*)_this, id, hookContext);
}
xdc_UInt ti_sysbios_knl_Swi_getPri__R( ti_sysbios_knl_Swi_Handle _this ) {
    return ti_sysbios_knl_Swi_getPri__F((void*)_this);
}
xdc_Void ti_sysbios_knl_Swi_inc__R( ti_sysbios_knl_Swi_Handle _this ) {
    ti_sysbios_knl_Swi_inc__F((void*)_this);
}
xdc_Void ti_sysbios_knl_Swi_or__R( ti_sysbios_knl_Swi_Handle _this, xdc_UInt mask ) {
    ti_sysbios_knl_Swi_or__F((void*)_this, mask);
}
xdc_Void ti_sysbios_knl_Swi_post__R( ti_sysbios_knl_Swi_Handle _this ) {
    ti_sysbios_knl_Swi_post__F((void*)_this);
}
xdc_Int ti_sysbios_knl_Swi_Module_startup__R( xdc_Int state ) { return ti_sysbios_knl_Swi_Module_startup__F(state); }
int ti_sysbios_knl_Swi_Instance_init__R( ti_sysbios_knl_Swi_Object* __obj, ti_sysbios_knl_Swi_FuncPtr fxn, const ti_sysbios_knl_Swi_Params* __prms, xdc_runtime_Error_Block* __eb ) {
    return ti_sysbios_knl_Swi_Instance_init__F(__obj, fxn, __prms, __eb); }
void ti_sysbios_knl_Swi_Instance_finalize__R( ti_sysbios_knl_Swi_Object* obj, int iStat ) {
    ti_sysbios_knl_Swi_Instance_finalize__F(obj, iStat); }
xdc_runtime_Types_Label* ti_sysbios_knl_Swi_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Handle__label(obj, lab);}
xdc_Ptr ti_sysbios_knl_Swi_Module__gateObj__S( void ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Module__gateObj();}
xdc_Ptr ti_sysbios_knl_Swi_Module__gatePrms__S( void ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Module__gatePrms();}
xdc_runtime_Types_GateRef ti_sysbios_knl_Swi_Module__G_alloc__S( xdc_Ptr prms, xdc_runtime_Error_Block* eb ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Module__G_alloc(prms, eb);}
xdc_IArg ti_sysbios_knl_Swi_Module__G_enter__S( xdc_runtime_Types_GateRef ref ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Module__G_enter(ref);}
xdc_Void ti_sysbios_knl_Swi_Module__G_free__S( xdc_runtime_Types_GateRef ref ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.Module__G_free(ref);}
xdc_Void ti_sysbios_knl_Swi_Module__G_leave__S( xdc_runtime_Types_GateRef ref, xdc_IArg key ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.Module__G_leave(ref, key);}
xdc_Bool ti_sysbios_knl_Swi_Module__G_query__S( xdc_Int qual ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Module__G_query(qual);}
xdc_Bool ti_sysbios_knl_Swi_Module__startupDone__S( void ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Module__startupDone();}
xdc_Ptr ti_sysbios_knl_Swi_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Object__create(__oa, __aa, __pa, __sz, __eb);}
xdc_Void ti_sysbios_knl_Swi_Object__delete__S( xdc_Ptr instp ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.Object__delete(instp);}
xdc_Void ti_sysbios_knl_Swi_Object__destruct__S( xdc_Ptr objp ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.Object__destruct(objp);}
xdc_Ptr ti_sysbios_knl_Swi_Object__get__S( xdc_Ptr oarr, xdc_Int i ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Object__get(oarr, i);}
xdc_Void ti_sysbios_knl_Swi_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) {
    ti_sysbios_knl_Swi_Module__MTAB__C.Params__init(dst, src, psz, isz);}
xdc_Bool ti_sysbios_knl_Swi_Proxy__abstract__S( void ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Proxy__abstract();}
xdc_Ptr ti_sysbios_knl_Swi_Proxy__delegate__S( void ) {
    return ti_sysbios_knl_Swi_Module__MTAB__C.Proxy__delegate();}
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Instance_init__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Instance_finalize__R);

#if defined(ti_sysbios_knl_Swi___EXPORT) && defined(__ti__)
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_Module_startup__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_disable__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_enable__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_restore__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_restoreHwi__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_self__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_getTrigger__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_raisePri__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_restorePri__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_andn__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_dec__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_getHookContext__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_setHookContext__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_getPri__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_inc__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_or__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Swi_post__R);
#endif
#endif /* __isrom__ */
#endif /* $__used__ */

#ifdef ti_sysbios_knl_Clock___used
/*
 *  ======== module Clock ========
 *  Do not modify this file; it is generated from the specification Clock.xdc
 *  and any modifications risk being overwritten.
 */

#ifndef ti_sysbios_knl_Clock__include
#ifndef __nested__
#define __nested__
#include <ti/sysbios/knl/Clock.h>
#undef __nested__
#else
#include <ti/sysbios/knl/Clock.h>
#endif
#endif

#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Clock___LOGOBJ) && ti_sysbios_knl_Clock___DGSINCL & 0x1
#define ti_sysbios_knl_Clock___L_ENTRY 1
#else
#define ti_sysbios_knl_Clock___L_ENTRY 0
#endif
#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Clock___LOGOBJ) && ti_sysbios_knl_Clock___DGSINCL & 0x2
#define ti_sysbios_knl_Clock___L_EXIT 1
#else
#define ti_sysbios_knl_Clock___L_EXIT 0
#endif
#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Clock___LOGOBJ) && ti_sysbios_knl_Clock___DGSINCL & 0x4
#define ti_sysbios_knl_Clock___L_LIFECYCLE 1
#else
#define ti_sysbios_knl_Clock___L_LIFECYCLE 0
#endif
#ifndef __isrom__

#undef Module__MID
#define Module__MID ti_sysbios_knl_Clock_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Clock_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Clock_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Clock_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Clock_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Clock_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Clock_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Clock_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Clock_Module__gateObj__S()
#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Clock_Module__gatePrms__S()
#undef Module__GF_alloc
#define Module__GF_alloc ti_sysbios_knl_Clock_Module__G_alloc__S
#undef Module__GF_enter
#define Module__GF_enter ti_sysbios_knl_Clock_Module__G_enter__S
#undef Module__GF_free
#define Module__GF_free ti_sysbios_knl_Clock_Module__G_free__S
#undef Module__GF_leave
#define Module__GF_leave ti_sysbios_knl_Clock_Module__G_leave__S
#undef Module__GF_query
#define Module__GF_query ti_sysbios_knl_Clock_Module__G_query__S
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_getTicks__R
#else
#define __FN__ ti_sysbios_knl_Clock_getTicks__F
#endif
xdc_UInt32 ti_sysbios_knl_Clock_getTicks__E( void ) {
#if ti_sysbios_knl_Clock___L_EXIT
    xdc_UInt32 __ret;
#else
#endif
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_getTicks__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_getTicks__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_getTimerHandle__R
#else
#define __FN__ ti_sysbios_knl_Clock_getTimerHandle__F
#endif
ti_sysbios_hal_Timer_Handle ti_sysbios_knl_Clock_getTimerHandle__E( void ) {
#if ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_hal_Timer_Handle __ret;
#else
#endif
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_getTimerHandle__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_getTimerHandle__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_tickStop__R
#else
#define __FN__ ti_sysbios_knl_Clock_tickStop__F
#endif
xdc_Void ti_sysbios_knl_Clock_tickStop__E( void ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_tickStop__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tickStop__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_tickReconfig__R
#else
#define __FN__ ti_sysbios_knl_Clock_tickReconfig__F
#endif
xdc_Bool ti_sysbios_knl_Clock_tickReconfig__E( void ) {
#if ti_sysbios_knl_Clock___L_EXIT
    xdc_Bool __ret;
#else
#endif
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_tickReconfig__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tickReconfig__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_tickStart__R
#else
#define __FN__ ti_sysbios_knl_Clock_tickStart__F
#endif
xdc_Void ti_sysbios_knl_Clock_tickStart__E( void ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_tickStart__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tickStart__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_tick__R
#else
#define __FN__ ti_sysbios_knl_Clock_tick__F
#endif
xdc_Void ti_sysbios_knl_Clock_tick__E( xdc_UArg arg ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tick__ENTRY_EVT, (xdc_IArg)arg);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__(arg);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tick__EXIT_EVT, 0);
#else
    __FN__(arg);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_workSwi__R
#else
#define __FN__ ti_sysbios_knl_Clock_workSwi__F
#endif
xdc_Void ti_sysbios_knl_Clock_workSwi__E( xdc_UArg arg0, xdc_UArg arg1 ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Clock_workSwi__ENTRY_EVT, (xdc_IArg)arg0, (xdc_IArg)arg1);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__(arg0, arg1);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_workSwi__EXIT_EVT, 0);
#else
    __FN__(arg0, arg1);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_start__R
#else
#define __FN__ ti_sysbios_knl_Clock_start__F
#endif
xdc_Void ti_sysbios_knl_Clock_start__E( ti_sysbios_knl_Clock_Handle _this ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_start__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_start__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_stop__R
#else
#define __FN__ ti_sysbios_knl_Clock_stop__F
#endif
xdc_Void ti_sysbios_knl_Clock_stop__E( ti_sysbios_knl_Clock_Handle _this ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_stop__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_stop__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_setPeriod__R
#else
#define __FN__ ti_sysbios_knl_Clock_setPeriod__F
#endif
xdc_Void ti_sysbios_knl_Clock_setPeriod__E( ti_sysbios_knl_Clock_Handle _this, xdc_UInt period ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Clock_setPeriod__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)period);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this, period);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_setPeriod__EXIT_EVT, 0);
#else
    __FN__((void*)_this, period);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_setTimeout__R
#else
#define __FN__ ti_sysbios_knl_Clock_setTimeout__F
#endif
xdc_Void ti_sysbios_knl_Clock_setTimeout__E( ti_sysbios_knl_Clock_Handle _this, xdc_UInt timeout ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Clock_setTimeout__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)timeout);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this, timeout);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_setTimeout__EXIT_EVT, 0);
#else
    __FN__((void*)_this, timeout);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_setFunc__R
#else
#define __FN__ ti_sysbios_knl_Clock_setFunc__F
#endif
xdc_Void ti_sysbios_knl_Clock_setFunc__E( ti_sysbios_knl_Clock_Handle _this, ti_sysbios_knl_Clock_FuncPtr fxn, xdc_UArg arg ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write3(ti_sysbios_knl_Clock_setFunc__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)fxn, (xdc_IArg)arg);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this, fxn, arg);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_setFunc__EXIT_EVT, 0);
#else
    __FN__((void*)_this, fxn, arg);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_getTimeout__R
#else
#define __FN__ ti_sysbios_knl_Clock_getTimeout__F
#endif
xdc_UInt ti_sysbios_knl_Clock_getTimeout__E( ti_sysbios_knl_Clock_Handle _this ) {
#if ti_sysbios_knl_Clock___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_getTimeout__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_getTimeout__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#if ti_sysbios_knl_Clock___scope == -1
xdc_Int ti_sysbios_knl_Clock_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Clock_Module_startup__R(state); }
#else
xdc_Int ti_sysbios_knl_Clock_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Clock_Module_startup__F(state); }
#endif
#ifdef ti_sysbios_knl_Clock___OBJHEAP
#undef ti_sysbios_knl_Clock_Instance_init
#if ti_sysbios_knl_Clock___scope == -1
#define ti_sysbios_knl_Clock_Instance_init ti_sysbios_knl_Clock_Instance_init__R
#else
#define ti_sysbios_knl_Clock_Instance_init ti_sysbios_knl_Clock_Instance_init__F
#endif
#undef ti_sysbios_knl_Clock_Instance_finalize
#if ti_sysbios_knl_Clock___scope == -1
#define ti_sysbios_knl_Clock_Instance_finalize ti_sysbios_knl_Clock_Instance_finalize__R
#else
#define ti_sysbios_knl_Clock_Instance_finalize ti_sysbios_knl_Clock_Instance_finalize__F
#endif
#endif

#if defined(ti_sysbios_knl_Clock___EXPORT) && defined(__ti__)
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_Handle__label__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_Object__create__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_Object__delete__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_Object__destruct__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_Object__get__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_Params__init__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_Module_startup__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_getTicks__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_getTimerHandle__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_tickStop__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_tickReconfig__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_tickStart__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_tick__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_workSwi__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_start__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_stop__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_setPeriod__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_setTimeout__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_setFunc__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_getTimeout__E);
#endif
__FAR__ ti_sysbios_knl_Clock_Module__MTAB__C__qual ti_sysbios_knl_Clock_MTab__ ti_sysbios_knl_Clock_Module__MTAB__C = {
#if ti_sysbios_knl_Clock___scope == -1
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_getTicks__E,
#else
    ti_sysbios_knl_Clock_getTicks__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_getTimerHandle__E,
#else
    ti_sysbios_knl_Clock_getTimerHandle__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_tickStop__E,
#else
    ti_sysbios_knl_Clock_tickStop__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_tickReconfig__E,
#else
    ti_sysbios_knl_Clock_tickReconfig__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_tickStart__E,
#else
    ti_sysbios_knl_Clock_tickStart__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_tick__E,
#else
    ti_sysbios_knl_Clock_tick__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_workSwi__E,
#else
    ti_sysbios_knl_Clock_workSwi__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_start__E,
#else
    ti_sysbios_knl_Clock_start__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_stop__E,
#else
    ti_sysbios_knl_Clock_stop__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_setPeriod__E,
#else
    ti_sysbios_knl_Clock_setPeriod__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_setTimeout__E,
#else
    ti_sysbios_knl_Clock_setTimeout__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_setFunc__E,
#else
    ti_sysbios_knl_Clock_setFunc__R,
#endif
#if ti_sysbios_knl_Clock___L_ENTRY || ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_knl_Clock_getTimeout__E,
#else
    ti_sysbios_knl_Clock_getTimeout__R,
#endif
    ti_sysbios_knl_Clock_Handle__label__S,
    0,  /* Module__gateObj */
    0,  /* Module__gatePrms */
    0,  /* Module__G_alloc */
    0,  /* Module__G_enter */
    0,  /* Module__G_free */
    0,  /* Module__G_leave */
    0,  /* Module__G_query */
    ti_sysbios_knl_Clock_Module__startupDone__S,
#ifdef ti_sysbios_knl_Clock___OBJHEAP
    ti_sysbios_knl_Clock_Object__create__S,
#else
    0,  /* Object__create */
#endif
#ifdef ti_sysbios_knl_Clock___DELETE
    ti_sysbios_knl_Clock_Object__destruct__S,
    ti_sysbios_knl_Clock_Object__delete__S,
#else
    0,  /* Object__destruct */
    0,  /* Object__delete */
#endif
    ti_sysbios_knl_Clock_Object__get__S,
#ifdef ti_sysbios_knl_Clock___OBJHEAP
    ti_sysbios_knl_Clock_Params__init__S,
#else
    0,  /* Params__init */
#endif
    0,  /* Proxy__abstract */
    0,  /* Proxy__delegate */
#else
0
#endif
};
#if ti_sysbios_knl_Clock___scope != -1
#ifdef __ti__
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module_State_oneshotQ__O, ".const:ti_sysbios_knl_Clock_Module_State_oneshotQ__O");
asm("	.sect \".const:ti_sysbios_knl_Clock_Module_State_oneshotQ__O\"");
asm("	 .clink");
asm("	.sect \"[0].const:ti_sysbios_knl_Clock_Module_State_oneshotQ__O\"");
asm("	 .clink");
asm("	.sect \"[1].const:ti_sysbios_knl_Clock_Module_State_oneshotQ__O\"");
asm("	 .clink");
#endif
__FAR__ const xdc_SizeT ti_sysbios_knl_Clock_Module_State_oneshotQ__O = offsetof(ti_sysbios_knl_Clock_Module_State, oneshotQ);
#endif
#if ti_sysbios_knl_Clock___scope != -1
#ifdef __ti__
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module_State_periodicQ__O, ".const:ti_sysbios_knl_Clock_Module_State_periodicQ__O");
asm("	.sect \".const:ti_sysbios_knl_Clock_Module_State_periodicQ__O\"");
asm("	 .clink");
asm("	.sect \"[0].const:ti_sysbios_knl_Clock_Module_State_periodicQ__O\"");
asm("	 .clink");
asm("	.sect \"[1].const:ti_sysbios_knl_Clock_Module_State_periodicQ__O\"");
asm("	 .clink");
#endif
__FAR__ const xdc_SizeT ti_sysbios_knl_Clock_Module_State_periodicQ__O = offsetof(ti_sysbios_knl_Clock_Module_State, periodicQ);
#endif
xdc_runtime_Types_Label* ti_sysbios_knl_Clock_Handle__label__S( Ptr obj, xdc_runtime_Types_Label* lab ) {
    lab->oaddr = obj;
    lab->modid = ti_sysbios_knl_Clock_Module__id__D;
#ifdef ti_sysbios_knl_Clock___NAMEDINST
    xdc_runtime_Core_assignLabel(lab, ((ti_sysbios_knl_Clock_Object__*)obj)->__name, 1);
#else
    xdc_runtime_Core_assignLabel(lab, 0, 0);
#endif
    return lab;
}
xdc_Void ti_sysbios_knl_Clock_Params__init__S( xdc_Ptr prms, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) {
#ifdef ti_sysbios_knl_Clock___OBJHEAP
    xdc_runtime_Core_assignParams__I(prms, (xdc_Ptr)(src ? src : &ti_sysbios_knl_Clock_Object__PARAMS__C), psz, isz);
#endif
}
xdc_Ptr ti_sysbios_knl_Clock_Object__get__S( xdc_Ptr oa, xdc_Int i ) {
    if (oa) return ((ti_sysbios_knl_Clock_Object*)oa) + i;
#if ti_sysbios_knl_Clock_Object__count__D != 0
    return ti_sysbios_knl_Clock_Object__table__V + i;
#else
    return 0;
#endif
}

#ifdef ti_sysbios_knl_Clock___OBJHEAP
typedef struct { ti_sysbios_knl_Clock_Object2__ s0; char c; } ti_sysbios_knl_Clock___S1;
#ifdef __ti__
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Object__DESC__C, ".const:ti_sysbios_knl_Clock_Object__DESC__C");
asm("	.sect \".const:ti_sysbios_knl_Clock_Object__DESC__C\"");
asm("	 .clink");
asm("	.sect \"[0].const:ti_sysbios_knl_Clock_Object__DESC__C\"");
asm("	 .clink");
asm("	.sect \"[1].const:ti_sysbios_knl_Clock_Object__DESC__C\"");
asm("	 .clink");
#endif
const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Clock_Object__DESC__C = {
    (Fxn)0, /* finalFxn */
    (Ptr)-1, /* fxnTab */
    &ti_sysbios_knl_Clock_Module__root__V.hdr.link, /* modLink */
    sizeof(ti_sysbios_knl_Clock___S1) - sizeof(ti_sysbios_knl_Clock_Object2__), /* objAlign */
    ti_sysbios_knl_Clock___OBJHEAP, /* objHeap */
#ifdef ti_sysbios_knl_Clock___NAMEDINST
    offsetof(ti_sysbios_knl_Clock_Object__, __name), /* objName */
#else
    0, /* objName */
#endif
    sizeof(ti_sysbios_knl_Clock_Object2__), /* objSize */
    (Ptr)&ti_sysbios_knl_Clock_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_knl_Clock_Params), /* prmsSize */
};
extern xdc_Ptr ti_sysbios_knl_Clock_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    ti_sysbios_knl_Clock_Params prms;
    ti_sysbios_knl_Clock_Object* obj;
    ti_sysbios_knl_Clock_Args__create* args = __aa;
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Clock_Object__DESC__C, __oa, &prms, __pa, __sz, __eb);
    if (!obj) return 0;
#ifdef ti_sysbios_knl_Clock___DELETE
#define ti_sysbios_knl_Clock___ISTAT 
#else
#define ti_sysbios_knl_Clock___ISTAT
#endif
    ti_sysbios_knl_Clock___ISTAT ti_sysbios_knl_Clock_Instance_init(obj, args->fxn, args->timeout, &prms);
#if ti_sysbios_knl_Clock___L_LIFECYCLE
#ifdef ti_sysbios_knl_Clock___NAMEDINST
    xdc_runtime_Log_write2(__oa ? xdc_runtime_Log_L_construct : xdc_runtime_Log_L_create, (xdc_IArg)obj, (xdc_IArg)(((ti_sysbios_knl_Clock_Object__*)obj)->__name));
#else
    xdc_runtime_Log_write2(__oa ? xdc_runtime_Log_L_construct : xdc_runtime_Log_L_create, (xdc_IArg)obj, 0);
#endif
#endif
    return obj;
}
#else
extern xdc_Ptr ti_sysbios_knl_Clock_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    xdc_runtime_Error_raiseX(NULL, ti_sysbios_knl_Clock_Module__id__C, NULL, 0, xdc_runtime_Error_E_generic, (xdc_IArg)"create policy error", 0);
    return 0;
}
#endif

#ifdef ti_sysbios_knl_Clock___DELETE
extern xdc_Void ti_sysbios_knl_Clock_Object__destruct__S( xdc_Ptr obj ) {
#if ti_sysbios_knl_Clock___L_LIFECYCLE
    xdc_runtime_Log_write1(xdc_runtime_Log_L_destruct, (xdc_IArg)obj);
#endif
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Clock_Object__DESC__C, obj, -1, TRUE);
}
extern xdc_Void ti_sysbios_knl_Clock_Object__delete__S( xdc_Ptr instp ) {
#if ti_sysbios_knl_Clock___L_LIFECYCLE
    xdc_runtime_Log_write1(xdc_runtime_Log_L_delete, (xdc_IArg)(*((ti_sysbios_knl_Clock_Object**)instp)));
#endif
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Clock_Object__DESC__C, *((ti_sysbios_knl_Clock_Object**)instp), -1, FALSE);
    *((ti_sysbios_knl_Clock_Handle*)instp) = 0;
}
#else
extern xdc_Void ti_sysbios_knl_Clock_Object__destruct__S( xdc_Ptr obj ) {
    xdc_runtime_Error_raiseX(NULL, ti_sysbios_knl_Clock_Module__id__C, NULL, 0, xdc_runtime_Error_E_generic, (xdc_IArg)"delete policy error", 0);
}
extern xdc_Void ti_sysbios_knl_Clock_Object__delete__S( xdc_Ptr instp ) {
    xdc_runtime_Error_raiseX(NULL, ti_sysbios_knl_Clock_Module__id__C, NULL, 0, xdc_runtime_Error_E_generic, (xdc_IArg)"delete policy error", 0);
}
#endif

#else /* __isrom__ */

#ifndef ti_sysbios_knl_Clock___ROMPATCH
#undef Module__MID
#define Module__MID ti_sysbios_knl_Clock_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Clock_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Clock_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Clock_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Clock_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Clock_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Clock_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Clock_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Clock_Module__gateObj__S()
#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Clock_Module__gatePrms__S()
#undef Module__GF_alloc
#define Module__GF_alloc ti_sysbios_knl_Clock_Module__G_alloc__S
#undef Module__GF_enter
#define Module__GF_enter ti_sysbios_knl_Clock_Module__G_enter__S
#undef Module__GF_free
#define Module__GF_free ti_sysbios_knl_Clock_Module__G_free__S
#undef Module__GF_leave
#define Module__GF_leave ti_sysbios_knl_Clock_Module__G_leave__S
#undef Module__GF_query
#define Module__GF_query ti_sysbios_knl_Clock_Module__G_query__S
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_getTicks__R
#else
#define __FN__ ti_sysbios_knl_Clock_getTicks__F
#endif
xdc_UInt32 ti_sysbios_knl_Clock_getTicks__E( void ) {
#if ti_sysbios_knl_Clock___L_EXIT
    xdc_UInt32 __ret;
#else
#endif
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_getTicks__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_getTicks__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_getTimerHandle__R
#else
#define __FN__ ti_sysbios_knl_Clock_getTimerHandle__F
#endif
ti_sysbios_hal_Timer_Handle ti_sysbios_knl_Clock_getTimerHandle__E( void ) {
#if ti_sysbios_knl_Clock___L_EXIT
    ti_sysbios_hal_Timer_Handle __ret;
#else
#endif
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_getTimerHandle__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_getTimerHandle__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_tickStop__R
#else
#define __FN__ ti_sysbios_knl_Clock_tickStop__F
#endif
xdc_Void ti_sysbios_knl_Clock_tickStop__E( void ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_tickStop__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tickStop__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_tickReconfig__R
#else
#define __FN__ ti_sysbios_knl_Clock_tickReconfig__F
#endif
xdc_Bool ti_sysbios_knl_Clock_tickReconfig__E( void ) {
#if ti_sysbios_knl_Clock___L_EXIT
    xdc_Bool __ret;
#else
#endif
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_tickReconfig__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tickReconfig__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_tickStart__R
#else
#define __FN__ ti_sysbios_knl_Clock_tickStart__F
#endif
xdc_Void ti_sysbios_knl_Clock_tickStart__E( void ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Clock_tickStart__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tickStart__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_tick__R
#else
#define __FN__ ti_sysbios_knl_Clock_tick__F
#endif
xdc_Void ti_sysbios_knl_Clock_tick__E( xdc_UArg arg ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tick__ENTRY_EVT, (xdc_IArg)arg);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__(arg);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_tick__EXIT_EVT, 0);
#else
    __FN__(arg);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_workSwi__R
#else
#define __FN__ ti_sysbios_knl_Clock_workSwi__F
#endif
xdc_Void ti_sysbios_knl_Clock_workSwi__E( xdc_UArg arg0, xdc_UArg arg1 ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Clock_workSwi__ENTRY_EVT, (xdc_IArg)arg0, (xdc_IArg)arg1);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__(arg0, arg1);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_workSwi__EXIT_EVT, 0);
#else
    __FN__(arg0, arg1);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_start__R
#else
#define __FN__ ti_sysbios_knl_Clock_start__F
#endif
xdc_Void ti_sysbios_knl_Clock_start__E( ti_sysbios_knl_Clock_Handle _this ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_start__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_start__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_stop__R
#else
#define __FN__ ti_sysbios_knl_Clock_stop__F
#endif
xdc_Void ti_sysbios_knl_Clock_stop__E( ti_sysbios_knl_Clock_Handle _this ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_stop__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_stop__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_setPeriod__R
#else
#define __FN__ ti_sysbios_knl_Clock_setPeriod__F
#endif
xdc_Void ti_sysbios_knl_Clock_setPeriod__E( ti_sysbios_knl_Clock_Handle _this, xdc_UInt period ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Clock_setPeriod__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)period);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this, period);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_setPeriod__EXIT_EVT, 0);
#else
    __FN__((void*)_this, period);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_setTimeout__R
#else
#define __FN__ ti_sysbios_knl_Clock_setTimeout__F
#endif
xdc_Void ti_sysbios_knl_Clock_setTimeout__E( ti_sysbios_knl_Clock_Handle _this, xdc_UInt timeout ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Clock_setTimeout__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)timeout);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this, timeout);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_setTimeout__EXIT_EVT, 0);
#else
    __FN__((void*)_this, timeout);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_setFunc__R
#else
#define __FN__ ti_sysbios_knl_Clock_setFunc__F
#endif
xdc_Void ti_sysbios_knl_Clock_setFunc__E( ti_sysbios_knl_Clock_Handle _this, ti_sysbios_knl_Clock_FuncPtr fxn, xdc_UArg arg ) {
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write3(ti_sysbios_knl_Clock_setFunc__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)fxn, (xdc_IArg)arg);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __FN__((void*)_this, fxn, arg);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_setFunc__EXIT_EVT, 0);
#else
    __FN__((void*)_this, fxn, arg);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Clock___scope == -1
#define __FN__ ti_sysbios_knl_Clock_getTimeout__R
#else
#define __FN__ ti_sysbios_knl_Clock_getTimeout__F
#endif
xdc_UInt ti_sysbios_knl_Clock_getTimeout__E( ti_sysbios_knl_Clock_Handle _this ) {
#if ti_sysbios_knl_Clock___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Clock___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_getTimeout__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Clock___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Clock_getTimeout__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#if ti_sysbios_knl_Clock___scope == -1
xdc_Int ti_sysbios_knl_Clock_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Clock_Module_startup__R(state); }
#else
xdc_Int ti_sysbios_knl_Clock_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Clock_Module_startup__F(state); }
#endif
#endif /* ti_sysbios_knl_Clock___ROMPATCH */
#ifdef ti_sysbios_knl_Clock___ROMPATCH
xdc_UInt32 ti_sysbios_knl_Clock_getTicks__E( void ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.getTicks();
}
ti_sysbios_hal_Timer_Handle ti_sysbios_knl_Clock_getTimerHandle__E( void ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.getTimerHandle();
}
xdc_Void ti_sysbios_knl_Clock_tickStop__E( void ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.tickStop();
}
xdc_Bool ti_sysbios_knl_Clock_tickReconfig__E( void ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.tickReconfig();
}
xdc_Void ti_sysbios_knl_Clock_tickStart__E( void ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.tickStart();
}
xdc_Void ti_sysbios_knl_Clock_tick__E( xdc_UArg arg ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.tick(arg);
}
xdc_Void ti_sysbios_knl_Clock_workSwi__E( xdc_UArg arg0, xdc_UArg arg1 ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.workSwi(arg0, arg1);
}
xdc_Void ti_sysbios_knl_Clock_start__E( ti_sysbios_knl_Clock_Handle _this ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.start((void*)_this);
}
xdc_Void ti_sysbios_knl_Clock_stop__E( ti_sysbios_knl_Clock_Handle _this ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.stop((void*)_this);
}
xdc_Void ti_sysbios_knl_Clock_setPeriod__E( ti_sysbios_knl_Clock_Handle _this, xdc_UInt period ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.setPeriod((void*)_this, period);
}
xdc_Void ti_sysbios_knl_Clock_setTimeout__E( ti_sysbios_knl_Clock_Handle _this, xdc_UInt timeout ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.setTimeout((void*)_this, timeout);
}
xdc_Void ti_sysbios_knl_Clock_setFunc__E( ti_sysbios_knl_Clock_Handle _this, ti_sysbios_knl_Clock_FuncPtr fxn, xdc_UArg arg ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.setFunc((void*)_this, fxn, arg);
}
xdc_UInt ti_sysbios_knl_Clock_getTimeout__E( ti_sysbios_knl_Clock_Handle _this ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.getTimeout((void*)_this);
}
#if ti_sysbios_knl_Clock___scope == -1
xdc_Int ti_sysbios_knl_Clock_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Clock_Module_startup__R(state); }
#else
xdc_Int ti_sysbios_knl_Clock_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Clock_Module_startup__F(state); }
#endif
#endif /* ti_sysbios_knl_Clock___ROMPATCH */
xdc_UInt32 ti_sysbios_knl_Clock_getTicks__R( void ) {
    return ti_sysbios_knl_Clock_getTicks__F();
}
ti_sysbios_hal_Timer_Handle ti_sysbios_knl_Clock_getTimerHandle__R( void ) {
    return ti_sysbios_knl_Clock_getTimerHandle__F();
}
xdc_Void ti_sysbios_knl_Clock_tickStop__R( void ) {
    ti_sysbios_knl_Clock_tickStop__F();
}
xdc_Bool ti_sysbios_knl_Clock_tickReconfig__R( void ) {
    return ti_sysbios_knl_Clock_tickReconfig__F();
}
xdc_Void ti_sysbios_knl_Clock_tickStart__R( void ) {
    ti_sysbios_knl_Clock_tickStart__F();
}
xdc_Void ti_sysbios_knl_Clock_tick__R( xdc_UArg arg ) {
    ti_sysbios_knl_Clock_tick__F(arg);
}
xdc_Void ti_sysbios_knl_Clock_workSwi__R( xdc_UArg arg0, xdc_UArg arg1 ) {
    ti_sysbios_knl_Clock_workSwi__F(arg0, arg1);
}
xdc_Void ti_sysbios_knl_Clock_start__R( ti_sysbios_knl_Clock_Handle _this ) {
    ti_sysbios_knl_Clock_start__F((void*)_this);
}
xdc_Void ti_sysbios_knl_Clock_stop__R( ti_sysbios_knl_Clock_Handle _this ) {
    ti_sysbios_knl_Clock_stop__F((void*)_this);
}
xdc_Void ti_sysbios_knl_Clock_setPeriod__R( ti_sysbios_knl_Clock_Handle _this, xdc_UInt period ) {
    ti_sysbios_knl_Clock_setPeriod__F((void*)_this, period);
}
xdc_Void ti_sysbios_knl_Clock_setTimeout__R( ti_sysbios_knl_Clock_Handle _this, xdc_UInt timeout ) {
    ti_sysbios_knl_Clock_setTimeout__F((void*)_this, timeout);
}
xdc_Void ti_sysbios_knl_Clock_setFunc__R( ti_sysbios_knl_Clock_Handle _this, ti_sysbios_knl_Clock_FuncPtr fxn, xdc_UArg arg ) {
    ti_sysbios_knl_Clock_setFunc__F((void*)_this, fxn, arg);
}
xdc_UInt ti_sysbios_knl_Clock_getTimeout__R( ti_sysbios_knl_Clock_Handle _this ) {
    return ti_sysbios_knl_Clock_getTimeout__F((void*)_this);
}
xdc_Int ti_sysbios_knl_Clock_Module_startup__R( xdc_Int state ) { return ti_sysbios_knl_Clock_Module_startup__F(state); }
void ti_sysbios_knl_Clock_Instance_init__R( ti_sysbios_knl_Clock_Object* __obj, ti_sysbios_knl_Clock_FuncPtr fxn, xdc_UInt timeout, const ti_sysbios_knl_Clock_Params* __prms ) {
    ti_sysbios_knl_Clock_Instance_init__F(__obj, fxn, timeout, __prms); }
xdc_runtime_Types_Label* ti_sysbios_knl_Clock_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Handle__label(obj, lab);}
xdc_Ptr ti_sysbios_knl_Clock_Module__gateObj__S( void ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Module__gateObj();}
xdc_Ptr ti_sysbios_knl_Clock_Module__gatePrms__S( void ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Module__gatePrms();}
xdc_runtime_Types_GateRef ti_sysbios_knl_Clock_Module__G_alloc__S( xdc_Ptr prms, xdc_runtime_Error_Block* eb ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Module__G_alloc(prms, eb);}
xdc_IArg ti_sysbios_knl_Clock_Module__G_enter__S( xdc_runtime_Types_GateRef ref ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Module__G_enter(ref);}
xdc_Void ti_sysbios_knl_Clock_Module__G_free__S( xdc_runtime_Types_GateRef ref ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.Module__G_free(ref);}
xdc_Void ti_sysbios_knl_Clock_Module__G_leave__S( xdc_runtime_Types_GateRef ref, xdc_IArg key ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.Module__G_leave(ref, key);}
xdc_Bool ti_sysbios_knl_Clock_Module__G_query__S( xdc_Int qual ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Module__G_query(qual);}
xdc_Bool ti_sysbios_knl_Clock_Module__startupDone__S( void ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Module__startupDone();}
xdc_Ptr ti_sysbios_knl_Clock_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Object__create(__oa, __aa, __pa, __sz, __eb);}
xdc_Void ti_sysbios_knl_Clock_Object__delete__S( xdc_Ptr instp ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.Object__delete(instp);}
xdc_Void ti_sysbios_knl_Clock_Object__destruct__S( xdc_Ptr objp ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.Object__destruct(objp);}
xdc_Ptr ti_sysbios_knl_Clock_Object__get__S( xdc_Ptr oarr, xdc_Int i ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Object__get(oarr, i);}
xdc_Void ti_sysbios_knl_Clock_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) {
    ti_sysbios_knl_Clock_Module__MTAB__C.Params__init(dst, src, psz, isz);}
xdc_Bool ti_sysbios_knl_Clock_Proxy__abstract__S( void ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Proxy__abstract();}
xdc_Ptr ti_sysbios_knl_Clock_Proxy__delegate__S( void ) {
    return ti_sysbios_knl_Clock_Module__MTAB__C.Proxy__delegate();}
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_Instance_init__R);

#if defined(ti_sysbios_knl_Clock___EXPORT) && defined(__ti__)
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_Module_startup__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_getTicks__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_getTimerHandle__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_tickStop__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_tickReconfig__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_tickStart__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_tick__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_workSwi__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_start__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_stop__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_setPeriod__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_setTimeout__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_setFunc__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Clock_getTimeout__R);
#endif
#if ti_sysbios_knl_Clock___scope != -1
#ifdef __ti__
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module_State_oneshotQ__O, ".const:ti_sysbios_knl_Clock_Module_State_oneshotQ__O");
asm("	.sect \".const:ti_sysbios_knl_Clock_Module_State_oneshotQ__O\"");
asm("	 .clink");
asm("	.sect \"[0].const:ti_sysbios_knl_Clock_Module_State_oneshotQ__O\"");
asm("	 .clink");
asm("	.sect \"[1].const:ti_sysbios_knl_Clock_Module_State_oneshotQ__O\"");
asm("	 .clink");
#endif
__FAR__ const xdc_SizeT ti_sysbios_knl_Clock_Module_State_oneshotQ__O = offsetof(ti_sysbios_knl_Clock_Module_State, oneshotQ);
#endif
#if ti_sysbios_knl_Clock___scope != -1
#ifdef __ti__
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module_State_periodicQ__O, ".const:ti_sysbios_knl_Clock_Module_State_periodicQ__O");
asm("	.sect \".const:ti_sysbios_knl_Clock_Module_State_periodicQ__O\"");
asm("	 .clink");
asm("	.sect \"[0].const:ti_sysbios_knl_Clock_Module_State_periodicQ__O\"");
asm("	 .clink");
asm("	.sect \"[1].const:ti_sysbios_knl_Clock_Module_State_periodicQ__O\"");
asm("	 .clink");
#endif
__FAR__ const xdc_SizeT ti_sysbios_knl_Clock_Module_State_periodicQ__O = offsetof(ti_sysbios_knl_Clock_Module_State, periodicQ);
#endif
#endif /* __isrom__ */
#endif /* $__used__ */

#ifdef ti_sysbios_knl_Task___used
/*
 *  ======== module Task ========
 *  Do not modify this file; it is generated from the specification Task.xdc
 *  and any modifications risk being overwritten.
 */

#ifndef ti_sysbios_knl_Task__include
#ifndef __nested__
#define __nested__
#include <ti/sysbios/knl/Task.h>
#undef __nested__
#else
#include <ti/sysbios/knl/Task.h>
#endif
#endif

#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Task___LOGOBJ) && ti_sysbios_knl_Task___DGSINCL & 0x1
#define ti_sysbios_knl_Task___L_ENTRY 1
#else
#define ti_sysbios_knl_Task___L_ENTRY 0
#endif
#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Task___LOGOBJ) && ti_sysbios_knl_Task___DGSINCL & 0x2
#define ti_sysbios_knl_Task___L_EXIT 1
#else
#define ti_sysbios_knl_Task___L_EXIT 0
#endif
#if defined(xdc_runtime_Log__include) && defined(ti_sysbios_knl_Task___LOGOBJ) && ti_sysbios_knl_Task___DGSINCL & 0x4
#define ti_sysbios_knl_Task___L_LIFECYCLE 1
#else
#define ti_sysbios_knl_Task___L_LIFECYCLE 0
#endif
#ifndef __isrom__

#undef Module__MID
#define Module__MID ti_sysbios_knl_Task_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Task_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Task_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Task_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Task_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Task_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Task_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Task_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Task_Module__gateObj__S()
#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Task_Module__gatePrms__S()
#undef Module__GF_alloc
#define Module__GF_alloc ti_sysbios_knl_Task_Module__G_alloc__S
#undef Module__GF_enter
#define Module__GF_enter ti_sysbios_knl_Task_Module__G_enter__S
#undef Module__GF_free
#define Module__GF_free ti_sysbios_knl_Task_Module__G_free__S
#undef Module__GF_leave
#define Module__GF_leave ti_sysbios_knl_Task_Module__G_leave__S
#undef Module__GF_query
#define Module__GF_query ti_sysbios_knl_Task_Module__G_query__S
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_startup__R
#else
#define __FN__ ti_sysbios_knl_Task_startup__F
#endif
xdc_Void ti_sysbios_knl_Task_startup__E( void ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_startup__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_startup__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_disable__R
#else
#define __FN__ ti_sysbios_knl_Task_disable__F
#endif
xdc_UInt ti_sysbios_knl_Task_disable__E( void ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_disable__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_disable__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_enable__R
#else
#define __FN__ ti_sysbios_knl_Task_enable__F
#endif
xdc_Void ti_sysbios_knl_Task_enable__E( void ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_enable__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_enable__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_restore__R
#else
#define __FN__ ti_sysbios_knl_Task_restore__F
#endif
xdc_Void ti_sysbios_knl_Task_restore__E( xdc_UInt key ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_restore__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_restore__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_restoreHwi__R
#else
#define __FN__ ti_sysbios_knl_Task_restoreHwi__F
#endif
xdc_Void ti_sysbios_knl_Task_restoreHwi__E( xdc_UInt key ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_restoreHwi__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_restoreHwi__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_self__R
#else
#define __FN__ ti_sysbios_knl_Task_self__F
#endif
ti_sysbios_knl_Task_Handle ti_sysbios_knl_Task_self__E( void ) {
#if ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_Handle __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_self__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_self__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_checkStacks__R
#else
#define __FN__ ti_sysbios_knl_Task_checkStacks__F
#endif
xdc_Void ti_sysbios_knl_Task_checkStacks__E( ti_sysbios_knl_Task_Handle oldTask, ti_sysbios_knl_Task_Handle newTask ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_checkStacks__ENTRY_EVT, (xdc_IArg)oldTask, (xdc_IArg)newTask);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__(oldTask, newTask);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_checkStacks__EXIT_EVT, 0);
#else
    __FN__(oldTask, newTask);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_exit__R
#else
#define __FN__ ti_sysbios_knl_Task_exit__F
#endif
xdc_Void ti_sysbios_knl_Task_exit__E( void ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_exit__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_exit__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_sleep__R
#else
#define __FN__ ti_sysbios_knl_Task_sleep__F
#endif
xdc_Void ti_sysbios_knl_Task_sleep__E( xdc_UInt nticks ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_sleep__ENTRY_EVT, (xdc_IArg)nticks);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__(nticks);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_sleep__EXIT_EVT, 0);
#else
    __FN__(nticks);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_yield__R
#else
#define __FN__ ti_sysbios_knl_Task_yield__F
#endif
xdc_Void ti_sysbios_knl_Task_yield__E( void ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_yield__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_yield__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getIdleTask__R
#else
#define __FN__ ti_sysbios_knl_Task_getIdleTask__F
#endif
ti_sysbios_knl_Task_Handle ti_sysbios_knl_Task_getIdleTask__E( void ) {
#if ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_Handle __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_getIdleTask__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getIdleTask__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getArg0__R
#else
#define __FN__ ti_sysbios_knl_Task_getArg0__F
#endif
xdc_UArg ti_sysbios_knl_Task_getArg0__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_UArg __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getArg0__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getArg0__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getArg1__R
#else
#define __FN__ ti_sysbios_knl_Task_getArg1__F
#endif
xdc_UArg ti_sysbios_knl_Task_getArg1__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_UArg __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getArg1__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getArg1__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getEnv__R
#else
#define __FN__ ti_sysbios_knl_Task_getEnv__F
#endif
xdc_Ptr ti_sysbios_knl_Task_getEnv__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_Ptr __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getEnv__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getEnv__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getHookContext__R
#else
#define __FN__ ti_sysbios_knl_Task_getHookContext__F
#endif
xdc_Ptr ti_sysbios_knl_Task_getHookContext__E( ti_sysbios_knl_Task_Handle _this, xdc_Int id ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_Ptr __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_getHookContext__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)id);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this, id);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getHookContext__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this, id);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getPri__R
#else
#define __FN__ ti_sysbios_knl_Task_getPri__F
#endif
xdc_Int ti_sysbios_knl_Task_getPri__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_Int __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getPri__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getPri__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setArg0__R
#else
#define __FN__ ti_sysbios_knl_Task_setArg0__F
#endif
xdc_Void ti_sysbios_knl_Task_setArg0__E( ti_sysbios_knl_Task_Handle _this, xdc_UArg arg ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_setArg0__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)arg);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, arg);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setArg0__EXIT_EVT, 0);
#else
    __FN__((void*)_this, arg);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setArg1__R
#else
#define __FN__ ti_sysbios_knl_Task_setArg1__F
#endif
xdc_Void ti_sysbios_knl_Task_setArg1__E( ti_sysbios_knl_Task_Handle _this, xdc_UArg arg ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_setArg1__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)arg);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, arg);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setArg1__EXIT_EVT, 0);
#else
    __FN__((void*)_this, arg);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setEnv__R
#else
#define __FN__ ti_sysbios_knl_Task_setEnv__F
#endif
xdc_Void ti_sysbios_knl_Task_setEnv__E( ti_sysbios_knl_Task_Handle _this, xdc_Ptr env ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_setEnv__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)env);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, env);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setEnv__EXIT_EVT, 0);
#else
    __FN__((void*)_this, env);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setHookContext__R
#else
#define __FN__ ti_sysbios_knl_Task_setHookContext__F
#endif
xdc_Void ti_sysbios_knl_Task_setHookContext__E( ti_sysbios_knl_Task_Handle _this, xdc_Int id, xdc_Ptr hookContext ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write3(ti_sysbios_knl_Task_setHookContext__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)id, (xdc_IArg)hookContext);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, id, hookContext);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setHookContext__EXIT_EVT, 0);
#else
    __FN__((void*)_this, id, hookContext);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setPri__R
#else
#define __FN__ ti_sysbios_knl_Task_setPri__F
#endif
xdc_UInt ti_sysbios_knl_Task_setPri__E( ti_sysbios_knl_Task_Handle _this, xdc_Int newpri ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_setPri__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)newpri);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this, newpri);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setPri__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this, newpri);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_stat__R
#else
#define __FN__ ti_sysbios_knl_Task_stat__F
#endif
xdc_Void ti_sysbios_knl_Task_stat__E( ti_sysbios_knl_Task_Handle _this, ti_sysbios_knl_Task_Stat* statbuf ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_stat__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)statbuf);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, statbuf);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_stat__EXIT_EVT, 0);
#else
    __FN__((void*)_this, statbuf);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_block__R
#else
#define __FN__ ti_sysbios_knl_Task_block__F
#endif
xdc_Void ti_sysbios_knl_Task_block__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_block__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_block__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_unblock__R
#else
#define __FN__ ti_sysbios_knl_Task_unblock__F
#endif
xdc_Void ti_sysbios_knl_Task_unblock__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_unblock__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_unblock__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_blockI__R
#else
#define __FN__ ti_sysbios_knl_Task_blockI__F
#endif
xdc_Void ti_sysbios_knl_Task_blockI__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_blockI__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_blockI__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_unblockI__R
#else
#define __FN__ ti_sysbios_knl_Task_unblockI__F
#endif
xdc_Void ti_sysbios_knl_Task_unblockI__E( ti_sysbios_knl_Task_Handle _this, xdc_UInt hwiKey ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_unblockI__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)hwiKey);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, hwiKey);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_unblockI__EXIT_EVT, 0);
#else
    __FN__((void*)_this, hwiKey);
#endif
}
#if ti_sysbios_knl_Task___scope == -1
xdc_Int ti_sysbios_knl_Task_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Task_Module_startup__R(state); }
#else
xdc_Int ti_sysbios_knl_Task_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Task_Module_startup__F(state); }
#endif
#ifdef ti_sysbios_knl_Task___OBJHEAP
#undef ti_sysbios_knl_Task_Instance_init
#if ti_sysbios_knl_Task___scope == -1
#define ti_sysbios_knl_Task_Instance_init ti_sysbios_knl_Task_Instance_init__R
#else
#define ti_sysbios_knl_Task_Instance_init ti_sysbios_knl_Task_Instance_init__F
#endif
#undef ti_sysbios_knl_Task_Instance_finalize
#if ti_sysbios_knl_Task___scope == -1
#define ti_sysbios_knl_Task_Instance_finalize ti_sysbios_knl_Task_Instance_finalize__R
#else
#define ti_sysbios_knl_Task_Instance_finalize ti_sysbios_knl_Task_Instance_finalize__F
#endif
#endif

#if defined(ti_sysbios_knl_Task___EXPORT) && defined(__ti__)
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Handle__label__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Object__create__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Object__delete__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Object__destruct__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Object__get__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Params__init__S);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Module_startup__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_startup__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_disable__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_enable__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_restore__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_restoreHwi__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_self__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_checkStacks__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_exit__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_sleep__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_yield__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getIdleTask__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getArg0__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getArg1__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getEnv__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getHookContext__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getPri__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setArg0__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setArg1__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setEnv__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setHookContext__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setPri__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_stat__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_block__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_unblock__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_blockI__E);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_unblockI__E);
#endif
__FAR__ ti_sysbios_knl_Task_Module__MTAB__C__qual ti_sysbios_knl_Task_MTab__ ti_sysbios_knl_Task_Module__MTAB__C = {
#if ti_sysbios_knl_Task___scope == -1
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_startup__E,
#else
    ti_sysbios_knl_Task_startup__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_disable__E,
#else
    ti_sysbios_knl_Task_disable__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_enable__E,
#else
    ti_sysbios_knl_Task_enable__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_restore__E,
#else
    ti_sysbios_knl_Task_restore__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_restoreHwi__E,
#else
    ti_sysbios_knl_Task_restoreHwi__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_self__E,
#else
    ti_sysbios_knl_Task_self__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_checkStacks__E,
#else
    ti_sysbios_knl_Task_checkStacks__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_exit__E,
#else
    ti_sysbios_knl_Task_exit__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_sleep__E,
#else
    ti_sysbios_knl_Task_sleep__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_yield__E,
#else
    ti_sysbios_knl_Task_yield__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_getIdleTask__E,
#else
    ti_sysbios_knl_Task_getIdleTask__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_getArg0__E,
#else
    ti_sysbios_knl_Task_getArg0__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_getArg1__E,
#else
    ti_sysbios_knl_Task_getArg1__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_getEnv__E,
#else
    ti_sysbios_knl_Task_getEnv__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_getHookContext__E,
#else
    ti_sysbios_knl_Task_getHookContext__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_getPri__E,
#else
    ti_sysbios_knl_Task_getPri__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_setArg0__E,
#else
    ti_sysbios_knl_Task_setArg0__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_setArg1__E,
#else
    ti_sysbios_knl_Task_setArg1__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_setEnv__E,
#else
    ti_sysbios_knl_Task_setEnv__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_setHookContext__E,
#else
    ti_sysbios_knl_Task_setHookContext__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_setPri__E,
#else
    ti_sysbios_knl_Task_setPri__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_stat__E,
#else
    ti_sysbios_knl_Task_stat__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_block__E,
#else
    ti_sysbios_knl_Task_block__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_unblock__E,
#else
    ti_sysbios_knl_Task_unblock__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_blockI__E,
#else
    ti_sysbios_knl_Task_blockI__R,
#endif
#if ti_sysbios_knl_Task___L_ENTRY || ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_unblockI__E,
#else
    ti_sysbios_knl_Task_unblockI__R,
#endif
    ti_sysbios_knl_Task_Handle__label__S,
    0,  /* Module__gateObj */
    0,  /* Module__gatePrms */
    0,  /* Module__G_alloc */
    0,  /* Module__G_enter */
    0,  /* Module__G_free */
    0,  /* Module__G_leave */
    0,  /* Module__G_query */
    ti_sysbios_knl_Task_Module__startupDone__S,
#ifdef ti_sysbios_knl_Task___OBJHEAP
    ti_sysbios_knl_Task_Object__create__S,
#else
    0,  /* Object__create */
#endif
#ifdef ti_sysbios_knl_Task___DELETE
    ti_sysbios_knl_Task_Object__destruct__S,
    ti_sysbios_knl_Task_Object__delete__S,
#else
    0,  /* Object__destruct */
    0,  /* Object__delete */
#endif
    ti_sysbios_knl_Task_Object__get__S,
#ifdef ti_sysbios_knl_Task___OBJHEAP
    ti_sysbios_knl_Task_Params__init__S,
#else
    0,  /* Params__init */
#endif
    0,  /* Proxy__abstract */
    0,  /* Proxy__delegate */
#else
0
#endif
};
#if ti_sysbios_knl_Task___scope != -1
#ifdef __ti__
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module_State_inactiveQ__O, ".const:ti_sysbios_knl_Task_Module_State_inactiveQ__O");
asm("	.sect \".const:ti_sysbios_knl_Task_Module_State_inactiveQ__O\"");
asm("	 .clink");
asm("	.sect \"[0].const:ti_sysbios_knl_Task_Module_State_inactiveQ__O\"");
asm("	 .clink");
asm("	.sect \"[1].const:ti_sysbios_knl_Task_Module_State_inactiveQ__O\"");
asm("	 .clink");
#endif
__FAR__ const xdc_SizeT ti_sysbios_knl_Task_Module_State_inactiveQ__O = offsetof(ti_sysbios_knl_Task_Module_State, inactiveQ);
#endif
xdc_runtime_Types_Label* ti_sysbios_knl_Task_Handle__label__S( Ptr obj, xdc_runtime_Types_Label* lab ) {
    lab->oaddr = obj;
    lab->modid = ti_sysbios_knl_Task_Module__id__D;
#ifdef ti_sysbios_knl_Task___NAMEDINST
    xdc_runtime_Core_assignLabel(lab, ((ti_sysbios_knl_Task_Object__*)obj)->__name, 1);
#else
    xdc_runtime_Core_assignLabel(lab, 0, 0);
#endif
    return lab;
}
xdc_Void ti_sysbios_knl_Task_Params__init__S( xdc_Ptr prms, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) {
#ifdef ti_sysbios_knl_Task___OBJHEAP
    xdc_runtime_Core_assignParams__I(prms, (xdc_Ptr)(src ? src : &ti_sysbios_knl_Task_Object__PARAMS__C), psz, isz);
#endif
}
xdc_Ptr ti_sysbios_knl_Task_Object__get__S( xdc_Ptr oa, xdc_Int i ) {
    if (oa) return ((ti_sysbios_knl_Task_Object*)oa) + i;
#if ti_sysbios_knl_Task_Object__count__D != 0
    return ti_sysbios_knl_Task_Object__table__V + i;
#else
    return 0;
#endif
}

#ifdef ti_sysbios_knl_Task___OBJHEAP
typedef struct { ti_sysbios_knl_Task_Object2__ s0; char c; } ti_sysbios_knl_Task___S1;
#ifdef __ti__
#pragma DATA_SECTION(ti_sysbios_knl_Task_Object__DESC__C, ".const:ti_sysbios_knl_Task_Object__DESC__C");
asm("	.sect \".const:ti_sysbios_knl_Task_Object__DESC__C\"");
asm("	 .clink");
asm("	.sect \"[0].const:ti_sysbios_knl_Task_Object__DESC__C\"");
asm("	 .clink");
asm("	.sect \"[1].const:ti_sysbios_knl_Task_Object__DESC__C\"");
asm("	 .clink");
#endif
const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Task_Object__DESC__C = {
    (Fxn)ti_sysbios_knl_Task_Instance_finalize, /* finalFxn */
    (Ptr)-1, /* fxnTab */
    &ti_sysbios_knl_Task_Module__root__V.hdr.link, /* modLink */
    sizeof(ti_sysbios_knl_Task___S1) - sizeof(ti_sysbios_knl_Task_Object2__), /* objAlign */
    ti_sysbios_knl_Task___OBJHEAP, /* objHeap */
#ifdef ti_sysbios_knl_Task___NAMEDINST
    offsetof(ti_sysbios_knl_Task_Object__, __name), /* objName */
#else
    0, /* objName */
#endif
    sizeof(ti_sysbios_knl_Task_Object2__), /* objSize */
    (Ptr)&ti_sysbios_knl_Task_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_knl_Task_Params), /* prmsSize */
};
extern xdc_Ptr ti_sysbios_knl_Task_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    ti_sysbios_knl_Task_Params prms;
    ti_sysbios_knl_Task_Object* obj;
#ifdef ti_sysbios_knl_Task___DELETE
    int iStat;
#endif
    ti_sysbios_knl_Task_Args__create* args = __aa;
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Task_Object__DESC__C, __oa, &prms, __pa, __sz, __eb);
    if (!obj) return 0;
#ifdef ti_sysbios_knl_Task___DELETE
#define ti_sysbios_knl_Task___ISTAT iStat = 
#else
#define ti_sysbios_knl_Task___ISTAT
#endif
    ti_sysbios_knl_Task___ISTAT ti_sysbios_knl_Task_Instance_init(obj, args->fxn, &prms, __eb);
    if (xdc_runtime_Error_check(__eb)) {
#ifdef ti_sysbios_knl_Task___DELETE
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Task_Object__DESC__C, obj, iStat, __oa == 0);
#endif
        return 0;
    }
#if ti_sysbios_knl_Task___L_LIFECYCLE
#ifdef ti_sysbios_knl_Task___NAMEDINST
    xdc_runtime_Log_write2(__oa ? xdc_runtime_Log_L_construct : xdc_runtime_Log_L_create, (xdc_IArg)obj, (xdc_IArg)(((ti_sysbios_knl_Task_Object__*)obj)->__name));
#else
    xdc_runtime_Log_write2(__oa ? xdc_runtime_Log_L_construct : xdc_runtime_Log_L_create, (xdc_IArg)obj, 0);
#endif
#endif
    return obj;
}
#else
extern xdc_Ptr ti_sysbios_knl_Task_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    xdc_runtime_Error_raiseX(NULL, ti_sysbios_knl_Task_Module__id__C, NULL, 0, xdc_runtime_Error_E_generic, (xdc_IArg)"create policy error", 0);
    return 0;
}
#endif

#ifdef ti_sysbios_knl_Task___DELETE
extern xdc_Void ti_sysbios_knl_Task_Object__destruct__S( xdc_Ptr obj ) {
#if ti_sysbios_knl_Task___L_LIFECYCLE
    xdc_runtime_Log_write1(xdc_runtime_Log_L_destruct, (xdc_IArg)obj);
#endif
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Task_Object__DESC__C, obj, 0, TRUE);
}
extern xdc_Void ti_sysbios_knl_Task_Object__delete__S( xdc_Ptr instp ) {
#if ti_sysbios_knl_Task___L_LIFECYCLE
    xdc_runtime_Log_write1(xdc_runtime_Log_L_delete, (xdc_IArg)(*((ti_sysbios_knl_Task_Object**)instp)));
#endif
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Task_Object__DESC__C, *((ti_sysbios_knl_Task_Object**)instp), 0, FALSE);
    *((ti_sysbios_knl_Task_Handle*)instp) = 0;
}
#else
extern xdc_Void ti_sysbios_knl_Task_Object__destruct__S( xdc_Ptr obj ) {
    xdc_runtime_Error_raiseX(NULL, ti_sysbios_knl_Task_Module__id__C, NULL, 0, xdc_runtime_Error_E_generic, (xdc_IArg)"delete policy error", 0);
}
extern xdc_Void ti_sysbios_knl_Task_Object__delete__S( xdc_Ptr instp ) {
    xdc_runtime_Error_raiseX(NULL, ti_sysbios_knl_Task_Module__id__C, NULL, 0, xdc_runtime_Error_E_generic, (xdc_IArg)"delete policy error", 0);
}
#endif

#else /* __isrom__ */

#ifndef ti_sysbios_knl_Task___ROMPATCH
#undef Module__MID
#define Module__MID ti_sysbios_knl_Task_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Task_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Task_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Task_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Task_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Task_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Task_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Task_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Task_Module__gateObj__S()
#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Task_Module__gatePrms__S()
#undef Module__GF_alloc
#define Module__GF_alloc ti_sysbios_knl_Task_Module__G_alloc__S
#undef Module__GF_enter
#define Module__GF_enter ti_sysbios_knl_Task_Module__G_enter__S
#undef Module__GF_free
#define Module__GF_free ti_sysbios_knl_Task_Module__G_free__S
#undef Module__GF_leave
#define Module__GF_leave ti_sysbios_knl_Task_Module__G_leave__S
#undef Module__GF_query
#define Module__GF_query ti_sysbios_knl_Task_Module__G_query__S
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_startup__R
#else
#define __FN__ ti_sysbios_knl_Task_startup__F
#endif
xdc_Void ti_sysbios_knl_Task_startup__E( void ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_startup__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_startup__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_disable__R
#else
#define __FN__ ti_sysbios_knl_Task_disable__F
#endif
xdc_UInt ti_sysbios_knl_Task_disable__E( void ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_disable__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_disable__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_enable__R
#else
#define __FN__ ti_sysbios_knl_Task_enable__F
#endif
xdc_Void ti_sysbios_knl_Task_enable__E( void ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_enable__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_enable__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_restore__R
#else
#define __FN__ ti_sysbios_knl_Task_restore__F
#endif
xdc_Void ti_sysbios_knl_Task_restore__E( xdc_UInt key ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_restore__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_restore__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_restoreHwi__R
#else
#define __FN__ ti_sysbios_knl_Task_restoreHwi__F
#endif
xdc_Void ti_sysbios_knl_Task_restoreHwi__E( xdc_UInt key ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_restoreHwi__ENTRY_EVT, (xdc_IArg)key);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__(key);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_restoreHwi__EXIT_EVT, 0);
#else
    __FN__(key);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_self__R
#else
#define __FN__ ti_sysbios_knl_Task_self__F
#endif
ti_sysbios_knl_Task_Handle ti_sysbios_knl_Task_self__E( void ) {
#if ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_Handle __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_self__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_self__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_checkStacks__R
#else
#define __FN__ ti_sysbios_knl_Task_checkStacks__F
#endif
xdc_Void ti_sysbios_knl_Task_checkStacks__E( ti_sysbios_knl_Task_Handle oldTask, ti_sysbios_knl_Task_Handle newTask ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_checkStacks__ENTRY_EVT, (xdc_IArg)oldTask, (xdc_IArg)newTask);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__(oldTask, newTask);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_checkStacks__EXIT_EVT, 0);
#else
    __FN__(oldTask, newTask);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_exit__R
#else
#define __FN__ ti_sysbios_knl_Task_exit__F
#endif
xdc_Void ti_sysbios_knl_Task_exit__E( void ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_exit__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_exit__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_sleep__R
#else
#define __FN__ ti_sysbios_knl_Task_sleep__F
#endif
xdc_Void ti_sysbios_knl_Task_sleep__E( xdc_UInt nticks ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_sleep__ENTRY_EVT, (xdc_IArg)nticks);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__(nticks);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_sleep__EXIT_EVT, 0);
#else
    __FN__(nticks);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_yield__R
#else
#define __FN__ ti_sysbios_knl_Task_yield__F
#endif
xdc_Void ti_sysbios_knl_Task_yield__E( void ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_yield__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_yield__EXIT_EVT, 0);
#else
    __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getIdleTask__R
#else
#define __FN__ ti_sysbios_knl_Task_getIdleTask__F
#endif
ti_sysbios_knl_Task_Handle ti_sysbios_knl_Task_getIdleTask__E( void ) {
#if ti_sysbios_knl_Task___L_EXIT
    ti_sysbios_knl_Task_Handle __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write0(ti_sysbios_knl_Task_getIdleTask__ENTRY_EVT);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__();
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getIdleTask__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__();
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getArg0__R
#else
#define __FN__ ti_sysbios_knl_Task_getArg0__F
#endif
xdc_UArg ti_sysbios_knl_Task_getArg0__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_UArg __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getArg0__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getArg0__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getArg1__R
#else
#define __FN__ ti_sysbios_knl_Task_getArg1__F
#endif
xdc_UArg ti_sysbios_knl_Task_getArg1__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_UArg __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getArg1__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getArg1__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getEnv__R
#else
#define __FN__ ti_sysbios_knl_Task_getEnv__F
#endif
xdc_Ptr ti_sysbios_knl_Task_getEnv__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_Ptr __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getEnv__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getEnv__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getHookContext__R
#else
#define __FN__ ti_sysbios_knl_Task_getHookContext__F
#endif
xdc_Ptr ti_sysbios_knl_Task_getHookContext__E( ti_sysbios_knl_Task_Handle _this, xdc_Int id ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_Ptr __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_getHookContext__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)id);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this, id);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getHookContext__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this, id);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_getPri__R
#else
#define __FN__ ti_sysbios_knl_Task_getPri__F
#endif
xdc_Int ti_sysbios_knl_Task_getPri__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_Int __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getPri__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_getPri__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setArg0__R
#else
#define __FN__ ti_sysbios_knl_Task_setArg0__F
#endif
xdc_Void ti_sysbios_knl_Task_setArg0__E( ti_sysbios_knl_Task_Handle _this, xdc_UArg arg ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_setArg0__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)arg);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, arg);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setArg0__EXIT_EVT, 0);
#else
    __FN__((void*)_this, arg);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setArg1__R
#else
#define __FN__ ti_sysbios_knl_Task_setArg1__F
#endif
xdc_Void ti_sysbios_knl_Task_setArg1__E( ti_sysbios_knl_Task_Handle _this, xdc_UArg arg ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_setArg1__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)arg);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, arg);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setArg1__EXIT_EVT, 0);
#else
    __FN__((void*)_this, arg);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setEnv__R
#else
#define __FN__ ti_sysbios_knl_Task_setEnv__F
#endif
xdc_Void ti_sysbios_knl_Task_setEnv__E( ti_sysbios_knl_Task_Handle _this, xdc_Ptr env ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_setEnv__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)env);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, env);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setEnv__EXIT_EVT, 0);
#else
    __FN__((void*)_this, env);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setHookContext__R
#else
#define __FN__ ti_sysbios_knl_Task_setHookContext__F
#endif
xdc_Void ti_sysbios_knl_Task_setHookContext__E( ti_sysbios_knl_Task_Handle _this, xdc_Int id, xdc_Ptr hookContext ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write3(ti_sysbios_knl_Task_setHookContext__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)id, (xdc_IArg)hookContext);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, id, hookContext);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setHookContext__EXIT_EVT, 0);
#else
    __FN__((void*)_this, id, hookContext);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_setPri__R
#else
#define __FN__ ti_sysbios_knl_Task_setPri__F
#endif
xdc_UInt ti_sysbios_knl_Task_setPri__E( ti_sysbios_knl_Task_Handle _this, xdc_Int newpri ) {
#if ti_sysbios_knl_Task___L_EXIT
    xdc_UInt __ret;
#else
#endif
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_setPri__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)newpri);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __ret = __FN__((void*)_this, newpri);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_setPri__EXIT_EVT, (xdc_IArg)__ret);
    return __ret;
#else
    return __FN__((void*)_this, newpri);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_stat__R
#else
#define __FN__ ti_sysbios_knl_Task_stat__F
#endif
xdc_Void ti_sysbios_knl_Task_stat__E( ti_sysbios_knl_Task_Handle _this, ti_sysbios_knl_Task_Stat* statbuf ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_stat__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)statbuf);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, statbuf);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_stat__EXIT_EVT, 0);
#else
    __FN__((void*)_this, statbuf);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_block__R
#else
#define __FN__ ti_sysbios_knl_Task_block__F
#endif
xdc_Void ti_sysbios_knl_Task_block__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_block__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_block__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_unblock__R
#else
#define __FN__ ti_sysbios_knl_Task_unblock__F
#endif
xdc_Void ti_sysbios_knl_Task_unblock__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_unblock__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_unblock__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_blockI__R
#else
#define __FN__ ti_sysbios_knl_Task_blockI__F
#endif
xdc_Void ti_sysbios_knl_Task_blockI__E( ti_sysbios_knl_Task_Handle _this ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_blockI__ENTRY_EVT, (xdc_IArg)_this);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_blockI__EXIT_EVT, 0);
#else
    __FN__((void*)_this);
#endif
}
#undef __FN__
#if ti_sysbios_knl_Task___scope == -1
#define __FN__ ti_sysbios_knl_Task_unblockI__R
#else
#define __FN__ ti_sysbios_knl_Task_unblockI__F
#endif
xdc_Void ti_sysbios_knl_Task_unblockI__E( ti_sysbios_knl_Task_Handle _this, xdc_UInt hwiKey ) {
#if ti_sysbios_knl_Task___L_ENTRY
    xdc_runtime_Log_write2(ti_sysbios_knl_Task_unblockI__ENTRY_EVT, (xdc_IArg)_this, (xdc_IArg)hwiKey);
#endif
#if ti_sysbios_knl_Task___L_EXIT
    __FN__((void*)_this, hwiKey);
    xdc_runtime_Log_write1(ti_sysbios_knl_Task_unblockI__EXIT_EVT, 0);
#else
    __FN__((void*)_this, hwiKey);
#endif
}
#if ti_sysbios_knl_Task___scope == -1
xdc_Int ti_sysbios_knl_Task_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Task_Module_startup__R(state); }
#else
xdc_Int ti_sysbios_knl_Task_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Task_Module_startup__F(state); }
#endif
#endif /* ti_sysbios_knl_Task___ROMPATCH */
#ifdef ti_sysbios_knl_Task___ROMPATCH
xdc_Void ti_sysbios_knl_Task_startup__E( void ) {
    ti_sysbios_knl_Task_Module__MTAB__C.startup();
}
xdc_UInt ti_sysbios_knl_Task_disable__E( void ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.disable();
}
xdc_Void ti_sysbios_knl_Task_enable__E( void ) {
    ti_sysbios_knl_Task_Module__MTAB__C.enable();
}
xdc_Void ti_sysbios_knl_Task_restore__E( xdc_UInt key ) {
    ti_sysbios_knl_Task_Module__MTAB__C.restore(key);
}
xdc_Void ti_sysbios_knl_Task_restoreHwi__E( xdc_UInt key ) {
    ti_sysbios_knl_Task_Module__MTAB__C.restoreHwi(key);
}
ti_sysbios_knl_Task_Handle ti_sysbios_knl_Task_self__E( void ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.self();
}
xdc_Void ti_sysbios_knl_Task_checkStacks__E( ti_sysbios_knl_Task_Handle oldTask, ti_sysbios_knl_Task_Handle newTask ) {
    ti_sysbios_knl_Task_Module__MTAB__C.checkStacks(oldTask, newTask);
}
xdc_Void ti_sysbios_knl_Task_exit__E( void ) {
    ti_sysbios_knl_Task_Module__MTAB__C.exit();
}
xdc_Void ti_sysbios_knl_Task_sleep__E( xdc_UInt nticks ) {
    ti_sysbios_knl_Task_Module__MTAB__C.sleep(nticks);
}
xdc_Void ti_sysbios_knl_Task_yield__E( void ) {
    ti_sysbios_knl_Task_Module__MTAB__C.yield();
}
ti_sysbios_knl_Task_Handle ti_sysbios_knl_Task_getIdleTask__E( void ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.getIdleTask();
}
xdc_UArg ti_sysbios_knl_Task_getArg0__E( ti_sysbios_knl_Task_Handle _this ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.getArg0((void*)_this);
}
xdc_UArg ti_sysbios_knl_Task_getArg1__E( ti_sysbios_knl_Task_Handle _this ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.getArg1((void*)_this);
}
xdc_Ptr ti_sysbios_knl_Task_getEnv__E( ti_sysbios_knl_Task_Handle _this ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.getEnv((void*)_this);
}
xdc_Ptr ti_sysbios_knl_Task_getHookContext__E( ti_sysbios_knl_Task_Handle _this, xdc_Int id ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.getHookContext((void*)_this, id);
}
xdc_Int ti_sysbios_knl_Task_getPri__E( ti_sysbios_knl_Task_Handle _this ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.getPri((void*)_this);
}
xdc_Void ti_sysbios_knl_Task_setArg0__E( ti_sysbios_knl_Task_Handle _this, xdc_UArg arg ) {
    ti_sysbios_knl_Task_Module__MTAB__C.setArg0((void*)_this, arg);
}
xdc_Void ti_sysbios_knl_Task_setArg1__E( ti_sysbios_knl_Task_Handle _this, xdc_UArg arg ) {
    ti_sysbios_knl_Task_Module__MTAB__C.setArg1((void*)_this, arg);
}
xdc_Void ti_sysbios_knl_Task_setEnv__E( ti_sysbios_knl_Task_Handle _this, xdc_Ptr env ) {
    ti_sysbios_knl_Task_Module__MTAB__C.setEnv((void*)_this, env);
}
xdc_Void ti_sysbios_knl_Task_setHookContext__E( ti_sysbios_knl_Task_Handle _this, xdc_Int id, xdc_Ptr hookContext ) {
    ti_sysbios_knl_Task_Module__MTAB__C.setHookContext((void*)_this, id, hookContext);
}
xdc_UInt ti_sysbios_knl_Task_setPri__E( ti_sysbios_knl_Task_Handle _this, xdc_Int newpri ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.setPri((void*)_this, newpri);
}
xdc_Void ti_sysbios_knl_Task_stat__E( ti_sysbios_knl_Task_Handle _this, ti_sysbios_knl_Task_Stat* statbuf ) {
    ti_sysbios_knl_Task_Module__MTAB__C.stat((void*)_this, statbuf);
}
xdc_Void ti_sysbios_knl_Task_block__E( ti_sysbios_knl_Task_Handle _this ) {
    ti_sysbios_knl_Task_Module__MTAB__C.block((void*)_this);
}
xdc_Void ti_sysbios_knl_Task_unblock__E( ti_sysbios_knl_Task_Handle _this ) {
    ti_sysbios_knl_Task_Module__MTAB__C.unblock((void*)_this);
}
xdc_Void ti_sysbios_knl_Task_blockI__E( ti_sysbios_knl_Task_Handle _this ) {
    ti_sysbios_knl_Task_Module__MTAB__C.blockI((void*)_this);
}
xdc_Void ti_sysbios_knl_Task_unblockI__E( ti_sysbios_knl_Task_Handle _this, xdc_UInt hwiKey ) {
    ti_sysbios_knl_Task_Module__MTAB__C.unblockI((void*)_this, hwiKey);
}
#if ti_sysbios_knl_Task___scope == -1
xdc_Int ti_sysbios_knl_Task_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Task_Module_startup__R(state); }
#else
xdc_Int ti_sysbios_knl_Task_Module_startup__E( xdc_Int state ) { return ti_sysbios_knl_Task_Module_startup__F(state); }
#endif
#endif /* ti_sysbios_knl_Task___ROMPATCH */
xdc_Void ti_sysbios_knl_Task_startup__R( void ) {
    ti_sysbios_knl_Task_startup__F();
}
xdc_UInt ti_sysbios_knl_Task_disable__R( void ) {
    return ti_sysbios_knl_Task_disable__F();
}
xdc_Void ti_sysbios_knl_Task_enable__R( void ) {
    ti_sysbios_knl_Task_enable__F();
}
xdc_Void ti_sysbios_knl_Task_restore__R( xdc_UInt key ) {
    ti_sysbios_knl_Task_restore__F(key);
}
xdc_Void ti_sysbios_knl_Task_restoreHwi__R( xdc_UInt key ) {
    ti_sysbios_knl_Task_restoreHwi__F(key);
}
ti_sysbios_knl_Task_Handle ti_sysbios_knl_Task_self__R( void ) {
    return ti_sysbios_knl_Task_self__F();
}
xdc_Void ti_sysbios_knl_Task_checkStacks__R( ti_sysbios_knl_Task_Handle oldTask, ti_sysbios_knl_Task_Handle newTask ) {
    ti_sysbios_knl_Task_checkStacks__F(oldTask, newTask);
}
xdc_Void ti_sysbios_knl_Task_exit__R( void ) {
    ti_sysbios_knl_Task_exit__F();
}
xdc_Void ti_sysbios_knl_Task_sleep__R( xdc_UInt nticks ) {
    ti_sysbios_knl_Task_sleep__F(nticks);
}
xdc_Void ti_sysbios_knl_Task_yield__R( void ) {
    ti_sysbios_knl_Task_yield__F();
}
ti_sysbios_knl_Task_Handle ti_sysbios_knl_Task_getIdleTask__R( void ) {
    return ti_sysbios_knl_Task_getIdleTask__F();
}
xdc_UArg ti_sysbios_knl_Task_getArg0__R( ti_sysbios_knl_Task_Handle _this ) {
    return ti_sysbios_knl_Task_getArg0__F((void*)_this);
}
xdc_UArg ti_sysbios_knl_Task_getArg1__R( ti_sysbios_knl_Task_Handle _this ) {
    return ti_sysbios_knl_Task_getArg1__F((void*)_this);
}
xdc_Ptr ti_sysbios_knl_Task_getEnv__R( ti_sysbios_knl_Task_Handle _this ) {
    return ti_sysbios_knl_Task_getEnv__F((void*)_this);
}
xdc_Ptr ti_sysbios_knl_Task_getHookContext__R( ti_sysbios_knl_Task_Handle _this, xdc_Int id ) {
    return ti_sysbios_knl_Task_getHookContext__F((void*)_this, id);
}
xdc_Int ti_sysbios_knl_Task_getPri__R( ti_sysbios_knl_Task_Handle _this ) {
    return ti_sysbios_knl_Task_getPri__F((void*)_this);
}
xdc_Void ti_sysbios_knl_Task_setArg0__R( ti_sysbios_knl_Task_Handle _this, xdc_UArg arg ) {
    ti_sysbios_knl_Task_setArg0__F((void*)_this, arg);
}
xdc_Void ti_sysbios_knl_Task_setArg1__R( ti_sysbios_knl_Task_Handle _this, xdc_UArg arg ) {
    ti_sysbios_knl_Task_setArg1__F((void*)_this, arg);
}
xdc_Void ti_sysbios_knl_Task_setEnv__R( ti_sysbios_knl_Task_Handle _this, xdc_Ptr env ) {
    ti_sysbios_knl_Task_setEnv__F((void*)_this, env);
}
xdc_Void ti_sysbios_knl_Task_setHookContext__R( ti_sysbios_knl_Task_Handle _this, xdc_Int id, xdc_Ptr hookContext ) {
    ti_sysbios_knl_Task_setHookContext__F((void*)_this, id, hookContext);
}
xdc_UInt ti_sysbios_knl_Task_setPri__R( ti_sysbios_knl_Task_Handle _this, xdc_Int newpri ) {
    return ti_sysbios_knl_Task_setPri__F((void*)_this, newpri);
}
xdc_Void ti_sysbios_knl_Task_stat__R( ti_sysbios_knl_Task_Handle _this, ti_sysbios_knl_Task_Stat* statbuf ) {
    ti_sysbios_knl_Task_stat__F((void*)_this, statbuf);
}
xdc_Void ti_sysbios_knl_Task_block__R( ti_sysbios_knl_Task_Handle _this ) {
    ti_sysbios_knl_Task_block__F((void*)_this);
}
xdc_Void ti_sysbios_knl_Task_unblock__R( ti_sysbios_knl_Task_Handle _this ) {
    ti_sysbios_knl_Task_unblock__F((void*)_this);
}
xdc_Void ti_sysbios_knl_Task_blockI__R( ti_sysbios_knl_Task_Handle _this ) {
    ti_sysbios_knl_Task_blockI__F((void*)_this);
}
xdc_Void ti_sysbios_knl_Task_unblockI__R( ti_sysbios_knl_Task_Handle _this, xdc_UInt hwiKey ) {
    ti_sysbios_knl_Task_unblockI__F((void*)_this, hwiKey);
}
xdc_Int ti_sysbios_knl_Task_Module_startup__R( xdc_Int state ) { return ti_sysbios_knl_Task_Module_startup__F(state); }
int ti_sysbios_knl_Task_Instance_init__R( ti_sysbios_knl_Task_Object* __obj, ti_sysbios_knl_Task_FuncPtr fxn, const ti_sysbios_knl_Task_Params* __prms, xdc_runtime_Error_Block* __eb ) {
    return ti_sysbios_knl_Task_Instance_init__F(__obj, fxn, __prms, __eb); }
void ti_sysbios_knl_Task_Instance_finalize__R( ti_sysbios_knl_Task_Object* obj, int iStat ) {
    ti_sysbios_knl_Task_Instance_finalize__F(obj, iStat); }
xdc_runtime_Types_Label* ti_sysbios_knl_Task_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Handle__label(obj, lab);}
xdc_Ptr ti_sysbios_knl_Task_Module__gateObj__S( void ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Module__gateObj();}
xdc_Ptr ti_sysbios_knl_Task_Module__gatePrms__S( void ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Module__gatePrms();}
xdc_runtime_Types_GateRef ti_sysbios_knl_Task_Module__G_alloc__S( xdc_Ptr prms, xdc_runtime_Error_Block* eb ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Module__G_alloc(prms, eb);}
xdc_IArg ti_sysbios_knl_Task_Module__G_enter__S( xdc_runtime_Types_GateRef ref ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Module__G_enter(ref);}
xdc_Void ti_sysbios_knl_Task_Module__G_free__S( xdc_runtime_Types_GateRef ref ) {
    ti_sysbios_knl_Task_Module__MTAB__C.Module__G_free(ref);}
xdc_Void ti_sysbios_knl_Task_Module__G_leave__S( xdc_runtime_Types_GateRef ref, xdc_IArg key ) {
    ti_sysbios_knl_Task_Module__MTAB__C.Module__G_leave(ref, key);}
xdc_Bool ti_sysbios_knl_Task_Module__G_query__S( xdc_Int qual ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Module__G_query(qual);}
xdc_Bool ti_sysbios_knl_Task_Module__startupDone__S( void ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Module__startupDone();}
xdc_Ptr ti_sysbios_knl_Task_Object__create__S( xdc_Ptr __oa, const xdc_Ptr __aa, const xdc_Ptr __pa, xdc_SizeT __sz, xdc_runtime_Error_Block* __eb ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Object__create(__oa, __aa, __pa, __sz, __eb);}
xdc_Void ti_sysbios_knl_Task_Object__delete__S( xdc_Ptr instp ) {
    ti_sysbios_knl_Task_Module__MTAB__C.Object__delete(instp);}
xdc_Void ti_sysbios_knl_Task_Object__destruct__S( xdc_Ptr objp ) {
    ti_sysbios_knl_Task_Module__MTAB__C.Object__destruct(objp);}
xdc_Ptr ti_sysbios_knl_Task_Object__get__S( xdc_Ptr oarr, xdc_Int i ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Object__get(oarr, i);}
xdc_Void ti_sysbios_knl_Task_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) {
    ti_sysbios_knl_Task_Module__MTAB__C.Params__init(dst, src, psz, isz);}
xdc_Bool ti_sysbios_knl_Task_Proxy__abstract__S( void ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Proxy__abstract();}
xdc_Ptr ti_sysbios_knl_Task_Proxy__delegate__S( void ) {
    return ti_sysbios_knl_Task_Module__MTAB__C.Proxy__delegate();}
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Instance_init__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Instance_finalize__R);

#if defined(ti_sysbios_knl_Task___EXPORT) && defined(__ti__)
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_Module_startup__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_startup__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_disable__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_enable__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_restore__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_restoreHwi__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_self__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_checkStacks__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_exit__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_sleep__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_yield__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getIdleTask__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getArg0__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getArg1__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getEnv__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getHookContext__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_getPri__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setArg0__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setArg1__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setEnv__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setHookContext__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_setPri__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_stat__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_block__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_unblock__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_blockI__R);
#pragma FUNC_EXT_CALLED(ti_sysbios_knl_Task_unblockI__R);
#endif
#if ti_sysbios_knl_Task___scope != -1
#ifdef __ti__
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module_State_inactiveQ__O, ".const:ti_sysbios_knl_Task_Module_State_inactiveQ__O");
asm("	.sect \".const:ti_sysbios_knl_Task_Module_State_inactiveQ__O\"");
asm("	 .clink");
asm("	.sect \"[0].const:ti_sysbios_knl_Task_Module_State_inactiveQ__O\"");
asm("	 .clink");
asm("	.sect \"[1].const:ti_sysbios_knl_Task_Module_State_inactiveQ__O\"");
asm("	 .clink");
#endif
__FAR__ const xdc_SizeT ti_sysbios_knl_Task_Module_State_inactiveQ__O = offsetof(ti_sysbios_knl_Task_Module_State, inactiveQ);
#endif
#endif /* __isrom__ */
#endif /* $__used__ */

#ifdef ti_sysbios_knl_Task_SupportProxy___used
/*
 *  ======== module Task_SupportProxy ========
 *  Do not modify this file; it is generated from the specification Task_SupportProxy.xdc
 *  and any modifications risk being overwritten.
 */

#ifndef ti_sysbios_knl_Task_SupportProxy__include
#ifndef __nested__
#define __nested__
#include <ti/sysbios/knl/Task_SupportProxy.h>
#undef __nested__
#else
#include <ti/sysbios/knl/Task_SupportProxy.h>
#endif
#endif

#ifndef __isrom__


#if defined(ti_sysbios_knl_Task_SupportProxy___DELEGATE)&& ti_sysbios_knl_Task_SupportProxy___scope != -1
#define ti_sysbios_knl_Task_SupportProxy___PROXYFXNS(__PRE)\
xdc_Bool ti_sysbios_knl_Task_SupportProxy_Module__startupDone__S() {\
    return __CONC__(__PRE,Module__startupDone__S)(); }\
xdc_Ptr ti_sysbios_knl_Task_SupportProxy_start__E( xdc_Ptr curTask, ti_sysbios_interfaces_ITaskSupport_FuncPtr enter, ti_sysbios_interfaces_ITaskSupport_FuncPtr exit, xdc_runtime_Error_Block* eb ) {\
    return __CONC__(__PRE,start)(curTask, enter, exit, eb); }\
xdc_Void ti_sysbios_knl_Task_SupportProxy_swap__E( xdc_Ptr* oldtskContext, xdc_Ptr* newtskContext ) {\
    __CONC__(__PRE,swap)(oldtskContext, newtskContext); }\
xdc_Ptr ti_sysbios_knl_Task_SupportProxy_checkStacks__E( xdc_Ptr oldTaskStack, xdc_SizeT oldTaskStackSize, xdc_Ptr newTaskStack, xdc_SizeT newTaskStackSize ) {\
    return __CONC__(__PRE,checkStacks)(oldTaskStack, oldTaskStackSize, newTaskStack, newTaskStackSize); }\
xdc_SizeT ti_sysbios_knl_Task_SupportProxy_stackUsed__E( xdc_Char* stack, xdc_SizeT size ) {\
    return __CONC__(__PRE,stackUsed)(stack, size); }\
xdc_UInt ti_sysbios_knl_Task_SupportProxy_getStackAlignment__E( void ) {\
    return __CONC__(__PRE,getStackAlignment)(); }\
xdc_SizeT ti_sysbios_knl_Task_SupportProxy_getDefaultStackSize__E( void ) {\
    return __CONC__(__PRE,getDefaultStackSize)(); }\

ti_sysbios_knl_Task_SupportProxy___PROXYFXNS(ti_sysbios_knl_Task_SupportProxy___DELEGATE)
#endif

xdc_Bool ti_sysbios_knl_Task_SupportProxy_Proxy__abstract__S()
#ifdef ti_sysbios_knl_Task_SupportProxy___ABSTRACT
    { return 1; }
#else
    { return 0; }
#endif
xdc_Ptr ti_sysbios_knl_Task_SupportProxy_Proxy__delegate__S() { return (xdc_Ptr)ti_sysbios_knl_Task_SupportProxy___DELEGATE_FXNS; }

#else /* __isrom__ */


#if defined(ti_sysbios_knl_Task_SupportProxy___DELEGATE)
#define ti_sysbios_knl_Task_SupportProxy___PROXYFXNS(__PRE)\
xdc_Bool ti_sysbios_knl_Task_SupportProxy_Module__startupDone__S() {\
    return __CONC__(__PRE,Module__startupDone__S)(); }\
xdc_Ptr ti_sysbios_knl_Task_SupportProxy_start__E( xdc_Ptr curTask, ti_sysbios_interfaces_ITaskSupport_FuncPtr enter, ti_sysbios_interfaces_ITaskSupport_FuncPtr exit, xdc_runtime_Error_Block* eb ) {\
    return __CONC__(__PRE,start)(curTask, enter, exit, eb); }\
xdc_Void ti_sysbios_knl_Task_SupportProxy_swap__E( xdc_Ptr* oldtskContext, xdc_Ptr* newtskContext ) {\
    __CONC__(__PRE,swap)(oldtskContext, newtskContext); }\
xdc_Ptr ti_sysbios_knl_Task_SupportProxy_checkStacks__E( xdc_Ptr oldTaskStack, xdc_SizeT oldTaskStackSize, xdc_Ptr newTaskStack, xdc_SizeT newTaskStackSize ) {\
    return __CONC__(__PRE,checkStacks)(oldTaskStack, oldTaskStackSize, newTaskStack, newTaskStackSize); }\
xdc_SizeT ti_sysbios_knl_Task_SupportProxy_stackUsed__E( xdc_Char* stack, xdc_SizeT size ) {\
    return __CONC__(__PRE,stackUsed)(stack, size); }\
xdc_UInt ti_sysbios_knl_Task_SupportProxy_getStackAlignment__E( void ) {\
    return __CONC__(__PRE,getStackAlignment)(); }\
xdc_SizeT ti_sysbios_knl_Task_SupportProxy_getDefaultStackSize__E( void ) {\
    return __CONC__(__PRE,getDefaultStackSize)(); }\

ti_sysbios_knl_Task_SupportProxy___PROXYFXNS(ti_sysbios_knl_Task_SupportProxy___DELEGATE)
#endif

#endif /* __isrom__ */
#endif /* $__used__ */


#endif
