/* --COPYRIGHT--,EPL
 *Copyright (c) 2012 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== setdate.c ========
 *  Simplified (and enhanced) touch command 
 *  
 *  usage: setdate [-help] [-r ref-file] dest-file ...
 */
#include <xdc/std.h>
#include <xdc/services/host/lib/xutl.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(xdc_target__os_Windows)
#include <io.h>
#else
#include <unistd.h>
#include <sys/types.h>
#endif

static String usage = "%s: [-help] [-r[:max|:min] ref-file] dest-file ...\n";

static String help =
    "Set timestamps of the specified files, creating the files if necessary\n"
    "  usage: setdate [-help] [-r[:max|:min] ref-file] dest-file ...\n"
    "    -help       Display this message\n"
    "    -r ref-file Use the corresponding times of the file ref-file\n"
    "                instead of the current time to update dest-file\n"
    "                times.\n\n"
    "                If the optional \":max\" qualifier is present, the\n"
    "                date of dest-file will be the max of ref-file and\n"
    "                the original dest-file date.  Similarily, if \":min\"\n"
    "                is used, the dest-file date will be the minimum of\n"
    "                the ref-file date and the original dest-file date.\n";
    
static String progName = NULL;
static String rfile    = NULL;
static Int    cmpFlag  = 0;

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    Int i;
    
    XUTL_init();
    
    progName = argv[0];
    
    /* parse command line options */
    for (i = 1; i < argc; i++) {
        if (argv[i][0] == '-') {
            switch (argv[i][1]) {
                case 'r': {
                    /* look for -r: options */
                    if (argv[i][2] == ':') {
                        if (strcmp(":min", argv[i] + 2) == 0) {
                            cmpFlag = -1;
                        }
                        else if (strcmp(":max", argv[i] + 2) == 0) {
                            cmpFlag = 1;
                        }
                        else {
                            fprintf(stderr, "%s: unrecognized option '%s'\n",
                                    progName, argv[i]);
                            return (1);
                        }
                    }

                    /* get reference file */
                    i++;
                    if (argc <= i) {
                        fprintf(stderr, usage, progName);
                         return (1);
                    }
                    rfile = argv[i];
                    if (access(rfile, 00) != 0) {
                        fprintf(stderr, "%s: %s does not exist\n",
                                progName, rfile);
                        return (1);
                    }
                    break;
                }
                case 'h': {
                    printf(help, progName);
                    return (0);
                }
                default: {
                    fprintf(stderr, usage, progName);
                    return (1);
                }
            }
        }
        else {
            break;
        }
    }

    /* there must be at least one file to update */
    if ((argc - i) < 1) {
        fprintf(stderr, usage, progName);
        return (1);
    }

    /* set date all files specified on the command line */
    for (; i < argc; i++) {
        if (XUTL_setDate(argv[i], rfile, cmpFlag) != TRUE) {
            fprintf(stderr, "%s: setdate of %s failed: %s\n", progName,
                    argv[i], XUTL_getLastErrorString());
            return (1);
        }
    }
    
    return (0);
}
