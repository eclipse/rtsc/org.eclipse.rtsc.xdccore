/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== stderr.c ========
 *  Execute command (with arguments) and re-direct its stderr to stdout.
 *  The exit status of this command is the exit status of the command
 *  executed.
 *
 *  Usage: stderr [-o file] [cmd [args ...]]
 *
 *  Options:
 *      -o file	    redirect stdout to the specified file.  If the file
 *		    name is '--', stdout is redirected to /dev/null on
 *		    UNIX platforms and nul in Win32 platforms.
 *
 *  Examples:
 *    The following commands run the 'ls foo' command and redirects both
 *    stderr and stdout to the file bar.
 *
 *	    stderr ls foo > bar
 *
 *	    stderr -o bar ls foo
 *
 *    The following command runs the 'ls foo' command and redirects both
 *    stderr and stdout to /dev/null
 *
 *	    stderr -o -- ls foo
 *
 *    This is useful when only the exit status of the command is needed.
 */

#include <xdc/std.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined(xdc_target__os_Windows)
#include <io.h>
#include <process.h>

#define DEVNULL	    "nul"

/* windows specific helper functions */
static char **getArgs(const char *const *argv);
static void u2d(char *buf);
#else
#define DEVNULL	    "/dev/null"
#endif

static char *usage = "%s [-o file] [prog arg ...]\n";
static int dflag = 0;
static char *progName = "";

static void printCmd(char *argv[]);
static uintptr_t runCmd(char *cmd, char *argv[]);

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    uintptr_t status = 0;
    char *fileName;

    progName = argv[0];

    argv++;
    argc--;
    while (argc >= 1 && (argv[0][0] == '-')) {
	switch (argv[0][1]) {
	    case 'o': {
		if (argc <= 1) {
		    fprintf(stderr, usage, progName);
		    exit(1);
		}
		fileName = argv[1];
		if (strcmp(fileName, "--") == 0) {
		    fileName = DEVNULL;
		}
		if (freopen(fileName, "w", stdout) == NULL) {
		    fprintf(stderr, "%s: can't open %s ", progName,
			fileName);
		    perror("because");
		    exit(1);
		}
		argc--;
		argv++;
		break;
	    }

	    case 'd': {
		dflag++;
		break;
	    }

	    default: {
		fprintf(stderr, usage, progName);
		exit(1);
		break;
	    }
	}

	argv++;
	argc--;
    }

    if (argc >= 1) {
#if defined(xdc_target__os_Windows)
	u2d(argv[0]);    /* needed by Win98 (sigh) */
#endif
    }

    /* re-direct all stderr output to stdout */
    dup2(1, 2);

    /* execute any command that is on the command line */
    if (argc >= 1) {
        status = runCmd(argv[0], argv);
    }

    /* if the exec failed, return its error status */
    exit ((UInt)status);
}

#if defined(xdc_target__os_Windows)
/*
 *  ======== getArgs ========
 *  add "'s around all arguments to counter-act argument processing done
 *  by #@^#%^! Win32 spawn command.
 */
static char **getArgs(const char *const *argv)
{
    const char *const *tmp;
    int argc, i;
    char **args;

    /* count number of arguments in argv and create local array of arguments */
    for (tmp = argv, argc = 0; *tmp != NULL; tmp++) {
	argc++;
    }
    if ((args = malloc(sizeof(char *) * (argc + 1))) == NULL) {
	return (NULL);
    }

    /* initialize local argument array from argv */
    args[0] = (char *)argv[0];

    /* quote all arguments to command (to preserve white space in args) */
    for (i = 1; i < argc; i++) {
	const char *src;
	char *dst;
	
	if ((args[i] = malloc(2 * strlen(argv[i]) + 3)) == NULL) {
	    return (NULL);
	}
	
	dst = args[i];
	*dst++ = '"';
	for (src = argv[i]; *src != '\0'; ) {
	    if (*src == '"') {
		*dst++ = '\\';	/* backquote embedded "'s */
	    }
	    *dst++ = *src++;
	}
	*dst++ = '"';
	*dst = '\0';
    }
    args[argc] = NULL;

    return (args);
}
#endif

/*
 *  ======== printCmd ========
 *  Dump command to stderr for debug
 */
static void printCmd(char *argv[])
{
    int i;
    int argc;
    char **tmp;

    /* count number of arguments in argv and create local array of arguments */
    for (tmp = argv, argc = 0; *tmp != NULL; tmp++) {
        argc++;
    }

    for (i = 0; i < argc; i++) {
        fprintf(stderr, "%s ", argv[i]);
    }
}

/*
 *  ======== runCmd ========
 *  Run specified command
 */
static uintptr_t runCmd(char *cmd, char *argv[])
{
    uintptr_t status;
#if defined(xdc_target__os_Windows)
    const char *const *args = getArgs(argv);

    if (dflag > 0) {
	printCmd((char **)args);
	fprintf(stderr, "\n");
    }

    /*
     *  Even though Win32 provides execvp, it does not have the same effect
     *  when it comes to the exit status of the calling program.  Therefore,
     *  we are forced to re-implement this function using Win32 primitives
     *  and explicitly exit the calling process with the proper status.
     */
    if ((status = _spawnvp(_P_WAIT, cmd, args)) >= 0) {
	return (status);
    }

    argv = (char **)args;   /* replace argv with new args for code below */
#else
    if (dflag > 0) {
	printCmd(argv);
	fprintf(stderr, "\n");
    }

    status = execvp(cmd, argv);
#endif

    fprintf(stderr, "%s: can't execute '", progName);
    printCmd(argv);
    fprintf(stderr, "' (exit status = %zu) ", status);
    perror("because");

    return (status);
}

#if defined(xdc_target__os_Windows)
/*
 *  ======== u2d ========
 */
static void u2d(char *buf)
{
    char *cp;
    for (cp = buf; *cp != '\0'; cp++) {
	if (*cp == '/') {
	    *cp = '\\';
	}
    }
}
#endif
