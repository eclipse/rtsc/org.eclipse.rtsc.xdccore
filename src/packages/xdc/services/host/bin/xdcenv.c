/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== xdcenv.c ========
 *  Create or overwrite an XDC environment file only if the new file
 *  and the old file differ.
 *
 *  Usage: xdcenv destination
 *
 *  Options:
 *      -d      debug flag
 *      -v      verbose flag
 *      -i      increment build count number
 *
 *  Examples:
 *    The following command overwrites env.mak only if the newly created
 *    contents differ from env.mak.
 *
 *          xdcenv env.mak
 */
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <xdc/services/host/lib/xutl.h>
#include <errno.h>

#if defined(xdc_target__os_Windows)
#include <io.h>
#include <sys/stat.h>
#include <sys/utime.h>

#define READWRITE   (S_IWRITE | S_IREAD)

#include <windows.h>
#define hide(fname)  (SetFileAttributes((fname), FILE_ATTRIBUTE_HIDDEN))

#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <utime.h>

#define READWRITE   (S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH)
#endif

#define BUFSIZE         512
#define MAXNAME         80
#define LINEQUANTUM     128

/*
 *  The following flags control how to compare and generate the xdcenv file
 */
#define ISPAIR          1   /* is Env a name-value pair? */
#define ISPATH          2   /* is value a path name (or names)? */
#define COMMENT         4   /* do differences require rebuild */
#define OVERRIDE        8   /* this name-value pair has an override prefix */

typedef struct Env {
    String name;    /* name of environment variable */
    String value;   /* value of variable */
    UInt    flags;   /* ISPAIR, ISPATH, ... */
} Env;

static Env envTab[] = {
    {"#\n",             NULL, 0},
    {"_XDCBUILDCOUNT",  NULL, ISPAIR},  /* this must appear first!!! otherwise
                                         * changes to another value will
                                         * short-circuit the processing and an
                                         * update to the count may not occur
                                         */
    {"ifneq (,$(findstring path,$(_USEXDCENV_)))\n",    NULL, 0},
    {"XDCPATH",         NULL, ISPAIR | ISPATH | OVERRIDE},
    {"XDCROOT",         NULL, ISPAIR | ISPATH | OVERRIDE},
    {"XDCBUILDCFG",     NULL, ISPAIR | ISPATH | OVERRIDE},
    {"endif\n",         NULL, 0},
    {"ifneq (,$(findstring args,$(_USEXDCENV_)))\n",    NULL, 0},
    {"XDCARGS",         NULL, ISPAIR | OVERRIDE},
    {"XDCTARGETS",      NULL, ISPAIR | OVERRIDE},
    {"endif\n",         NULL, 0},
    {"#\n",             NULL, 0},
    {"ifeq (0,1)\n",    NULL, 0},
    {"PKGPATH",         NULL, ISPAIR | ISPATH},
    {"HOSTOS",          NULL, ISPAIR},
    {"endif\n",         NULL, 0},
    {NULL,              NULL, 0},
};

#define NUMENV (sizeof(envTab) / sizeof(Env))

static Int envPairs = 0;   /* num of 'name = value' entries in envTab */
static Int oldPairs = 0;   /* num of 'name = value' lines read from file */

static Char *lineBuf;
static SizeT lineBufLen;

static String usage = "usage: %s [-d] [-v] [-i] destination\n";

static Int  compare(Env new[], Char *old);
static Int  cmpEnv(Env *env, String value);
static Char *genContents(Env new[]);
static Void genTab(Env new[]);
static Void getTime(String fileName, struct utimbuf *timePtr);
static Int  processLine(String line);

static struct utimbuf timeBuf;
static Bool rebuildFlag = FALSE;
static String progName = "";

static Int dflag = 0;   /* debug flag */
static Int vflag = 0;   /* verbose debug flag */
static Int iflag = 0;   /* increment build count flag */

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    Int status = 0;
    Int i;
    Char *contents;
    FILE *file;

    XUTL_init();

    progName = argv[0];

    /* parse command line options */
    for (i = 1; i < argc && (argv[i][0] == '-'); i++) {
        switch (argv[i][1]) {
            case 'd': {
                dflag++;
                break;
            }
            case 'v': {
                vflag++;
                break;
            }
            case 'i': {
                iflag++;
                break;
            }
            default: {
                fprintf(stderr, usage, progName);
                exit(1);
                break;
            }
        }
    }
    argv += i;
    argc -= i;

    if (argc != 1) {
        fprintf(stderr, usage, progName);
        exit(1);
    }

    /* get name-value data from environment */
    genTab(envTab);

    /* compare to previous contents in file named by argv[0] */
    if ((status = compare(envTab, argv[0])) > 0) {
        /* new contents differ from saved contents */

        /* capture file times so we can set output file times appropriately */
        getTime(argv[0], &timeBuf);

        /* generate new file contents */
        if ((contents = genContents(envTab)) == NULL) {
            fprintf(stderr, "%s: can't update '%s', ", progName, argv[0]);
            perror("because");
            return (2);
        }

        /* remove destination file */
        XUTL_rm(argv[0]);

        /* re-write destination file */
        if ((file = fopen(argv[0], "wb")) == NULL) {
            fprintf(stderr, "%s: can't open '%s', ", progName, argv[0]);
            perror("because");
        }
        else {
            if (fwrite(contents, strlen(contents), 1, file) <= 0) {
                fprintf(stderr, "%s: can't write '%s', ",
                    progName, argv[0]);
                perror("because");
            }
            fclose(file);

            /* make sure file is readable by others */
            chmod(argv[0], READWRITE);

            /* if the changes are insignificant for build, restore old date */
            if (rebuildFlag == FALSE) {
                utime(argv[0], &timeBuf);
                if (dflag && vflag) {
                    printf("file %s changed but post dated.\n", argv[0]);
                }
            }
            status = 0;
        }
#if defined(xdc_target__os_Windows)
        if (argv[0][0] == '.') {
            hide(argv[0]);
        }
#endif
    }
    else if (status == 0) {
        /* source and destination files both exist and are identical */
        if (dflag && vflag) {
            printf("file %s identical.\n", argv[0]);
        }
    }
    else {
        fprintf(stderr, "%s: can't compare to '%s' ", progName, argv[0]);
        perror("because");
        status = 2;
    }

    /* if the command failed, return its error status */
    return (status);
}

/*
 *  ======== compare ========
 */
static Int compare(Env new[], String fileName)
{
    FILE *in = NULL;
    Char *done = NULL;
    String line;
    SizeT lineLen;
    Int status = 0;

    if (new == NULL) {
        status = -1;
        goto exit;
    }

    if ((lineBuf = (Char *)malloc(LINEQUANTUM)) == NULL) {
        perror("out of memory");
        status = -1;
        goto exit;
    }
    lineBufLen = LINEQUANTUM;

    if ((in = fopen(fileName, "rb")) == NULL) {
        status = 1;
        if (errno == ENOENT) {  /* the file doesn't exist (must be created) */
            String use;
            Int i;
            /* if "re-building", emit a warning that env file is missing */
            if ((use = getenv("_USEXDCENV_")) != NULL && use[0] != '\0') {
                fprintf(stderr, "%s: warning: can't rebuild a package that has never been built; using the values from the current environment to create %s:\n", progName, fileName);
                for (i = 0; i < NUMENV - 1; i++) {
                    if (new[i].flags & OVERRIDE) {
                        fprintf(stderr, "    %s = \"%s\"\n",
                            new[i].name, new[i].value);
                    }
                }
            }
        }
        goto exit;
    }

    do {
        /* read a complete line from the file */
        lineBuf[0] = '\0';
        line = lineBuf;
        lineLen = lineBufLen;
        while ((done = fgets(line, (UInt)lineLen, in)) != NULL) {
            SizeT len = strlen(lineBuf);
            if (lineBuf[len - 1] == '\n') {
                lineBuf[len - 1] = '\0';  /* trim newlines to simplify
                                           * compare in processLine()
                                           */
                break;
            }
            else if (len < (lineBufLen - 1)) {
                break;
            }
            else {
                /* don't have a full line yet */
                lineBufLen += LINEQUANTUM;
                if ((lineBuf = (Char *)realloc(lineBuf, lineBufLen)) == NULL) {
                    perror("out of memory");
                    status = -1;
                    goto exit;
                }
                line = lineBuf + len;
            }
            lineLen = lineBufLen - (line - lineBuf);
        }

        /* check this line */
        if (processLine(lineBuf) != 0) {
            status = 1;                     /* re-write env file */
            if (rebuildFlag == TRUE) {      /* update time of file */
                break;
            }
        }
    } while (done != NULL);

    /* if all lines match but the tables have differing lengths, fail */
    if (status == 0 && oldPairs != envPairs) {
        if (dflag) {
            printf("env differs:\n    old: %d entries\n    new: %d entries\n",
                oldPairs, envPairs);
        }
        status = 1;
    }

exit:
    if (in) {
        fclose(in);
    }
    return (status);
}

/*
 *  ======== cmpEnv ========
 */
static Int cmpEnv(Env *env, String value)
{
    Int status;

#if defined(xdc_target__os_Windows)
    /* if the value is a path, we do case insensitive compare because Windows is
     * case-insensitive and although it preserves case on file names it does not
     * preserve case of drive letters(!)
     */
    status = (env->flags & ISPATH)
        ? stricmp(env->value, value) : strcmp(env->value, value);
#else
    status = strcmp(env->value, value);
#endif

    if (status != 0) {
        status = 1;
        if ((env->flags & COMMENT) == 0) {
            rebuildFlag = TRUE;
        }
        if (dflag) {
            printf("env differs:\n    old: %s = %s\n", env->name, value);
            printf("    new: %s = %s\n", env->name, env->value);
        }
    }

    return (status);
}

/*
 *  ======== genContents ========
 *  Generate the contents of the file as a string and return the string.
 *  If an error occurs (e.g., out of memory), return NULL.
 */
static Char *genContents(Env new[])
{
    Int i;
    SizeT len = 0;  /* length of physically allocated buffer */
    Char *buf;      /* output buffer */

    /* allocate initial output buffer */
    if ((buf = malloc(LINEQUANTUM)) != NULL) {
        buf[0] = '\0';

        /* append content to buf */
        for (i = 0; new[i].name != NULL; i++) {

            /* if item is a name-value pair, gen an "name=value" assignment */
            if (new[i].flags & ISPAIR) {
                /* get the value for the name */
                String val = new[i].value;

                /* allocate space for the pair */
                len += strlen(new[i].name) + 32 + strlen(val);
                if ((buf = realloc(buf, len)) == NULL) {
                    return (NULL);
                }

                /* write the pair to the buffer (the contents of the file) */
                if (new[i].flags & OVERRIDE) {
                    strcat(buf, "override ");
                }
                strcat(buf, new[i].name);
                strcat(buf, " = ");
                strcat(buf, val);
                strcat(buf, "\n");
            }
            else {  /* otherwise the new[i] is simply an output line */
                /* increase buffer by length of comment */
                len += strlen(new[i].name) + 8;
                if ((buf = realloc(buf, len)) == NULL) {
                    return (NULL);
                }
                /* concatenate comment to the buffer */
                strcat(buf, new[i].name);
            }
        }
    }

    return (buf);
}

/*
 *  ======== genTab ========
 *  Generate table of the name-value pairs.
 */
static Void genTab(Env env[])
{
    Int i;

    for (i = 0; env[i].name != NULL; i++) {

        /* if item is a name-value pair, get the new value */
        if (env[i].flags & ISPAIR) {

            /* look it up in the environment */
            Char *val = getenv(env[i].name);

            if (val == NULL) {
                val = "";
            }

            /* write the value to the Env table env */
            env[i].value = val;
            envPairs++;
        }
    }
}

/*
 *  ======== getTime ========
 */
static Void getTime(String fileName, struct utimbuf *timePtr)
{
    time_t t;
    struct stat buf;
    timePtr->modtime = 0;
    timePtr->actime = 0;

    if (stat(fileName, &buf) >= 0) {
        timePtr->modtime = buf.st_mtime;
        timePtr->actime = buf.st_atime;
        return;
    }

    if (errno == ENOENT) {
        /* create an empty file to get a time from the filesystem rather
         * than the CPU because there are often problems with file servers
         * that have poor time synchronization with the running CPU.
         */
        FILE *tmp = fopen(fileName, "w");
        if (tmp != NULL) {
            fclose(tmp);
            if (stat(fileName, &buf) >= 0) {
                timePtr->modtime = buf.st_mtime;
                timePtr->actime = buf.st_atime;
                return;
            }
        }
    }

    /* Use CPU time only as a last resort */
    if ((t = time(NULL)) >= 0) {
        timePtr->modtime = t;
        timePtr->actime = t;
    }
}

/*
 *  ======== processLine ========
 *  Given a line from the environment file, if the line defines a
 *  name-value pair, compare it to the corresponding line in the
 *  newly generated environment file.  Otherwise, we ignore the line; i.e.,
 *  differences are not grounds for updating the output.
 *
 *  Returns non-0 for match failure, otherwise 0.
 */
static Int processLine(String line)
{
    Int i;
    Char name[MAXNAME];
    Char c;
    String value;

    /* skip over optional "override " prefix */
    if (strncmp(line, "override ", 9) == 0) {
        line += 9;
    }

    name[0] = '\0';
    /* name-value pair lines are of the form 'name = value' */
    if (sscanf(line, "%s %c ", name, &c) == 2 && c == '=') {
        value = line + strlen(name) + 3;    /* +3 to skip over " = " */
        oldPairs++;
        /* find corresponding line in env table to compare */
        for (i = 0; i < NUMENV - 1; i++) {
            if (envTab[i].flags & ISPAIR) {
                if (strcmp(envTab[i].name, name) == 0) {
                    /* update envTab for _XDCBUILDCOUNT */
                    if (strcmp(envTab[i].name, "_XDCBUILDCOUNT") == 0) {
                        static Char buildCount[32];
                        sprintf(buildCount, "%ld", atol(value) + iflag);
                        envTab[i].value = buildCount;
                        return (iflag);
                    }
                    /* otherwise compare values */
                    return (cmpEnv(envTab + i, value));
                }
            }
        }

        if (dflag) {
            printf("env differs:\n    old: '%s = %s'\n    new: ''\n",
                name, value);
        }
        return (1); /* old file has an unrecognized name-value pair */
    }

    return (0);  /* ignore lines that are not of the form 'name = value' */
}
