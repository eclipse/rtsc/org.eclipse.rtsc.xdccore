/* --COPYRIGHT--,EPL
 *Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== fixlist.c ========
 *  Fix generated tar include files to be one flat file list and remove
 *  files in packages nested under ".".
 *
 *  usage: fixlist [-z] inc-file [prefix [out-file [goal out-deps-file]]]
 *
 *  where
 *      -z          if this option is passed, the output file will be
 *                  suitable input to zip (rather than the default tar)
 *
 *      -t          adjust times of files whose access times are less than
 *                  the modification times.  This should never happen on
 *                  Windows systems but, due to network file system bugs,
 *                  it may occur.  This option allows one to avoid errors
 *                  from tar which checks times of files to detect file
 *                  changes during the archive process, for example.
 *
 *      -v          verbose output when errors are detected
 *
 *      -X xfile    exclude files that match the lines that appear in xfile;
 *                  one name per line
 *
 *      inc-file    is a simple list of files (one per line, UNIX file
 *                  format).  File names that match the pattern "*.xdc.inc" or
 *                  "*.cfg.inc" are recursively expanded into the resulting
 *                  list of files.
 *
 *      prefix      is a string that is affixed at the beginning of each
 *                  file name.  If prefix is not specified then it defaults
 *                  to "".
 *
 *      out-file    is the file where the results are written.  If
 *                  out-file is  not specified then output is written to
 *                  stdout.  Note: out-file may be the same as inc-file.  In
 *                  this case, the input file will be overwritten with the
 *                  appropriate results.
 *
 *      goal        goal name used in the generation of a file of makefile
 *                  dependencies (out-deps)
 *
 *      out-deps    file of makefile dependencies with goal string as the
 *                  goal and every file in the inc-file as a prerequisite.
 *
 *  Warning: fixlist _must_ be run from the base directory of a package.
 *
 *  The fixlist algorithm is:
 *      1. find all packages below "."
 *      2. recursivly expand .inc files starting from the specified .inc file
 *      3. remove all files or directories whose prefixes match the relative
 *         path to a package below "."
 *      4. remove all "duplicates": files below a directory already specified
 *      5. recursively "flatten" the remaining list to only include ordinary
 *         files or empty directories by decending into all specifed
 *         directories.
 *
 *  Notes:
 *  0. We need filter out files that are in other packages because the *.inc
 *  files are often created by tools that simply output _all_ files included
 *  as part of a "compilation"; the .inc files can potentially list files
 *  found in "nested" packages and it is important the we not include files
 *  from another package.
 *
 *  1. We intensionally delay directory expansion until _after_ filtering
 *  nested packages to allow the possibility of including a nested package
 *  via Pkg.otherFiles[].
 *
 *  2. We have to decend into all directories in order to properly generate
 *  dependencies that trigger a re-archive if any included file changes (or
 *  is removed).
 *
 *  3. As a special case, we include directories that are empty in the
 *  generated file list so directories that are expected to exist in the
 *  package are preserved (even if they are empty).
 */
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xdc/services/host/lib/xutl.h>

#define LINEQUANTUM     128
#define MAXEXCLUDE      256

typedef struct ScanFSObj {
    String  goal;
    FILE    *out;
} ScanFSObj;

String usage = "usage: %s [-z] [-v] [-t] [-x baseName] [-X baseNameFile] "
    "inc-file [prefix [output-file [goal output-dep-file]]]\n";

String progName = "";

static Char *lineBuf;
static SizeT lineBufLen = 0;

static String *excludeTab;
static Int excludeTabLen = 0;

static String filterTab[MAXEXCLUDE];
static Int filterTabIndex = 0;
static SizeT filterTabLen = sizeof(filterTab) / sizeof(String);

static String *lineTab;
static SizeT lineTabLen = 0;
static Int lineTabIndex = 0;

static Int modified = FALSE;
static String goal = NULL;

static Bool zflag = FALSE;  /* generate zip input file format */
static Bool tflag = FALSE;  /* fixup access times to be >= mod times */
static Bool vflag = FALSE;  /* verbose output to stderr */

static Void appendLine(String line);
static Int  compare(const Void *ptr1, const Void *ptr2);
static Bool exclude(String line);
static Void expand(String file);
static Bool filter(IArg arg, String baseName, String dirName, Char *end);
static Void fixTimes(String fileName);
static Int fixTimesScan(IArg arg, IArg *cookie, String name, String dir);
static Int genDirLine(IArg arg, IArg *cookie, String name, String dir);
static Void genFileLine(String line);
static Void genLine(String line);
static FILE *openForWrite(String file);
static Void processLine(String line);
static Void quoteSpaceOut(FILE *out, String src);
static Void readFilterList(String file, Bool optional);
static Void readImplicitList(String inFile);

static FILE *depOut = NULL;
static FILE *out;
static String prefix = "";

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    String incFileName = NULL;
    Int i;

    XUTL_init();

    progName = argv[0];

    while (argc > 1) {
        if (argv[1][0] != '-') {
            break;
        }

        switch (argv[1][1]) {
            case 'x': {
                if (argc < 2 || filterTabIndex >= filterTabLen) {
                    fprintf(stderr, usage, progName);
                    exit(1);
                }
                filterTab[filterTabIndex++] = argv[2];
                argc--;
                argv++;
                break;
            }

            case 'X': {
                if (argc < 2) {
                    fprintf(stderr, usage, progName);
                    exit(1);
                }
                if (argv[1][2] == 'i') {
                    readImplicitList(argv[2]);
                }
                else {
                    readFilterList(argv[2], FALSE);
                    argc--;
                    argv++;
                }
                break;
            }

            case 'z': {
                /* this flag is no longer used (xdcutils-e05) but is retained
                 * for backward compatibility and for future updates that may
                 * need to know if we are generateing a list for zip or tar.
                 */
                zflag = TRUE;
                break;
            }

            case 't': {
                tflag = TRUE;
                break;
            }

            case 'v': {
                vflag = TRUE;
                break;
            }

            default: {
                fprintf(stderr, usage, progName);
                exit(1);
            }
        }
        argc--;
        argv++;
    }

    if (argc < 2 || argc > 6) {
        fprintf(stderr, usage, progName);
        exit(1);
    }

    if (argc >= 3) {
        prefix = argv[2];
    }
    if (argc >= 4) {
        incFileName = argv[3];
    }
    if (argc >= 5) {
        goal = argv[4];
        if (argc >= 6) {
            depOut = openForWrite(argv[5]);
        }
        else {
            fprintf(stderr, usage, progName);
            exit(1);
        }
    }

    if (prefix != NULL && prefix[0] != '\0') {
        modified = TRUE;
    }

    /* compute the exclude list (nested packages) */
    if ((excludeTab = XUTL_findPackages(".", TRUE, &excludeTabLen)) == NULL) {
        excludeTabLen = 0;
    }
    if (excludeTabLen < 0) {
        excludeTabLen = -1 * excludeTabLen; /* ignore errors; use any pkgs */
    }
    if (excludeTabLen > 1) {
        qsort(excludeTab, excludeTabLen, sizeof(String), compare);
    }
    if (excludeTabLen > 0) {
        /* Warning: we assume here (for performance reasons) that every item in
         * the list returned by XUTL_findPackages has "./" as a prefix.
         */
        excludeTab++;                           /* skip first package: "." */
        excludeTabLen--;
        for (i = 0; i < excludeTabLen; i++) {
            excludeTab[i] = excludeTab[i] + 2;  /* trim leading "./" prefix */
            /* printf("excludeTab[%d] = %s\n", i, excludeTab[i]); */
        }
    }

    /* initialize lineBuf */
    if ((lineBuf = (Char *)malloc(LINEQUANTUM)) == NULL) {
        fprintf(stderr, "%s: out of memory\n", progName);
        exit(1);
    }
    lineBufLen = LINEQUANTUM;

    /* expand specified file into lineTab */
    expand(argv[1]);

    /* sort lineTab to identify/remove dups */
    qsort(lineTab, lineTabIndex, sizeof(String), compare);
    for (i = 0; i < lineTabIndex - 1; i++) {
        if (strcmp(lineTab[i], lineTab[i + 1]) == 0
            || exclude(lineTab[i])) {
            free(lineTab[i]);
            lineTab[i] = NULL;          /* remove/mark duplicate lines */
            modified = TRUE;
        }
        else {
            /*  Check for files in sub-directory of directory already named.
             *  We require that files in sub-directories appear *immediately*
             *  after the directory name.  This is ensured by the compare
             *  function used to sort the names
             */
            SizeT len = strlen(lineTab[i]);
            Int j;
            if (strncmp(lineTab[i], lineTab[i + 1], len) == 0
                && lineTab[i + 1][len] == '/') {
                /* lineTab[i] is a dir since lineTab[i+1] uses it with '/' */

                /* remove files in the directory (they are redundent) */
                for (j = i + 1; j < lineTabIndex; j++) {
                    if (strncmp(lineTab[i], lineTab[j], len) != 0
                        || lineTab[j][len] != '/') {
                        break;
                    }
                    free(lineTab[j]);
                    lineTab[j] = NULL;
                    modified = TRUE;
                }
                i = j - 1;
            }
        }
    }
    /* check the last element for exclusion */
    if (i < lineTabIndex && exclude(lineTab[i])) {
        free(lineTab[i]);
        lineTab[i] = NULL;
        modified = TRUE;
    }

    /* if input file differs from generated output, output results */
    if (modified == TRUE) {
        if (incFileName != NULL) {
            out = openForWrite(incFileName);
        }
        else {
            out = stdout;
        }

        /* write include file with generated output contents */
        for (i = 0; i < lineTabIndex; i++) {
            if (lineTab[i] != NULL) {
                genLine(lineTab[i]);
            }
        }
        if (out != stdout) {
            fclose(out);
        }
    }

    if (tflag) {
        /* tar/NFS HACK!!!!! */
        for (i = 0; i < lineTabIndex; i++) {
            if (lineTab[i] != NULL && lineTab[i][0] != '\0') {
                if (XUTL_isDir(lineTab[i])) {
                    XUTL_scanFS(lineTab[i], fixTimesScan, NULL, (IArg)NULL, XUTL_FSNORM);
                }
                else {
                    fixTimes(lineTab[i]);
                }
            }
        }
    }

    return (0);
}

/*
 *  ======== appendLine ========
 *  Append line to lineTab
 */
static Void appendLine(String line)
{
    String newLine;
    Char *cp;

    /* allocate new line */
    if (line == NULL || (newLine = (String)strdup(line)) == NULL) {
        fprintf(stderr, "%s: out of memory\n", progName);
        exit(1);
    }

    if (lineTabIndex >= lineTabLen) {
        /* grow table */
        lineTabLen += 128;
        lineTab = (String *)realloc(lineTab, sizeof(String) * lineTabLen);
        if (lineTab == NULL) {
            fprintf(stderr, "%s: out of memory\n", progName);
            exit(1);
        }
    }

    /* ensure that all filenames use '/' as directory separator */
    for (cp = newLine; *cp != '\0'; cp++) {
        if (*cp == '\\') {
            *cp = '/';
            modified = TRUE;
        }
        else if (*cp == '\n') {
            *cp = '\0';
            break;
        }
    }

    /* append new line to lineTab */
    lineTab[lineTabIndex++] = newLine;
}

/*
 *  ======== compare ========
 *  The compare function to be used by qsort() on lineTab
 *
 *  This function ensures that files in subdirectories always appear
 *  immediately after the directory name (if both appear in the list)
 */
static Int compare(const Void *ptr1, const Void *ptr2)
{
    String a = *(String *)ptr1;
    String b = *(String *)ptr2;
    Int i;

    /* can't just use strcmp because '.' appears before '/' lexically */
    for (i = 0; a[i] != '\0'; i++) {
        if (b[i] == '\0') {
            return (1); /* a > b; because b is prefix of a */
        }
        if (a[i] != b[i]) {
            /* treat '/' as lexically before any other character */
            if (a[i] == '/') {
                return (-1);
            }
            else if (b[i] == '/') {
                return (1);
            }
            return (a[i] > b[i] ? 1 : -1);
        }
    }

    /* either a is a prefix of b or they are equal */
    return (b[i] == '\0' ? 0 : -1);
}

/*
 *  ======== exclude ========
 *  Return TRUE iff there is a string in excludeTab that is a "directory"
 *  prefix of line.
 *
 *  excludeTab is assumed to be sorted with shorter strings first.
 */
static Bool exclude(String line)
{
    /* printf("fixlist: checking line: %s", line); */
    if (excludeTabLen > 0) {
        Int i;
        for (i = 0; i < excludeTabLen; i++) {
            Char *ep = excludeTab[i];
            Char *lp = line;
            /* printf("    checking against %s ...\n", ep); */
            while (ep[0] != '\0') {
                if (lp[0] == '\0') {
                    /* since excludeTab is sorted, no other string in
                     * excludeTab can be a prefix if this one is too long
                     */
                    return (FALSE);
                }
                if (ep[0] != lp[0]) {
                    break;
                }
                ep++;
                lp++;
            }
            if (ep[0] == '\0') {
                /* we reached the end of excludeTab[i] with no mismatch so
                 *  line needs to be excluded if:
                 *      1. line is an exact match (lp[0] = '\0'), or
                 *      2. line suffix is a directory (lp[0] = '/' or '\\')
                 */
                if (lp[0] == '\0' || lp[0] == '\\' || lp[0] == '/') {
                    /* printf("fixlist: excluded %s\n", line); */
                    return (TRUE);
                }
                return (FALSE);
            }
        }
    }

    return (FALSE);
}

/*
 *  ======== expand ========
 */
static Void expand(String file)
{
    FILE *in;
    Char *line;
    SizeT lineLen;
    Char *done = NULL;

    if ((in = fopen(file, "r")) == NULL) {
        fprintf(stderr, "%s: can't open '%s' ", progName, file);
        perror("because");
        exit(1);
    }

    do {
        lineBuf[0] = '\0';
        line = lineBuf;
        lineLen = lineBufLen;
        while ((done = fgets(line, (UInt)lineLen, in)) != NULL) {
            SizeT len = strlen(lineBuf);
            if (len < (lineBufLen - 1) || lineBuf[len - 1] == '\n') {
                /* concatenate lines that end with \ */
                if (len >= 2
                    && lineBuf[len - 2] == '\\'
                    && lineBuf[len - 1] == '\n') {
                    line = lineBuf + len - 2;
                }
                else {
                    break;  /* got a full line; process it now */
                }
            }
            else {
                lineBufLen += LINEQUANTUM;
                if ((lineBuf = (Char *)realloc(lineBuf, lineBufLen)) == NULL) {
                    fprintf(stderr, "%s: out of memory\n", progName);
                    exit(1);
                }
                line = lineBuf + len;
            }
            lineLen = lineBufLen - (line - lineBuf);
        }
        processLine(lineBuf);

    } while (done != NULL);

    fclose(in);
}

/*
 *  ======== filter ========
 *  exclude specified directories
 *
 *  Return TRUE to skip dirName/baseName, FALSE to include it.
 */
static Bool filter(IArg arg, String baseName, String dirName, Char *end)
{
    Int i;

    for (i = 0; i < filterTabIndex; i++) {
        if (strcmp(filterTab[i], baseName) == 0) {
            return (TRUE);
        }
    }

    return (FALSE);
}

/*
 *  ======== genDirLine ========
 */
static Int genDirLine(IArg arg, IArg *cookie, String name, String dir)
{
    static Char *lineBuf = NULL;
    static SizeT lineLen = 0;
    SizeT len;

    /* re-alloc lineBuf, if necessary */
    len = (strlen(name) + strlen(dir)) + 2;
    if (lineLen < len) {
        if (lineBuf != NULL) {
            free(lineBuf);
            lineLen = 0;
        }

        if (len < FILENAME_MAX) {
            len = FILENAME_MAX;
        }
        if ((lineBuf = (Char *)malloc(len)) == NULL) {
            return (0);
        }
        lineLen = len;
    }

    /* copy dir and name to scratch lineBuf and generate output */
    sprintf(lineBuf, "%s/%s", dir, name);
    genFileLine(lineBuf);

    return (0);
}

/*
 *  ======== genFileLine ========
 */
static Void genFileLine(String line)
{
    /* reduce the path because zip ignores paths of the form:
     *    foo/./bar
     * even if foo/bar exists!  See SDSCM00027394.
     */
    XUTL_reducePath(line, TRUE);

    if (prefix != NULL && prefix[0] != '\0') {
        fprintf(out, "%s/%s\n", prefix, line);
    }
    else {
        fprintf(out, "%s\n", line);
    }

    if (depOut != NULL) {
        /* goal depends on file in the directory */
        fprintf(depOut, "%s: ", goal);
        quoteSpaceOut(depOut, line);
        fprintf(depOut, "\n");

        /* create an empty rule so make triggers a rebuild of goal
         * rather than complaining that the file is missing
         */
        quoteSpaceOut(depOut, line);
        fprintf(depOut, ":\n");
    }
}

/*
 *  ======== genLine ========
 */
static Void genLine(String line)
{
    if (XUTL_isDir(line)) {
        XUTL_scanFS(line, genDirLine, filter, (IArg)NULL, XUTL_FSALL | XUTL_FSNEDIR);
    }
    else {
        genFileLine(line);
    }
}

/*
 *  ======== openForWrite ========
 */
static FILE *openForWrite(String file)
{
    FILE *out;

    XUTL_rm(file);

    /* "wb" is important on Windows since tar does not like \r in files */
    if ((out = fopen(file, "wb")) == NULL) {
        fprintf(stderr, "%s: can't open '%s' ", progName, file);
        perror("because");
        exit(1);
    }

    return (out);
}

/*
 *  ======== processLine ========
 *  Ignore, append to lineTab, or recursively expand line, as necessary
 *
 *  Note: line may be overwritten!!!!
 */
static Void processLine(String line)
{
    SizeT len = strlen(line);

    /* if the line names another file to include, recurse */
    if (len > 8 && 
        ((strcmp(".xdc.inc\n", line + len - 9) == 0) ||
         (strcmp(".cfg.inc\n", line + len - 9) == 0))) {
        line[len - 1] = '\0';   /* strip the new line so fopen() works */
        expand(line);
        modified = TRUE;        /* since *.inc is expanded, modified is true */
    }
    else {
        if (len <= 0 || line[0] == '\r' || line[0] == '\n') {
            modified = TRUE;   /* skip blank lines */
        }
        else {
            appendLine(line);
        }
    }
}

/*
 *  ======== quoteSpaceOut ========
 *  Copy src to out inserting a '\' character in front of every space
 */
static Void quoteSpaceOut(FILE *out, String src)
{
    for (; *src != '\0'; src++) {
        if (*src == ' ') {
            fputc('\\', out);
        }
        fputc(*src, out);
    }
}

#ifdef xdc_target__os_Windows
#include <windows.h>
#include <io.h>
#include <direct.h>
#include <sys/stat.h>

#define S_ISDIR(m)   (((m) & S_IFMT) == S_IFDIR)
#else
#include <utime.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

/*
 *  ======== fixTimes ========
 *  This function exists to force the file modification times to be
 *  fixed when accessed from a networked workstation.
 *
 *  Ideally this function would simply return when fileName is a local
 *  file.  How do we detect local verses network files?
 */
static Void fixTimes(String fileName)
{
#ifdef xdc_target__os_Windows
    HANDLE h;
    FILETIME atime, wtime;

    h = CreateFile(fileName, FILE_WRITE_ATTRIBUTES,
        FILE_SHARE_READ, NULL, OPEN_EXISTING,
        FILE_ATTRIBUTE_READONLY, NULL);
    if (h != NULL) {
        if (GetFileTime(h, NULL, &atime, &wtime)) {
            if (CompareFileTime(&atime, &wtime) < 0) {
                /* whacked times: access time should be >= write time */
                SYSTEMTIME satime, swtime;
                FileTimeToSystemTime(&atime, &satime);
                FileTimeToSystemTime(&wtime, &swtime);
                if (vflag) {
                    fprintf(stderr, "fixlist: bogus %s times"
                        " (access: %d:%d:%d.%d, write %d:%d:%d.%d)\n",
                        fileName,
                        satime.wHour, satime.wMinute, satime.wSecond,
                            satime.wMilliseconds,
                        swtime.wHour, swtime.wMinute, swtime.wSecond,
                            swtime.wMilliseconds);
                }
                if (!SetFileTime(h, NULL, &wtime, NULL)) {
                    fprintf(stderr,
                        "fixlist: failed to patch bogus %s times.\n", fileName);
                }
            }
        }
        CloseHandle(h);
    }
#elif defined(xdc_target__os_Linux) || defined(xdc_target__os_MacOS)
    struct stat buf;

    if (stat(fileName, &buf) == 0) {
        if (buf.st_atime < buf.st_mtime) {
            struct utimbuf times;

            times.actime = buf.st_mtime;
            times.modtime = buf.st_mtime;
            if (utime(fileName, &times) == 0) {
                if (vflag) {
                    fprintf(stderr, "fixlist: patched bogus %s times.\n",
                        fileName);
                }
            }
            else {
                fprintf(stderr, "fixlist: failed to patch bogus %s times.\n",
                    fileName);
            }
        }
    }
#else
    struct stat buf;

    if (stat(fileName, &buf) == 0) {
        FILE *in;
        in = fopen(fileName, "r");
        if (in != NULL) {
            static Char rbuf[2 * 4096];
            /* it's not enough to read a portion of the file, even reading
             * the first and last character does not sync the file times
             */
            while (fread(rbuf, sizeof (rbuf), 1, in) > 0) {
                ;
            }
            fclose(in);
        }
    }
#endif
/*    printf("fixTimes: %s\n", fileName); */
}

/*
 *  ======== fixTimesScan ========
 */
static Int fixTimesScan(IArg arg, IArg *cookie, String name, String dir)
{
    SizeT len = strlen(dir) + strlen(name) + 2;
    if (len > lineBufLen) {
        lineBufLen = len;
        if ((lineBuf = (Char *)realloc(lineBuf, lineBufLen)) == NULL) {
            fprintf(stderr, "%s: out of memory\n", progName);
            exit(1);
        }
    }

    sprintf(lineBuf, "%s/%s", dir, name);
    fixTimes(lineBuf);

    return (0);
}


/*
 *  ======== readFilterList ========
 */
static Void readFilterList(String file, Bool optional)
{
    FILE *in;
    static Char lineBuf[FILENAME_MAX + 32];

    if ((in = fopen(file, "r")) == NULL) {
        if (optional) {
            return;     /* nothing to filter */
        }
        fprintf(stderr, "%s: can't open %s\n", progName, file);
        perror("because");
        exit(1);
    }

    while (fgets(lineBuf, sizeof(lineBuf), in) != NULL) {
        String newString;
        SizeT len = strlen(lineBuf);

        if (len <= 0
            || strchr("\r\n", lineBuf[0])
            || (lineBuf[0] == '#' && strchr(" \t\r\n", lineBuf[1]))) {
            continue;       /* skip blank lines and comments */
        }

        if (lineBuf[len - 1] == '\n') {
            lineBuf[len - 1] = '\0';
        }

        if (filterTabIndex >= filterTabLen) {
            fprintf(stderr, "%s: too many exclude names in '%s'; %d lines\n",
                progName, file, filterTabIndex);
            exit(1);
        }

        newString = strdup(lineBuf);
        if (newString == NULL) {
            fprintf(stderr, "%s: out of memory\n", progName);
            exit(1);
        }

        filterTab[filterTabIndex++] = newString;
    }

    fclose(in);
}

/*
 *  ======== readImplicitList ========
 */
static Void readImplicitList(String inFile)
{
    static Char lineBuf[FILENAME_MAX + 32];
    Char *cp;
    SizeT len = strlen(inFile);

    if (len > FILENAME_MAX) {
        fprintf(stderr, "%s: include file name too long: '%s'\n",
            progName, inFile);
        exit(1);
    }

    strcpy(lineBuf, inFile);

    if ((cp = strrchr(lineBuf, '.')) == NULL) {
        cp = lineBuf + len;
    }
    strcpy(cp, ".ninc");

    readFilterList(lineBuf, TRUE);
}

/* not currently used */
#if 0
/*
 *  ======== trim ========
 *  Erase trailing '/' characters from the line
 */
static Void trim(String line)
{
    Char *cp;

    /* set cp to end of string */
    for (cp = line; cp[0] != '\0'; cp++) {
        ;
    }

    /* erase trailing '/' characters */
    for (; cp > line && cp[-1] == '/'; cp--) {
        cp[-1] = '\0';
    }
}
#endif
