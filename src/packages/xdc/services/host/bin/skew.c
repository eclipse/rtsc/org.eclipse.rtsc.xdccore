/* --COPYRIGHT--,EPL
 *Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h>
#include <sys/stat.h>	

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    struct stat sb;
    time_t curTime;
    time_t fileTime;
    FILE *file;
    Int trys;
    
    if (argc <= 1) {
	fprintf(stderr, "usage: %s file\n", argv[0]);
	exit(1);
    }
    
    for (trys = 1; (file = fopen(argv[1], "w")) != NULL; trys++) {
	char buf = '\0';
	fwrite(&buf, 1, 1, file);
	fclose(file);

	curTime = time(NULL);
	
	stat(argv[1], &sb);
	fileTime = sb.st_mtime;
	
	if ((curTime - fileTime) < 0) {
	    printf("clock skew detected (%d < %d), after %d trys\n",
		curTime, fileTime, trys);
	    return (1);
	}
    }

    return (0);
}
