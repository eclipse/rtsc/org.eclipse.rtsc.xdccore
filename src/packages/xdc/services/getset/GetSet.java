/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

package xdc.services.getset;

import org.mozilla.javascript.Callable;
import xdc.services.intern.xsr.Value;

/**
 * A helper class to add getters and setters to a field of an XDCscript
 * object.
 */
public class GetSet
{
    static boolean debug = false;

    /**
     * Initialize getter and setter support on every field.
     */
    public static void init(Value.Observable obj)
    {
        for (Object id : obj.getIds()) {
            if (id instanceof String) {
                init(obj, (String)id);
            }
            else if (id instanceof Number) {
                init(obj, ((Number)id).intValue());
            }
        }
    }

    /**
     * Initialize getter and setter support on a field, or other
     * property labeled by an identifier. If it does not already
     * exist, add it.
     */
    public static void init(Value.Observable obj, String prop)
    {
        Setters.init(obj, prop);
        Getters.init(obj, prop);
    }

    /**
     * Initialize getter and setter support on an array element,
     * or other property with a numeric index.
     * If it does not already exist, add it.
     */
    public static void init(Value.Observable obj, int prop)
    {
        Setters.init(obj, prop);
        Getters.init(obj, prop);
    }

    /**
     * Add a user setter to the named field of the object, or other
     * property labeled by an identifier.
     */
    public static void onSet(Value.Observable obj, String prop, Callable setter)
    {
        Setters.init(obj, prop).add(setter);
    }

    /**
     * Add a user setter to the indicated array element, or other
     * property with a numeric index.
     */
    public static void onSet(Value.Observable obj, int prop, Callable setter)
    {
        Setters.init(obj, prop).add(setter);
    }

    /**
     * Add a user getter to the named field of the object, or other
     * property labeled by an identifier.
     */
    public static void onGet(Value.Observable obj, String prop, Callable getter)
    {
        Getters.init(obj, prop).add(getter);
    }

    /**
     * Add a user getter to the indicated array element, or other
     * property with a numeric index.
     */
    public static void onGet(Value.Observable obj, int prop, Callable getter)
    {
        Getters.init(obj, prop).add(getter);
    }

    /**
     * Get whether to print debug trace.
     */
    public static boolean getDebug()
    {
        return debug;
    }

    /**
     * Set whether to print debug trace.
     */
    public static void setDebug(boolean debug)
    {
        GetSet.debug = debug;
    }
}
