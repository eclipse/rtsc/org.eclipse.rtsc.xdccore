/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

package xdc.services.getset;

import java.util.HashSet;
import java.util.Set;

import org.mozilla.javascript.BaseFunction;
import org.mozilla.javascript.Callable;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;

import xdc.services.intern.xsr.Value;

/**
 * <p>A getter function to be added to a field of an XDCscript
 * Value.Obj object. The Getters service manages a set of user-defined
 * getter functions that notifies client code of reads from the field.</p>
 * 
 * <p>There is also a set of global user-defined getter functions that are
 * notified on any read. This avoids having to manually add such getters to
 * every field. Note, though, that the global getters only apply to
 * fields that have had the Getters service added to them.</p>
 * 
 * The JavaScript getter functions have the signature:
 * <pre>
 * <b>this</b>.function(name, value)
 *     <b>this</b> is set to the object whose field was read
 *     name is the name of the field that was read
 *     value is the value that will be returned by the read
 * </pre>
 */
public class Getters extends BaseFunction
{
    private static final long serialVersionUID = 3564481724610600999L;
    private static HashSet<Callable> globalGetters = new HashSet<Callable>();

    private Object member;

    public Getters(Object member)
    {
        this.member = member;
    }

    /**
     * Add getter support to a field of an object.
     * @return The set of user getters for the field.
     */
    public static Set<Callable> init(Value.Observable obj, Object prop)
    {
        Object member = null;
        if (prop instanceof String) {
            member = obj.lookupFld((String)prop);
        }
        else if (prop instanceof Number) {
            member = obj.lookupFld(((Number)prop).intValue());
        }
        if (member == null) {
            /* TODO: complain */
            return null;
        }

        Function getter = obj.getGetter(member);
        if (getter == null) {
            /* if no getter already, create the infrastructure */
            getter = new Getters(member);
            obj.setGetter(member, getter);
        }
        else if (!(getter instanceof Getters)) {
            /* already has a getter, and it's not me */
            throw new RuntimeException("already has a getter");
        }
        return GetSetData.getData(obj, member).getters;
    }

    /**
     * The master getter function called by XDCscript. First calls all
     * the global getters, then calls the individual getters registered
     * on this field.
     */
    @Override
    public Object call(Context cx, Scriptable scope, Scriptable thisObj,
            Object[] args)
    {
        Value.Observable obj = (Value.Observable)thisObj;
        /* get the value from the private storage location */
        Object value = obj.getFld(member);

        /* create new arguments that include the value */
        Object[] newArgs = new Object[]{args[0], value};

        try {
            /* notify all the global getters */
            for (Callable f : globalGetters) {
                f.call(cx, scope, thisObj, newArgs);
            }

            /*
             * "data" can be null if the member has a getter on
             * it in some objects but not all. For example
             * in instance configs, or in module configs that
             * are inherited from a shared interface.
             */
            GetSetData data = (GetSetData)obj.getData(member);
            if (data != null) {
                /* notify all the user getters */
                for (Callable f : data.getters) {
                    f.call(cx, scope, thisObj, newArgs);
                }
            }
        }
        catch (Exception e) {
            if (GetSet.getDebug()) {
                e.printStackTrace();
            }
        }

        return value;
    }

    /*
     *  ======== addGlobal ========
     */
    public static void addGlobal(Callable getter)
    {
        globalGetters.add(getter);
    }
}
