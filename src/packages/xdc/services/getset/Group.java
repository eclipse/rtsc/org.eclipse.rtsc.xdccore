/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

package xdc.services.getset;

import java.util.ArrayList;
import java.util.Collection;
import org.mozilla.javascript.Callable;

import xdc.services.intern.xsr.Value;

/**
 * A group of {@link Fxn}s that are each run to completion before any other
 * Fxn in the group.
 *
 * The purpose of a Group is to control interactions between a group of
 * setters.  Within each Group the Fxns execute one at a time.  Even if
 * a Fxn makes changes that will cause another Fxn in the Group to run,
 * that won't happen until after the first Fxn completes.  This allows
 * setters within a group to modify internal state without interference
 * from other setters in the same Group (that may operate on the same
 * internal state).
 *
 * Each group runs to completion. When a setter in the group runs, it
 * might modify other config params and schedule further setters in the
 * same group to run. All of these complete before control is returned
 * to the original execution flow. From the point of view of code
 * outside the group, all the setters ran as an atomic operation.
 *
 * Groups may be interrupted. When a setter modifies a config param
 * that is being monitored by a second group, the second group starts
 * running immediately. It will run to completion before returning
 * control to the first group. So the first group sees the second
 * group as atomic. This allows groups to be tested independently,
 * and combined without affecting their behavior.
 *
 * Cycles aren't permitted in the execution flow between groups. If a
 * setter gets triggered from a group that has already been interrupted,
 * it is deferred and run when control eventually returns to its own
 * group. Cycles between groups can break the appearance of atomic
 * execution, and so should be avoided when possible.
 *
 * Exceptions thrown by a Fxn f in a group only trigger the retraction
 * the value of the Observable associated with f or any Observable that
 * recursively triggered f.  Groups only control the order of execution
 * of setters and do not, in the event of an exception, cause retraction
 * of all values set by the setters in the group.  Since retractions occur
 * "one value at a time", setters that check consistency amoung multiple
 * values risk triggering an exception during a retraction; this results in
 * a "double fault" fatal error and must be avoided by the setters.
 */
public class Group
{
    private Collection<Fxn> fxns;
    private int numStale = 0;
    private boolean scheduled = false;
    private Fxn running = null;
    private static int maxIterations = 0;

    /** Create a new Group governed by the given scheduler */
    public Group()
    {
        this.fxns = new ArrayList<Fxn>();
    }

    /**
     * <p>Wrap the given function body as a Fxn and specify one
     * input to fire on. Add it to the group and execute it once.</p>
     *
     * @return the Fxn, so that additional inputs and outputs can be
     *      specified.
     */
    public Fxn onSet(Value.Observable obj, String name, Callable body)
    {
        Fxn fxn = new Fxn(this, body);
        fxn.addInput(obj, name);
        add(fxn);
        return fxn;
    }

    public Fxn onSet(Value.Observable obj, int index, Callable body)
    {
        Fxn fxn = new Fxn(this, body);
        fxn.addInput(obj, index);
        add(fxn);
        return fxn;
    }

    /**
     * <p>Wrap the given function body as an AutoFxn and add it to the
     * group. Executes the function once.</p>
     *
     * A typical use is to establish two-way update between
     * related XDCscript fields, say x.a and y.b :
     * <pre>
     *   group.addAuto(function bToA(){ x.a = y.b; });
     *   group.addAuto(function aToB(){ y.b = x.a; });
     * </pre>
     * <p>This example constrains x.a and y.b to have the same value. Whenever
     * one of them changes, one AutoFxn in the Group runs and copies the
     * change to the other field. The Group is responsible for deciding
     * when to execute each AutoFxn, based on examining its inputs and
     * outputs.</p>
     * 
     * @return the AutoFxn, so that additional inputs and outputs can be
     *      specified.
     */
    public AutoFxn addAuto(Callable body)
    {
        AutoFxn autoFxn = new AutoFxn(this, body);
        add(autoFxn);
        return autoFxn;
    }

    /**
     * Add a new Fxn to the group, and execute it once.
     */
    public Fxn add(Fxn fxn)
    {
        fxns.add(fxn);
        schedule(fxn);
        return fxn;
    }

    /**
     * Ask if the group needs to be run. Should be run
     * whenever any Fxn in the group is stale.
     */
    public boolean getStale()
    {
        return numStale > 0;
    }

    /** Indicate that a Fxn in the group needs to be run. */
    public void addStale(Fxn fxn)
    {
        if (!fxn.getStale()) {
            fxn.setStale(true);
            numStale++;
        }
    }

    /** Indicate that a Fxn in the group no longer needs to be run. */
    public void removeStale(Fxn fxn)
    {
        if (fxn.getStale()) {
            fxn.setStale(false);
            numStale--;
        }
    }

    /** Indicate that no Fxn's in the group need to be run. */
    public void removeStale()
    {
        for (Fxn fxn : fxns) {
            removeStale(fxn);
        }
    }
    
    /** Indicate that the Group has now been scheduled to be run */
    public void setScheduled(boolean scheduled)
    {
        this.scheduled = scheduled;
    }

    /** Ask if the Group has already been scheduled to be run */
    public boolean getScheduled()
    {
        return scheduled;
    }

    /** Get the currently running function (unused?) */
    public Fxn getRunning()
    {
        return running;
    }

    /** Run all the Fxns in the Group that need it. */
    public void run()
    {
        /* keep looping until there are no stale Fxns */
        int i = 0;
        while (getStale()) {
            for (Fxn fxn : fxns) {
                if (fxn.getStale()) {
                    try {
                        running = fxn;
                        fxn.run();
                    }
                    finally {
                        running = null;
                        removeStale(fxn);
                    }
                }
            }

            /* handle failure to converge */
            if (maxIterations > 0 && i++ >= maxIterations) {
                throw new RuntimeException("Group.maxIterations exceeded");
            }
        }
    }

    /**
     * Mark the given Fxn as stale, and schedule the whole group to be
     * run at the next opportunity.
     */
    public void schedule(Fxn fxn)
    {
        addStale(fxn);
        Scheduler.schedule(this);
    }

    /**
     * Get the maximum number of iterations allowed in each group, as a
     * debugging aid.
     */
    public static int getMaxIterations() {
        return maxIterations;
    }

    /**
     * Get the maximum number of iterations allowed in each group, as a
     * debugging aid.
     */
    public static void setMaxIterations(int maxIterations) {
        Group.maxIterations = maxIterations;
    }
}
