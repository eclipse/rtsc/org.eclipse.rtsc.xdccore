/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

package xdc.services.getset;

import java.util.HashSet;
import org.mozilla.javascript.Callable;
import xdc.services.intern.xsr.Value;

/**
 * Private data for the getters and setters.
 */
public class GetSetData {

    public HashSet<Callable> getters = new HashSet<Callable>();
    public HashSet<Callable> setters = new HashSet<Callable>();

    /**
     * Get the private data associated with a field of an object.
     * If it doesn't already exist, create it.
     */
    public static GetSetData getData(Value.Observable obj, Object mbr) {
        GetSetData data = (GetSetData)obj.getData(mbr);
        if (data == null) {
            data = new GetSetData();
            obj.setData(mbr, data);
        }
        return data;
    }
}
