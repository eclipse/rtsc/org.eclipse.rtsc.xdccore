# --COPYRIGHT--,EPL
#  Copyright (c) 2008 Texas Instruments and others.
#  All rights reserved. This program and the accompanying materials
#  are made available under the terms of the Eclipse Public License v1.0
#  which accompanies this distribution, and is available at
#  http://www.eclipse.org/legal/epl-v10.html
# 
#  Contributors:
#      Texas Instruments - initial implementation
# 
# --/COPYRIGHT--*/

include $(XDCROOT)/packages/xdc/bld/xdc_java.mak

## deriving .jars from the requires list fails with spaces
## in the repository name
JARS = \
	$(XDCROOT)/packages/xdc/shelf/java/js.jar \
	$(XDCROOT)/packages/xdc/services/intern/xsr/java/package.jar
JARP := $(if $(JARS),$(subst $(space),$(PATHCHAR),$(JARS))$(PATHCHAR),)

JAVAFILES= $(wildcard *.java)
CLASSES  = $(patsubst %.java,%,$(JAVAFILES))
CLASSFILES=$(patsubst %,$(CLASSDIR)/%.class,$(CLASSES))
JARFILE  = java/package.jar

DOCDIR      = doc-files
PKGNAME     = $(subst /,.,$(PKGDIR))

## Signal that classfiles shouldn't be considered in whether to
## rebuild -- only the java files. Allows the .jar file to remain
## current even if the classfiles don't exist.
.SECONDARY: $(CLASSFILES)

.libraries: $(JARFILE)
$(JARFILE): $(CLASSFILES)

## build and release javadoc
.executables: .javadoc

.javadoc: .libraries
	$(JDK)/bin/javadoc -Xdoclint:none -quiet -d $(DOCDIR) -sourcepath $(PKGROOT) -classpath "$(JCPATH)" *.java
	$(RM) $@
	$(TOUCH) $@

clean::
	$(RM) .javadoc $(JARFILE)
	$(RMDIR) $(DOCDIR)
