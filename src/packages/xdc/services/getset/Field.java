/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

package xdc.services.getset;

import xdc.services.intern.xsr.Value;

/**
 * An immutable object that identifies a specific field of a specific
 * XDCscript object. Used internally as a key to index an object-plus-field
 * pair in a Set.
 *
 */
/* how is field related to member? */
public class Field
{
    private Value.Observable obj;
    private Object prop;

    public Field(Value.Observable obj, String prop)
    {
        this.obj = obj;
        this.prop = prop;
    }

    public Field(Value.Observable obj, int prop)
    {
        this.obj = obj;
        this.prop = prop;
    }

    /*
     *  ======== getObject ========
     */
    public Value.Observable getObject()
    {
        return obj;
    }

    /*
     *  ======== getName ========
     */
    public String getName()
    {
        return prop.toString();
    }

    /**
     * Two Field objects compare as equal() if they name the same field
     * of the same XDCscript object instance.
     */
    @Override
    public boolean equals(Object x)
    {
        if (!(x instanceof Field)) {
            return false;
        }
        Field field = (Field)x;
        return (obj.equals(field.obj)) && prop.equals(field.prop);
    }

    /**
     * If objects compare equal, also return equal hash codes.
     * If objects are unequal, no requirement on hash codes.
     */
    @Override
    public int hashCode()
    {
        return obj.hashCode() ^ prop.hashCode();
    }
}
