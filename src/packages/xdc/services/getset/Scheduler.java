/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

package xdc.services.getset;

/**
 * <p>Schedules execution of Fxns and Groups. Fxns within a
 * group always run to completion before another function
 * within the group. Groups may interrupt each other.</p>
 * 
 * <b>Note:</b> Does not support multiple Threads accessing the XDCscript
 * object model.
 */
public class Scheduler
{
    private static Group running = null;   /* the current running group */

    /*
     *  ======== createGroup ========
     */
    public static Group createGroup()
    {
        return new Group();
    }

    /**
     * Get which Group is currently running, or null if none.
     */
    public static Group getRunningGroup()
    {
        return running;
    }

    /**
     * Get which Fxn is currently running, or null if none.
     */
    public static Fxn getRunningFxn()
    {
        return (running == null) ? null : running.getRunning();
    }

    /**
     * <p>Schedule the group for execution. If the group isn't already running
     * then execute it immediately. Interrupts any other groups that are
     * currently running.</p>
     * 
     * If a group that is already running anywhere on the stack is
     * rescheduled, ignore the request and let the group complete on it own
     * time.
     */
    public static void schedule(Group group)
    {
        if (!group.getScheduled()) {
            /*
             * Save whichever group was previously running. If null, then
             * no group was running.
             */
            Group wasRunning = running;

            try {
                /* start running the new group */
                running = group;
                running.setScheduled(true);
                running.run();
            }
            finally {
                /* the group has completed; normally or by exception */
                running.setScheduled(false);
    
                /* restore whichever group was previously running */
                running = wasRunning;
            }
        }
    }
}
