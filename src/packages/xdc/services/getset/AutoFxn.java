/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

package xdc.services.getset;

import org.mozilla.javascript.Callable;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import xdc.services.intern.xsr.Value;

/**
 * A {@link Fxn} that automatically determines its own inputs and outputs.
 * Eliminates the need for client code to specify these explicitly, or
 * even to know. Useful, for example, when the body calls other functions
 * and doesn't want to track the details of their implementation.
 */
public class AutoFxn extends Fxn
{
    private static boolean initialized = false;

    /**
     * Create a new AutoFxn that will execute the given body, as scheduled
     * by the given Group.
     */
    public AutoFxn(Group group, Callable body)
    {
        super(group, body);
        init();
    }

    /**
     * Add global listeners that detect a read or write to any field,
     * and attribute it to the currently executing AutoFxn, if any.
     */
    private static void init()
    {
        if (initialized) {
            return;
        }
        initialized = true;

        /*
         * Add a global getter that watches every variable read,
         * and adds it as an input to the currently executing AutoFxn.
         */
        Getters.addGlobal(new Callable() {
            public Object call(Context cx, Scriptable scope,
                    Scriptable thisObj, Object[] args) {
                Fxn fxn = Scheduler.getRunningFxn();
                if (fxn instanceof AutoFxn) {
                    Value.Observable obj = (Value.Observable)thisObj;
                    Object prop = args[0];
                    if (prop instanceof String) {
                        ((AutoFxn)fxn).addInput(obj, (String)prop);
                    }
                    else if (prop instanceof Number) {
                        ((AutoFxn)fxn).addInput(obj, ((Number)prop).intValue());
                    }
                }
                return null;
            }
        });

        /*
         * Add a global setter that watches every variable write,
         * and adds it as an output to the currently executing AutoFxn.
         */
        Setters.addGlobal(new Callable() {
            public Object call(Context cx, Scriptable scope,
                    Scriptable thisObj, Object[] args) {
                Fxn fxn = Scheduler.getRunningFxn();
                if (fxn instanceof AutoFxn) {
                    Value.Observable obj = (Value.Observable)thisObj;
                    Object prop = args[0];
                    if (prop instanceof String) {
                        ((AutoFxn)fxn).addOutput(obj, (String)prop);
                    }
                    else if (prop instanceof Number) {
                        ((AutoFxn)fxn).addOutput(obj, ((Number)prop).intValue());
                    }
                }
                return null;
            }
        });
    }
}
