/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

/*
 * Test vetoing and rolling back a change of map elements.
 */

var Mod = xdc.module("xdc.services.getset.tests.Mod");
var GetSet = xdc.module("xdc.services.getset.GetSet");

GetSet.debug = false;
var passed = true;

function show() {
    var isSet = false;
    var value;
    out = "";

    for (var key in Mod.map) {
        var thisval = Mod.map[key];
        out += key + ": " + thisval + " ";
        if (isSet) {
            if (value != thisval) {
                passed = false;
            }
        }
        else {
            value = thisval;
            isSet = true;
        }
    }

    print(out);
}

function test(sel, val) {
    var e;
    try {
        Mod.map[sel] = val;
    }
    catch (e) {
        show();
        Mod.map[sel] = "ok";
    }
    show();
}

/* assign legal values */
test("a", "hi");
test("b", "ho");
test("c", "hoo");
test("d", "eek");

/* assign illegal values */
test("a", "oops");
test("b", "oops");
test("c", "oops");
test("d", "oops");

/* test length setter */
var count = 0;
var req = 2;
GetSet.onSet(Mod.map, "length", function() { count++; });
Mod.map["hello"] = "goodbye";
Mod.map["hello"] = "again";
Mod.map[0] = "something";
Mod.map["dolly"] = "baa";

print("length setter called " + count + " times, required " + req);
if (count != req) {
    passed = false;
}

/* exit with test status */
print(passed ? "PASS" : "FAIL");
java.lang.System.exit(passed ? 0 : 1);

