/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

/*
 * Test veto and group "atomic" setters
 */

var Mod = xdc.module("xdc.services.getset.tests.Mod");
var GetSet = xdc.module("xdc.services.getset.GetSet");

GetSet.debug = false;
var passed = true;
var divider = 0;

/*
 *  ======== init ========
 */
function init() 
{
    /* create a group to enable "atomic" setter execution */
    var group = GetSet.createGroup();

    /* use divider state */
    group.onSet(Mod, "x", function useDiv() { 
		    if (divider > 0) {
			Mod.y = Math.floor(Mod.x / divider);

			/* divider should never change inside this block */
			if (divider <= 0) {
			    print("fail!");
			    passed = false;
			}
		    }
		}
    );

    /* set divider state */
    group.onSet(Mod, "y", function setDiv() {divider = Mod.y;});

    print("init done.\n");
}

/*
 *  ======== test ========
 */
function test(sel, val, msg) 
{
    if (msg) {
	print(msg);
    }

    /* set the specified field */
    try {
        Mod[sel] = val;
    }
    catch (e) {
        print("Exception thrown: " + e);
    }

    /* show the current state */
    show();
}

/*
 *  ======== show ========
 */
function show()
{
    print("divider: " + divider + ", x: " + Mod.x + ", y: " + Mod.y);
}

/* initialize the setters for x, y */
init();

/* assignments that should succeed */
divider = 2;
test("x", 10);
test("x", 5);
test("x", 2);

test("x", 1, "test: x = 1");

/* exit with test status */
print(passed ? "PASS" : "FAIL");
java.lang.System.exit(passed ? 0 : 1);
