/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

/*!
 * Test module with constraints between config params.
 *
 * Enforces the following constraints:
 * @p(nlist)
 *      - requires d != "oops", arr[4] != "oops", and map["d"] != "oops" by
 *        throwing an error if any are set to "oops"
 *      - ensures a == b == c == d, arr[0] == arr[1] == arr[2] == arr[3],
 *        and map["a"] == map["b"] == map["c"] == map["d"], by updating
 *        the value of all four of them if any of them changes
 * @p
 *
 * The constraints are checked on every write to any
 * of the config params.
 */
metaonly module Mod {
    config String a;
    config String b;
    config String c;
    config String d;

    config String arr[4];

    config String map[string];

    /*!
     *  ======== doubleFault ========
     * Assignments always fail, and then fail again during
     * rollback.
     */
    config String doubleFault;

    /*
     *  ======== x, y, z ========
     *  unconstrained config parameters
     */
    config Int x = 0;
    config Int y = 0;
    config Int z = 0;
}
