/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

/*
 * Test vetoing and rolling back a change of array values.
 */

var Mod = xdc.module("xdc.services.getset.tests.Mod");
var GetSet = xdc.module("xdc.services.getset.GetSet");

GetSet.debug = false;
var passed = true;

function show() {
    var val = Mod.arr[0];
    print("0: " + Mod.arr[0] + " 1: " + Mod.arr[1] +
        " 2: " + Mod.arr[2] + " 3: " + Mod.arr[3]);
    if (Mod.arr[1] != val || Mod.arr[2] != val || Mod.arr[3] != val) {
        passed = false;
    }
}

function test(sel, val) {
    var e;
    try {
        Mod.arr[sel] = val;
    }
    catch (e) {
        show();
        Mod.arr[0] = "ok";
    }
    show();
}

/* assign legal values */
test(0, "hi");
test(1, "ho");
test(2, "hoo");
test(3, "eek");

/* assign illegal values */
test(0, "oops");
test(1, "oops");
test(2, "oops");
test(3, "oops");

/* exit with test status */
print(passed ? "PASS" : "FAIL");
java.lang.System.exit(passed ? 0 : 1);
