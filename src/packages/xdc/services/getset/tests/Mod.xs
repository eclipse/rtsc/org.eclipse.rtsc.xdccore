/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

var GetSet;
var Mod;

/*
 *  ======== module$meta$init ========
 */
function module$meta$init()
{
    Mod = this;
    GetSet = xdc.module("xdc.services.getset.GetSet");

    setup(Mod, "a", "b", "c", "d");
    setup(Mod.arr, 0, 1, 2, 3);
    setup(Mod.map, "a", "b", "c", "d");

    /* always fail, even while recoving from failure */
    GetSet.onSet(Mod, "doubleFault",
        function(name, newVal, oldVal) {
            throw new Error("can't assign " + newVal + ", was "  + oldVal);
        }
    );
}

/*
 *  ======== setup ========
 */
function setup(obj, a, b, c, d) 
{
    GetSet.init(obj);

    /* make a == b */
    var ab = GetSet.createGroup();
    ab.onSet(obj, a, function a2b() { obj[b] = obj[a] });
    ab.onSet(obj, b, function b2a() { obj[a] = obj[b] });

    /* make a == c */
    var ac = GetSet.createGroup();
    ac.onSet(obj, a, function a2c() { obj[c] = obj[a] });
    ac.onSet(obj, c, function c2a() { obj[a] = obj[c] });

    /* make b == d */
    var bd = GetSet.createGroup();
    bd.onSet(obj, b, function b2d() { obj[d] = obj[b] });
    bd.onSet(obj, d, function d2b() { obj[b] = obj[d] });

    /* make c == d */
    var cd = GetSet.createGroup();
    cd.onSet(obj, c, function c2d() { obj[d] = obj[c] });
    cd.onSet(obj, d, function d2c() { obj[c] = obj[d] });

    /* require d != "oops" */
    cd.onSet(obj, d, function chk() {
        if (obj[d] == "oops") {
            throw new Error("obj[d] cannot be equal to \"oops\"");
        }
    });
}
