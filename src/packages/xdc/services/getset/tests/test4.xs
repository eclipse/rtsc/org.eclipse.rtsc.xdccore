/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

/*
 * Test veto and group "atomic" setters
 */

var Mod = xdc.module("xdc.services.getset.tests.Mod");
var GetSet = xdc.module("xdc.services.getset.GetSet");

GetSet.debug = false;
var passed = true;

/*
 *  ======== init ========
 *  In this test we have 
 *     o three numeric values, x, y, and z, which must always have values
 *       such that: x <= y <= z
 *     o a config parameter a which when set to a value also sets both x
 *       and y to the numeric value of a.
 *
 *  Without the group mechanism, the setter for a would be ugly: we would 
 *  have to first determine the order to assign x and y before doing the
 *  assignment because each assignment must maintain the constraint that
 *  x <= y.  
 * 
 *  With groups, however, it is possible to simply assign both x and y in
 *  any order (the "x <= y" check will not fire until we are done with 
 *  _both_ assignments.)
 */
function init() 
{
    /* create a group to enable "atomic" setter executation */
    var group = GetSet.createGroup();

    /* assert: x <= y <= z */
    var check = function check(name, newValue, oldValue, ex) { 
        print("checking: ", name, newValue, oldValue, ex, " ...");
	if (ex || Mod.x <= Mod.y && Mod.y <= Mod.z) {
            return;
	}
	print("    fail!");
	throw new Error("veto: x = " + Mod.x
                        + ", y = " + Mod.y + ", z = " + Mod.z);
    };
    group.onSet(Mod, "x", check);
    group.onSet(Mod, "y", check);
    group.onSet(Mod, "z", check);

    /* set x and y "atomically" to the numeric value of Mod.a */
    Mod.a = "0";    /* init Mod.a since setters are called as part of onSet */
    group.onSet(Mod, "a", function seta(name, newValue, oldValue, ex) {
            print("seta: ", name, newValue, oldValue, ex);
            Mod.x = Mod.a - 0;
            Mod.y = Mod.a - 0;
        }
    );

    print("init done.\n");
}

/*
 *  ======== test ========
 */
function test(sel, val, msg) 
{
    if (msg) {
	print(msg);
    }

    /* set the specified field */
    try {
        Mod[sel] = val;
    }
    catch (e) {
        print("Exception thrown: " + e);
    }

    /* show the current state */
    show();
}

/*
 *  ======== show ========
 */
function show()
{
    print(Mod.x + " <= " + Mod.y + " <= " + Mod.z);
    if (Mod.x > Mod.y || Mod.y > Mod.z) {
        passed = false;
    }
}

/* initialize the setters for x, y, z */
init();

/* assignments that should fail (x, y, and z are initially == 0) */
test("a", "1", "test: a = 1");
test("x", 1,   "\ntest: x = 1");
test("y", 1,   "\ntest: y = 1");

/* assignments that should succeed */
test("z", 1,   "\ntest z = 1");
test("a", "1", "test a = 1");
test("a", "0", "test a = 0");
test("x", 0,   "test x = 0");
test("y", 0,   "test y = 0");

/* exit with test status */
print(passed ? "PASS" : "FAIL");
java.lang.System.exit(passed ? 0 : 1);
