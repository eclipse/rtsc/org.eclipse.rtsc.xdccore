/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

/*
 * Test reporting an exception that occurs while recovering from
 * another exception.
 */

var Mod = xdc.module("xdc.services.getset.tests.Mod");
var GetSet = xdc.module("xdc.services.getset.GetSet");

GetSet.debug = false;

var passed = false;
var e;
try {
    Mod.doubleFault = "oops";
}
catch (e) {
    print(e);
    passed = String(e).match(/nested exception/);
}

/* exit with test status */
print(passed ? "PASS" : "FAIL");
java.lang.System.exit(passed ? 0 : 1);
