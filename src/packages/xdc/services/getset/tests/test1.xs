/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

/*
 * Test vetoing and rolling back a change.
 */

var Mod = xdc.module("xdc.services.getset.tests.Mod");
var GetSet = xdc.module("xdc.services.getset.GetSet");

GetSet.debug = false;
var passed = true;

function show() {
    var val = Mod.a;
    print("a: " + Mod.a + " b: " + Mod.b + " c: " + Mod.c + " d: " + Mod.d);
    if (Mod.b != val || Mod.c != val || Mod.d != val) {
        passed = false;
    }
}

function test(sel, val) {
    var e;
    try {
        Mod[sel] = val;
    }
    catch (e) {
        show();
        Mod.a = "ok";
    }
    show();
}

/* assign legal values */
test("a", "hi");
test("b", "ho");
test("c", "hoo");
test("d", "eek");

/* assign illegal values */
test("a", "oops");
test("b", "oops");
test("c", "oops");
test("d", "oops");

/* exit with test status */
print(passed ? "PASS" : "FAIL");
java.lang.System.exit(passed ? 0 : 1);
