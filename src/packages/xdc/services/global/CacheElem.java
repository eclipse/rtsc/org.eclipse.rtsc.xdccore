/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.global;

import java.io.File;

/*
 *  ======== CacheElem ========
 */
public class CacheElem
{
    static public enum Kind {
        GLOBAL, // a dedicated pkg cache directory has been specified
        LOCAL,  // not used ???
        TEMP    // no dedicated pkg cache directory or pkg is not reelased
    };
    
    private String pkgName;
    private String pkgBase;
    private String elemName;
    private int    elemVers;
    
    private long relDate = -1;
    private Kind kind;
    
    private File cacheRoot = null;
    private File curElemDir = null;
    private File tmpElemDir = null;
    
    public CacheElem(String pkgName, String pkgBase, String elemName, int elemVers)
    {
        this.pkgName = pkgName;
        this.pkgBase = pkgBase;
        this.elemName = elemName;
        this.elemVers = elemVers;
        
        String cdir = System.getenv("XDCPKGCACHE");
        if (cdir != null && cdir.length() > 0) {
            this.cacheRoot = new File(cdir);
            if (!this.cacheRoot.isDirectory() && !this.cacheRoot.canWrite()) {
                this.cacheRoot = null;
            }
        }
        
        File rel = new File(pkgBase + "/package/package.rel.xml");
        if (rel.exists()) {
            this.relDate = Vers.getDate(pkgBase);
        }
        
//      this.kind = this.relDate == -1 ? Kind.LOCAL : this.cacheRoot == null ? Kind.TEMP : Kind.GLOBAL;
        this.kind = (this.relDate == -1 || this.cacheRoot == null) ? Kind.TEMP : Kind.GLOBAL;
    }
    
    // access
    public File access()
    {
        return this.curElemDir;
    }
    
    // commit
    public void commit()
    {
        if (this.tmpElemDir != null && this.curElemDir != null) {
            if (!this.tmpElemDir.renameTo(this.curElemDir)) {
                Host.rmdir(tmpElemDir);
            }
            this.tmpElemDir = null;
        }
    }
    
    // create
    public File create()
    {
        try {

            String en = this.elemName + "," + this.elemVers;
            
            if (this.kind == Kind.LOCAL) {
                File dir = new File(this.pkgBase + "/package/");
                this.curElemDir = new File(dir, en);
                if (this.curElemDir.exists()) {
                    return null;
                }
                this.tmpElemDir = File.createTempFile(en + "-", null, dir);
                this.tmpElemDir.delete();
                this.tmpElemDir.mkdir();
                return this.tmpElemDir;
            }

            String ds = String.format("%1$tY.%1$tm.%1$td-%1$tH.%1$tM.%1$tS", this.relDate);
            String ps = this.pkgName + "@" + ds;

            if (this.kind == Kind.TEMP) {
                this.curElemDir = File.createTempFile("CACHE-" + ps + "-" + en, "");
                this.curElemDir.delete();
                this.curElemDir.mkdir();
                return this.curElemDir;
            }
            else {
                File dir = new File(this.cacheRoot, ps);
                this.curElemDir = new File(dir, en);
                if (this.curElemDir.exists()) {
                    return null;
                }
                dir.mkdirs();
                this.tmpElemDir = File.createTempFile(en + "-", null, dir);
                this.tmpElemDir.delete();
                this.tmpElemDir.mkdir();
                return this.tmpElemDir;
            }
        }
        catch (Exception e) {
            Err.exit(e);
        }

        return null;
    }
    
    // isTemp
    public boolean isTemp()
    {
        return this.kind == Kind.TEMP;
    }
    
    // release
    public void release()
    {
        if (this.kind == Kind.TEMP) {
            Host.rmdir(this.curElemDir);
        }
        
        this.curElemDir = null;
    }
}
