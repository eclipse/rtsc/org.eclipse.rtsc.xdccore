/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008-2017 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== Out.java ========
 */
package xdc.services.global;

import java.io.BufferedReader;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;

/*
 *  ======== Out ========
 */
public class Out extends PrintStream
{
    protected int col = 0;
    protected int inc = 4;
    private String tmpFileName = null;
    OutputStream os;

    /*
     *  ======== Out ========
     */
    public Out(String tmpFileName)
        throws FileNotFoundException
    {
        this(new BufferedOutputStream(new FileOutputStream(tmpFileName)));
        this.tmpFileName = tmpFileName;
    }

    public Out(File file)
        throws FileNotFoundException
    {
        this(new BufferedOutputStream(new FileOutputStream(file)));
    }
    public Out( OutputStream out ) { super(out); this.os = out; }
    public Out( OutputStream out, int inc ) { this(out); this.inc = inc;}

    /*
     *  ======== closeAndCopy ========
     */
    public void closeAndCopy(String fileName)
        throws IOException
    {
        this.close();

    //  System.out.print("comparing "+fileName+" to "+tmpFileName+" ...: ");

        File dst = new File(fileName);
        File tmp = new File(tmpFileName);

        /* compare tmpFileName contents to fileName contents and replace
         * FileName if the contents differ
         */
        if (compare(dst)) {
            tmp.delete();
            return;
        }

        dst.delete();
        tmp.renameTo(dst);
    }

    /*
     *  ======== compare ========
     */
    private boolean compare(File other)
        throws IOException
    {
        /* check sizes before reading their contents */
        if (!other.exists()) {
            return (false);
        }
        File tmp = new File(tmpFileName);
        if (other.length() != tmp.length()) {
            return (false);
        }

        /* open both files */
        FileInputStream ins1, ins2;
        try {
            ins1 = new java.io.FileInputStream(other);
        }
        catch (FileNotFoundException e) {
            return (false);
        }
        try {
            ins2 = new java.io.FileInputStream(tmp);
        }
        catch (FileNotFoundException e) {
            ins1.close();
            return (false);
        }

        /* compare byte by byte */
        byte [] buf1 = new byte[1024];
        byte [] buf2 = new byte[1024];
        int n;
        while ((n = ins1.read(buf1)) > 0) {
            ins2.read(buf2);
            while (--n >= 0) {
                if (buf1[n] != buf2[n]) {
                    ins1.close();
                    ins2.close();
                    return (false);
                }
            }
        }

        ins1.close();
        ins2.close();
        return (true);
    }

    public OutputStream getOutputStream() { return this.out; }

    public void insert( File file )
    {
        Reader rdr = null;
        try {
            rdr = new BufferedReader(new FileReader(file));
            this.tab();
            int ch;
            boolean nl = false;
            while ((ch = rdr.read()) != -1) {
                if (nl) {
                    this.tab();
                }
                this.write(ch);
                nl = (ch == '\n');
           }
            rdr.close();
            this.flush();
        } catch (Exception e) { Err.exit(e); }
    }

    public void indent() { indent(1); }
    public void outdent() { outdent(1); }

    public void indent( int n ) { col += inc * n; }
    public void outdent( int n ) { col -= inc * n; }

    public void printf( String fmt ) {
        doPrint(fmt, "", "", "", "", "");
    }
    public void printf( String fmt, char c ) {
        doPrint(fmt, new Character(c), "", "", "", "");
    }
    public void printf( String fmt, int i ) {
        doPrint(fmt, new Integer(i), "", "", "", "");
    }
    public void printf( String fmt, long l ) {
        doPrint(fmt, new Long(l), "", "", "", "");
    }
    public void printf( String fmt, Object s0 ) {
        doPrint(fmt, s0, "", "", "", "");
    }
    public void printf( String fmt, Object s0, Object s1 ) {
        doPrint(fmt, s0, s1, "", "", "");
    }
    public void printf( String fmt, Object s0, Object s1, Object s2 ) {
        doPrint(fmt, s0, s1, s2, "", "");
    }
    public void printf( String fmt, Object s0, Object s1, Object s2, Object s3 ) {
        doPrint(fmt, s0, s1, s2, s3, "");
    }
    public void printf( String fmt, Object s0, Object s1, Object s2, Object s3,
        Object s4) {
        doPrint(fmt, s0, s1, s2, s3, s4);
    }

    /* All printf* methods support the concept of multiple output files, so
     * we want all users of this class to use these methods. The println methods
     * inherited from PrintStream are disabled.
     */
    public void printfln(String s) {
        this.printf(s + "\n");
    }

    @Override
    public void println() {
        throw new UnsupportedOperationException();
    }
    @Override
    public void println(String s) {
        throw new UnsupportedOperationException();
    }
    @Override
    public void println(boolean b) {
        throw new UnsupportedOperationException();
    }
    @Override
    public void println(char c) {
        throw new UnsupportedOperationException();
    }
    @Override
    public void println(char[] ca) {
        throw new UnsupportedOperationException();
    }
    @Override
    public void println(int i) {
        throw new UnsupportedOperationException();
    }
    @Override
    public void println(long l) {
        throw new UnsupportedOperationException();
    }
    @Override
    public void println(float f) {
        throw new UnsupportedOperationException();
    }
    @Override
    public void println(double d) {
        throw new UnsupportedOperationException();
    }
    @Override
    public void println(Object o) {
        throw new UnsupportedOperationException();
    }

    public void tab()
    {
        for (int i = 0; i < col; i++) {
            print(' ');
        }
    }

    public void doPrint( String fmt, Object s0, Object s1, Object s2, Object s3, Object s4 )
    {
        Object[] args = {s0, s1, s2, s3, s4};
        int argi = 0;
        char[] ca = fmt.toCharArray();
        for (int i = 0; i < ca.length; i++) {
            if (ca[i] != '%') {
                this.print(ca[i]);
                continue;
            }
            switch (ca[++i]) {
            case '%':
                this.print(ca[i]);
                continue;
            case 's':
                this.print((String)(args[argi++]));
                continue;
            case 'd':
                this.print(((Number)(args[argi++])).intValue());
                continue;
            case 'x':
                this.print("0x" + Integer.toHexString(((Number)(args[argi++])).intValue()));
                continue;
            case 'o':
                this.print("0" + Integer.toOctalString(((Number)(args[argi++])).intValue()));
                continue;
            case 'D':
                this.print(((Number)(args[argi++])).longValue());
                continue;
            case 'X':
                this.print("0x" + Long.toHexString(((Number)(args[argi++])).intValue()));
                continue;
            case 'O':
                this.print("0" + Long.toOctalString(((Number)(args[argi++])).intValue()));
                continue;
            case 'c':
                this.print((Character)(args[argi++]));
                continue;
            case 't':
                this.tab();
                continue;
            case '+':
                this.indent();
                continue;
            case '-':
                this.outdent();
                continue;
            case '1':
                this.print(args[0]);
                continue;
            case '2':
                this.print(args[1]);
                continue;
            case '3':
                this.print(args[2]);
                continue;
            case '4':
                this.print(args[3]);
                continue;
            case '5':
                this.print(args[4]);
                continue;
            }
        }
        this.flush();
    }

    /*
     *  ======== main ========
     */
    public static void main(String[] args)
    {
        Out out = new Out(System.out);
        out.printf("hello from %s%o\n", "bob", new Integer(123));
        out.indent();
        out.tab();
        out.printf("hello from %s%d\n", "bob", new Integer(123));
        out.outdent();
        out.printf("hello from %2%1\n", "bob", new Integer(123));
    }
}
