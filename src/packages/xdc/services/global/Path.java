/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== xdc.services.global.Path ========
 */

package xdc.services.global;

/*
 *  ======== Path ========
 */
public class Path
{
    /**
     * Global package path instance.
     *
     * Calls to setPath() or other setters will change the global
     * System properties.
     */
    private static Env global = null;

    /**
     * Get the global package path environment.
     */
    public static synchronized Env getGlobal() {
        if (global == null) {
            global = new Env(System.getProperties());
        }
        return global;
    };
    
    /**
     *  Initialize this class's data.
     *
     *  dirvec is initialized to a vector of Strings that name package
     *  repositories.  The repository names may contain the character '^';
     *  this character represents the absolute path to the "current"
     *  package's base directory.
     */
    public static void init()
    {
        getGlobal().init();
    }
    
    /**
     *  Return the current package path
     */
    public static String curpath()
    {
        return getGlobal().curpath();
    }

    /**
     * Set the current package path.
     * @param path an array of Strings.
     */
    public static void setPath(String[] path)
    {
        getGlobal().setPath(path);
    }
    
    /**
     * Return the current package path.
     * @return an array of Strings.
     */
    public static String[] getPath()
    {
        return getGlobal().getPath();
    }
    
    /**
     * Return the current package root. The package root is the unique
     * directory that is the expansion of the "^" character in the XDCPATH.
     * @see #expand(String)
     */
    public static String getCurPkgRoot()
    {
        return getGlobal().getCurPkgRoot();
    }
    
    /**
     * Set the current package root. The package root is the unique
     * directory that is the expansion of the "^" character in the XDCPATH.
     * @param proot the directory of the package root
     * @see #expand(String)
     */
    public static void setCurPkgRoot(String proot)
    {
        getGlobal().setCurPkgRoot(proot);
    }
    
    /**
     * Set the current package root, relative to a particular package.
     * The package root is the unique directory that is the expansion of
     * the "^" character in the XDCPATH.
     * The new package root is derived from the package name. For example,
     * if the package name is "xdc.bld", then the package root will be set
     * to two directory levels above the package directory.
     * @param pbase the directory of a package
     * @param pname the fully qualified name of a package
     * @see #expand(String)
     */
    public static void setCurPkgBase(String pbase, String pname)
    {
        getGlobal().setCurPkgBase(pbase, pname);
    }
    
    /**
     *  Compute name of "unnamed" package; we need to give a name to all
     *  packages in the config and build domains.  Unnamed packages are
     *  packages that do not declare a name in their package.xdc
     *  specification.  In this case, the name given to the package is the
     *  last component of the package's absolute path.
     *
     *  For example, the name of the unnamed package whose package.xdc 
     *  file is located in "/foo/bar/hello/" is just "hello".
     */
    public static String getUnnamedPkgName()
    {
        return getGlobal().getUnnamedPkgName();
    }
    
    public static String getUnnamedPkgName(String fileOrDirName)
    {
        return getGlobal().getUnnamedPkgName(fileOrDirName);
    }
    
    /**
     * Find module or interface specification file name from name of
     * module or interface.
     *
     * @param qn    qualified module or interface name
     * @return      file name of the module or interface's specification file.
     *              Null if the specification file can not be found.
     */
    public static String resolve( String qname )
    {
        return getGlobal().resolve(qname);
    }
    
    /**
     *  Locate fname along the package path (unless it begins with "./")
     *  and return its canonical file name.
     *
     *  If the name begins with the characters "./", then only the current
     *  working directory is searched. This provides a mechanism to avoid
     *  using the package's name when naming a file in the current package.
     *
     *  @return canonical file name if fname exists, otherwise null.
     */
    public static String search( String fname )
    {
        return getGlobal().search(fname);
    }
    
    /**
     * Validate the proposed name for a new package to be created in the
     * current directory.
     *
     * @param pname the dot-separated name of a package.
     *
     * @return -1   if the package name can't be based in the current working
     *              directory, or
     *          0   if the current working directory is in the package path, or
     *          n   (n > 0) the number directories up from the current working
     *              directory where pname's repository is.
     */
    public static int validate(String pname)
    {
        return getGlobal().validate(pname);
    }
    
    /**
     * Expand the package root in the given path. The current package root
     * will be inserted wherever the "^" character occurs in the path.
     */
    public static String expand(String path)
    {
        return getGlobal().expand(path);
    }
}
