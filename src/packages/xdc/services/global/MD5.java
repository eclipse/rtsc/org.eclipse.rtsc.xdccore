/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== MD5.java ========
 */
package xdc.services.global;

import java.io.FileInputStream;

import java.security.MessageDigest;

/*
 *  ======== MD5 ========
 */
public class MD5
{
    /*
     *  ======== encode ========
     */
    public static String encode(String fileName)
    {
	StringBuffer hexString = new StringBuffer(); 
	FileInputStream in;
	
	try {
	    in = new FileInputStream(fileName);
	}
	catch (java.io.FileNotFoundException e) {
	    return (null);
	}

	try {
	    MessageDigest algorithm = MessageDigest.getInstance("MD5"); 
	    byte [] buffer = new byte[4096];
    
	    /* compute MD5 sum */
	    algorithm.reset();
	    int len;
	    while ((len = in.read(buffer)) > 0) {
		algorithm.update(buffer, 0, len);
	    }
	    byte[] digest = algorithm.digest();
	    in.close();
	    
	    /* convert sum into a string */
	    for (int i = 0; i < digest.length; i++) { 
		int d = 0xFF & digest[i];
		String tmp = Integer.toHexString(d);
		if (d <= 0xF) {
		    hexString.append("0");
		}
		hexString.append(tmp);
	    } 
	}
	catch (Exception e) {
	    return (null);
	}

	return (hexString.toString());
    }

    /*
     *  ======== main ========
     */
    public static void main(String args[])
    {
	for (int i = 0; i < args.length; i++) {
	    System.out.println(args[i] + " sum: " + MD5.encode(args[i]));
	}
    }
}
