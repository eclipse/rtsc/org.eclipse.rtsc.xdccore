/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
/*
 *  ======== Alias.java ========
 */

package xdc.services.intern.gen;

import xdc.services.global.*;
import java.io.*;
import java.util.*;
import xdc.services.spec.*;

public class Alias
{
    private boolean defcmd;
    private boolean global;
    private String prefix;

    private Glob glob;

    Alias( Glob glob )
    {
        this.glob = glob;
    }

    // gen
    void gen( Unit unit, String prefix )
    {
        gen(unit, prefix, false, true);
    }

    // gen
    void gen( Unit unit, String prefix, boolean global )
    {
        gen(unit, prefix, global, true);
    }

    // gen
    void gen( Unit unit, String prefix, boolean global, boolean defcmd )
    {
        this.prefix = prefix;
        this.global = global;
        this.defcmd = defcmd;

        genOpaques(unit);
        genDecls(unit, true);

        if (unit.isMod()) {
            genModCfgs(unit);
            genOffsets(unit);
        }
        if (unit.isInst()) {
            genInsCfgs(unit);
        }
        genFxnDcls(unit);
        if (unit.isHeir()) {
            genConvs(unit);
        }
    }

    // genConvs
    void genConvs( Unit unit )
    {
        if (global) {
            return;
        }

        int k = 0;
        for (Unit iu : unit.getInherits()) {
            if (iu.getQualName().equals("xdc.runtime.IModule")) {
                continue;
            }
            String iin = glob.mkCname(iu.getQualName());
            iin = iin.substring(0, iin.length() - 1);
            String ks = ++k == 1 ? "" : ("" + k);
            genOut("Module_upCast" + ks);
            genOut("Module_to_" + iin);
            if (unit.isInter()) {
                genOut("Module_downCast" + ks);
                genOut("Module_from_" + iin);
            }
            if (!unit.isInst() || !iu.isInst()) {
                continue;
            }
            genOut("Handle_upCast" + ks);
            genOut("Handle_to_" + iin);
            genOut("Handle_downCast" + ks);
            genOut("Handle_from_" + iin);
        }
    }

    // genDecls
    void genDecls( Unit unit, boolean eflag )
    {
        for (Decl d : unit.getDecls()) {
            if (!d.isMeta() && d instanceof Decl.AuxDef) {
                genOut(d.getName());
            }
        }
    }

    // genFxnDcls
    void genFxnDcls( Unit unit )
    {
        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isMeta() || fxn.isSys()) {
                continue;
            }

            genOut(fxn.getName());
            if (fxn.isVarg()) {
                genOut(fxn.getName() + "_va");
            }

            if (unit.isInter()) {
                genOut(fxn.getName() + "_fxnP");
                genOut(fxn.getName() + "_FxnT");
            }
        }

        if (global) {
            return;
        }

        if (!unit.needsRuntime()) {
            return;
        }

        genOut("Module_name");

        if (unit.isMod()) {
            genOut("Module_id");
            genOut("Module_startup");
            genOut("Module_startupDone");
            genOut("Module_hasMask");
            genOut("Module_getMask");
            genOut("Module_setMask");
            genOut("Object_heap");
            genOut("Module_heap");
        }

        if (unit.isMod() && unit.isInst()) {
            genOut("construct");
            genOut("create");
            genOut("handle");
            genOut("struct");
            genOut("Handle_label");
            genOut("Handle_name");
            genOut("Instance_init");
            genOut("Object_count");
            genOut("Object_get");
            genOut("Object_first");
            genOut("Object_next");
            genOut("Object_sizeof");
            genOut("Params_copy");
            genOut("Params_init");
            if (unit.attrBool(Attr.A_InstanceFinalize)) {
                genOut("Instance_finalize");
            }
        }

        if (unit.isProxy()) {
            genOut("Proxy_abstract");
            genOut("Proxy_delegate");
        }

        if (unit.isInter() && unit.isInst() && unit.getCreator() != null) {
            genOut("create");
        }

        if (unit.isInst()) {
            genOut("delete");
        }

        if (unit.isMod() && unit.isInst()) {
            genOut("destruct");
        }

        if (unit.isInter() && unit.isInst()) {
            genOut("Handle_label");
            genOut("Handle_to_Module");
        }

        if (unit.delegatesTo() != null && unit.delegatesTo().isInst()) {
            genOut("Object_delegate");
        }
    }

    // genInsCfgs
    void genInsCfgs( Unit unit )
    {
        if (global) {
            return;
        }

        genOut("Params");
    }

    // genModCfgs
    void genModCfgs( Unit unit )
    {
        for (Decl.Config cfg : unit.getConfigs()) {
            if (cfg.isInst() || cfg.isMeta() || cfg.isSys()) {
                continue;
            }
            genOut(cfg.getName());
        }

        if (global) {
            return;
        }
    }

    // genOffsets
    void genOffsets( Unit unit )
    {
        for (String sn : new String[] {"Module_State", "Instance_State"}) {
            Decl.Struct str = (Decl.Struct)unit.getSession().findDecl(unit, sn);
            if (str != null) {
                for (Decl.Field fld : str.getFields()) {
                    Decl.Signature.ObjKind ok = Decl.objKind(fld);
                    if (ok == Decl.Signature.ObjKind.NONE) {
                        continue;
                    }
                    String on = sn + '_' + fld.getName();
                    genOut(on);
                }
            }
        }
    }

    // genOpaques
    void genOpaques( Unit unit )
    {
        if (global) {
            return;
        }

        if (unit.isInst()) {
            genOut("Instance");
            genOut("Handle");
            if (unit.isMod()) {
                genOut("Module");
                genOut("Object");
                genOut("Struct");
            }
        }
        if (unit.isInter()) {
            genOut("Module");
        }
    }

    // genOut
    void genOut( String id )
    {
        if (defcmd) {
            glob.out.printf("#define %1 %2\n", prefix + id, glob.cname + id);
        }
        else {
            glob.out.printf("#undef %1\n", prefix + id);
        }
    }
}
