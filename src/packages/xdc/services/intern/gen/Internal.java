/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
package xdc.services.intern.gen;

import xdc.services.global.*;
import java.io.*;
import java.util.*;

import xdc.services.spec.Unit;
import xdc.services.spec.Decl;
import xdc.services.spec.Attr;

public class Internal
{
    static final String IS = "_INTERNAL__";
    static final String IAS = "_internalaccess";

    private Glob glob = new Glob();

    // gen
    public void gen( Unit unit, Out out )
    {
        if (unit.isInter()) {
            return;
        }

        glob.setNames(unit);
        glob.mode = Glob.CDLMODE;
        glob.out = out;

        glob.genWarning();
        glob.out.printf("\n");
        glob.out.printf("#ifndef %1%2\n#define %1%2\n\n", glob.cname, IS);

        glob.out.printf("#ifndef %1%2\n#define %1%2\n#endif\n\n", glob.cname,
            IAS);

        glob.out.printf("#include <%1.h>\n\n", glob.mkFname(unit.getQualName()));

        glob.out.printf("#undef xdc_FILE__\n");
        glob.out.printf("#ifndef xdc_FILE\n");
        glob.out.printf("#define xdc_FILE__ NULL\n");
        glob.out.printf("#else\n");
        glob.out.printf("#define xdc_FILE__ xdc_FILE\n");
        glob.out.printf("#endif\n\n");

        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isMeta() || fxn.isSys() || fxn.attrBool(Attr.A_Macro)) {
                continue;
            }
            String fn = glob.cname + fxn.getName() + ((fxn.isVarg()) ? "_va" :
                "");
            String suf = fxn.attrBool(Attr.A_DirectCall) ? "__E" : "__F";
            glob.genTitleD(fxn.getName());
            glob.out.printf("#undef %1\n", fn);
            glob.out.printf("#define %1 %1%2\n", fn, suf);
        }

        if (unit.needsRuntime()) {

            for (Decl.Fxn fxn : unit.getInternFxns()) {
                if (fxn.isMeta()) {
                    continue;
                }
                String fn = glob.cname + fxn.getName() + "__I";
                glob.genTitleD(fxn.getName());
                glob.out.printf("#define %1_%2 %3\n", unit.getName(),
                                fxn.getName(), fn);
            }

            String[] fns = {"Module_startup", "Instance_init",
                "Instance_finalize"};

            for (String fn : fns) {
                glob.genTitleD(fn);
                glob.out.printf("#undef %1%2\n", glob.cname, fn);
                glob.out.printf("#define %1%2 %1%2__E\n", glob.cname, fn);
            }

            if (unit.getSession().findDecl(unit, "Module_State") != null) {
                glob.genTitleD("module");
                /* ROM check */
                glob.out.printf("#ifdef %1Module__state__VR\n", glob.cname);
                glob.out.printf("#define %1_module ((%2Module_State *)"
                    + "(xdcRomStatePtr + %2Module__state__V_offset))\n",
                    unit.getName(), glob.cname);
                glob.out.printf("#define module ((%1Module_State *)"
                    + "(xdcRomStatePtr + %1Module__state__V_offset))\n",
                    glob.cname);
                glob.out.printf("#else\n");
                glob.out.printf("#define %1_module ((%2Module_State *)"
                    + "(xdc__MODOBJADDR__(%2Module__state__V)))\n",
                    unit.getName(), glob.cname);
                glob.out.printf("#if !defined(__cplusplus) ||"
                    + "!defined(%1_cplusplus)\n", glob.cname);
                glob.out.printf("#define module ((%1Module_State *)"
                    + "(xdc__MODOBJADDR__(%1Module__state__V)))\n", glob.cname);
                glob.out.printf("#endif\n");
                glob.out.printf("#endif\n");
            }

            glob.genModuleDefines();

            // TODO: replace with something more robust

            if (unit.isInst()) {
                glob.genTitleD("Object__sizingError");
                glob.out.printf("#line 1 \"Error_inconsistent_object_size_in_%1\"\n", unit.getQualName());
                glob.out.printf("typedef char %1%2[(sizeof(%1Object) > "
                    + "sizeof(%1Struct)) ? -1 : 1];\n",
                    glob.cname, "Object__sizingError");
            }
        }

        glob.out.printf("\n\n#endif /* %1%2__ */\n", glob.iname, IS);
    }
}
