/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.gen;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import xdc.services.global.Err;
import xdc.services.global.Out;

import xdc.services.spec.XDoc;
import xdc.services.spec.Pkg;
import xdc.services.spec.Unit;
import xdc.services.spec.Decl;
import xdc.services.spec.Expr;
import xdc.services.spec.Node;
import xdc.services.spec.Type;
import xdc.services.spec.Ref;
import xdc.services.spec.Attr;
import xdc.services.spec.Atom;

/*
 *  ======== Doc ========
 */
public class Doc
{
    static final String ELIPSIS = "<b>&nbsp;&nbsp;...</b>";
    static final String IDREP = "@";

    static final int SPEC_DOM = 0;
    static final int META_DOM = 1;
    static final int TARG_DOM = 2;

    private XDoc curDeclDoc = null;
    private Pkg curPkg = null;
    private Unit curUnit = null;
    private String curPre = null;
    private String curRoot = null;
    
    private Glob glob = new Glob();

    // gen
    public void gen( Pkg pkg, Out out )
    {
        glob.out = out;

        curPkg = pkg;
        curRoot = pkg.getName().replace(".", "/");
        curRoot = curRoot.replaceAll("(\\w+)", "..") + "/";

        glob.out.printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

        genPkg(pkg);
    }

    // genCmdDocs
    void genCmdDocs( Pkg pkg, XDoc xd )
    {
        if (xd.getSummary().length() > 0) {
            return;
        }

        Unit unit = null;
        for (Unit u : pkg.getChildren()) {
            if (u.getName().equals("Main")) {
                unit = u;
                break;
            }
        }

        if (unit == null) {
            return;
        }

        List<Unit> uL = unit.getInherits();
        if (uL.size() == 0 || !(uL.get(uL.size() - 1).getQualName().equals("xdc.tools.ICmd"))) {
            return;
        }

        String us = "<tt>xs " + pkg.getQualName() + " [--help]";
        Decl.Config ucfg = (Decl.Config)unit.getDecl("usage");
        Expr.Array ea = (Expr.Array)ucfg.getInit();
        for (Expr es : ea.getElems()) {
            String s = Expr.toText(es);
            s = s.replace("<", "&lt;");
            s = s.replace(">", "&gt;");
            us += "<br />&nbsp;&nbsp;&nbsp;&nbsp;" + s.substring(1, s.length() - 1);
        }
        us += "</tt>";
        xd.addBeforeSect("usage", us);

        String os = "<dl>";
        for (Decl.Config cfg : unit.getConfigs()) {
            String opt = cfg.attrString(Attr.A_CommandOption);
            if (opt != null) {
                XDoc cxd = cfg.makeXDoc();
                if (cxd.isNodoc()) {
                    /* don't document option if nodoc'd */
                    continue;
                }

                /* an option may have multiple names, separated by commas */
                String[] flags = opt.split(",");
                String sep = "";
                os += "<dt>";
                for (String flag : flags) {
                    String ds;
                    if (flag.equals("")) {
                        /* ignore empty names */
                        continue;
                    }
                    else if (flag.charAt(0) == '-') {
                        /* option already includes a dash prefix */
                        ds = "";
                    }
                    else if (flag.length() == 1) {
                        /* short option names start with '-' */
                        ds = "-";
                    }
                    else {
                        /* long option names start with '--' */
                        ds = "--";
                    }
                    os += sep + "<tt>" + ds + flag + "</tt>";
                    sep = ", ";
                }
                os += "</dt>";
                os += "<dd>" + cxd.getSummary();
                for (String lines : cxd.getDetails()) {
                    os += "<p>" + lines + "</p>";
                }
                os += "</dd>";
            }
        }
        xd.addBeforeSect("options", os);

        XDoc uxd = unit.makeXDoc();
        xd.inherit(uxd);
        uxd.setSummary("Command implementation");
    }

    // genDecl
    void genDecl( Decl d )
    {
        XDoc xd = d.makeXDoc();
        XDoc xdOld = curDeclDoc;
        curDeclDoc = xd;

        glob.xmlElem("decl");
        glob.xmlAttr("kind", d.getXmlTag());
        glob.xmlAttr("name", d.getName());
        glob.xmlAttr("anchor", mkAnchor(d.getName()));
        glob.xmlAttr("altext", curUnit.getQualName() + '.' + d.getName());
        glob.xmlAttr("nodoc", xd.isNodoc());
        glob.xmlAttr("external", d.isExternal());
        glob.xmlAttr("overrides", d.overrides() != null);
        glob.xmlAttr("readonly", d.isReadonly());
        glob.xmlAttr("instance", d.isInst());
        glob.xmlAttr("summary", mkDocText(xd.getSummary()));

        if (d.getParent() instanceof Unit && d.getParent() != curUnit) {
            glob.xmlAttr("origin", glob.encode(mkLink((Unit)d.getParent(), d)));
        }
        else if (d.overrides() != null) {
            glob.xmlAttr("origin", glob.encode(mkLink((Unit)d.overrides().getParent(), d)));
        }

        if (!d.getParent().isMeta() && d.isMeta()) {
            glob.xmlAttr("metaonly", d.isMeta());
        }

//        if (curUnit.isInter()) {
//            glob.xmlAttr("final", d.isFinal());
//        }

        glob.xmlContent();

        Class c;
        Class c1;
        Method m = null;

        try {
            String cn = d.getClass().getName();
            cn = cn.substring(cn.lastIndexOf('.') + 1);
            m = Doc.class.getDeclaredMethod("gen" + cn,d.getClass());
            m.invoke(this, new Object[] { d });
        }
        catch (InvocationTargetException ite ) {
            System.err.println(ite.getTargetException());
            Err.abort(ite);
        }
        catch (Exception e) {
            Err.abort(e);
        }

        genDocs(curDeclDoc, d);
        curDeclDoc = xdOld;

        glob.xmlEnd("decl");
    }

    // genDecl$Config
    void genDecl$Config( Decl.Config cfg )
    {
        String cs =
            (cfg.isOver() ? kw("override ") : "") +
            (cfg.isMeta() && !cfg.getParent().isMeta() ? kw("metaonly ") : "") +
            (cfg.isReadonly() ? kw("readonly ") : "") +
            kw("config");

        glob.xmlElem("synopSpec");
        glob.xmlContent();
        if (cfg.getInit() == null) {
            genLine(0, "%1 %2;", cs, mkDtorX(cfg));
        }
        else {
            genLine(0, "%1 %2 = %3", cs, mkDtorX(cfg), mkInitHd(cfg.getInit()));
            genInitTl(cfg.getInit(), 1);
        }
        glob.xmlEnd("synopSpec");

        if (curUnit.isInter()) {
            return;
        }

        glob.xmlElem("synopMeta");
        glob.xmlContent();
        if (cfg.isInst()) {
            genLine(0, "%1 params = %2 %3.%4;",
                    kw("var"), kw("new"), curUnit.getName(), mkIdX("Params"));
            genLine(0, ELIPSIS);
        }
        if (cfg.isReadonly()) {
            String vs = cfg.getInit() == null ? "<i>computed value</i>;" : mkInitHd(cfg.getInit());
            genLine(0, "%1 %2 = %3 %4", kw("const"), mkDtorS(cfg), mkType(cfg), vs);
        }
        else {
            String vs = cfg.getInit() == null ? kw2("undefined") + ';' : mkInitHd(cfg.getInit());
            genLine(0, "%1 = %2 %3", mkDtorS(cfg), mkType(cfg), vs);
        }
        genInitTl(cfg.getInit(), 1);
        glob.xmlEnd("synopMeta");

        if (cfg.isMeta()) {
            return;
        }

        glob.xmlElem("synopTarg");
        glob.xmlContent();
        if (cfg.isInst()) {
            genLine(0, "%1 %2 {", kw("struct"), mkLink(curUnit, "Params"));
            genLine(1, "%1", ELIPSIS);
            genLine(1, "%1;", mkDtorC(cfg, cfg.getName()));
        }
        else {
            genLine(0, "%1 %2;", kw("extern const"), mkDtorC(cfg));
        }
        glob.xmlEnd("synopTarg");
    }

    // genDecl$Const
    void genDecl$Const( Decl.Const cst )
    {
        String os = cst.isOver() ? kw("override ") : "";

        glob.xmlElem("synopSpec");
        glob.xmlContent();
        if (cst.isFinal()) {
            genLine(0, "%4%1 %2 = %3", kw("const"), mkDtorX(cst), mkInitHd(cst.getInit()), os);
        }
        else {
            genLine(0, "%3%1 %2;", kw("const"), mkDtorX(cst), os);
        }
        glob.xmlEnd("synopSpec");

        if (curUnit.isInter()) {
            return;
        }

        glob.xmlElem("synopMeta");
        glob.xmlContent();
        genLine(0, "%1 %2 = %3", kw("const"), mkDtorS(cst), mkInitHd(cst.getInit()));
        glob.xmlEnd("synopMeta");

        if (cst.isMeta()) {
            return;
        }

        glob.xmlElem("synopTarg");
        glob.xmlContent();
        String dn = mkIdC(cst, cst.getName());
        if (cst.isFinal()) {
            genLine(0, "%1 %2  (%3)%4",
                span("xdoc-kw3", "#define"), dn, mkType(cst, true), mkInitHd(cst.getInit(), ""));
        }
        else {
            genLine(0, "%1 %2;", kw("extern const"), mkDtorC(cst));
        }
        glob.xmlEnd("synopTarg");
    }

    // genDecl$Enum
    void genDecl$Enum( Decl.Enum enm )
    {
        for (Decl.EnumVal evl : enm.getChildren()) {
            glob.xmlElem("decl");
            glob.xmlAttr("kind", evl.getXmlTag());
            glob.xmlAttr("name", evl.getName());
            glob.xmlAttr("anchor", mkAnchor(evl.getName()));
            glob.xmlEnd();
        }

        glob.xmlElem("synopSpec");
        glob.xmlContent();
        genLine(0, "%1 %2 {", kw("enum"), mkIdX(enm));
        genDecl_EnumVals(enm, SPEC_DOM);
        genLine(0, "};");
        glob.xmlEnd("synopSpec");

        if (curUnit.isInter()) {
            return;
        }

        glob.xmlElem("synopMeta");
        glob.xmlContent();
        genLine(0, "<i>values of type %1</i>", mkDtorS(enm));
        genDecl_EnumVals(enm, META_DOM);
        glob.xmlEnd("synopMeta");

        if (enm.isMeta()) {
            return;
        }

        glob.xmlElem("synopTarg");
        glob.xmlContent();
        if (enm.getRep() != null) {
            genLine(0, "%1 %2 %3;", kw("typedef"), mkType(enm.getRep(), true, false), mkDtorC(enm));
            genLine(0, "%1 {", kw("enum"));
        }
        else {
            genLine(0, "%1 %2 %3 {", kw("typedef"), kw("enum"), mkDtorC(enm));
        }
        genDecl_EnumVals(enm, TARG_DOM);
        genLine(0, "} %1;", enm.getRep() != null ? "" : mkDtorC(enm));
        glob.xmlEnd("synopTarg");
    }

    // genDecl_EnumVals
    void genDecl_EnumVals( Decl.Enum enm, int dom )
    {
        int k = enm.getChildren().size();
        for (Decl.EnumVal evl : enm.getChildren()) {
            if (dom == META_DOM) {
                genLineC(1, evl.getSummary(), "%1 %2;", kw("const"), mkDtorS(evl), mkDtorS(enm));
            }
            else {
                genLineC(1, evl.getSummary(), "%1%2", dom == SPEC_DOM ? mkIdX(evl) : mkDtorC(evl), (--k > 0 ? ", " : ""));
            }
        }
    }

    // genDecl_Field
    void genDecl_Field( Decl.Field fld, int dom, int tab ) { genDecl_Field(fld, dom, tab, ""); }
    void genDecl_Field( Decl.Field fld, int dom, int tab, String pre )
    {
        Ref ref = fld.getType().tspec().getRef();
        Node sn = ref.getNode();

        if (!ref.isGlobal() && sn.getName().startsWith("__struct__")) {
            Decl.Struct str = (Decl.Struct)sn;
            if (dom == META_DOM) {
                if (str.isUnion()) {
                    Decl.Field f0 = str.getChildren().get(0);
                    genLine(1, "<i>obj</i>.%1%2 = %3%4", pre, mkIdX(fld), mkType(f0), ELIPSIS);
                }
                else {
                    genLineC(tab, fld.getSummary(), "<i>obj</i>.%1%2 = %3%4",
                            pre, mkIdX(fld), mkType(fld), ELIPSIS);
                    for (Decl.Field f2 : str.getChildren()) {
                        genDecl_Field(f2, dom, tab + 1, pre + fld.getName() + '.');
                    }
                }
            }
            else {
                genLineC(tab, fld.getSummary(), "%1 {", kw(str.isUnion() ? "union" : "struct"));
                for (Decl.Field f2 : str.getChildren()) {
                    genDecl_Field(f2, dom, tab + 1);
                }
                genLine(tab, "} %1;", dom == SPEC_DOM ? mkIdX(fld) : mkIdX(fld));
            }
        }
        else if (dom == SPEC_DOM) {
            genLineC(tab, fld.getSummary(), "%1;", mkDtorX(fld));
        }
        else if (dom == META_DOM) {
            genLineC(tab, fld.getSummary(), "<i>obj</i>.%1%2 = %3%4",
                    pre, mkIdX(fld), mkType(fld), ELIPSIS);
        }
        else {
          genLineC(tab, fld.getSummary(), "%1;", mkDtorC(fld, mkIdX(fld)));
        }
    }

    // genDecl$Extern
    void genDecl$Extern( Decl.Extern fxn )
    {
        /* TODO: generate spec synopsys: show how the extern is declared */

        /* generate Target synopsys */
        glob.xmlElem("synopTarg");
        glob.xmlContent();

        genLine(0, "%1; // linked as extern %2", mkDtorC(fxn), fxn.getValue());

        glob.xmlEnd("synopTarg");
    }

    // genDecl$Fxn
    void genDecl$Fxn( Decl.Fxn fxn )
    {
        String args;
        String sep;

        boolean mflg = fxn.hasAttr(Attr.A_Macro);

        glob.xmlElem("synopSpec");
        glob.xmlContent();
        if (fxn.getChildren() != null) {
            args = "";
            sep = "";
            for (Decl.Arg a : fxn.getChildren()) {
                a.makeXDoc();
                args += sep + mkDtorX(a);
                sep = ", ";
            }
            args += fxn.isVarg() ? sep + "..." : "";
            genLine(0, "%1%2%3%4( %5 );",
                    mflg ? span("xdoc-kw3", "@Macro ") : "",
                    fxn.isOver() ? kw("override ") : "",
                    fxn.isMeta() && !fxn.getParent().isMeta() ? kw("metaonly ") : "",
                    mkDtorX(fxn),
                    args);
        }
        else {
            genLine(0, "%1%2%3;", 
                    fxn.isOver() ? kw("override ") : "",
                    fxn.isMeta() && !fxn.getParent().isMeta() ? kw("metaonly ") : "",
                    mkDtorX(fxn));
        }
        glob.xmlEnd("synopSpec");

        if (curUnit.isInter()) {
            return;
        }

        if (fxn.isMeta()) {
            glob.xmlElem("synopMeta");
            glob.xmlContent();
            if (fxn.getChildren() != null) {
                args = "";
                sep = "";
                for (Decl.Arg a : fxn.getChildren()) {
                    args += sep + mkType(a) + ' ' + mkIdX(a);
                    sep = ", ";
                }
                args += fxn.isVarg() ? sep + "..." : "";
                genLine(0, "%1( %2 ) <i>returns</i> %3", mkDtorS(fxn), args, mkType(fxn));
            }
            else {
                genLine(0, "%1(%2&nbsp;&nbsp;)", mkDtorS(fxn), ELIPSIS);
            }
            glob.xmlEnd("synopMeta");
            return;
        }

        glob.xmlElem("synopTarg");
        glob.xmlContent();
        if (fxn.isInst()) {
            XDoc xd = fxn.makeXDoc();
            if (fxn.getParent() != curUnit) {
                xd = xd.copy();
                curDeclDoc = xd;
            }
            xd.pushChild("handle", "handle of a previously-created `" + curUnit.getName() + "` instance object");
            args = mkLink(curUnit, "Handle") + " handle";
            sep = ", ";
        }
        else {
            args = "";
            sep = "";
        }
        for (Decl.Arg a : fxn.getChildren()) {
            args += sep + mkDtorC(a, a.getName());
            sep = ", ";
        }
        args += fxn.isVarg() ? sep + "..." : "";
        genLine(0, "%3%1( %2 );", mkDtorC(fxn), args, mflg ? "<i>macro</i> " : "");
        glob.xmlEnd("synopTarg");
    }

    // genDecl$Proxy
    void genDecl$Proxy( Decl.Proxy prx )
    {
        String cs;

        glob.xmlElem("synopSpec");
        glob.xmlContent();
        genLine(0, "%1 %2 %3 %4;", kw("proxy"), prx.getName(), kw("inherits"), mkLink(prx.getInherits()));
        glob.xmlEnd("synopSpec");

        glob.xmlElem("synopMeta");
        glob.xmlContent();

        genLine(0, "%1 = <i>%2.Module</i> %3", mkDtorS(prx), mkLink(prx.getInherits()), kw2("null"));
        genLineE(true);
        genLineC(0, "some delegate module inheriting the " + mkLink(prx.getInherits()) + " interface", null);
        genLineE(false);
        genLine(1, "%1.delegate$ = <i>%2.Module</i> %3", mkDtorS(prx), mkLink(prx.getInherits()), kw2("null"));
        genLineE(true);
        genLineC(1, "explicit access to the currently bound delegate module", null);
        genLineE(false);
        if (prx.getInherits().isInst()) {
            genLine(1, "%1.abstractInstances$ = %2", mkDtorS(prx), span("xdoc-kw2", "false"));
            genLineE(true);
            genLineC(1, "use indirect runtime function calls if true", null);
            genLineE(false);
        }
        glob.xmlEnd("synopMeta");
    }

    // genDecl$Struct
    void genDecl$Struct( Decl.Struct str )
    {
        String kw = str.isUnion() ? kw("union") : kw("struct");
        String ms = str.isMeta() ? kw("metaonly ") : "";
        String os = str.hasAttr(Attr.A_Opaque) ? span("xdoc-kw3", "@Opaque ") : "";

        boolean opFlg = str.getChildren() == null || str.hasAttr(Attr.A_Opaque);

        glob.xmlElem("synopSpec");
        glob.xmlContent();
        if (opFlg) {
            genLine(0, "%3%4%1 %2;", kw, mkIdX(str), os, ms);
        }
        else {
            genLine(0, "%3%1 %2 {", kw, mkIdX(str), ms);
            for (Decl.Field fld : str.getChildren()) {
                genDecl_Field(fld, SPEC_DOM, 1);
            }
            genLine(0, "};");
        }
        glob.xmlEnd("synopSpec");

        if (curUnit.isInter()) {
            return;
        }

        if (str.getChildren() != null && str.getChildren().size() > 0 && !str.hasAttr(Attr.A_Opaque)) {
            glob.xmlElem("synopMeta");
            glob.xmlContent();
            genLine(0, "%1 <i>obj</i> = %2 %3;", kw("var"), kw("new"), mkDtorS(str));
            genLineB();
            if (str.isUnion()) {
                Decl.Field fld = str.getChildren().get(0);
                genLine(1, "<i>obj</i>.%1 = %2%3", mkIdX(fld), mkType(fld), ELIPSIS);
            }
            else {
                for (Decl.Field fld : str.getChildren()) {
                    genDecl_Field(fld, META_DOM, 1);
                }
            }
            glob.xmlEnd("synopMeta");
        }

        if (str.isMeta()) {
            return;
        }

        glob.xmlElem("synopTarg");
        glob.xmlContent();
        if (opFlg) {
            genLine(0, "%1 %2 %3 %3;", kw("typedef"), kw, mkDtorC(str));
        }
        else {
            genLine(0, "%1 %2 %3 {", kw("typedef"), kw, mkDtorC(str));
            for (Decl.Field fld : str.getChildren()) {
                genDecl_Field(fld, TARG_DOM, 1);
            }
            genLine(0, "} %1;", mkDtorC(str));
        }
        glob.xmlEnd("synopTarg");
    }

    // genDecl$Typedef
    void genDecl$Typedef( Decl.Typedef typ )
    {
        boolean enFlg = typ.hasAttr(Attr.A_Encoded);

        glob.xmlElem("synopSpec");
        glob.xmlContent();
        genLine(0, "%3%1 %2;", kw("typedef"), mkDtorX(typ), enFlg ? span("xdoc-kw3", "@Encoded ") : "");
        glob.xmlEnd("synopSpec");

        if (curUnit.isInter()) {
            return;
        }

        glob.xmlElem("synopTarg");
        glob.xmlContent();
        if (enFlg) {
            genLine(0, "%1 <i>opaque</i> %2;", kw("typedef"), mkIdX(curUnit.getName() + '_' + typ.getName()));
        }
        else {
            genLine(0, "%1 %2;", kw("typedef"), mkDtorC(typ));
        }
        glob.xmlEnd("synopTarg");
    }

    // genDocs
    void genDocs( XDoc xd, Decl d )
    {
        if (xd.getChild().size() > 0) {
            genDocChild(xd, d);
        }

        for (Map.Entry<String,List<String>> me : xd.getHeaders().entrySet()) {
            genDocSect(me.getKey(), me.getValue());
        }

        if (xd.getDetails().size() > 0) {
            genDocSect("details", xd.getDetails());
        }

        for (Map.Entry<String,List<String>> me : xd.getOthers().entrySet()) {
            genDocSect(me.getKey(), me.getValue());
        }

        for (Map.Entry<String,List<String>> me : xd.getTrailers().entrySet()) {
            genDocSect(me.getKey(), me.getValue());
        }
    }

    // genDocChild
    void genDocChild( XDoc xd, Decl d )
    {
        glob.xmlElem("docSect");
        glob.xmlAttr("name", d instanceof Decl.Fxn ? "ARGUMENTS" : d instanceof Decl.Struct ? "FIELDS" : "VALUES");
        glob.xmlContent();

        for (Map.Entry<String,List<String>> me : xd.getChild().entrySet()) {
            List<String> lines = me.getValue();
            glob.xmlElem("docChild");
            glob.xmlAttr("name", me.getKey());
            glob.xmlAttr("summary", mkDocText(lines.get(0)));
            glob.xmlContent();
            for (int i = 1; i < lines.size(); i++) {
                glob.xmlElem("docPara");
                glob.xmlAttr("content", mkDocText(lines.get(i)));
                glob.xmlEnd();
            }
            glob.xmlEnd("docChild");
        }

        glob.xmlEnd("docSect");
    }

    // genDocSect
    void genDocSect( String name, List<String> lines )
    {
        glob.xmlElem("docSect");
        glob.xmlAttr("name", name);
        glob.xmlContent();
        for (String ln : lines) {
            glob.xmlElem("docPara");
            glob.xmlAttr("content", mkDocText(ln));
            glob.xmlEnd();
        }
        glob.xmlEnd("docSect");
    }

    // genGrpCST
    void genGrpCST( Decl[] dA )
    {
        glob.xmlElem("group");
        glob.xmlAttr("name", "module-wide constants &amp; types");
        glob.xmlContent();

        for (Decl d : dA) {
            if (d instanceof Decl.Const) {
                genDecl(d);
            }
        }
        for (Decl d : dA) {
            if (d instanceof Decl.Enum) {
                genSpacer();
                genDecl(d);
            }
        }
        genSpacer();
        for (Decl d : dA) {
            if (d instanceof Decl.Typedef) {
                genDecl(d);
            }
        }
        for (Decl d : dA) {
            if (d instanceof Decl.Struct && !d.getName().startsWith("__struct__")) {
                genSpacer();
                genDecl(d);
            }
        }

        glob.xmlEnd("group");
    }

    // genGrpMWB
    void genGrpMWB( Unit unit )
    {
        glob.xmlElem("group");
        glob.xmlAttr("name", "module-wide built-ins");
        glob.xmlContent();

        if (unit.isMeta() || unit.hasAttr(Attr.A_NoRuntime)) {
        }
        else {
            glob.xmlElem("synopTarg");
            glob.xmlContent();
            genLineC(0, "Get this module's unique id", "%1 %2( );", mkRtRef("Types", "ModuleId"), mkIdC(unit, "Module_id"));
            genLineB();
            genLineC(0, "Test if this module has completed startup", "%1 %2( );", kw2("Bool"), mkIdC(unit, "Module_startupDone"));
            genLineB();
            String cs = "The heap from which this module allocates memory";
            genLineC(0, cs, "%1 %2( );", mkRtRef("IHeap", "Handle"), mkIdC(unit, "Module_heap"));
            
            genLineB();
            cs = "Test whether this module has a diagnostics mask";
            genLineC(0, cs, "%1 %2( );", kw2("Bool"), mkIdC(unit, "Module_hasMask"));
            
            genLineB();
            cs = "Returns the diagnostics mask for this module";
            genLineC(0, cs, "%1 %2( );", kw2("Bits16"), mkIdC(unit, "Module_getMask"));
            
            
            genLineB();
            cs = "Set the diagnostics mask for this module";
            genLineC(0, cs, "%1 %2( %3 %4 );", kw2("Void"), 
                     mkIdC(unit, "Module_setMask"), kw2("Bits16"), span("xdoc-id", "mask"));

            glob.xmlEnd("synopTarg");
        }

        glob.xmlEnd("group");
    }

    // genGrpMWD
    void genGrpMWD( Decl.Extern[] xDA )
    {
        glob.xmlElem("group");
        glob.xmlAttr("name", "module-wide data");
        glob.xmlContent();

        for (Decl.Extern d : xDA) {
            genDecl(d);
        }

        glob.xmlEnd("group");
    }

    // genGrpMWF
    void genGrpMWF( Decl.Fxn[] fA, Decl.Extern[] xFA )
    {
        glob.xmlElem("group");
        glob.xmlAttr("name", "module-wide functions");
        glob.xmlContent();

        for (Decl.Fxn d : fA) {
            if (!d.isMeta() && !d.isSys() && !d.isInst()) {
                genDecl(d);
            }
        }
        for (Decl.Extern d : xFA) {
            genDecl(d);
        }
        genSpacer();
        for (Decl.Fxn d : fA) {
            if (d.isMeta() && !d.isSys() && !d.isInst()) {
                genDecl(d);
            }
        }

        glob.xmlEnd("group");
    }

    // genGrpMWG
    void genGrpMWG( Decl.Config[] cA, boolean hasNoRuntime )
    {
        glob.xmlElem("group");
        glob.xmlAttr("name", "module-wide config parameters");
        glob.xmlContent();

        for (Decl.Config d : cA) {
            if (!d.isMeta() && !d.isSys() && !d.isInst() && d.isReadonly()) {
                genDecl(d);
            }
        }
        genSpacer();
        for (Decl.Config d : cA) {
            if (d.isMeta() && !d.isSys() && !d.isInst() && d.isReadonly()) {
                genDecl(d);
            }
        }
        genSpacer();
        for (Decl.Config d : cA) {
            if (!d.isMeta() && !d.isSys() && !d.isInst() && !d.isReadonly()) {
                genDecl(d);
            }
        }
        genSpacer();
        for (Decl.Config d : cA) {
            if (d.isMeta() && !d.isSys() && !d.isInst() && !d.isReadonly()) {
                /* HACK: we should really exclude all configs defined by IModule */
                if (hasNoRuntime && d.getName().equals("common$")) {
                    continue;
                }
                genDecl(d);
            }
        }

        glob.xmlEnd("group");
    }

    // genGrpPIB
    void genGrpPIB( Unit unit )
    {
        String hs = mkLink(unit, "Handle");

        glob.xmlElem("group");
        glob.xmlAttr("name", "per-instance built-ins");
        glob.xmlContent();

        if (unit.isMeta()) {
        }
        else {
            String cs;
            glob.xmlElem("synopTarg");
            glob.xmlContent();
            cs = "The number of statically-created instance objects";
            genLineC(0, cs, "%1 %2( );", kw2("Int"), mkIdC(unit, "Object_count"));
            genLineB();
            cs = "The handle of the i-th statically-created instance object (array == NULL)";
            genLineC(0, cs, "%1 %2( %3 *array, %4 i );", hs, mkIdC(unit, "Object_get"), mkLink(unit, "Object"), kw2("Int"));
            genLineB();
            cs = "The handle of the first dynamically-created instance object, or NULL";
            genLineC(0, cs, "%1 %2( );", hs, mkIdC(unit, "Object_first"));
            genLineB();
            cs = "The handle of the next dynamically-created instance object, or NULL";
            genLineC(0, cs, "%1 %2( %1 handle );", hs, mkIdC(unit, "Object_next"));
            genLineB();
            cs = "The heap used to allocate dynamically-created instance objects";
            genLineC(0, cs, "%1 %2( );", mkRtRef("IHeap", "Handle"), mkIdC(unit, "Object_heap"));
            genLineB();
            cs = "The label associated with this instance object";
            genLineC(0, cs, "%1 *%2( %3 handle, %1 *buf );", mkRtRef("Types", "Label"), mkIdC(unit, "Handle_label"), hs);
            genLineB();
            cs = "The name of this instance object";
            genLineC(0, cs, "%1 %2( %3 handle );", kw2("String"), mkIdC(unit, "Handle_name"), hs);
            glob.xmlEnd("synopTarg");
        }

        glob.xmlEnd("group");
    }

    // genGrpPIC
    void genGrpPIC( Unit unit )
    {
        glob.xmlElem("group");
        glob.xmlAttr("name", "per-instance creation");

        Decl.Fxn fxn = unit.getCreator();

        if (!isVisible(fxn)) {
            glob.xmlEnd();
            return;
        }

        glob.xmlContent();

        XDoc xd = fxn.makeXDoc();
        if (fxn.getParent() != curUnit) {
            xd = xd.copy();
        }

        String args;
        String sep;

        glob.xmlElem("synopSpec");
        glob.xmlContent();
        args = "";
        sep = "";
        for (Decl.Arg a : fxn.getChildren()) {
            a.makeXDoc();
            args += sep + mkDtorX(a);
            sep = ", ";
        }
        genLineC(0, "Create an instance-object", "%1( %2 );", mkIdX("create"), args);
        glob.xmlEnd("synopSpec");

        if (curUnit.isMod()) {

            glob.xmlElem("synopMeta");
            glob.xmlContent();
            args = "";
            sep = "";
            for (Decl.Arg a : fxn.getChildren()) {
                args += sep + mkType(a) + ' ' + mkIdX(a.getName());
                sep = ", ";
            }
            genLineE(true);
            String ls = mkLink(curPkg.getQualName(), curUnit.getName(), "Params", "Params");
            genLineC(0, "Allocate instance config-params", "%1 params = %2 %3.%4;",
                    kw("var"), kw("new"), curUnit.getName(), ls);
            genLineC(0, "Assign individual configs", "params.<i>config</i> = %1", ELIPSIS);
            genLineB();
            genLineE(false);
            genLineC(0, "Create an instance-object", "%1 inst = %2.%3( %4params );",
                    kw("var"),
                    curUnit.getName(),
                    mkIdX("create"),
                    args + sep);
            glob.xmlEnd("synopMeta");
        }

        if (!curUnit.isMeta() && curUnit.isMod()) {

            glob.xmlElem("synopTarg");
            glob.xmlContent();

            args = "";
            sep = "";
            for (Decl.Arg a : fxn.getChildren()) {
                args += sep + mkDtorC(a, a.getName());
                sep = ", ";
            }

            String es = mkRtRef("Error", "Block");
            String hs = mkLink(curUnit, "Handle");
            String ps = mkLink(curUnit, "Params");
            String ss = mkLink(curUnit, "Struct");

            String ctc = "Allocate and initialize a new instance object and return its handle";
            String csc = "Initialize a new instance object inside the provided structure";

            xd.addChild("params", "per-instance config params, or `NULL` to select default values (target-domain only)");
            xd.addChild("eb", "active error-handling block, or `NULL` to select default policy (target-domain only)");

            genLineC(0, ctc, "%1 %2( %3%4 %5 *params, %6 *eb );", hs, mkIdC(fxn, "create"), args + sep, kw("const"), ps, es);
            genLineB();
            String ebArg = curUnit.hasAttr(Attr.A_InstanceInitError) ? (", " + es + " *eb") : "";
            genLineC(0, csc, "%7 %1( %2 *structP, %3%4 %5 *params%6 );", mkIdC(fxn, "construct"), ss, args + sep, kw("const"), ps, ebArg, kw2("Void"));

            glob.xmlEnd("synopTarg");
        }

        genDocs(xd, fxn);
        glob.xmlEnd("group");
    }

    // genGrpPID
    void genGrpPID( Unit unit )
    {
        glob.xmlElem("group");
        glob.xmlAttr("name", "per-instance deletion");

        if (!isVisible(unit.getCreator())) {
            glob.xmlEnd();
            return;
        }

        glob.xmlContent();

        glob.xmlElem("synopTarg");
        glob.xmlContent();

        String hs = mkLink(curUnit, "Handle");
        String ss = mkLink(curUnit, "Struct");

        String dtc = "Finalize and free this previously allocated instance object, setting the referenced handle to NULL";
        String dsc = "Finalize the instance object inside the provided structure";

        genLineC(0, dtc, "%3 %1( %2 *handleP );", mkIdC(unit, "delete"), hs, kw2("Void"));
        genLineB();
        genLineC(0, dsc, "%3 %1( %2 *structP );", mkIdC(unit, "destruct"), ss, kw2("Void"));

        glob.xmlEnd("synopTarg");

        glob.xmlEnd("group");
    }

    // genGrpPIF
    void genGrpPIF( Decl.Fxn[] fA )
    {
        glob.xmlElem("group");
        glob.xmlAttr("name", "per-instance functions");
        glob.xmlContent();

        for (Decl.Fxn d : fA) {
            if (!d.isMeta() && !d.isSys() && d.isInst()) {
                genDecl(d);
            }
        }
        genSpacer();
        for (Decl.Fxn d : fA) {
            if (d.isMeta() && !d.isSys() && d.isInst()) {
                genDecl(d);
            }
        }

        glob.xmlEnd("group");
    }

    // genGrpPIG
    void genGrpPIG( Unit unit, Decl.Config[] cA )
    {
        String ps = mkIdC(unit, "Params");

        glob.xmlElem("group");
        glob.xmlAttr("name", "per-instance config parameters");

        glob.xmlContent();

        glob.xmlElem("synopMeta");
        glob.xmlContent();
        genLineC(0, "Instance config-params object", "%1 params = %2 %3.%4;",
                kw("var"), kw("new"), curUnit.getName(), mkIdX("Params"));
        for (Decl.Config cfg : cA) {
            if (!cfg.isSys() && cfg.isInst()) {
                String vs = cfg.getInit() == null ? kw2("undefined") + ';' : mkInitHd(cfg.getInit());
                genLineD(1, cfg, "params.%1 = %2 %3", mkIdX(cfg), mkType(cfg), vs);
                genInitTl(cfg.getInit(), 2);
            }
        }
        glob.xmlEnd("synopMeta");

        if (!unit.isMeta()) {
            glob.xmlElem("synopTarg");
            glob.xmlContent();
            String pc = "Instance config-params structure";
            genLineC(0, pc, "%1 %2 %3 {", kw("typedef"), kw("struct"), ps);
            if (!unit.getQualName().equals("xdc.runtime.IInstance")) {
                genLineC(1, "Common per-instance configs", "%1 *instance;", mkRtRef("IInstance", "Params"));
            }
            for (Decl.Config cfg : cA) {
                if (!cfg.isSys() && !cfg.isMeta() && cfg.isInst()) {
                    genLineD(1, cfg, "%1;", mkDtorC(cfg, cfg.getName()));
                }
            }
            genLine(0, "} %1;", ps);
            if (unit.isMod()) {
                String pic = "Initialize this config-params structure with supplier-specified defaults before instance creation";
                String pl = mkLink(unit, "Params");
                genLineB(true);
                genLineC(0, pic, "%1 %2( %3 *params );", kw2("Void"), mkIdC(unit, "Params_init"), pl);
            }
            glob.xmlEnd("synopTarg");
        }

        for (Decl.Config d : cA) {
            if (!d.isMeta() && !d.isSys() && d.isInst()) {
                genDecl(d);
            }
        }
        genSpacer();
        for (Decl.Config d : cA) {
            if (d.isMeta() && !d.isSys() && d.isInst()) {
                genDecl(d);
            }
        }

        glob.xmlEnd("group");
    }

    // genGrpPIT
    void genGrpPIT( Unit unit )
    {
        String hs = mkIdC(unit, "Handle");
        String os = mkIdC(unit, "Object");
        String ps = mkIdC(unit, "Params");
        String ss = mkIdC(unit, "Struct");

        String hl = mkLink(unit, "Handle");
        String ol = mkLink(unit, "Object");
        String pl = mkLink(unit, "Params");
        String sl = mkLink(unit, "Struct");

        String hc = "Client reference to an instance object";
        String oc = "Opaque internal representation of an instance object";
        String sc = "Opaque client structure large enough to hold an instance object";

        String h2c = "Convert this instance structure pointer into an instance handle";
        String s2c = "Convert this instance handle into an instance structure pointer";

        glob.xmlElem("group");
        glob.xmlAttr("name", "per-instance object types");
        glob.xmlContent();

        glob.xmlElem("synopTarg");
        glob.xmlContent();

        if (unit.isInter()) {
            hc = "Client reference to an abstract instance object";
            genLineC(0, hc, "%1 %2 %3 *%4;", kw("typedef"), kw("struct"), os, hs);
        }
        else {
            genLineC(0, oc, "%1 %2 %3 %3;", kw("typedef"), kw("struct"), os);
            genLineB();
            genLineC(0, hc, "%1 %2 *%3;", kw("typedef"), ol, hs);
            genLineB();
            genLineC(0, sc, "%1 %2 %3 %3;", kw("typedef"), kw("struct"), ss);
            genLineB(true);
            genLineC(0, h2c, "%1 %2( %3 *structP );", hl, mkIdC(unit, "handle"), sl);
            genLineB();
            genLineC(0, s2c, "%3 *%2( %1 handle );", hl, mkIdC(unit, "struct"), sl);
        }

        glob.xmlEnd("synopTarg");
        glob.xmlEnd("group");
    }

    // genGrpPIV
    void genGrpPIV( Unit unit )
    {
        String hl = mkLink(unit, "Handle");

        glob.xmlElem("group");
        glob.xmlAttr("name", "per-instance convertors");
        glob.xmlContent();

        glob.xmlElem("synopTarg");
        glob.xmlContent();
        int k = 0;
        boolean sp = false;
        for (Unit iu : unit.getInherits()) {
            if (iu.getQualName().equals("xdc.runtime.IModule")) {
                continue;
            }
            if (!iu.isInst()) {
                break;
            }
            String il = mkLink(iu, "Handle");
            String ks = ++k == 1 ? "" : ("" + k);
            String ls = k == 1 ? "one level" : "" + k + " levels";
            String iin = glob.mkCname(iu.getQualName());
            iin = iin.substring(0, iin.length() - 1);
            String uc = "unconditionally move " + ls + " up the inheritance hierarchy";
            String dc = "conditionally move " + ls + " down the inheritance hierarchy; NULL upon failure";
            if (sp) {
                genLineB();
            }
            else {
                sp = true;
            }
            genLineC(0, uc, "%1 %2( %3 handle );", il, mkIdC(unit, "Handle_upCast" + ks), hl);
            genLineB();
            genLineC(0, dc, "%1 %2( %3 handle );", hl, mkIdC(unit, "Handle_downCast" + ks), il);
        }
        glob.xmlEnd("synopTarg");

        glob.xmlEnd("group");
    }

    // genGrpPRX
    void genGrpPRX( Decl[] dA )
    {
        glob.xmlElem("group");
        glob.xmlAttr("name", "local proxy modules");
        glob.xmlContent();

        for (Decl d : dA) {
            if (!d.isSys() && d instanceof Decl.Proxy) {
                genSpacer();
                genDecl(d);
            }
        }

        glob.xmlEnd("group");
    }

    void genInclude( Unit u )
    {
        genLine(0, "%1 %2",
                span("xdoc-kw3", "#include"), span("xdoc-id", "&lt;" + u.getQualName().replace('.', '/') + ".h&gt;"));
    }

    // genInitTl
    void genInitTl( Expr e, int tab )
    {
        if (!(e instanceof Expr.Aggregate)) {
            return;
        }

        if (((Expr.Aggregate)e).isEmpty()) {
            return;
        }

        String xs = Expr.toXml(e);
        xs = xs.substring(xs.indexOf("@+") + 2);

        Pattern p = Pattern.compile("@([@+=-])");
        Matcher m = p.matcher(xs);

        int cur = 0;

        while (m.find()) {
            if (m.group(1).equals("@")) {
                continue;
            }
            genLine(tab, "%1", mkInit(xs.substring(cur, m.start())));
            if (m.group(1).equals("+")) {
                tab++;
            }
            else if (m.group(1).equals("-")) {
                tab--;
            }
            cur = m.end();
        }

        genLine(tab, "%1;", mkInit(xs.substring(cur)));
    }

    // genLine
    void genLine( int tab, String fmt, String... args )
    {
        genLineX(tab, null, null, null, fmt, args);
    }

    // genLineB
    void genLineB() { genLineB(false); }
    void genLineB( boolean always )
    {
        glob.xmlElem("line");
        glob.xmlAttr("blank", true);
        glob.xmlAttr("always", always);
        glob.xmlEnd();
    }

    // genLineC
    void genLineC( int tab, String com, String fmt, String... args )
    {
        genLineX(tab, (com == null || com.length() == 0) ? null : com, null, null, fmt, args);
    }

    // genLineD
    void genLineD( int tab, Decl d, String fmt, String... args )
    {
        XDoc xd = d.makeXDoc();
        String nd = xd.isNodoc() ? "1" : null;

        /* if the cur unit has target decls, we need to identify any
         * metaonly decls to support unfortunate anchor conventions:
         *    a meta-domain decl's anchor is pre-fixed with 'meta' _only_
         *    if the decl _also_ appears in both the target-domain.  There 
         *    is no such thing as a target only decl, so there are only
         *    two possibilities: the decl is metaonly or it is in both.
         *    So, it's only necessary to identify those decls that are
         *    metaonly in non-metaonly units .
         */
        String mo = (!curUnit.isMeta() && d.isMeta()) ? "1" : null;

        genLineD2(tab, nd, mo, mkDocText(d.getSummary()), mkAnchor(d.getName()), d.getQualName(), fmt, args);
    }

    // genLineD2
    void genLineD2(int tab, String nodoc, String metaonly, String com, String anc, String alt, String fmt, String... args)
    {
        glob.xmlElem("line");
        glob.xmlAttr("tab", tab);
        if (nodoc != null) {
            glob.xmlAttr("nodoc", glob.encode(nodoc));
        }
        if (metaonly != null) {
            glob.xmlAttr("metaonly", glob.encode(metaonly));
        }
        if (com != null) {
            glob.xmlAttr("comment", com);
        }
        if (anc != null) {
            glob.xmlAttr("anchor", anc);
        }
        if (alt != null) {
            glob.xmlAttr("altext", alt);
        }
        if (fmt != null) {
            String cs = fmt;
            for (int i = 0; i < args.length; i++) {
              cs = cs.replace("%" + (i + 1), args[i]);
            }
            glob.xmlAttr("content", glob.encode(cs));
        }

        glob.xmlEnd();
    }

    // genLineE
    void genLineE( boolean stat )
    {
        glob.xmlElem("line");
        glob.xmlAttr("elem", stat);
        glob.xmlEnd();
    }

    // genLineX
    void genLineX( int tab, String com, String anc, String alt, String fmt, String... args )
    {
        glob.xmlElem("line");
        glob.xmlAttr("tab", tab);
        if (com != null) {
            glob.xmlAttr("comment", glob.encode(com));
        }
        if (anc != null) {
            glob.xmlAttr("anchor", anc);
        }
        if (alt != null) {
            glob.xmlAttr("altext", alt);
        }
        if (fmt != null) {
            String cs = fmt;
            for (int i = 0; i < args.length; i++) {
              cs = cs.replace("%" + (i + 1), args[i]);
            }
            glob.xmlAttr("content", glob.encode(cs));
        }

        glob.xmlEnd();
    }

    // genPkg
    void genPkg( Pkg pkg )
    {
        XDoc xd = pkg.makeXDoc();

        genCmdDocs(pkg, xd);

        glob.xmlElem("package");
        glob.xmlAttr("kind", "package");
        glob.xmlAttr("name", pkg.getName());
        glob.xmlAttr("nodoc", xd.isNodoc());
        glob.xmlAttr("root", curRoot);
        glob.xmlAttr("summary", mkDocText(xd.getSummary()));
        glob.xmlContent();

        boolean sp = false;
        for (Atom a : pkg.getRequires()) {
            String pn = a.getText();
            String vs = "";
            if (pn.startsWith("*")) {
                continue;
            }
            int k = pn.indexOf('{');
            if (k < pn.length() - 1) {
                vs = "[" + pn.substring(k + 1) + "]";
            }
            pn = pn.substring(0, k);
            if (pkg.getName().equals(pn)) {
                continue;
            }
            genLine(0, "%1 %2%3;", kw("requires"), mkLink(pn, null, null, pn), vs);
            sp = true;
        }

        if (sp) {
            genLineB();
        }

        String vs = pkg.getKey() != null ? (" [" + pkg.getKey() + "]") : "";
        genLine(0, "%1 %2%3 {", kw("package"), mkIdX(pkg), vs);
        genLine(0, "}");

        Unit[] uA = pkg.getUnits().toArray(new Unit[]{});
        Arrays.sort(uA);

        for (Unit u : uA) {
            if (u.isInter()) {
                genUnit(u);
            }
        }

        for (Unit u : uA) {
            if (u.isMod()) {
                genUnit(u);
            }
        }

        genDocs(xd, null);

        glob.xmlEnd("package");
    }

    // genSpacer
    void genSpacer()
    {
        glob.xmlElem("decl");
        glob.xmlAttr("spacer", true);
        glob.xmlEnd();
    }

    // genUnit
    void genUnit( Unit unit )
    {
        XDoc xd = unit.makeXDoc();

        if (unit.isProxy()) {
            return;
        }

        curUnit = unit;
        if (unit.hasAttr(Attr.A_GlobalNames)) {
            curPre = "";
        }
        else if (unit.hasAttr(Attr.A_Prefix)) {
            curPre = unit.attrString(Attr.A_Prefix) + '_';
        }
        else {
            curPre = unit.getName() + '_';
        }

        glob.xmlElem("unit");
        glob.xmlAttr("name", unit.getName());
        glob.xmlAttr("kind", unit.getXmlTag());
        glob.xmlAttr("nodoc", xd.isNodoc());
        glob.xmlAttr("metaonly", unit.isMeta());
        glob.xmlAttr("summary", mkDocText(xd.getSummary()));

        Unit sup = unit.getSuper();
        if (sup != null && !sup.getQualName().equals("xdc.runtime.IModule")) {
            glob.xmlAttr("inherits", sup.getQualName());
        }
        else {
            sup = null;
        }

        glob.xmlContent();

        glob.xmlElem("synopSpec");
        glob.xmlContent();

        genLine(0, "%1 %2;", kw("package"), mkLink(curPkg.getName(), null, null, curPkg.getName()));
        genLineB();

        String attrs = "";
        String[] aL = new String[] {Attr.A_Gated, Attr.A_GlobalNames, Attr.A_ModuleStartup, Attr.A_Prefix};
        for (String a : aL) {
            if (unit.hasAttr(a)) {
                attrs += span("xdoc-kw3", a + ' ');
            }
        }
        if (attrs.length() > 0) {
            genLine(0, attrs);
        }

        String ms = unit.isMeta() ? kw("metaonly ") : "";
        String is = sup != null ? kw(" inherits ") + mkLink(sup) : "";
        genLine(0, "%1%2 %3%4 {", ms, kw(unit.getXmlTag()), mkIdX(unit), is);
        genLine(0, "}");
        glob.xmlEnd("synopSpec");

        glob.xmlElem("specInfo");
        glob.xmlContent();
        if (attrs.length() > 0) {
            genLine(0, attrs);
        }
        genLine(0, "%1%2 %3 {%4", ms, kw(unit.getXmlTag()), mkIdX(unit), ELIPSIS);
        int tab = 0;
        for (Unit iu : unit.getInherits()) {
            genLineC(tab++, "inherits " + mkLink(iu.getPkgName(), iu.getName(), null, iu.getQualName()), null);
        }
        if (unit.isInst()) {
            genLine(0, "%1:%2", kw("instance"), ELIPSIS);
        }
        glob.xmlEnd("specInfo");

        if (unit.isMod()) {
            glob.xmlElem("synopMeta");
            glob.xmlContent();
            genUseMod(unit);
            glob.xmlEnd("synopMeta");
        }

        if (!unit.isMeta() && (unit.isMod() || unit.isInst())) {
            glob.xmlElem("synopTarg");
            glob.xmlContent();
            genInclude(unit);
            glob.xmlEnd("synopTarg");
        }
        genDocs(xd, null);

        Decl[] dA = unit.getDecls().toArray(new Decl[]{});
        Arrays.sort(dA);
        Decl.Config[] cA = unit.getConfigs().toArray(new Decl.Config[]{});
        Arrays.sort(cA);
        Decl.Fxn[] fA = unit.getFxns().toArray(new Decl.Fxn[]{});
        Arrays.sort(fA);

        /* get "extern" declarations: provides linkage into non-module code */
        Decl.Extern[] extFxnA = getExtFxns(unit).toArray(new Decl.Extern[]{});
        Arrays.sort(extFxnA);
        Decl.Extern[] extVarA = getExtData(unit).toArray(new Decl.Extern[]{});
        Arrays.sort(extVarA);

        boolean hasNoRuntime = unit.hasAttr(Attr.A_NoRuntime);
        if (!unit.isMeta() && !hasNoRuntime) {
            genGrpPRX(dA);
        }
        genGrpCST(dA);          /* module-wide constants: #define, enum ... */
        genGrpMWG(cA, hasNoRuntime);          /* module-wide const data: const int, ... */
        genGrpMWD(extVarA);     /* module-wide mutable data: int, ... */
        genGrpMWF(fA, extFxnA); /* module-wide functions: int Mod_fxn(), ... */
        if (unit.isMod() && !unit.isMeta()) {
            genGrpMWB(unit);    /* module-wide built-ins */
        }

        if (unit.isInst()) {

            glob.xmlElem("group");
            glob.xmlAttr("name", "$instance");
            glob.xmlContent();
                glob.xmlElem("synopSpec");
                glob.xmlContent();
                genLine(0, "%1:", kw("instance"));
                genLineB();
                glob.xmlEnd("synopSpec");
            glob.xmlEnd("group");

            if (!unit.isMeta()) {
                genGrpPIT(unit);
            }
            genGrpPIG(unit, cA);
            genGrpPIC(unit);
            if (unit.isMod() && !unit.isMeta()) {
                genGrpPID(unit);
            }
            genGrpPIF(fA);
            if (sup != null && sup.isInst() && !unit.isMeta()) {
                genGrpPIV(unit);
            }
            if (unit.isMod() && !unit.isMeta()) {
                genGrpPIB(unit);
            }
        }

        glob.xmlEnd("unit");
    }

    // genUseMod
    void genUseMod( Unit u )
    {
        genLine(0, "%1 %2 = %3(%4);",
                kw("var"), u.getName(), span("xdoc-kw3", "xdc.useModule"), span("xdoc-id", "'" + u.getQualName() + "'"));

    }

    // getExtFxns - get just the "extern" function declarations
    List<Decl.Extern> getExtFxns(Unit unit)
    {
        return (getExterns(unit, true));
    }

    // getExtData - get just the "extern" data declarations
    List<Decl.Extern> getExtData(Unit unit)
    {
        return (getExterns(unit, false));
    }

    // getExterns - get all "extern" declarations
    List<Decl.Extern> getExterns(Unit unit, Boolean fxnFlag)
    {
        List<Decl.Extern> list = new ArrayList();
        
        for (Decl d : unit.getDecls()) {
            if (!d.isMeta() && d instanceof Decl.Extern) {
                Decl.Extern x = (Decl.Extern)d;
                if (fxnFlag == x.getTypeCode().startsWith("F")) { 
                    list.add(x);
                }
            }
        }

        return (list);
    }

    // isDefined
    boolean isDefined( String s )
    {
        return s != null && s.length() > 0;
    }

    // isVisible
    boolean isVisible( Decl.Fxn cfxn )
    {
        return cfxn != null && !cfxn.isNodoc();
    }

    // lenDtor
    int lenDtor( Decl d )
    {
        Type t = ((Decl.Signature)d).getType();

        if (t == null) {
            //TODO improve
            return 0;
        }

        String xs = ((Decl.Signature)d).getType().xmlsig();

        Pattern p = Pattern.compile("T\\!([^!]+)\\.([^!]+):([^!]+)\\!");
        Matcher m = p.matcher(xs);
        String res = "";
        int cur = 0;

        while (m.find()) {
            res += xs.substring(cur, m.start());
            res += m.group(2) + '.' + m.group(3);
            cur = m.end();
        }
        res += xs.substring(cur);

        res = res.replaceAll("K\\!([^!]*)\\!", "$1");
        res = res.replace(IDREP, d.getName());

        return res.length();
    }

    // lenInit
    int lenInit( Expr e )
    {
        if (e == null) {
            return -1;
        }

        if (e instanceof Expr.Aggregate) {
            return 0;
        }

        String xs = Expr.toXml(e);

        Pattern p = Pattern.compile("$\\!([^!]+)\\.([^!]+):([^!]+)\\!");
        Matcher m = p.matcher(xs);
        String res = "";
        int cur = 0;

        while (m.find()) {
            res += xs.substring(cur, m.start());
            res += m.group(2) + '.' + m.group(3);
            cur = m.end();
        }
        res += xs.substring(cur);

        res = res.replaceAll("\\$K(\\w+)", "$1");
        res = res.replace("$$", "$");

        return res.length();
    }

    // mkAnchor
    String mkAnchor( String dn )
    {
        return (dn.replaceAll("([A-Z])", ".$1"));
    }

    // mkDocText
    String mkDocText( String line )
    {
        line = line.trim();

        Pattern p = Pattern.compile("\\{@link\\s*(\\S|[^}]*?)\\}");
        Matcher m = p.matcher(line);
        String res = "";
        int cur = 0;

        while (m.find()) {
            res += line.substring(cur, m.start());
            res += mkDocLink(m.group(1).trim());
            cur = m.end();
        }
        res += line.substring(cur);

        return glob.encode(res.replace("@@", "@"));
    }

    // mkDocLink
    /**
     * Given a link specified in the spec, parse it and covert it to an HTML
     * link that should be planted in the generated .html file.
     */
    String mkDocLink( String link )
    {
        int df = link.indexOf("doxy(");
        if (df != -1) {
            String name = link.substring(df);
            String suf = name.substring(name.lastIndexOf(')'));
            name = name.substring(df + 5, name.lastIndexOf(')'));
            int kb = suf.indexOf(' ');
            String alt = (kb == -1 ? name : suf.substring(kb)).trim();
            return "<a class=\"xdoc-linkDoxygen\" href=\"" + name + "\">" + alt + "</a>";
        }

        int kb = link.indexOf(' ');

        String ref = kb == -1 ? link : link.substring(0, kb);
        String alt = (kb == -1 ? ref : link.substring(kb)).trim();

        if (ref.matches("^\\w+://.*$")) {
            return "<a class=\"xdoc-linkExt\" href=\"" + ref + "\" title=\"" + ref + "\">" + alt + "</a>";
        }

        if (ref.indexOf('/') != -1) {
            String rs = ref.startsWith(".") ? ref : curRoot + ref;
            return "<a class=\"xdoc-linkFile\" href=\"" + rs + "\" title=\"" + ref + "\">" + alt + "</a>";
        }

        Pattern p = Pattern.compile("^((\\w|\\$|\\.|:)*)(#((\\w|\\$|-)+)(.*))?$");
        Matcher m = p.matcher(ref);

        if (!m.find()) {
            System.out.printf("can't make link: %s\n", link);
            return link;
        }

        String hs = "";
        String ts = "";
        String dot = ".";
        String path = m.group(1).replace('.', '/').replace(':', '/');

        int km = m.group(1).lastIndexOf(':');

        if (path.length() == 0) {
            hs = curUnit.getName();
            dot = "";
        }
        else if (km > 0) {    // pkg:Mod
            hs = curRoot + path;
            ts = path.replace('/', '.');
        }
        else if (km == 0) {   // :Mod
            hs = path.substring(1);
            ts = path.substring(1);
        }
        else {
            int kl = path.lastIndexOf('/') + 1;
            boolean isUp = Character.isUpperCase(path.charAt(kl));
            if (kl > 0) {   // p.q
                hs = curRoot + path + (isUp ? "" : "/package");
                ts = path.replace('/', '.');
            }
            else if (isUp){  // M
                hs = path;
                ts = path;
            }
            else { // p
                hs = curRoot + path + "/package";
                ts = path.replace('/', '.');
            }
        }

        hs += ".html";

        if (m.group(4) != null) {
            ts += dot + m.group(4);
            hs += '#' + mkAnchor(mkSpecialLink(m.group(4)));
        }

        if (kb == -1) {
            alt = ts + (m.group(6) != null ? m.group(6) : "");
        }
        return "<a class=\"xdoc-link\" href=\"" + hs + "\" title=\"" + ts + "\">" + alt + "</a>";
    }

    // mkDtorC
    String mkDtorC( Decl d ) { return mkDtorC(d, null); }

    String mkDtorC( Decl d, String dn )
    {
        if (dn == null) {
            dn = curPre + d.getName();
        }

        if (!(d instanceof Decl.Signature)) {
            return mkGo(span("xdoc-id", dn));
        }

        Decl.Signature sig = (Decl.Signature)d;
        String res = "";

        String xs = sig.getType().xmlsig();
        xs = xs.replace(" ", "&nbsp;").replace("+", "%2B");
        xs = xs.replaceAll("K\\!([^!]*)\\!", "<span class=\"xdoc-kw2\">$1</span>");
        xs = xs.replace(IDREP, mkGo(span("xdoc-id", dn)));
        xs = repRefsC(xs);

        return xs;
    }

    // mkDtorS
    String mkDtorS( Decl d )
    {
        String pre = !d.isInst() ? curUnit.getName() : d instanceof Decl.Config ? "params" : "inst";
        return  mkGo(pre + '.' + span("xdoc-id", d.getName()));
    }

    // mkDtorX
    String mkDtorX( Decl d )
    {
        Decl.Signature sig = (Decl.Signature)d;

        if (sig.getType() == null) {
            return mkGo((d instanceof Decl.Fxn ? kw("function ") : "") + span("xdoc-id", d.getName()));
        }

        String xs = sig.getType().xmlsig();
        xs = xs.replace(" ", "&nbsp;").replace("+", "%2B");
        xs = xs.replace(IDREP, mkGo(span("xdoc-id", d.getName())));
        xs = xs.replace("K!length!", kw("length"));
        xs = xs.replace("K!string!", kw("string"));
        xs = xs.replaceAll("K\\!([^!]*)\\!", "<span class=\"xdoc-kw2\">$1</span>");
        xs = repRefsX(xs);

        return xs;
    }

    // mkGo
    String mkGo( String s )
    {
        return "<go>" + s + "</go>";
    }

    // mkIdC
    String mkIdC( Node n, String s )
    {
        String pre = n instanceof Decl ? curPre : (n.getName() + '_');
        return mkGo(span("xdoc-id", pre + s));
    }

    // mkIdX
    String mkIdX( Node n ) { return mkIdX(n.getName()); }
    String mkIdX( String s )
    {
        return mkGo(span("xdoc-id", s));
    }

    // mkInit
    String mkInit( String s )
    {
        s = s.replace("<", "&lt;");
        s = s.replace(">", "&gt;");
        s = repRefsI(s);
        s = s.replaceAll("\\$K(\\w+)", "<span class=\"xdoc-kw2\">$1</span>");
        s = s.replace("@@", "@");

        return glob.encode(s);
    }

    // mkInitHd
    String mkInitHd( Expr e ) { return mkInitHd(e, ";"); }

    String mkInitHd( Expr e, String t )
    {
        String res = Expr.toXml(e);

        if (e instanceof Expr.Aggregate) {
            if (!((Expr.Aggregate)e).isEmpty()) {
                return mkInit(res.substring(0, res.indexOf("@+")));
            }
        }

        return mkInit(res) + t;
    }

    // mkLink
    String mkLink( Unit u ) { return mkLink(u.getPkgName(), u.getName(), null, u.getName()); }
    String mkLink( Unit u, Decl d ) { return mkLink(u.getPkgName(), u.getName(), d.getName(), u.getName()); }
    String mkLink( Unit u, String dn ) { return mkLink(u.getPkgName(), u.getName(), dn, u.getName() + '_' + dn); }

    String mkLink( String pn, String mn, String dn, String alt )
    {
        String href = curRoot + pn.replace('.', '/');
        String title = pn;

        if (mn == null) {
            href += "/package.html";
        }
        else {
            href += "/" + mn + ".html";
            title += "." + mn;
            if (dn != null) {
                title += "." + dn;
                href += "#" + mkAnchor(mkSpecialLink(dn));
            }
        }

        return "<a class=\"xdoc-link\" href=\"" + href + "\" title=\"" + title + "\">" + alt + "</a>";
    }

    // mkRtRef
    String mkRtRef( String mn, String dn )
    {
        return mkLink("xdc.runtime", mn, dn, mn + '_' + dn);
    }

    // repRefs
    String repRefsX( String xs ) { return repRefs(xs, false, "T"); }
    String repRefsC( String xs  ) { return repRefs(xs, true, "T"); }
    String repRefsI( String xs ) { return repRefs(xs, false, "\\$"); }

    String repRefs( String xs, boolean isC, String key )
    {
        Pattern p = Pattern.compile(key + "\\!([^!]+)\\.([^!]+):([\\w\\$]+)\\!");
        Matcher m = p.matcher(xs);
        StringBuffer sb = new StringBuffer();
        String res = "";
        int cur = 0;

        while (m.find()) {
            res += xs.substring(cur, m.start());
            res += mkLink(m.group(1), m.group(2), m.group(3), m.group(2) + (isC ? '_' : '.') + m.group(3));
            cur = m.end();
        }

        return res + xs.substring(cur);
    }

    // mkSpecialLink
    String mkSpecialLink( String dn )
    {
        if (dn.matches("Handle|Object|Struct")) {
            return "per-instance_object_types";
        }
        else if (dn.matches("Params|Params_init")) {
            return "per-instance_config_parameters";
        }
        else if (dn.matches("create|construct")) {
            return "per-instance_creation";
        }
        else if (dn.matches("delete|destruct")) {
            return "per-instance_deletion";
        }
        else if (dn.matches("Handle_\\w+Cast\\w+")) {
            return "per-instance_convertors";
        }
        else if (dn.matches("Handle_\\w+|Object_\\w+")) {
            return "per-instance_built-ins";
        }
        else if (dn.matches("Module_\\w+")) {
            return "module-wide_built-ins";
        }
        else {
            return dn;
        }
    }

    // mkType
    String mkType( Decl.Signature ds ) { return mkType(ds, false); }
    String mkType( Decl.Signature ds, boolean isC ) {
            return mkType(isC ? ds.getType() : glob.rawType(ds.getType()), isC, true);
    }
    String mkType( Type t, boolean isC, boolean italics )
    {
        String ts;

        if (t == null) {
           ts = kw2("Any");
        }
        else {
            ts = t.xmlsig();
            ts = ts.replace(" ", "").replace("+", "%2B");
            ts = ts.replace("K!length!", kw("length"));
            ts = ts.replace("K!string!", kw("string"));
            ts = ts.replaceAll("K\\!([^!]*)\\!", "<span class=\"xdoc-kw2\">$1</span>");
            ts = ts.replace(IDREP, "");
            ts = isC ? repRefsC(ts) : repRefsX(ts);
        }
        return !italics ? ts : "<i>" + ts + "</i>";
    }

    // kw
    String kw( String ks )
    {
        return span("xdoc-kw1", ks);
    }

    // kw2
    String kw2( String ks )
    {
        return span("xdoc-kw2", ks);
    }

    // span
    String span( String cls, String s )
    {
        return "<span class=\"" + cls + "\">" + s + "</span>";
    }
}
