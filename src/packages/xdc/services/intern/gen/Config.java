/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008-2020 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
package xdc.services.intern.gen;

import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.Undefined;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.JavaScriptException;

import xdc.services.global.Out;
import xdc.services.global.Path;
import xdc.services.global.Vers;

import xdc.services.spec.Pkg;
import xdc.services.spec.Unit;
import xdc.services.spec.Attr;
import xdc.services.spec.Decl;

import xdc.services.intern.xsr.Extern;
import xdc.services.intern.xsr.Proto;
import xdc.services.intern.xsr.Addr;
import xdc.services.intern.xsr.Template;
import xdc.services.intern.xsr.Global;
import xdc.services.intern.xsr.Member;
import xdc.services.intern.xsr.Ptr;
import xdc.services.intern.xsr.Value;
import xdc.services.intern.xsr.Utils;

import java.io.ByteArrayOutputStream;

import java.util.HashSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Collections;
import java.util.Arrays;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Config
{
    private static final int MOD = 0;
    private static final int INST = 1;
    private static final int STRUCT = 2;

    private static final int DCLMODE = 0;
    private static final int VALMODE = 1;

    private static final int STATIC_POLICY = 0;
    private static final int DELETE_POLICY = 2;

    private static final int IOBJ_BASIC = 0;
    private static final int IOBJ_PROXY = 1;
    private static final int IOBJ_DLGST = 2;
    private static final int IOBJ_DLGPR = 3;

    private static final int L_ENTRY = 0x1;
    private static final int L_EXIT = 0x2;
    private static final int L_LIFECYCLE = 0x4;

    private HashSet<Value.Obj> asmset = new HashSet();
    private HashSet<String> baseset = new HashSet();
    private ArrayList<String> clvec = new ArrayList();
    private ArrayList<String> cltype = new ArrayList();
    private ArrayList<Value.Obj> modvec = new ArrayList();
    private HashMap<String,Value.Obj> modmap = new HashMap<String,Value.Obj>();
    private HashMap<String,Unit> intmap = new HashMap<String,Unit>();
    private HashSet<Value.Obj> modset = new HashSet();
    private HashSet<String> pkgset = new HashSet();
    private HashSet<String> strset = new HashSet();

    /* These two maps hold constants and module states that are assembled in
     * two large structures that are referenced through two pointers.
     * The key for each map is the name of the symbol, while the value is a
     * list of two strings - the type and the value.
     */
    private LinkedHashMap<String,List<String>> romConstFields
        = new LinkedHashMap<String,List<String>>();
    private LinkedHashMap<String,List<String>> romModStates
        = new LinkedHashMap<String,List<String>>();

    /* map package's base directory to its xdc version */
    private HashMap<String,String> pkgVers = new HashMap<String,String>();

    private int mInitCnt = 0;
    private int mode;
    private boolean isRom;

    private Glob glob;
    /* we need 'target' to be global because clink needs it. */
    private Value.Obj target;
    private FileManager fm;
    private Prog progGen;

    Config( Glob glob, Prog progGen )
    {
        this.glob = glob;
        this.progGen = progGen;
    }

    /*
     *  ======== gen ========
     */
    void gen(Value om, Value prog, FileManager out)
        throws JavaScriptException
    {
        glob.mode = Glob.CDLMODE;
        glob.out = out;

        isRom = prog.geti("$$isrom") > 0;

        Value.Obj target = getTargetObject(prog);
        this.target = target;

        fm = out; // aliasing so I can use FM functions without cast 
        fm.switchAll();
        genPrologue();

        /* loop over all modules in the configuration */
        Value modarr = om.getv("$modules");
        for (int i = 0; i < modarr.geti("length"); i++) {
            Value.Obj m = ((Value.Obj)(modarr.getv(i))).getOrig();

            /* skip over modules that are not used */
            if (m.geti("$used") == 0) {
                continue;
            }

            if (m.geti("$hostonly") == 1) {
                continue;
            }

            /* asmset contains all mods that need runtime and
             * are in packages that have not been excluded from gen
             */
            if (!unitSpec(m).needsRuntime()) {
                continue;
            }
            if (m.getv("$package").geti("$$genflg") == 1) {
                asmset.add(m.getOrig());
            }

            if (!isasm(m)) {
                continue;
            }

            /* modvec contains all used modules */
            modvec.add(m);
            modmap.put(m.gets("$name"), m);

            if (m.has("MODULE_STARTUP$", m) && m.geti("MODULE_STARTUP$") == 1){
                mInitCnt++;
            }

            pkgset.add(m.getv("$package").gets("$name"));
        }

        Collections.sort(modvec);

        /* create baseset with interfaces that are inherited by any of the
         * used modules.
         */
        for (Value.Obj m : modvec) {
            Unit u = unitSpec(m);
            if (u.isMeta() || !isasm(m) || u.isProxy()) {
                continue;
            }
            if (!u.isHeir()) {
                continue;
            }
            /* If a function pointer table is disabled in a module, we don't
             * need to be able to follow its inheritance chain. A longer
             * description of the purpose of 'fxntab' is in Types.xdc.
             */
            if (commonGeti(m, "fxntab") != 1) {
                continue;
            }
            Unit iunit = u.getSuper();
            while (iunit != null && baseset.add(iunit.getQualName())) {
                intmap.put(iunit.getQualName(), iunit);
                iunit = iunit.getSuper();
            }
        }

        /* includes */
        glob.genTitle("MODULE INCLUDES");
        for (Value.Obj m : modvec) {
            genInclude(m);
        }

        /* top-level pragmas */
        genPragmas();

        /* loop over all interfaces in the configuration to see if any of them
         * was used but it's not in baseset. If that's the case we have to add
         * their includes and add them to baseset, so we can generate their
         * BASE__C constants later.
         */
        Value pkgarr = om.getv("$packages");
        for (int i = 0; i < pkgarr.geti("length"); i++) {
            Value intarr = ((Value.Obj)(pkgarr.getv(i))).getv("$interfaces");
            for (int j = 0; j < intarr.geti("length"); j++) {
                Value.Obj interj = (Value.Obj)(intarr.getv(j));
                if (interj.has("$used", null) && interj.geti("$used") != 0) {
                    Unit iu = unitSpec(interj);
                    if (!iu.isMeta() && iu != null
                        && baseset.add(iu.getQualName())) {
                        intmap.put(iu.getQualName(), iu);
                        genInclude(interj);
                    }
                }
            }
        }

        /* internals */
        for (Value.Obj m : modvec) {
            if (fm.switchMod(m) == FileManager.SEPARATE_FILE) {
                genInternals(m);
                /* modset keeps track of the modules whose typedefs and externs
                 * for instances are already generated so as to not generate
                 * them again in the same file. We clear it between the files
                 * because each of the files must be selfsufficient.
                 */
                modset.clear();
            }
        }
        fm.switchCommon();

        if (isRom) {
            /* The constants and the module states that are kept in the two
             * large structures are read from Program.$$rom.romStruct.
             */
            Scriptable romobj = (Scriptable)(prog.find("$$rom"));
            NativeArray members =
                (NativeArray)(romobj.get("romStruct", romobj));
            long size = members.getLength();
            for (int i = 0; i < size; i++) {
                String name = (String)(members.get(i, members));
                if (name.matches(".*Module__state__V")) {
                    romModStates.put(name, new ArrayList<String>(2));
                }
                else {
                    romConstFields.put(name, new ArrayList<String>(2));
                }
            }
        }

        /* We also need to generate internals for the common C file, and all
         * modules need to contribute to that one, even the modules with the
         * separate C files. Their types and definitions might be used in the
         * common C file.
         */
        for (Value.Obj m : modvec) {
            genInternals(m);
        }

        /* inherits */
        glob.genTitle("INHERITS");
        for (String iface : baseset) {
            genInherits(iface);
        }

        /* vtable */
        for (Value.Obj m : modvec) {
            genFxnTabV(m);
        }

        mode = DCLMODE;

        /* declarations */
        for (Value.Obj m : modvec) {
            genTitle(m, "DECLARATIONS");
            genMod(m);
        }

        /* embedded object offsets */
        for (Value.Obj m : modvec) {
            genOffsets(m);
        }

        /* templates
         *
         * We scan all modules again, rather than use modvec, to
         * allow meta-only modules to contribute to the config.c file
         */
        for (int i = 0; i < modarr.geti("length"); i++) {
            Value.Obj m = ((Value.Obj)(modarr.getv(i))).getOrig();
            if (m.geti("$used") != 0) {
                genTemplate(m);
            }
        }

        mode = VALMODE;
        strset = new HashSet();

        /* initializers */
        for (Value.Obj m : modvec) {
            genTitle(m, "INITIALIZERS");
            genMod(m);
        }

        /* function stubs */
        for (Value.Obj m : modvec) {
            fm.switchMod(m);
            genFxnStubs(m);
        }

        /* proxy bodies */
        for (Value.Obj m : modvec) {
            fm.switchMod(m);
            genProxy(m);
        }
        fm.switchCommon();

        /* create support */
        for (Value.Obj m : modvec) {
            genCreate(m);
        }

        /* ROM structures */
        if (isRom) {
            glob.genTitle("ROM STRUCTURES");
            genRomStruct();
        }

        /* interface system functions */
        for (String istr : baseset) {
            /* 'baseset' already contains a set of interfaces inherited by at
             * least one module with a function pointer table. We just need to
             * go through the list and generate create() and delete() functions.
             */
            Value.Obj inter = (Value.Obj)(om.getv(istr));
            genSysInherits(inter);
        }

        /* system functions */
        for (Value.Obj m : modvec) {
            fm.switchMod(m);
            genSysFxns(m);
        }
        fm.switchCommon();

        /* pragmas */
        for (Value.Obj m : modvec) {
            genPragmas(m, target);
        }

        genPostInit();

        glob.genTitle("PROGRAM GLOBALS");
        genGlobals(prog, false);

        Hashtable<String,String> aliases = new Hashtable<String,String>();
        for (String pname : pkgset) {
            Value.Obj pkg = ((Value.Obj)(om.getv(pname))).getOrig();
            getPkgAliases(pkg, modmap, aliases);
        }
        prog.bind("$$aliases", aliases);
    }

    /*
     *  ======== genAgg ========
     */
    private void genAgg(Value.Obj agg)
    {
        if (agg.geti("$hostonly") == 1) {
            return;
        }

        String ts = agg.getProto().tname();
        int ot = otype(ts);

        if (ot != STRUCT) {
            return;             /// FOR NOW!!!!!
        }

        Value.Obj obj = (Value.Obj)agg.getv("$object");

        if (obj == null && ts.endsWith(".Object")) {
            /* Protos whose name ends with .Object are embedded objects. These
             * are the types that are passed to the constructor functions. They
             * don't have an instance header.
             */
            xdc.services.intern.xsr.Err.exit("an embedded object of the "
                + "following type is not initialized: " + ts);
            return;
        }

        glob.out.printf("{\n%+");

        if (obj != null) {
            Value.Obj mod = (Value.Obj)agg.getv("$module");
            genInstObj(obj, mod, glob.mkCname(mod.gets("$name")));
            glob.out.printf("%-%t");
            return;
        }

        if (ts.endsWith(".Params")) {
            String cs = ts.replace('.', '_');
            glob.out.printf("%tsizeof (%1), /* __size */\n", cs);
        }
        genFlds(agg);
        glob.out.printf("%-%t");
    }

    /*
     *  ======== genArr ========
     */
    private void genArr(Value.Arr arr, String aname, Value.Obj obj,
        String path, boolean isConst)
    {
        if (obj.geti("$hostonly") == 1 || aname.startsWith("$")) {
            return;
        }

        String ts = obj.getProto().tname();
        int k = ts.lastIndexOf('.');
        String pre = ts.substring(0, k);

        genArr(arr, glob.mkCname(pre), aname, "", path, isConst);
    }

    private void genArr(Value.Arr arr, String cname, String an, String xs,
        String path, boolean isConst )
    {
        Proto.Arr parr = arr.getProto();
        Proto base = parr.getBase();
        int len = arr.geti("length");

        if (base instanceof Proto.Pred && !arr.isVirgin()) {
            for (int i = 0; i < len; i++) {
                Object oval = arr.get(i, arr);
                String ps = path + '[' + String.valueOf(i) + ']';
                if (base instanceof Proto.Str) {
                    genPreds((Value.Obj)oval, ps, xs + '_' + i, isConst);
                }
                else if (oval instanceof Value.Arr) {
                    genArr((Value.Arr)oval, cname, an, xs + '_' + i, ps,
                        isConst);
                }
                else if (oval instanceof Extern) {
                    genExt((Extern)oval);
                }
                else if (oval instanceof Value.Obj) {
                    genStruct((Value.Obj)oval, isConst);
                }
            }
        }

        if (len == 0 || parr.getDim() > 0) {
            return;
        }

        arr.setCtype("__T" + parr.depth() + "_" + cname + an);

        glob.genTitleD("--> " + aname(arr));
        if (mode == VALMODE && isConst && !arr.isVirgin()
            && arr.getMemSect() == null) {
            clink(aname(arr), arr.getCtype());
        }

        glob.out.printf("%4%1 %2[%3]",
                arr.getCtype(),
                aname(arr),
                Integer.toString(len),
                isConst ? "const " : "");

        if (mode == VALMODE) {
            genArrVals(arr, an, xs, true);
        }
        glob.out.printf(";\n");

        if (mode == DCLMODE) {
            String sectionPart = "";
            String alignPart = "";
            String comma = " ";
            if (arr.getMemSect() != null) {
                sectionPart = "section(\"" + arr.getMemSect() + "\")";
                comma = "";
            }
            if (arr.getMemAlign() != 0) {
                alignPart = "aligned(" + arr.getMemAlign() + ")";
                if (comma.equals(" ")) {
                    comma = "";
                }
                else {
                    comma = ", ";
                }
            }
            if (sectionPart.isEmpty() && alignPart.isEmpty()) {
                return;
            }
            /* we don't output section directives for MacOS (see the top of
             * the generated config C file for relevant directives) because
             * its section strings must be of the form 
             * <segment_name>,<section_name>,
             * where <segment_name> is "__DATA", "__TEXT", ... _and_
             * the section name is limited to just 16 characters.  So, while
             * it's possible to support the section directive by devising some
             * scheme for compressing section name strings to fit in 16
             * characters, the effort is probably wasted since the MacOS
             * targets are only used to create host apps (which don't need
             * to place data via linker scripts).
             *
             * To get a list of segment section names from a MacOS obj/exe,
             * use the MacOS command line tool "otool":
             *    otool -l hello.x86_64M | egrep "sectname|segname" | sort -u
             */
            glob.out.printf("#if !(defined(__MACH__) && defined(__APPLE__))\n");
            glob.out.printf("%4%1 %2[%3]", arr.getCtype(), aname(arr),
                Integer.toString(len), isConst ? "const " : "");
            glob.out.printf(" __attribute__ ((%1%2%3));\n", sectionPart, comma,
                alignPart);
            glob.out.printf("#endif\n");
        }
    }

    /*
     *  ======== genArrVals ========
     */
    private void genArrVals(Value.Arr arr, String an, String xs)
    {
        genArrVals(arr, an, xs, false);
    }

    private void genArrVals(Value.Arr arr, String an, String xs, boolean top)
    {
        if (arr.isVirgin()) {
            return;
        }

        int len = arr.geti("length");

        glob.out.printf("%1{\n%+", top ? " = " : "");
        for (int i = 0; i < len; i++) {
            Object oval = arr.get(i, arr);
            if (undef(oval)) {
                arr.error("the initial value of element [" + i
                    + "] of this array is undefined. All elements of this array must be given a specific initial value.");
                continue;
            }
            glob.out.printf("%t");
            genVal(arr.getProto().getBase(), oval, null);
            glob.out.printf(",  /* %1 */\n", "[" + i + "]");
        }
        glob.out.printf("%-%1", top ? "}" : "");
    }

    /*
     *  ======== genCreate ========
     */
    void genCreate( Value.Obj mod )
    {
        Unit unit = unitSpec(mod);

        if (!unit.isInst() || unit.isProxy()) {
            return;
        }

        genTitle(mod, "OBJECT DESCRIPTOR");

        Value.Obj vref = (Value.Obj)commonGetv(mod, "instanceHeap");
        String instHeap = vref != null ? valToStr(vref) : null;

        Unit dlg = unit.delegatesTo();
        if (dlg != null && dlg.isInst() && !unit.isSized()) {
            String icn = glob.mkCname(dlg.getQualName());
            fm.switchMod(mod);
            glob.out.printf(
                "extern %1Handle %2Object_delegate( %2Object* obj )", icn, 
                glob.cname);
            glob.out.printf(
                " { return ((%1Handle)&((%2Object__*)obj)->__deleg); }\n",
                icn, glob.cname);
            fm.switchCommon();
        }

        String ns = glob.cname + "Object__DESC__C";
        String cs = "xdc_runtime_Core_";

        glob.genTitleD("Object__DESC__C");
        glob.out.printf(
            "%ttypedef struct { %1Object2__ s0; char c; } %1__S1;\n",
            glob.cname);

        /* If this const is to be a part of a large structure referenced
         * by ROM, its global name will not exist. However, ROM builds can
         * include code that references such consts, so we define global names
         * in the linker command files as offsets within the ROM structures.
         * We need to record the type and value for this consts to be able to
         * create a type definition, and to lay down the initializer for the
         * structure.
         */
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (isRom && romConstFields.containsKey(ns)) {
            // switching output to a string
            fm.switchString(baos);
        }

        clink(ns, "xdc_runtime_Core_ObjDesc");
        glob.out.printf("__FAR__ const %1ObjDesc %2 = {\n%+", cs, ns);

        /* now the value for Object__DESC__C is generated */
        if (unit.isHeir()) {
            if (commonGeti(mod, "fxntab") == 1) {
                glob.out.printf(
                    "%t(xdc_CPtr)&%1Module__FXNS__C, /* fxnTab */\n",
                    glob.cname);
            }
            else {
                glob.out.printf("%t(xdc_CPtr)NULL, /* fxnTab */\n");

            }
        }
        else {
            glob.out.printf("%t(xdc_CPtr)-1, /* fxnTab */\n");
        }

        /* If there are no dynamically created instances, Module__root__V may
         * not exist, or if it does, it contains only the diagnostic mask.
         */
        if (commonGeti(mod, "memoryPolicy") != STATIC_POLICY) {
            glob.out.printf("%t&%1Module__root__V.link, /* modLink */\n",
                glob.cname);
        }
        else {
            glob.out.printf("%t(xdc_Ptr)NULL, /* modLink */\n");
        }
        glob.out.printf("%tsizeof(%1__S1) - sizeof(%1Object2__), /* objAlign */\n", glob.cname);
        glob.out.printf("%t%1, /* objHeap */\n", instHeap != null ? instHeap : "0");
        if (commonGeti(mod, "namedInstance") == 1) {
            glob.out.printf("%toffsetof(%1Object__, __name), /* objName */\n", glob.cname);
        }
        else {
            glob.out.printf("%t0, /* objName */\n");
        }

        glob.out.printf("%tsizeof(%1Object2__), /* objSize */\n", glob.cname);
        glob.out.printf("%t(xdc_CPtr)&%1Object__PARAMS__C, /* prmsInit */\n", glob.cname);
        glob.out.printf("%tsizeof(%1Params), /* prmsSize */\n", glob.cname);
        glob.out.printf("%-};\n");

        if (isRom && romConstFields.containsKey(ns)) {
            /* From the output that would normally be send to the config C file
             * we will determine the type and the value for the constant.
             */
            List<String> values = romConstFields.get(ns);
            values.add("xdc_runtime_Core_ObjDesc");

            /* We only need the part between '=' and ';' for value */
            String whole = baos.toString();

            Pattern pat = Pattern.compile(
                "^.*const xdc_runtime_Core_ObjDesc.*=(.*);$",
                Pattern.DOTALL | Pattern.MULTILINE);
            Matcher m = pat.matcher(whole);
            m.find();
            values.add(m.group(1).trim());
            romConstFields.get(ns).add(baos.toString());
            fm.switchCommon();
        }
    }

    /*
     *  ======== genCreateSig ========
     */
    private void genCreateSig()
    {
        glob.out.printf("%2Ptr %1Object__create__S(", glob.cname, "xdc_");
        glob.out.printf("%+\n%txdc_CPtr req_args,\n%tconst xdc_UChar *"
            + "paramsPtr,\n%txdc_SizeT prm_size,\n%t%1%-)\n{\n%+",
            Glob.ERRARG);
    }

    /*
     *  ======== genError ========
     */
    private void genError( Unit unit, String msg )
    {
        glob.out.printf("%t%1_raiseX(NULL, %2Module__id__C, NULL, 0, %1_E_generic, (xdc_IArg)\"%3\", 0);\n",
                "xdc_runtime_Error", glob.cname, msg);
    }

    /*
     *  ======== genExt ========
     *  This function generates an extern declaration. The actual usage of the
     *  Extern instance is generated somewhere else.
     */
    private void genExt(Extern ext)
    {
        if (mode == DCLMODE && ext.isRaw()
            && ext.getName().matches("\\s*[a-zA-Z_]\\w*")) {

            glob.genTitleD("--> " + ext.getName());
            if (ext.isFxnT()) {
                if (ext.getSig() == null) {
                    glob.out.printf("extern void %1();\n", ext.getName());
                }
                else {
                    glob.out.printf("extern %1;\n",
                        ext.getSig().replace("(*)", " " + ext.getName()));
                }
            }
            else {
                if (ext.getSig() == null) {
                    glob.out.printf("extern void* %1;\n", ext.getName());
                }
                else {
                    glob.out.printf("extern %1 %2;\n", ext.getSig(), ext.getName());
                }
            }
        }
    }

    /*
     *  ======== genFlds ========
     *
     *  This function goes through the fields of a structure represented by
     *  a Value.Obj, and generates the values in the C definition of the
     *  structure. The actual work is done by genVal.
     */
    private void genFlds(Value.Obj vobj)
    {
        for (Member fld : vobj.getProto().allFlds()) {
            if (fld.isMeta() || fld.getName().startsWith("$")) {
                continue;
            }
            Object fldval = vobj.get(fld.getName(), vobj);
            if (undef(fldval)) {
                vobj.error("'" + fld.getName() + "' is undefined");
                continue;
            }
            glob.out.printf("%t");
            genVal(fld.getProto(), fldval, null);
            glob.out.printf(",  /* %1 */\n", fld.getName());
        }
    }

    /*
     *  ======== genFxnBody ========
     *  Generate a  __E function that delagates to the "real" function.
     *
     *      __E calls either __F or __PATCH based on a #define of __PATCH
     *
     *      __PATCH is defined in Code.java when a module's $$patchsyms names
     *      the function to be patched.
     *
     *  var-arg functions are generated twice:
     *     first we generate a fixed arg stub that takes a va_list arg
     *     the second uses "..." and simply calls the first
     *
     *  caller                   *.c                callee
     *  ----------------------   -----------------  ---------
     *  foo(...) := foo__E(...)  foo__E(...)
     *                            |
     *                            +->foo_va__F(va)  foo_va(va) := foo_va__F(va)
     *
     *  ????                     foo_va__E(va)
     *                            |
     *                            +->foo_va__F(va)
     *
     *  The vargs parameter is set to false for the first pass and true for
     *  the second pass but only for vararg functions.
     */
    private void genFxnBody(Unit unit, Decl.Fxn fxn, int mask, String patch, boolean vargs)
    {
        String ls = "xdc_runtime_Log_";

        boolean lflg = fxn.isLoggable();

        String prefix = glob.cname + fxn.getName();
        String en = prefix + (fxn.isVarg() && !vargs ? "_va" : "") + "__E";
        String fn = prefix + (fxn.isVarg() ? "_va" : "") + "__F";

        boolean ret = !(glob.isVoid(fxn));

        if (patch != null) {
            fn = patch;
        }

        /* generate the __E declaration */

        String dn = fxn.getName() + (fxn.isVarg() && !vargs ? "_va" : "") + "__E";
        glob.genTitleD(dn);

        glob.genType(fxn.getType(), Glob.Type$TYP, en);
        glob.out.print("(");

        String sp = " ";
        if (fxn.isInst()) {
            glob.out.printf("%1Handle __inst", glob.cname);
            sp = ", ";
        }
        glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, sp);
        glob.out.printf("%1)\n{\n%+",
            vargs ? ", ..." : fxn.isVarg() ? ", va_list arg__va" : "");

        /* start generating the function body */

        /* if there is a return value, declare __ret to hold it */
        if (ret && (vargs || (lflg && (mask & L_EXIT) != 0))) {
            glob.out.tab();
            glob.genType(fxn.getType(), Glob.Type$TYP, "retval;\n");
            skip();
        }

        /* if it's a vararg function, declare a va_list __va and "start" it */
        if (vargs) {
            Decl.Arg lastArg = fxn.getArgs().get(fxn.getArgs().size() - 1);
            glob.out.printf("%tva_list arg__va;\n");
            glob.out.printf("%t(void)va_start(arg__va, %1);\n",
                lastArg.getName());
        }

        // TODO factor into common genLog(); symbolize logging masks

        /* generate entry trace */
        if (!vargs && lflg && (mask & L_ENTRY) != 0) {
            int argc = fxn.isInst() ? 1 : 0;
            for (Decl.Arg arg : fxn.getArgs()) {
                if (glob.fmtSpec(arg).length() == 0) {
                    break;
                }
                argc++;
            }
            if (argc > Glob.LOGARGS) {
                argc = Glob.LOGARGS;
            }
            glob.out.printf("%t%3write%4(%1%2__ENTRY_EVT",
                glob.cname, fxn.getName(), ls, argc);
            if (fxn.isInst()) {
                glob.out.printf(", (xdc_IArg)__inst");
                --argc;
            }
            for (int i = 0; i < argc; i++) {
                glob.out.printf(", (xdc_IArg)%1", fxn.getArgs().get(i).getName());
            }
            glob.out.printf(");\n");
        }

        /* call with exit trace */
        if (lflg && (mask & L_EXIT) != 0) {

            /* call fn */
            glob.out.printf("%t%1%2(", ret ? "retval = " : "", fn);
            sp = "";
            if (fxn.isInst()) {
                glob.out.printf("(void*)__inst");
                sp = ", ";
            }
            glob.genArgNames(fxn.getArgs(), sp);
            glob.out.printf("%1);\n", fxn.isVarg() ? ", arg__va" : "");

            /* "end" any varargs */
            if (vargs) {
                glob.out.printf("%tva_end(arg__va);\n");
            }

            /* generate Log exit event with the fn return value */
            glob.out.printf("%t%3write1(%1%2__EXIT_EVT, %4);\n",
                    glob.cname, fxn.getName(), ls,
                    ret ? "(xdc_IArg)retval" : "0");

            if (ret) {
                glob.out.printf("\n%treturn retval;\n");
            }
        }

        /* call fn without exit trace */
        else {

            glob.out.printf("%t%1%2(",
                (vargs && ret) ? "retval = " : ret ? "return " : "", fn);
            sp = "";
            if (fxn.isInst()) {
                glob.out.printf("(void*)__inst");
                sp = ", ";
            }
            glob.genArgNames(fxn.getArgs(), sp);
            glob.out.printf("%1);\n", fxn.isVarg() ? ", arg__va" : "");

            /* "end" any varargs */
            if (vargs) {
                glob.out.printf("\n%tva_end(arg__va);\n");
                if (ret) {
                    glob.out.printf("%treturn retval;\n");
                }
            }
        }

        /* close the body of __E */
        glob.out.printf("}\n%-");
    }

    /*
     *  ======== genFxnDeleg ========
     */
    private void genFxnDeleg( Unit unit, Decl.Fxn fxn )
    {
        String en = glob.cname + fxn.getName() + "__E";
        String dn = glob.mkCname(unit.delegatesTo().getQualName()) + fxn.getName() + "__E";

        String ls = "xdc_runtime_Log";

        boolean ret = !(glob.isVoid(fxn));

        glob.genType(fxn.getType(), Glob.Type$TYP, en);
        glob.out.print("(");

        String sp = " ";
        if (fxn.isInst()) {
            glob.out.printf(" %1Handle __inst", glob.cname);
            sp = ", ";
        }
        glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, sp);
        glob.out.printf(" ) {\n%+");

        if (ret) {
            glob.out.tab();
            glob.genType(fxn.getType(), Glob.Type$TYP, "__ret;\n");
        }

        glob.out.printf("%t%1%2(", !glob.isVoid(fxn) ? "__ret = " : "", dn);
        sp = "";
        if (fxn.isInst()) {
            glob.out.printf("%1Object_delegate((%1Object*)__inst)", glob.cname);
            sp = ", ";
        }
        glob.genArgNames(fxn.getArgs(), sp);
        glob.out.printf("%1);\n", fxn.isVarg() ? ", __va" : "");

        if (ret) {
            glob.out.printf("%treturn __ret;\n");
        }

        glob.out.printf("}\n%-");
    }

    /*
     *  ======== genFxnStubs ========
     *  Generate the __E function stubs for functions in unit.
     */
    private void genFxnStubs(Value.Obj mod)
    {
        Unit unit = unitSpec(mod);

        if (unit.isProxy()) {
            return;
        }

        HashMap<String,String> pmap = null;
        if (mod.has("$$patchsyms", null)) {
            pmap = new HashMap();
            String[] psarr = mod.gets("$$patchsyms").split(";");
            for (String ps : psarr) {
                String[] sa = ps.split("=");
                pmap.put(sa[0], sa[1]);
            }
        }

        boolean first = true;

        int mask = commonGetv(mod, "logger") != null ?
                   getDiagsIncluded(mod) : 0;

        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isMeta() || fxn.isSys() || fxn.attrBool(Attr.A_Macro)
                || fxn.attrBool(Attr.A_DirectCall)) {
                continue;
            }

            String patch = null;
            if (pmap != null && ((patch = pmap.get(fxn.getName())) == null)) {
                continue;
            }

            if (first) {
                genTitle(mod, "FUNCTION STUBS");
                if (mask != 0) {
                    if (isRom) {
                        glob.genModuleDefines();
                    }
                    else {
                        glob.genModuleDefines2();
                    }
                    genModLog(mod);
                }
                first = false;
            }
            if ((unit.delegatesTo() != null && fxn.getParent() != unit)) {
                genFxnDeleg(unit, fxn);
            }
            else {
                /* vararg functions need to be generated twice:
                 *   first we generate a fix arg stub that takes a va_list arg
                 *   and calls the va_list implementation
                 *
                 *   the second uses "..." converts to va_list and also calls
                 *   the va_list implementation
                 */

                genFxnBody(unit, fxn, mask, patch, false);
                if (fxn.isVarg()) {
                    genFxnBody(unit, fxn, mask, patch, true);
                }
            }
        }
    }

    /*
     *  ======== genFxnTabV ========
     *
     *  This function generates function pointer tables, which are the actual
     *  module representations at runtime.
     */
    void genFxnTabV(Value.Obj mod)
    {
        Unit unit = unitSpec(mod);

        if (unit.isProxy() || !isasm(mod)) {
            return;
        }
        if (!unit.isHeir()) {
            return;
        }
        if (commonGeti(mod, "fxntab") != 1) {
            return;
        }

        genTitle(mod, "VTABLE");

        String tn = "Module__FXNS__C";
        String cn = glob.mkCname(unit.getQualName());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (isRom && romConstFields.containsKey(cn + tn)) {
            fm.switchString(baos);
        }

        glob.genTitleD(tn);
        clink(cn + tn, cn + "Fxns__");
        glob.out.printf("const %1Fxns__ %1%2 = {\n%+", cn, tn);

        glob.out.printf("%t");
        if (isBefore350(mod)) {
            /* With 3.50, the parameter 'base' is of type const Base *, while
             * older modules declared it as Base *, so we have to cast away
             * the const qualifier for the value assigned to 'base'.
             */
            glob.out.printf("(void *)");
        }
        glob.genBase(unit.getSuper());
        glob.out.printf(", /* __base */\n");
        glob.out.printf("%t&%1%2.__sfxns, /* __sysp */\n", cn, tn);

        for (Decl.Fxn fxn : unit.getAllFxns()) {
            if (fxn.isMeta() || fxn.isSys() || fxn.overrides() != null) {
                continue;
            }
            if (unit.isMod() && fxn.getParent().isMod()) {
                continue;
            }
            glob.out.printf("%t%1%2%3,\n", cn, fxn.getName(), fxn.isVarg() ? "_va" : "__E");
        }

        glob.out.printf("%t{\n%+");

        int pol = commonGeti(mod, "memoryPolicy");

        if (unit.getCreator() != null && pol != STATIC_POLICY) {
            glob.out.printf("%t%1Object__create__S,\n", cn);
        }
        else {
            glob.out.printf("%tNULL, /* __create */\n");
        }

        if (unit.isInst() && pol == DELETE_POLICY) {
            glob.out.printf("%t%1Object__delete__S,\n", cn);
            glob.out.printf("%t%1Handle__label__S,\n", cn);
        }
        else {
            glob.out.printf("%tNULL, /* __delete */\n");
            glob.out.printf("%tNULL, /* __label */\n");
        }

        glob.out.printf("%t%1, /* __mid */\n", String.format("0x%x", mod.geti("Module__id")));
        glob.out.printf("%-%t} /* __sfxns */\n");
        glob.out.printf("%-%t};\n");

        if (isRom && romConstFields.containsKey(cn + tn)) {
            List<String> values = romConstFields.get(cn + tn);
            values.add(cn + "Fxns__");

            /* We only need the part between '=' and ';' for value */
            String whole = baos.toString();

            Pattern pat = Pattern.compile("const.*Fxns__ .*=(.*);$",
                Pattern.DOTALL | Pattern.MULTILINE);
            Matcher m = pat.matcher(whole);
            m.find();
            values.add(m.group(1).trim());
            fm.switchCommon();
        }
    }

    /*
     *  ======== genGlobals ========
     */
    void genGlobals(Value prog, Out out)
    {
        glob.mode = Glob.CDLMODE;
        glob.out = out;

        genGlobals(prog, true);

    }

    void genGlobals(Value prog, boolean hdr)
    {
        if (hdr) {
            glob.genWarning();
            skip();
            glob.out.printf("#include <xdc/std.h>\n\n");
        }

        Value gmap = prog.getv("global");
        String sect = prog.gets("globalSection");
        Object[] keys = gmap.getIds();

        for (int i = 0; i < keys.length; i++) {
            String errMsg = null;
            String vname = keys[i].toString();
            Object oval = gmap.get(vname, gmap);
            if (oval instanceof Value.Obj) {
                Value.Obj vobj = ((Value.Obj)oval);
                String tname = vobj.getProto().tname();
                if (otype(tname) == INST) {
                    String qn = tname.substring(0, tname.lastIndexOf('.'));
                    String cs = qn.replace('.', '_');
                    String ts = cs + "_Handle";
                    if (hdr) {
                        glob.out.printf("#include <%1.h>\n", glob.mkFname(qn));
                        glob.out.printf("extern const %1 %2;\n", ts, vname);
                    }
                    else {
                        if (sect != null) {
                            glob.out.printf("#ifdef __ti__sect\n");
                            glob.out.printf("%+%t#pragma DATA_SECTION(%1, \"%2\");\n%-",
                                vname, sect);
                            glob.out.printf("#endif\n");
                        }
                        glob.out.printf("%+%t__attribute__ ((used))\n%-");
                        glob.out.printf("const %1 %2 = (%1)(%3);\n", ts, vname,
                            valToStr(vobj.getOrig()));
                    }
                }
                else {
                    errMsg = "Program.global." + vname
                        + " (of type " + oval.getClass().toString()
                        + "): not an Instance";
                }
            }
            // TODO add section placement for Value.Arr and Ptr externs
            else if (oval instanceof Value.Arr) {
                Value.Arr arr = (Value.Arr)oval;
                if (hdr) {
                    String qn = val2mod(arr);
                    glob.out.printf("#include <%1.h>\n", glob.mkFname(qn));
                    glob.out.printf("extern const %1* %2;\n", arr.getCtype(), vname);
                }
                else {
                    glob.out.printf("const %1* %2 = %3;\n", arr.getCtype(), vname, aname(arr));
                }
            }
            else if (oval instanceof java.lang.CharSequence) {
                if (hdr) {
                    glob.out.printf("#define %1 \"%2\"\n", vname,
                                    oval.toString());
                }
            }
            else if (oval instanceof java.lang.Boolean) {
                if (hdr) {
                    glob.out.printf("#define %1 %2\n", vname,
                        ((java.lang.Boolean)oval).booleanValue() ? "1" : "0");
                }
            }
            else if (oval instanceof java.lang.Number) {
                if (hdr) {
                    glob.out.printf("#define %1 %2\n", vname,
                        Proto.Elm.numval((Number)oval));
                }
            }
            else if (oval instanceof Ptr) {
                if (hdr) {
                    glob.out.printf("extern const void* %1;\n", vname);
                }
                else {
                    glob.out.printf("const void* %1 = %2;\n", vname, ptrToStr(oval, ""));
                }
            }
            else if (oval == null) {
                if (hdr) {
                    glob.out.printf("#define %1 NULL\n", vname);
                }
            }
            else if (oval instanceof Undefined) {
                ; /* ignore undefined names; should we undefined them? */
            }
            else {
                errMsg = "Program.global." + vname
                    + " (of type " + oval.getClass().toString()
                    + " ) = " + oval
                    + ": unsupported generation type";
            }

            if (errMsg != null) {
                xdc.services.intern.xsr.Err.exit(errMsg);
                return;
            }

            glob.out.printf("\n");
        }
    }

    /*
     *  ======== genInclude ========
     */
    private void genInclude(Value.Obj mod)
    {
        Unit u = unitSpec(mod);

        if (u.isProxy()) {
            return;
        }

        glob.out.printf("#include <%1.h>\n", glob.mkFname(u.getQualName()));
    }

    /*
     *  ======== genInherits ========
     *
     *  This function generates pointers that allows us to follow inheritance
     *  hierarchy at runtime.
     *
     */
    private void genInherits(String ifName)
    {
        String cname = glob.mkCname(ifName);
        String iname = glob.mkIname(ifName);

        String bs = "Interface__BASE__C";

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (isRom && romConstFields.containsKey(cname + bs)) {
            fm.switchString(baos);
        }

        clink(cname + bs, "xdc_runtime_Types_Base");

        glob.out.printf("__FAR__ const xdc_runtime_Types_Base %1%2 = {", cname,
            bs);
        glob.genBase(intmap.get(ifName).getSuper());
        glob.out.printf("};\n\n");

        if (isRom && romConstFields.containsKey(cname + bs)) {
            List<String> values = romConstFields.get(cname + bs);
            values.add("xdc_runtime_Types_Base");

            /* We only need the part between '=' and ';' for value */
            String whole = baos.toString();

            Pattern pat = Pattern.compile(
                "^.*const xdc_runtime_Types_Base.*=(.*);$",
                Pattern.DOTALL | Pattern.MULTILINE);
            Matcher m = pat.matcher(whole);
            m.find();

            values.add(m.group(1).trim());
            fm.switchCommon();
        }
    }

    /*
     *  ======== genInstDecls ========
     *
     *  This function generates the types Mod_Object__ and Mod_Object2__, which
     *  are used for Mod_Object__table__V for static instances, and for dynamic
     *  instances linked in Mod_Module__root__V.link.
     */
    private void genInstDecls(Value.Obj mod, Unit u)
    {
        String cname = glob.mkCname(u.getQualName());

        if (mod == null || !modset.add(mod)) {
            glob.genTitleD("<-- " + cname + "Object");
            return;
        }

        for (Unit ud : u.getEmbeds()) {
            if (ud != u && !ud.isMeta() && ud.isInst() && ud.isMod()) {
                genInstDecls(modmap.get(ud.getQualName()), ud);
            }
        }

        int kind = iobjKind(mod);
        String pre = glob.curUnit == u ? "" : ("@@@ " + cname);

        if (kind == IOBJ_BASIC || kind == IOBJ_DLGST) {
            glob.genTitleD(pre + "Object__");
            glob.out.printf("%ttypedef struct %1Object__ {%+\n", cname);
            glob.genStateFlds(u, true);
            if (commonGeti(mod, "namedInstance") == 1) {
                glob.out.printf("%txdc_runtime_Types_CordAddr __name;\n");
            }
            glob.out.printf("%-%t} %1Object__;\n", cname);
        }
        else if (kind == IOBJ_PROXY) {
            Value.Obj delmod = ((Value.Obj)(mod.getv("delegate$"))).getOrig();
            Unit du = unitSpec(delmod);
            genInstDecls(delmod, du);
            glob.genTitleD(pre + "Object");
            glob.out.printf("typedef %1Object__ %2Object__;\n",
                glob.mkCname(du.getQualName()), cname);
        }
        else { // kind == IOBJ_DLGPR
            Value.Obj delmod = modDeleg(mod);
            Unit du = unitSpec(delmod);
            genInstDecls(delmod, du);
            String dcname = glob.mkCname(du.getQualName());
            glob.genTitleD(pre + "Object");
            glob.out.printf("struct %1Object {%+\n", cname);
            glob.genStateFlds(u, true);
            glob.out.printf("%t%1Object __deleg;\n", dcname);
            if (commonGeti(mod, "namedInstance") == 1) {
                glob.out.printf("%txdc_runtime_Types_CordAddr __name;\n");
            }
            glob.out.printf("%-};\n", cname);
            glob.genTitleD(pre + "Object__");
            glob.out.printf("typedef %1Object %1Object__;\n", cname);
        }

        glob.genTitleD(pre + "Object2__");
        glob.out.printf("typedef struct {%+\n");
        glob.out.printf("%txdc_runtime_Types_InstHdr hdr;\n");
        glob.out.printf("%t%1Object__ obj;\n", cname);
        glob.out.printf("%-} %1Object2__;\n", cname);
    }

    /*
     *  ======== genInstObj ========
     *
     *  This function generates the generic fields of the static instance
     *  structure, and calls genFlds for the module-specific part.
     */
    private void genInstObj(Value.Obj iobj, Value.Obj mod, String cname)
    {
        if (mod.geti("$$iflag") == 1) {
            if (commonGeti(mod, "fxntab") == 1) {
                glob.out.printf("%t&%1Module__FXNS__C,\n", cname);
            }
            else {
                glob.out.printf("%t0,\n", cname);
            }
        }
        genFlds(iobj);

        if (commonGeti(mod, "namedInstance") == 1) {
            glob.out.printf("%t%1 /* __name */\n", genCordAddr(iobj.geti("__name")));
        }
    }

    /*
     *  ======== genInternals ========
     */
    private void genInternals(Value.Obj mod)
    {
        Unit unit = unitSpec(mod);

        genTitle(mod, "INTERNALS");
        genModDecls(mod, unit);
        if (unit.isInst()) {
            genInstDecls(mod, unit);
        }
    }

    /*
     *  ======== genMod ========
     */
    private void genMod(Value.Obj mod)
    {
        Unit u = unitSpec(mod);

        if (mode == VALMODE) {
            pkgset.add(mod.getv("$package").gets("$name"));
        }

        if (u.isProxy()) {
            return;
        }

        genModInsts(mod, u);
        genModState(mod, u);
        genModConfigs(mod, u);
    }

    /*
     *  ======== genModConfigs ========
     */
    private void genModConfigs(Value.Obj mod, Unit unit)
    {
        /* In DCLMODE, genPreds only generates declarations of externs,
         * structures and arrays that are assigned to the config parameters of
         * the module. The functions genArr, genStruct and others invoked from
         * genPreds are also checking 'mode', and they will only generate
         * declarations when invoked in DCLMODE.
         */
        genPreds(mod, "", "", true);

        if (mode == DCLMODE) {
            return;
        }

        String ts = mod.getProto().tname();
        String iname = glob.mkIname(ts.substring(0, ts.lastIndexOf('.')));

        int inscnt = unit.isInst() && !unit.isProxy() ? mod.getv("$instances").geti("length") : 0;

        for (Member fld : mod.getProto().allFlds()) {
            if (fld.isMeta() || fld.getName().startsWith("$")) {
                continue;
            }

            String fn = iname + fld.getName();

            Object fldval = mod.get(fld.getName(), mod);
            if (undef(fldval)) {
                mod.error("property '" + fld.getName() + "' is undefined");
                continue;
            }

            if (checkNogen(fld.getProto(), fldval)) {
                continue;
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (isRom && romConstFields.containsKey(fn + "__C")) {
                fm.switchString(baos);
            }

            glob.genTitleD(fld.getName() + "__C");
            clink(fn + "__C", "CT__" + fn);
            glob.out.printf("__FAR__ const CT__%1 %1__C", fn);
            glob.out.printf(" = ");

            /* now, the value for __C constant is generated */
            if (fld.getName().equals("Object__sizeof")) {
                if (unit.isInst()) {
                    glob.out.printf("sizeof(%1Object__)", glob.cname);
                }
                else {
                    glob.out.printf("0");
                }
            }
            else if (fld.getName().equals("Object__count")) {
                glob.out.printf("%1", Integer.toString(inscnt));
            }
            else if (fld.getName().equals("Object__table")) {
                glob.out.printf("%1", inscnt > 0
                    ? (glob.cname + "Object__table__V") : "NULL");
            }
            else if (fld.getName().equals("Object__heap")) {
                Value.Obj vref = (Value.Obj)commonGetv(mod, "instanceHeap");
                glob.out.printf("%1", vref != null ? valToStr(vref) : "NULL");
            }
            else {
                /* all config parameters are declared 'const' so we know that
                 * the definition has to have 'const' qualifier.
                 */
                genVal(fld.getProto(), fldval, "const CT__" + fn);
            }
            glob.out.printf(";\n");

            if (isRom && romConstFields.containsKey(fn + "__C")) {
                List<String> values = romConstFields.get(fn + "__C");
                values.add("CT__" + fn);

                /* We only need the part between '=' and ';' for value */
                String whole = baos.toString();

                Pattern pat = Pattern.compile("^.*const.*=(.*);$",
                    Pattern.DOTALL | Pattern.MULTILINE);
                Matcher m = pat.matcher(whole);
                m.find();

                values.add(m.group(1).trim());
                /* Initializers are always printed to the common C file, so we
                 * know we need to switch back to common.
                 */
                fm.switchCommon();
            }
        }
    }

    /*
     *  ======== genModDecls ========
     *
     *  This function generates the types for Module__root__V structures.
     */
    private void genModDecls(Value.Obj mod, Unit u)
    {
        String cname = glob.mkCname(u.getQualName());

        boolean ihdr = u.isInst()
            && commonGeti(mod, "memoryPolicy") != STATIC_POLICY;
        boolean dmask = needsDiagsMask(mod);

        /* Only if dynamic instances can be created or diagsMask can be
         * changed at runtime, we need to create Module__root__V structures.
         */
        if (ihdr || dmask) {
            glob.genTitleD("Module__");
            glob.out.printf("typedef struct %1Module__ {\n%+", cname);
            if (ihdr) {
                glob.out.printf("%txdc_runtime_Types_Link link;\n");
            }
            if (dmask) {
                glob.out.printf("%txdc_runtime_Types_DiagsMask mask;\n");
            }
            glob.out.printf("%-} %1Module__;\n", cname);

            glob.genTitleD("Module__root__V");
            glob.out.printf("extern %1Module__ %1Module__root__V;\n", cname);
        }
    }

    /*
     *  ======== genModDesc ========
     *
     *  This function generates DESC__C declarations, but does it when 'mode'
     *  is VALMODE.
     */
    private void genModDesc(Value.Obj mod, String cname)
    {
        if (mode == DCLMODE) {
            return;
        }

        String ds = "Object__DESC__C";

        fm.switchMod(mod);
        glob.genTitleD(ds);
        if (fm.switchMod(mod) == FileManager.SEPARATE_FILE) {
            /* ROM check */
            glob.out.printf("#ifndef %1%2R\n", cname, ds);
            glob.out.printf(
                "extern __FAR__ const xdc_runtime_Core_ObjDesc %1%2;\n", cname, 
                ds);
            glob.out.printf("#else\n");
            glob.out.printf("#define %1%2 (*((xdc_runtime_Core_ObjDesc*)"
                + "(xdcRomConstPtr + %1%2_offset)))\n", cname, ds);
            glob.out.printf("#endif\n");
        }
        else {
            glob.out.printf("__FAR__ const xdc_runtime_Core_ObjDesc %1%2;\n",
                cname, ds);
        }
        fm.switchCommon();
    }

    /*
     *  ======== genModInsts ========
     */
    private void genModInsts(Value.Obj mod, Unit u)
    {
        if (!u.isInst()) {
            return;
        }

        boolean ihdr = commonGeti(mod, "memoryPolicy") != STATIC_POLICY;
        boolean dmask = needsDiagsMask(mod);

        String cname = glob.mkCname(u.getQualName());

        /* These two are NOPs in DCLMODE. */
        genModDesc(mod, cname);
        genModParams(mod, cname);

        Value insarr = mod.getv("$instances");
        int inscnt = insarr.geti("length");
        for (int i = 0; i < inscnt; i++) {
            Value.Obj inst = (Value.Obj)insarr.getv(i);
            genPreds((Value.Obj)inst.getv("$object"),
                "", "Instance_State__", false);
        }

        boolean noInsts = (inscnt == 0) || (mod.geti("PROXY$") != 0);

        if (mode == VALMODE && (ihdr || dmask)) {
            /* This code generates initializers for Module__root__V structures
             * for modules that can have dynamically created instances.
             * Any change here must be synchronized with code in genModDecls.
             */
            String rn = cname + "Module__root__V";
            glob.genTitleD("Module__root__V");
            glob.out.printf("%1Module__ %2 = {\n%+", cname, rn);
            if (ihdr) {
                glob.out.printf("%t{&%1.link,  /* link.next */\n", rn);
                glob.out.printf("%t&%1.link},  /* link.prev */\n", rn);
            }
            if (dmask) {
                glob.out.printf("%t%1,  /* mask */\n", Integer.toString(mod.geti("$$diagsMask")));
            }
            glob.out.printf("%-};\n");

            // TODO: anomaly with lack of instance$static$init and "empty" instances
        }

        if (noInsts) {
            return;
        }

        String instSect = commonGets(mod, "instanceSection");
        if (mode == VALMODE && instSect != null) {
            glob.out.printf("#if !(defined(__MACH__) && defined(__APPLE__))\n");
            glob.out.printf("%1Object__ %1Object__table__V[%2] __attribute__ "
                + "((section(\"%3\")));\n", cname, Integer.toString(inscnt), 
                instSect);
            glob.out.printf("#endif\n#endif\n");
        }

        /* Object__table__V is an array of statically created instances */
        glob.genTitleD("Object__table__V");
        glob.out.printf("%1Object__ %1Object__table__V[%2]", cname, Integer.toString(inscnt));

        if (mode == DCLMODE) {
            glob.out.printf(";\n");
            return;
        }

        int kind = iobjKind(mod);

        glob.out.printf(" = {\n%+");
        for (int i = 0; i < inscnt; i++) {
            Value.Obj iobj = (Value.Obj)insarr.getv(i).getv("$object");
            glob.out.printf("%t{/* instance#%1 */\n%+", Integer.toString(i));
            if (kind == IOBJ_PROXY) {
                Value.Obj dobj = (Value.Obj)iobj.getv("$delegate");
                Value.Obj dmod = ((Value.Obj)mod.getv("delegate$")).getOrig();
                String dcname = glob.mkCname(dmod.gets("$name"));
                genInstObj(dobj, dmod, dcname);
            }
            else if (kind == IOBJ_DLGST) {
                genInstObj(iobj, mod, cname);
                Value.Obj dobj = (Value.Obj)iobj.getv("$delegate").getv("$object");
                Value.Obj dmod = ((Value.Obj)mod.getv("delegate$")).getOrig();
                String dcname = glob.mkCname(dmod.gets("$name"));
                int dkind = iobjKind(dmod);
                glob.out.printf("%t{/* __deleg */\n%+", Integer.toString(i));
                genInstObj(dobj, dmod, dcname);
                glob.out.printf("%-%t},\n");
            }
            else if (kind == IOBJ_DLGPR) {
                genInstObj(iobj, mod, cname);
                Value.Obj dobj = (Value.Obj)iobj.getv("$delegate").getv("$object");
                Value.Obj dmod = ((Value.Obj)mod.getv("delegate$")).getOrig();
                dobj = (Value.Obj)dobj.getv("$delegate");
                dmod = ((Value.Obj)dmod.getv("delegate$")).getOrig();
                String dcname = glob.mkCname(dmod.gets("$name"));
                int dkind = iobjKind(dmod);
                glob.out.printf("%t{/* __deleg */\n%+", Integer.toString(i));
                genInstObj(dobj, dmod, dcname);
                glob.out.printf("%-%t},\n");
            }
            else {
                genInstObj(iobj, mod, cname);
            }

            glob.out.printf("%-%t},\n");
        }

        glob.out.printf("%-};\n");
    }

    /*
     *  ======== genModLog ========
     */
    private void genModLog(Value.Obj mod)
    {
        if (mod.has("$$fxnEvtIds", mod)) {
            glob.genTitleD("entry/exit Log events");
            String evtIds = mod.gets("$$fxnEvtIds");
            if (evtIds != null) {
                for (String eid : evtIds.split(",")) {
                    String[] sa = eid.split(":");
                    glob.out.printf("#define %1%2__ENTRY_EVT %3\n",
                        glob.cname, sa[0], String.format("0x%x", Long.parseLong(sa[1])));
                    glob.out.printf("#define %1%2__EXIT_EVT %3\n",
                        glob.cname, sa[0], String.format("0x%x", Long.parseLong(sa[2])));
                }
            }
        }
    }

    /*
     *  ======== genModParams ========
     *
     *  This function generates a constant structure with the default values
     *  for instance config parameters.
     */
    private void genModParams(Value.Obj mod, String cname)
    {
        String ps = "Object__PARAMS__C";

        Value.Obj prms = (Value.Obj)mod.getv("PARAMS");
        genPreds(prms, "", "", true);
        if (mode == VALMODE) {
            /* If a module requires functions in a separate file, we still need
             * to generate the whole PARAMS structure in the big C file.
             */ 
            if (fm.switchMod(mod) == FileManager.SEPARATE_FILE) {
                glob.genTitleD(ps);
                /* ROM check */
                glob.out.printf("#ifndef %1%2R\n", cname, ps);
                glob.out.printf("extern __FAR__ const %1Params %1%2;\n",
                    cname, ps);
                glob.out.printf("#else\n");
                glob.out.printf("#define %1%2 (*((%1Params*)"
                    + "(xdcRomConstPtr + %1%2_offset)))\n",
                    cname, ps);
                glob.out.printf("#endif\n");
            }
            fm.switchCommon();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (isRom && romConstFields.containsKey(cname + ps)) {
                fm.switchString(baos);
            }
            glob.genTitleD(ps);
            clink(cname + ps, cname + "Params");
            glob.out.printf("__FAR__ const %1Params %1%2 = {\n%+", cname, ps);
            glob.out.printf("%tsizeof (%1Params), /* __size */\n", cname);
            glob.out.printf("%tNULL, /* __self */\n", cname, ps);
            glob.out.printf("%tNULL, /* __fxns */\n");
            glob.out.printf("%t(xdc_runtime_IInstance_Params*)&%1%2.__iprms, /* instance */\n",
                cname, ps);
            /* Here is where the declared parameters are generated. */
            genFlds(prms);
            glob.out.printf("%-};\n");
            if (isRom && romConstFields.containsKey(cname + ps)) {
                List<String> values = romConstFields.get(cname + ps);
                values.add(cname + "Params");

                /* We only need the part between '=' and ';' for value */
                String whole = baos.toString();

                Pattern pat = Pattern.compile("^.*__FAR__ const.*=(.*);$",
                    Pattern.DOTALL | Pattern.MULTILINE);
                Matcher m = pat.matcher(whole);
                m.find();

                values.add(m.group(1).trim());
                /* Initializers are always printed to the common C file, so we
                 * know we need to switch back to common.
                 */
                fm.switchCommon();
            }
        }
    }

    /*
     *  ======== genModState ========
     */
    private void genModState(Value.Obj mod, Unit u)
    {
        String cname = glob.mkCname(u.getQualName());
        boolean dmask = needsDiagsMask(mod);

        if (mode == VALMODE && !u.isInst() && dmask) {
            /* This code generates initializers for Module__root__V structures
             * for modules that have no instances.
             * Any change here must be synchronized with code in genModDecls.
             */ 
            glob.genTitleD("Module__root__V");
            glob.out.printf("%1Module__ %1Module__root__V", cname);
            glob.out.printf(" = {\n%+%t%1,  /* mask */\n%-}",
                Integer.toString(mod.geti("$$diagsMask")));
            glob.out.printf(";\n");
        }

        Value.Obj vobj = (Value.Obj)mod.getv("$object");

        if (vobj == null) {
            return;
        }

        if (mode == DCLMODE) {
            glob.genTitleD("Module_State__");
            glob.out.printf("typedef struct %1Module_State__ {%+\n", cname);
            glob.genStateFlds(u, false);
            glob.out.printf("%-} %1Module_State__;\n", cname);
        }

        /* declarations of structures and arrays that are assigned to the
         * fields of the Module_State structure. */
        genPreds(vobj, "", "Module_State__", false);

        String suf = "Module__state__V";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (isRom && mode != DCLMODE && romModStates.containsKey(cname + suf)) {
            fm.switchString(baos);
        }
        glob.genTitleD(suf);
        if (mode != DCLMODE) {
            glob.out.printf("#ifdef __ti__\n");
            /* this is for TI compilers */
            glob.out.printf("%1Module_State__ %1Module__state__V "
                + "__attribute__ ((section(\".data:%1Module__state__V\")));\n",
                cname);
            glob.out.printf("#elif !(defined(__MACH__) && "
                + "defined(__APPLE__))\n");
            /* this is for the GNU except MacOS (see genArr comments), and the
             * IAR compilers
             */
            glob.out.printf("%1Module_State__ %1Module__state__V "
                + "__attribute__ ((section(\".data_%1Module__state__V\")));\n",
                cname);
            glob.out.printf("#endif\n");
        }
        /* If the mode is DECL, we don't generate the declaration for states in
         * the big ROM structure because IAR complains about that declaration
         * because we define a symbol with the same name in the linker command
         * file. GNU and TI tools do not care because we never actually define
         * this symbol.
         */
        if (mode == DCLMODE && (!romModStates.containsKey(cname + suf))) {
            glob.out.printf("%1Module_State__ %1Module__state__V;\n", cname);
        }
        else if (mode != DCLMODE) {
            glob.out.printf("%1Module_State__ %1Module__state__V", cname);
            glob.out.printf(" = {\n%+");
            genFlds(vobj);
            glob.out.printf("%-};\n");
        }
        if (isRom && mode != DCLMODE && romModStates.containsKey(cname + suf)) {
            List<String> values = romModStates.get(cname + suf);
            values.add(cname + "Module_State__");

            /* We only need the part between '=' and ';' for value */
            String whole = baos.toString();

            Pattern pat = Pattern.compile("^.*Module__state__V\\s=(.*);$",
                Pattern.DOTALL | Pattern.MULTILINE);
            Matcher m = pat.matcher(whole);
            m.find();

            values.add(m.group(1).trim());
            fm.switchCommon();
        }
    }

    /*
     *  ======== getOffsets ========
     *
     *  If there are embedded objects in Instance_State or Module_State
     *  structures, they are accessed through function calls that use the
     *  offsets generated here to find the address of an embedded object.
     */
    void genOffsets( Value.Obj mod )
    {
        Unit unit = unitSpec(mod);

        List<Decl.Field> fL = glob.offsetFlds(unit);

        if (fL.size() == 0) {
            return;
        }

        glob.genTitle("OBJECT OFFSETS");

        for (Decl.Field fld : fL) {
            String sn = fld.getParent().getName();
            xdc.services.spec.Ref r = glob.rawType(fld.getType()).tspec().getRef();
            String on = glob.cname + sn + '_' + fld.getName() + "__O";
            sn = sn.equals("Module_State") ? (sn += "__") : "Object__";
            /* For ROM, we don't want to generate __O constants because they
             * are defined as macro replacements in a special header file. That
             * special header file is present when the inline access functions
             * are compiled for ROM, so these functions use literals for offset.
             * The __O constants in the big ROM struct are only needed for
             * previously compiled libraries that embed inline access functions.
             */
            if (isRom && romConstFields.containsKey(on)) {
                List<String> values = romConstFields.get(on);
                values.add("xdc_SizeT");
                values.add("offsetof(" + glob.cname + sn
                    + ",Object_field_" + fld.getName() + ")");
            }
            else {
                clink(on, "xdc_SizeT");
                glob.out.printf("__FAR__ const xdc_SizeT %1 = offsetof(%2%3,"
                    + "Object_field_%4);\n", on, glob.cname, sn, fld.getName());
            }
        }
        skip();
    }

    /*
     *  ======== getPkgAliases ========
     */
    private void getPkgAliases(Value.Obj po, HashMap<String,Value.Obj> modmap,
        Hashtable<String,String> aliases)
    {
        Pkg pkg = pkgSpec(po);  /* get spec object for the config pkg value */

        /* scan the package spec for runtime modules */
        for (Unit unit: pkg.getUnits()) {
            if (!unit.isMeta() && !unit.isProxy()
                && !unit.isInter() && unit.needsRuntime()) {
                glob.setNames(unit);

                /* get config module object from modvec */
                Value.Obj mod = modmap.get(unit.getQualName());
                if (mod == null || !isasm(mod)) {
                    continue;   /* unit is not part of the configuration */
                }


                /* get mod's Diags mask and logger */
                int mask = 0;
                Object loggerObj = null;
                if (mod.has("Module__diagsIncluded", mod)) {
                    mask = getDiagsIncluded(mod);
                    loggerObj = commonGetv(mod, "logger");
                }
                else {
//                    System.out.println("Warning: "
//                        + mod.gets("$name") + " does not have diags");
                    continue;
                }

                /* scan all functions in the module's spec */
                for (Decl.Fxn fxn : unit.getFxns()) {
                    if (fxn.isMeta() || fxn.isSys()
                            || fxn.attrBool(Attr.A_Macro)
                            || fxn.attrBool(Attr.A_DirectCall)) {
                        continue;
                    }
                    if ((unit.delegatesTo() != null
                        && fxn.getParent() != unit)) {
                        continue;
                    }

                    /* if there is no logging for fxn, it's an alias */
                    if (!fxn.isLoggable()
                        || loggerObj == null || undef(loggerObj)
                        || (mask & 0x3) == 0) {

                        String prefix = glob.cname + fxn.getName()
                            + (fxn.isVarg() ? "_va" : "");

                        aliases.put(prefix + "__E", prefix + "__F");
                    }
                }
            }
        }
    }

    /*
     *  ======== genPostInit ========
     */
    private void genPostInit()
    {
        if (mInitCnt == 0) {
            return;
        }

        glob.genTitle("INITIALIZATION ENTRY POINT");
        glob.out.printf("#include <stdint.h>\n");
        glob.out.printf("extern int_least32_t __xdc__init(void);\n");
        glob.out.printf("%+%t__attribute__ ((used))\n%-");
        glob.out.printf("__FAR__ int_least32_t (* volatile __xdc__init__addr)(void) = &__xdc__init;\n");
        skip();
    }

    /*
     *  ======== genPragmas ========
     *  Top-level pragmas
     */
    void genPragmas()
    {
        /* needed for Klockwork MISA checker (cast away const warning) */
        skip();
        glob.out.printf("/* suppress 'type qualifier is meaningless on cast type' warning */\n");
        glob.out.printf("#if defined(__ti__) && !defined(__clang__)\n");
        glob.out.printf("#pragma diag_suppress 193\n");
        glob.out.printf("#endif\n");
        glob.out.printf("#ifdef __IAR_SYSTEMS_ICC__\n");
        glob.out.printf("#pragma diag_suppress=Pe191\n");
        glob.out.printf("#endif\n\n");

        glob.out.printf("#if !(defined(__GNUC__))\n");
        glob.out.printf("#if !(defined(__TI_GNU_ATTRIBUTE_SUPPORT__) || ");
        glob.out.printf("defined(__IAR_SYSTEMS_ICC__)) || defined(__ARP32__)\n");
        glob.out.printf("#define __attribute__(x)\n");
        glob.out.printf("#endif\n#endif\n");
    }

    /*
     *  ======== genPragmas ========
     *  Module specific pragmas
     */
    void genPragmas( Value.Obj mod, Value.Obj target )
    {
        Unit unit = unitSpec(mod);

        // TODO: generalize to use all system functions

        if (unit.isProxy()) {
            return;
        }

        int scope = mod.geti("$$scope");
        if (scope == 1) {
            boolean titleAdded = false;

            /* Module-wide configs can be accessed directly or indirectly
             * (through Log functions, for example) from within the user code.
             * We give a list of the generated data for these configs so the
             * build target can ensure that such data is not eliminated in
             * partial builds.
             */
            ArrayList<String> configNames = new ArrayList();
            ArrayList<String> configQuals = new ArrayList();
            ArrayList<String> configTypes = new ArrayList();
            for (Decl.Config cfg : unit.getConfigs()) {
                if (cfg.isInst() || cfg.isMeta()
                    || mod.get(cfg.getName(), mod) ==
                    xdc.services.intern.xsr.Value.NOGEN) {
                    continue;
                }

                String cn = glob.cname + cfg.getName();
                /* consts that end up in the big structure for ROM can't be
                 * declared extern because they are not separate variables
                 * anymore.
                 */
                if (romConstFields.containsKey(cn + "__C")) {
                    continue;
                }

                configQuals.add("const");
                configTypes.add("CT__" + cn);
                configNames.add(cn + "__C");
            }
            Context cx = Global.getContext();
            Scriptable gscope = Global.getTopScope();
            Scriptable quals = cx.newArray(gscope, configQuals.toArray());
            Scriptable types = cx.newArray(gscope, configTypes.toArray());
            Scriptable names = cx.newArray(gscope, configNames.toArray());
            if (target.has("genVisibleData", target)) {
                Object res = target.invoke("genVisibleData",
                             new Object[] {quals, types, names});
                if (res != null) {
                    if (!titleAdded) {
                        genTitle(mod, "PRAGMAS");
                        titleAdded = true;
                    }
                    glob.out.print(res.toString());
                }
            }

            ArrayList<String> retTypes = new ArrayList();
            ArrayList<String> fxnNames = new ArrayList();
            ArrayList<String> fxnArgs = new ArrayList();
            ArrayList<String> retTypesLib = new ArrayList();
            ArrayList<String> fxnNamesLib = new ArrayList();
            ArrayList<String> fxnArgsLib = new ArrayList();
            ByteArrayOutputStream bos;
            Out saved = glob.out;

            /* There are some system functions that are generated only when a
             * module has instances. However, such functions used to be
             * returned by getFxns because all modules inherit them from
             * IModule. Now, we remove those at the spec generation time, but
             * for packages built before that change, we have to remove them
             * here too. Once we increment the schema number from 170, and the
             * spec number from 180, we'll be able to remove this code.
             */
            boolean oldPackage = false;
            ArrayList<String> removable = new ArrayList(
                Arrays.asList(new String [] {"Handle__label", "Object__create",
                    "Object__delete", "Object__first", "Object__get",
                    "Object__next", "Object__init", "Params__init"}));
            if (!unit.isInst() && unit.getDecl("Object__create") != null) {
                oldPackage = true;
            }

            for (Decl.Fxn fxn : unit.getFxns()) {
                if (fxn.isMeta() || fxn.attrBool(Attr.A_Macro)) {
                    continue;
                }

                if (!unit.isProxy() && fxn.getName().startsWith("Proxy")) {
                    continue;
                }

                if (oldPackage && removable.contains(fxn.getName())) {
                    removable.remove(fxn.getName());
                    continue;
                }

                bos = new ByteArrayOutputStream(50);
                glob.out = new Out(bos);

                String suf = fxn.isSys() ? "__S" : "__E";

                String sp = "";
                if (fxn.isInst()) {
                    glob.out.printf("%1Handle", glob.cname);
                    sp = ", ";
                }
                glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, sp);
                if (fxn.isVarg()) {
                    glob.out.printf(", ...");
                }

                String args = bos.toString();

                if (fxn.attrBool(Attr.A_DirectCall)) {
                    retTypesLib.add(fxn.getType().tsig());
                    fxnNamesLib.add(glob.cname + fxn.getName() + suf);
                    fxnArgsLib.add(args);
                }
                else {
                    retTypes.add(fxn.getType().tsig());
                    fxnNames.add(glob.cname + fxn.getName() + suf);
                    fxnArgs.add(args);
                }
            }
            glob.out = saved;

            Scriptable rets = cx.newArray(gscope, retTypes.toArray());
            Scriptable fnames = cx.newArray(gscope, fxnNames.toArray());
            Scriptable fargs = cx.newArray(gscope, fxnArgs.toArray());
            fm.switchMod(mod);
            if (target.has("genVisibleFxns", target)) {
                Object res = target.invoke("genVisibleFxns",
                             new Object[] {rets, fnames, fargs});
                if (res != null) {
                    if (!titleAdded) {
                        genTitle(mod, "PRAGMAS");
                        titleAdded = true;
                    }
                    glob.out.print(res.toString());
                    glob.out.flush();
                }
            }

            rets = cx.newArray(gscope, retTypesLib.toArray());
            fnames = cx.newArray(gscope, fxnNamesLib.toArray());
            fargs = cx.newArray(gscope, fxnArgsLib.toArray());
            if (target.has("genVisibleLibFxns", target)) {
                Object res = target.invoke("genVisibleLibFxns",
                             new Object[] {rets, fnames, fargs});
                if (res != null) {
                    if (!titleAdded) {
                        genTitle(mod, "PRAGMAS");
                        titleAdded = true;
                    }
                    glob.out.print(res.toString());
                    glob.out.flush();
                }
            }
            fm.switchCommon();
        }
    }

    /*
     *  ======== genPreds ========
     */
    private void genPreds(Value.Obj vobj, String path, String scope,
                          boolean isConst)
    {
        if (vobj == null) {
            return;
        }

        if (vobj.getProto().tname().endsWith(".Object")) {
            genPreds((Value.Obj)vobj.getv("$object"), "", "Instance_State__",
                     false);
        }

        for (Member fld : vobj.getProto().allFlds()) {

            if (fld.isMeta() || fld.getName().startsWith("$")) {
                continue;
            }

            Object fldval = vobj.get(fld.getName(), vobj);
            if (undef(fldval)) {
                vobj.error("'" + fld.getName()
                    + "' must be statically initialized.");
                continue;
            }

            if (!(fld.getProto() instanceof Proto.Pred)) {
                continue;
            }

            String ps = path + '.' + fld.getName();

            if (fld.getProto() instanceof Proto.Str) {
                /* For aggregate types used inside of a structure, type names
                 * are scoped by this structure's type name. So, we need to pass
                 * this structure's name to subsequent genPreds().
                 * The scope is needed so we can have
                 * struct S1 {int a[];} and also struct S2 {Ptr a[];}
                 * If there are structures within structures, we only care about
                 * the name of the immediate structure enclosing the array.
                 * When the array type is declared, it's declared only when the
                 * the structure enclosing the array was processed for typedefs.
                 */
                String baseStrName =
                    ((Proto.Str)fld.getProto()).getBase().getName();
                String strScope = baseStrName.substring(
                    baseStrName.lastIndexOf('.') + 1) + "__";
                genPreds((Value.Obj)fldval, ps, strScope, isConst);
            }
            else if (fldval instanceof Value.Arr) {
                genArr((Value.Arr)fldval, scope + fld.getName(), vobj, ps,
                       isConst);
            }
            else if (fldval instanceof Extern) {
                genExt((Extern)fldval);
            }
            else if (fldval instanceof Value.Obj) {
                genStruct((Value.Obj)fldval, isConst);
            }
        }
    }

    /*
     *  ======== genPrologue ========
     */
    private void genPrologue()
    {
        glob.genWarning();
        skip();

        glob.out.printf("#define __nested__\n");
        glob.out.printf("#define __config__\n");
        skip();

        glob.out.printf("#include <xdc/std.h>\n");
        skip();

        glob.genSections(
                "",
                "MODULE INCLUDES",
                "",
                "<module-name> INTERNALS",
                "<module-name> INHERITS",
                "<module-name> VTABLE",
                "<module-name> PATCH TABLE",
                "<module-name> DECLARATIONS",
                "<module-name> OBJECT OFFSETS",
                "<module-name> TEMPLATES",
                "<module-name> INITIALIZERS",
                "<module-name> FUNCTION STUBS",
                "<module-name> PROXY BODY",
                "<module-name> OBJECT DESCRIPTOR",
                "<module-name> VIRTUAL FUNCTIONS",
                "<module-name> SYSTEM FUNCTIONS",
                "<module-name> PRAGMAS",
                "",
                "INITIALIZATION ENTRY POINT",
                "PROGRAM GLOBALS",
                "CLINK DIRECTIVES"
        );
        skip();
    }

    /*
     *  ======== genProxy ========
     */
    private void genProxy( Value.Obj mod )
    {
        Unit unit = unitSpec(mod);

        if (!unit.isProxy()) {
            return;
        }

        genTitle(mod, "PROXY BODY");

        Value delmod = mod.getv("delegate$");
        String dcname = glob.mkCname(delmod.gets("$name"));
        glob.genTitleD("DELEGATES TO " + delmod.gets("$name"));

        String icn = glob.mkCname(unit.getSuper().getQualName());

        glob.genTitleD("Module__startupDone__S");
        glob.out.printf("xdc_Bool %1Module__startupDone__S(void)\n{%+\n", glob.cname);
        glob.out.printf("%treturn %1Module__startupDone__S();\n}\n%-", dcname);

        /* There are proxies that don't support create() because their
         * delegates might have different create signatures.
         */
        if (unit.isInst() && !unit.hasAttr(Attr.A_NoProxyCreate)) {
            glob.genTitleD("create");
            glob.out.printf("%1Handle %1create(", glob.cname);
            glob.genCreArgDecls(unit, Glob.Type$LCL, " ");
            glob.out.printf("const %1Params *prms, %2 )\n{\n%+", glob.cname,
                Glob.ERRARG);
            glob.out.printf("%treturn (%1Handle)%2create(", glob.cname, dcname);
            if (unit.hasCreateArgs()) {
                for (Decl.Arg arg : unit.getCreator().getArgs()) {
                    glob.out.printf("%1, ", arg.getName());
                }
            }
            glob.out.printf("(const %1Params *)prms, eb);\n}\n%-", dcname);

            glob.genTitleD("delete");
            glob.out.printf("void %1delete(%1Handle *instp)\n{\n%+",
                glob.cname);
            glob.out.printf("%t%1Object__delete__S(instp);\n}\n%-", dcname);

            glob.genTitleD("Params__init__S");

            String fmt = "void %1Params__init__S( xdc_Ptr dst, const void *src,"
                + " xdc_SizeT psz, xdc_SizeT isz )\n{\n%+";
            glob.out.printf(fmt, glob.cname);

            String cast = isBeforeConst((Value.Obj)delmod) ? "(void *)" : "";
            glob.out.printf("%t%1Params__init__S(dst, %2src, psz, isz);\n}\n%-", 
                dcname, cast);

            glob.genTitleD("Handle__label__S");
            String ls = "xdc_runtime_Types_Label";
            glob.out.printf("%1 *%2Handle__label__S(xdc_Ptr obj, %1 *lab)\n{\n%+", ls, glob.cname);
            glob.out.printf("%treturn %1Handle__label__S(obj, lab);\n}\n%-", 
                dcname);
        }

        boolean isAbs = unit.isInst() && mod.geti("abstractInstances$") == 1;

        for (Decl.Fxn fxn : unit.getFxns()) {
            if (fxn.isMeta() || fxn.isSys()) {
                continue;
            }

            String fn = fxn.getName() + ((fxn.isVarg()) ? "_va" : "") + "__E";
            glob.genTitleD(fn);
            fn = glob.cname + fn;
            glob.genType(fxn.getType(), Glob.Type$TYP, fn);
            glob.out.print("(");
            String sp = " ";
            if (fxn.isInst()) {
                glob.out.printf(" %1Handle inst", glob.cname);
                sp = ", ";
            }
            glob.genArgDecls(fxn.getArgs(), Glob.Type$LCL, sp);
            if (fxn.isVarg()) {
                glob.out.printf(", va_list arg_va");
            }
            glob.out.printf(" )\n{\n%+%t");
            if (!glob.isVoid(fxn)) {
                glob.out.printf("return ");
            }
            sp = "";
            if (!fxn.isInst()) {
                glob.out.printf("%1%2(", dcname, fxn.getName());
            }
            else if (!isAbs) {
                glob.out.printf("%1%2((%1Handle)inst", dcname,
                    fxn.getName());
                sp = ", ";
            }
            else {
                glob.out.printf("%1%2((%1Handle)inst", icn, fxn.getName());
                sp = ", ";
            }
            glob.genArgNames(fxn.getArgs(), sp);
            if (fxn.isVarg()) {
                glob.out.printf(", arg_va");
            }
            glob.out.printf(");\n}\n%-");
        }
    }

    /*
     *  ======== genCordAddr ========
     */
    private String genCordAddr(int cid)
    {
        if (cid == 0 || cid >= 0x8000) {
            return "0";
        }

        String tab = "xdc_runtime_Text_charTab__A";
        return "(xdc_runtime_Text_CordAddr)(" + tab + "+" + cid + ")";
    }

    /*
     *  ======== genRomStruct ========
     *
     *  Generates two structure that contain all constants referenced by ROM,
     *  and all module states referenced by ROM.
     */
    private void genRomStruct()
    {
        glob.genTitleD("CT__constStruct");
        glob.out.printf("typedef struct {\n%+");
        for (String cnst: romConstFields.keySet()) {
            glob.out.printf("%t%1 %2;\n", romConstFields.get(cnst).get(0),
                cnst.replaceFirst("__C$", "__R"));
        }
        glob.out.printf("} CT__constStruct;%-\n\n");

        glob.genTitleD("TROM__stateStruct");
        glob.out.printf("%ttypedef struct {%+\n");
        for (String state: romModStates.keySet()) {
            glob.out.printf("%t%1 %2;\n", romModStates.get(state).get(0),
                state);
        }
        glob.out.printf("%-} TROM__stateStruct;\n\n");

        for (String cnst: romConstFields.keySet()) {
            glob.out.printf("const xdc_SizeT %1_offset = "
                + "offsetof(CT__constStruct, %2);\n", cnst,
                cnst.replaceFirst("__C$", "__R"));
        }

        for (String state: romModStates.keySet()) {
            glob.out.printf("const xdc_SizeT %1_offset = "
                + "offsetof(TROM__stateStruct, %1);\n", state);
        }

        for (String cnst: romConstFields.keySet()) {
            glob.out.printf("#define %1 (ROM_constStruct.%2)\n", cnst,
                cnst.replaceFirst("__C$", "__R"));
        }
        for (String state: romModStates.keySet()) {
            glob.out.printf("#define %1 (ROM_stateStruct.%1)\n", state);
        }

        glob.genTitleD("ROM_constStruct");
        clink("ROM_constStruct", "CT__constStruct");
        glob.out.printf("__FAR__ const CT__constStruct ROM_constStruct = {\n");
        for (String cnst: romConstFields.keySet()) {
            String value = romConstFields.get(cnst).get(1);
            /* need to add 4 spaces to each new line */
            Pattern pat = Pattern.compile("^",Pattern.MULTILINE);
            Matcher m = pat.matcher(value);
            value = m.replaceAll("    ");
            glob.out.printf("%1, /* %2 */\n", value,
                cnst.replaceFirst("__C$", "__R"));
        }
        glob.out.printf("};\n");

        glob.genTitleD("ROM_stateStruct");
        /* this is for all the TI compilers, including clang */
        glob.out.printf("#ifdef __ti__\n");
        glob.out.printf("__FAR__ TROM__stateStruct ROM_stateStruct "
            + "__attribute__ ((section(\".data:ROM_stateStruct\")));\n");
        glob.out.printf("#elif !(defined(__MACH__) && defined(__APPLE__))\n");
        glob.out.printf("__FAR__ TROM__stateStruct ROM_stateStruct "
            + "__attribute__ ((section(\".data_ROM_stateStruct\")));\n");
        glob.out.printf("#endif\n");

        glob.out.printf("__FAR__ TROM__stateStruct ROM_stateStruct = {%+\n");
        for (String state: romModStates.keySet()) {
            String value = romModStates.get(state).get(1);
            /* need to add 4 spaces to each new line */
            Pattern pat = Pattern.compile("^",Pattern.MULTILINE);
            Matcher m = pat.matcher(value);
            value = m.replaceAll("    ");
            glob.out.printf("%1, /* %2 */\n", value, state);
        }
        glob.out.printf("%-};\n");

        glob.genTitleD("xdcRomConstPtr");
        glob.out.printf("#ifdef __ti__\n");
        glob.out.printf("const xdc_UInt8 * xdcRomConstPtr "
            + "__attribute__ ((section(\".data:xdcRomConstPtr\")));\n");
        glob.out.printf("#elif !(defined(__MACH__) && defined(__APPLE__))\n");
        glob.out.printf("const xdc_UInt8 * xdcRomConstPtr "
            + "__attribute__ ((section(\".data_xdcRomConstPtr\")));\n");
        glob.out.printf("#endif\n");

        glob.out.printf("const xdc_UInt8 * xdcRomConstPtr"
            + " = (xdc_UInt8 *)&ROM_constStruct;\n");

        glob.genTitleD("xdcRomStatePtr");
        glob.out.printf("#ifdef __ti__\n");
        glob.out.printf("xdc_UInt8 * xdcRomStatePtr "
            + "__attribute__ ((section(\".data:xdcRomStatePtr\")));\n");
        glob.out.printf("#elif !(defined(__MACH__) && defined(__APPLE__))\n");
        glob.out.printf("xdc_UInt8 * xdcRomStatePtr "
            + "__attribute__ ((section(\".data_xdcRomStatePtr\")));\n");
        glob.out.printf("#endif\n");
        glob.out.printf("xdc_UInt8 * xdcRomStatePtr = (xdc_UInt8 *)"
            + "&ROM_stateStruct;\n");
    }

    /*
     *  ======== genStruct ========
     */
    private void genStruct(Value.Obj str, boolean isConst)
    {
        if (str.geti("$hostonly") == 1) {
            return;
        }

        String ts = str.getProto().tname();

        if (otype(ts) != STRUCT) {
            return;
        }

        if (ts.endsWith(".Module_State") || ts.endsWith(".Instance_State")) {
            return;
        }

        if (str.getRoot() != str && str.getRoot() instanceof Value.Obj) {
            genStruct((Value.Obj)str.getRoot(), isConst);
            return;
        }

        if (str.getPrnt() != null) {
            return;
        }

        String is = str.getPath().substring(0, str.getPath().indexOf('/')).replace('#', '_').replace('.', '_');
        String cs = ts.replace('.', '_');

        if (!strset.add(is)) {
            return;
        }

        genPreds(str, "", (ts.substring(ts.lastIndexOf('.') + 1) + "__"), isConst);

        is += "__T";

        glob.genTitleD("--> " + is);
        if (mode == VALMODE && isConst) {
            clink(is, cs);
        }

        glob.out.printf("__FAR__ %3%1 %2", cs, is, isConst ? "const " : "");
        if (mode == DCLMODE) {
            glob.out.printf(";\n");
        }
        else {
            glob.out.printf(" = {\n%+");
            if (ts.endsWith(".Params")) {
                glob.out.printf("%tsizeof (%1), /* __size */\n", cs);
                glob.out.printf("%t&%1, /* __self */\n", is);
                glob.out.printf("%t0, /* __fxns */\n");
                glob.out.printf("%t(xdc_runtime_IInstance_Params*)&%1.__iprms, /* instance */\n", is);
            }
            genFlds(str);
            glob.out.printf("%-};\n");
        }
    }

    /*
     *  ======== genSymbols ========
     */
    void genSymbols(Value prog, Out out)
    {
        glob.mode = Glob.CDLMODE;
        glob.out = out;

        Value smap = prog.getv("symbol");
        Object[] keys = smap.getIds();

        for (int i = 0; i < keys.length; i++) {
            String errMsg = null;
            String vname = keys[i].toString();
            Object oval = smap.get(vname, smap);
            if (oval instanceof Value.Obj) {
                Value.Obj vobj = ((Value.Obj)oval).getOrig();
                String tname = vobj.getProto().tname();
                if (otype(tname) == INST) {
                    String qn = tname.substring(0, tname.lastIndexOf('.'));
                    String cs = qn.replace('.', '_');
                    String ts = cs + "_Struct";
//                  String ts = cs + "_Object";
                    glob.genInclude(glob.mkIname(qn) + "_", glob.mkFname(qn) + ".h", out);
                    glob.out.printf("extern %1 %2;\n", ts, vname);
                }
                else {
                    errMsg = "Program.symbol." + vname
                        + " (of type " + oval.getClass().toString()
                        + "): not an Instance";
                }
            }
            else if (oval instanceof java.lang.Number) {
                glob.out.printf("extern int %1;\n", vname);
            }
            else if (oval instanceof Ptr) {
                String s = ptrToStr(oval, "");
            }
            else {
                errMsg = "Program.symbol." + vname
                    + " (of type " + oval.getClass().toString()
                    + "): unsupported generation type";
            }

            if (errMsg != null) {
                xdc.services.intern.xsr.Err.exit(errMsg);
                return;
            }

            glob.out.printf("\n");
        }

    }

    /*
     *  ======== genCreateBody2 ========
     *  Generates create() functions
     *
     *  The function genCreateBody generates create__S and construct
     */
    private void genCreateBody2(Value.Obj mod, int useCase)
    {
        Unit unit = unitSpec(mod);
        boolean iInitErr = unit.attrBool(Attr.A_InstanceInitError);
        int pol = commonGeti(mod, "memoryPolicy");

        if (pol == STATIC_POLICY) {
            genError(unit, "create policy error");
            glob.out.printf("%treturn NULL;\n%-}\n");
            return;
        }

        String finSuffix = isBeforeRom(mod) ? "__F" : "__E";
        String fs = unit.attrBool(Attr.A_InstanceFinalize)
           ? ("(xdc_Fxn)" + glob.cname + "Instance_finalize" + finSuffix)
           : "NULL";
        String cs = "xdc_runtime_Core_";
        String los = "xdc_runtime_Log_";

        String logCall = "%1L_create";

        glob.out.printf("%t%1Params prms;\n", glob.cname);
        glob.out.printf("%t%1Object *obj;\n", glob.cname);
        skip();

        String iStat = "";
        if ((mod.geti("$$dlgflag") == 1 || mod.geti("$$iobjflag") == 1)
            && iInitErr) {
            if ((pol & DELETE_POLICY) != 0) {
                glob.out.printf("%tint iStat;\n");
                iStat = "iStat = ";
            }
        }
        skip();

        glob.out.printf("%t/* common instance initialization */\n");
        glob.out.printf("%tobj = %1createObject__I(&%2Object__DESC__C, NULL, "
            + "&prms, (xdc_CPtr)paramsPtr, 0, eb);\n", cs, glob.cname);
        glob.out.printf(
            "%tif (obj == NULL) {\n%+%treturn NULL;\n%-%t}\n\n");

        if (mod.geti("$$dlgflag") == 1 || mod.geti("$$iobjflag") == 1) {
            glob.out.printf("%t/* module-specific initialization */\n");
            glob.out.printf("%t%2%1Instance_init__%3(obj, ", glob.cname,
                iStat, isBeforeRom(mod) ? "F" : "E");
            if (unit.hasCreateArgs()) {
                for (Decl.Arg arg : unit.getCreator().getArgs()) {
                    glob.out.printf("%1%2, ", useCase != 0 ? "" : "__args->", 
                        arg.getName());
                }
            }
            glob.out.printf("&prms%1);\n", iInitErr ? ", eb" : "");
            if (iInitErr) {
                if ((pol & DELETE_POLICY) != 0) {
//                    glob.out.printf(
//                        "%tif (xdc_runtime_Error_check(__eb)%+\n");
//                    glob.out.printf("%t|| iStat && (__eb == "
//                        + "&xdc_runtime_Error_IgnoreBlock)) {\n");
                    glob.out.printf("%tif (iStat) {\n%+");
                    glob.out.printf("%txdc_runtime_Core_deleteObject__I(&%1"
                        + "Object__DESC__C, obj, %2, iStat, 0);\n",
                        glob.cname, fs);
                }
                else {
                    glob.out.printf("%tif (xdc_runtime_Error_check(eb)) {\n%+");
                }
                glob.out.printf("%treturn NULL;\n");
                glob.out.printf("%-%t}\n\n");
            }
        }

        int mask = commonGetv(mod, "logger") != null
            ? getDiagsIncluded(mod) : 0;
        if ((mask & L_LIFECYCLE) != 0) {
            if (commonGeti(mod, "namedInstance") == 1) {
                glob.out.printf(
                    "%t%1write2(" + logCall + ", (xdc_IArg)obj, (xdc_IArg)"
                    + "(((%2Object__*)obj)->__name));\n", los, glob.cname);
            }
            else {
                glob.out.printf("%t%1write2(" + logCall
                    + ", (xdc_IArg)obj, 0);\n", los);
            }
        }

        glob.out.printf("%treturn obj;\n");
        glob.out.printf("%-}\n");
        return;
    }

    /*
     *  ======== genCreateBody ========
     * use case 0 - old create__S invoked by inlined create
     * use case 1 - create - doesn't use this function anymore
     * use case 2 - construct
     */
    private void genCreateBody(Value.Obj mod, int useCase)
    {
        Unit unit = unitSpec(mod);
        boolean iInitErr = unit.attrBool(Attr.A_InstanceInitError);
        int pol = commonGeti(mod, "memoryPolicy");

        if (pol == STATIC_POLICY
            && !(unit.attrBool(Attr.A_InstanceInitStatic) && useCase == 2)) {
            genError(unit, "create policy error");
            if (useCase == 0) {
                glob.out.printf("%treturn NULL;\n");
            }
            glob.out.printf("%-}\n");
            return;
        }

        String finSuffix = isBeforeRom(mod) ? "__F" : "__E";
        String fs = unit.attrBool(Attr.A_InstanceFinalize)
           ? ("(xdc_Fxn)" + glob.cname + "Instance_finalize" + finSuffix)
           : "NULL";
        String cs = "xdc_runtime_Core_";
        String los = "xdc_runtime_Log_";

        String newObj = (useCase == 0) ? "objp" : "(xdc_Ptr)objp";
        String logCall = (useCase == 0) ? "%1L_create" : "%1L_construct";

        if ((mod.geti("$$dlgflag") == 1 || mod.geti("$$iobjflag") == 1)
            && unit.hasCreateArgs()) {
            if (useCase == 0) {
                glob.out.printf("%tconst %1Args__create *__args = req_args;\n",
                    glob.cname);
            }
        }

        glob.out.printf("%t%1Params instPrms;\n", glob.cname);

        if (useCase == 0) {
            glob.out.printf("%t%1Object *objp;\n", glob.cname);
        }

        String iStat = "";
        if (iInitErr) {
            iStat = "(Void)";
            if (mod.geti("$$dlgflag") == 1 || mod.geti("$$iobjflag") == 1) {
                if ((pol & DELETE_POLICY) != 0) {
                    glob.out.printf("%tint iStat;\n");
                    iStat = "iStat = ";
                }
            }
        }
        skip();

        glob.out.printf("%t/* common instance initialization */\n");
        if (useCase == 2) {
            glob.out.printf("%t(Void)%1constructObject__I(&%2Object__DESC__C, "
                + "objp, &instPrms, (xdc_CPtr)paramsPtr, 0, %3);\n",
                cs, glob.cname, iInitErr ? "eb" : "NULL");
        }
        else if (useCase == 0) {
            glob.out.printf("%tobjp = %1createObject__I(&%2Object__DESC__C,"
                + " NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);\n", cs,
                glob.cname);
            glob.out.printf(
                "%tif (objp == NULL) {\n%+%treturn NULL;\n%-%t}\n\n");
        }
        skip();

        if (mod.geti("$$dlgflag") == 1 || mod.geti("$$iobjflag") == 1) {
            glob.out.printf("%t/* module-specific initialization */\n");
            glob.out.printf("%t%2%1Instance_init__%4(%3, ", glob.cname, iStat,
                newObj, isBeforeRom(mod) ? "F" : "E");
            if (unit.hasCreateArgs()) {
                for (Decl.Arg arg : unit.getCreator().getArgs()) {
                    glob.out.printf("%1%2, ", useCase != 0 ? "" : "__args->", 
                        arg.getName());
                }
            }
            glob.out.printf("&instPrms%1);\n", iInitErr ? ", eb" : "");
            if (iInitErr) {
//                glob.out.printf("%t__err = xdc_runtime_Error_check(__eb);\n");
                if ((pol & DELETE_POLICY) != 0) {
                    glob.out.printf("%tif (iStat) {\n%+");
//                    glob.out.printf(
//                        "%tif (((__eb != &xdc_runtime_Error_IgnoreBlock) && "
//                        + "__err)%+\n");
//                    glob.out.printf("%t|| iStat && (__eb == "
//                        + "&xdc_runtime_Error_IgnoreBlock)) {\n");
                    glob.out.printf("%txdc_runtime_Core_deleteObject__I(&%1"
                        + "Object__DESC__C, objp, %2, iStat, %3);\n",
                        glob.cname, fs, useCase == 0 ? 0 : 1);
                    if (useCase == 0) {
                        glob.out.printf("%treturn NULL;\n");
                    }
                    glob.out.printf("%-%t}\n\n");
                }
                else if (useCase == 0) {
                    glob.out.printf("%tif (xdc_runtime_Error_check(eb)) {\n%+");
                    glob.out.printf("%treturn NULL;\n");
                    glob.out.printf("%-%t}\n\n");
                }
            }
        }

        int mask = commonGetv(mod, "logger") != null
            ? getDiagsIncluded(mod) : 0;
        if ((mask & L_LIFECYCLE) != 0) {
            if (commonGeti(mod, "namedInstance") == 1) {
                glob.out.printf("%t%1write2(" + logCall + ", (xdc_IArg)%2,"
                    + "(xdc_IArg)(((%3Object__*)%2)->__name));\n", los, newObj,
                    glob.cname);
            }
            else {
                glob.out.printf("%t%1write2(" + logCall + ", (xdc_IArg)%2, 0);"
                    + "\n", los, newObj);
            }
        }

        if (useCase == 0) {
            glob.out.printf("%treturn objp;\n");
        }
        glob.out.printf("%-}\n");
    }

    /*
     *  ======== genSysFxns ========
     */
    private void genSysFxns(Value.Obj mod)
    {
        /* unitSpec sets some properties for Glob, so it has to be invoked
         * before invoking genModuleDefines, because that function depends on
         * these properties. We should fix this, so we get $spec only when we
         * need something specific out of it.
         */
        Unit unit = unitSpec(mod);
        genTitle(mod, "SYSTEM FUNCTIONS");
        /* If we generate create() function for this module, we must also
         * check if we need to log LIFECYCLE events, in which case we
         * need module defines used in Log_write and Diags_query. If we don't
         * use Log and Diags, these can be avoided.
         */
        Value.Obj logMod = modmap.get("xdc.runtime.Log");
        Value.Obj diagsMod = modmap.get("xdc.runtime.Diags");
        if ((logMod != null && logMod.geti("$used") != 0)
            || (diagsMod != null && diagsMod.geti("$used") != 0)) {
            if (mod.geti("$$instflag") == 1) {
                if (isRom) {
                    glob.genModuleDefines();
                }
                else {
                    glob.genModuleDefines2();
                }
            }
        }

        if (mod.geti("PROXY$") == 1) {
            // Proxy__abstract
            glob.out.printf("\nxdc_Bool %1Proxy__abstract__S(void)\n{\n%+",
                glob.cname);
            glob.out.printf("%treturn %1;\n}\n%-",
                mod.geti("$$instflag") == 1
                && mod.geti("abstractInstances$") == 1 ? "1" : "0");
            // Proxy__delegate
            glob.out.printf("xdc_CPtr %1Proxy__delegate__S(void)\n{\n%+%treturn ", glob.cname);
            Value delmod = mod.getv("delegate$");
            String dcname = glob.mkCname(delmod.gets("$name"));
            if (commonGeti(delmod, "fxntab") == 1) {
                glob.out.printf("(const void *)&%1Module__FXNS__C;", dcname);
            }
            else {
                glob.out.printf("0;");
            }
            glob.out.printf("\n}\n%-");
            skip();
            return;
        }

        // Module__startupDone
        glob.genTitleD("Module__startupDone__S");
        glob.out.printf("xdc_Bool %1Module__startupDone__S( void )\n{%+\n",
            glob.cname);
        if (!unit.attrBool(Attr.A_ModuleStartup)) {
            glob.out.printf("%treturn 1;\n");
        }
        else {
            /* When generating startupDone__S function for the modules that
             * require a separate config C file, we have to create a complete
             * body of the function here. The normal flow would be to generate a
             * call to startupDone__F, and then the Startup template would
             * generate startupDone__F. However, we can't extract only
             * startupDone__F function for one module from the Startup
             * template's output and redirect it to a different C file.
             * Therefore, the Startup template is changed as to not generate at
             * all the startupDone__F functions for the modules that require
             * separate files. What it does instead is to create a target
             * function that maps module IDs to stateTab indices, and
             * that's the function invoked below.
             */
            if (fm.switchMod(mod) == FileManager.SEPARATE_FILE) {
                String name = mod.gets("$name").replaceAll("\\.", "_");
                glob.out.printf("%treturn (xdc_runtime_Startup_getState__I("
                    + "Module__MID) < 0);\n");
            }
            else {
                glob.out.printf("%treturn %1Module__startupDone__F();\n",
                    glob.cname);
            }
        }
        glob.out.printf("}\n%-");

        if (!unit.isInst()) {
            skip();
            return;
        }

        String cs = "xdc_runtime_Core_";
        String las = "xdc_runtime_Types_Label";
        String los = "xdc_runtime_Log_";

        int pol = commonGeti(mod, "memoryPolicy");

        // Handle__label
        glob.genTitleD("Handle__label__S");
        glob.out.printf("%1 *%2Handle__label__S(xdc_Ptr obj, %1 *lab) \n{\n%+",
            las, glob.cname);
        glob.out.printf("%tlab->handle = obj;\n");
        if (fm.switchMod(mod) == FileManager.SEPARATE_FILE) {
            glob.out.printf("%tlab->modId = Module__MID;\n");
        }
        else {
            glob.out.printf("%tlab->modId = %1;\n", mod.geti("Module__id"));
        }

        if (commonGeti(mod, "namedInstance") == 1) {
            glob.out.printf("%t%2assignLabel(lab, ((%1Object__*)obj)->__name, 1);\n", glob.cname, cs);
        }
        else {
            glob.out.printf("%tlab->named = FALSE;\n");
            /* If Text is used, we can reference it, otherwise use the default
             * values for Text_nameUnknown.
             */
            Value.Obj textMod = modmap.get("xdc.runtime.Text");
            if (textMod != null && textMod.geti("$used") != 0) {
                glob.out.printf
                    ("%tlab->iname = xdc_runtime_Text_nameUnknown;\n");
            }
            else {
                glob.out.printf
                    ("%tlab->iname = \"{unknown-instance-name}\";\n");
            }
        }
        glob.out.printf("\n%treturn lab;\n");
        glob.out.printf("}\n%-");

        // Params__init
        glob.genTitleD("Params__init__S");
        /* In xdc-z* we changed the Params__init__S signature to be "const
         * correct".  To maintain compatibility with packages built before
         * xdc-z*, we need to generate different function definitions
         */
        String fmt = "%2Void %1Params__init__S( %2Ptr prms, const void *src, %2SizeT psz, %2SizeT isz ) \n{\n%+";
        if (isBeforeConst(mod)) {
            fmt = "%2Void %1Params__init__S( %2Ptr prms, %2Ptr src, %2SizeT psz, %2SizeT isz ) \n{\n%+";
        }
        glob.out.printf(fmt, glob.cname, "xdc_");

        /* If STATIC_POLICY is used and the module doesn't have a static-only
         * Instance_init() function, no Instance_init() function will be called
         * so prms will not be used. No need to call assignParams. Most likely,
         * create() or construct() will be invoked later and they will fail, but
         * we don't want to fail prematurely in case the user has some way of
         * detecting that these functions should not be called. (ECL349505)
         */
        if (pol != STATIC_POLICY || unit.attrBool(Attr.A_InstanceInitStatic)) {
            glob.out.printf("%t%2assignParams__I(prms, (xdc_CPtr)(src ? src : &%1Object__PARAMS__C), psz, isz);\n",
                    glob.cname, cs);
        }
        glob.out.printf("}\n%-");

        // Object__get
        glob.genTitleD("Object__get__S");
        glob.out.printf("xdc_Ptr %1Object__get__S(xdc_Ptr oa, xdc_Int i)\n{"
            + "\n%+", glob.cname);
        glob.out.printf("%tif (oa != NULL) {\n%+%treturn ((%1Object__ *)oa) + i;\n%-%t}\n\n", glob.cname);
        /* We can detect that there are no static instances for this module, so
         * we can return NULL without any further checks.
         * Proxy modules and modules without instances are detected earlier in
         * the function, so we don't have to check for them here.
         */
        if (mod.getv("$instances").geti("length") == 0) {
            glob.out.printf("%treturn NULL;\n%-%t}\n\n");
        }
        else {
            /* MISRA hack. //490928
             * The const in the cast below triggers TI compiler warning #193:
             *     type qualifier is meaningless on cast type
             * we can hide this via
             *     #ifdef __ti__
             *     #pragma diag_suppress 193
             *     #endif
             */
            glob.out.printf("%t/* the bogus 'const' in the cast suppresses "
                + "Klocwork MISRA complaints */\n");
            glob.out.printf("%treturn ((%1Object__ * const)%1Object__table__C) "
                + "+ i;\n", glob.cname);
            glob.out.printf("}\n%-");
        }

        // Object__first
        glob.genTitleD("Object__first__S");
        glob.out.printf("xdc_Ptr %1Object__first__S( void ) \n{\n%+", 
            glob.cname);
        if (pol != STATIC_POLICY) {
            if (fm.switchMod(mod) == FileManager.SEPARATE_FILE) {
                glob.out.printf("%txdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)%1Object__DESC__C.modLink->next;\n\n", glob.cname);
                glob.out.printf("%tif (iHdr != (xdc_runtime_Types_InstHdr *)%1Object__DESC__C.modLink) {\n%+", glob.cname);
            }
            else {
                glob.out.printf("%txdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)%1Module__root__V.link.next;\n\n", glob.cname);
                glob.out.printf("%tif (iHdr != (xdc_runtime_Types_InstHdr *)&%1Module__root__V.link) {\n%+", glob.cname);
            }
            glob.out.printf("%treturn iHdr + 1;\n");
            glob.out.printf("%-%t}\n");
            glob.out.printf("%telse {%+\n");
            glob.out.printf("%treturn NULL;\n");
            glob.out.printf("%-%t}\n");
        }
        else {
            glob.out.printf("%treturn NULL;\n");
        }
        glob.out.printf("%-}\n");

        // Object__next
        glob.genTitleD("Object__next__S");
        glob.out.printf("xdc_Ptr %1Object__next__S( xdc_Ptr obj )\n{\n%+", glob.cname);
        if (pol != STATIC_POLICY) {
            glob.out.printf("%txdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;\n\n");
            if (fm.switchMod(mod) == FileManager.SEPARATE_FILE) {
                glob.out.printf("%tif (iHdr->link.next != (xdc_runtime_Types_Link *)%1Object__DESC__C.modLink) {\n%+", glob.cname);
            }
            else {
                glob.out.printf("%tif (iHdr->link.next != (xdc_runtime_Types_Link *)&%1Module__root__V.link) {\n%+", glob.cname);
            }
            glob.out.printf("%treturn (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;\n");
            glob.out.printf("%-%t}\n");
            glob.out.printf("%telse {%+\n");
            glob.out.printf("%treturn NULL;\n");
            glob.out.printf("%-%t}\n");
        }
        else {
            glob.out.printf("%treturn NULL;\n");
        }
        glob.out.printf("%-}\n");

        boolean iInitErr = unit.attrBool(Attr.A_InstanceInitError);
        boolean iFinalize = unit.attrBool(Attr.A_InstanceFinalize);

        int mask = commonGetv(mod, "logger") != null ?
            getDiagsIncluded(mod) : 0;

        String finSuffix = isBeforeRom(mod) ? "__F" : "__E";
        String fs = iFinalize ? ("(xdc_Fxn)" + glob.cname +
                    "Instance_finalize" + finSuffix) : "NULL";

        // Object__create
        glob.genTitleD("Object__create__S");

        /* Only modules are processed here, so we know it's not an interface
         * and also there are instances, otherwise this function would return
         * earlier.
         * If this module is built with 3.25 or earlier, its create__S
         * declaration is in its header file, and the signature is different
         * from what we would define here.
         */
        if (isBefore350(mod)) {
            glob.out.printf("#error The package '%1' is not compatible with "
                + "XDCtools used in this build. Please rebuild the package with"
                + " XDCtools 3.50 or later.",
                mod.getv("$package").gets("$name"));
            return;
        }

        genCreateSig();
        genCreateBody(mod, 0);

        glob.genTitleD("create");
        glob.out.printf("%1Handle %1create(", glob.cname);
        glob.genCreArgDecls(unit, Glob.Type$LCL, " ");
        glob.out.printf("const %1Params *paramsPtr, %2)\n{\n%+", glob.cname,
            Glob.ERRARG);
        genCreateBody2(mod, 1);
        // end of create

        // Object construct
        glob.genTitleD("construct");
        glob.out.printf("void %1construct(", glob.cname);
        glob.out.printf("%1Struct *objp", glob.cname);
        glob.genCreArgDecls(unit, Glob.Type$LCL, ", ");
        glob.out.printf("const %1Params *paramsPtr%2)\n{\n%+", glob.cname,
            glob.errArg(unit));
        genCreateBody(mod, 2);
        // end of construct

        // destruct
        glob.genTitleD("destruct");
        glob.out.printf("void %1destruct(%1Struct *obj)\n{\n%+", glob.cname);
        if ((mask & L_LIFECYCLE) != 0) {
            glob.out.printf("%t%1write1(%1L_destruct, (xdc_IArg)obj);\n", los);
        }
        /* using Core_delete verses Core_destruct is a code verses time
         * tradeoff.  Maybe we should always just use destruct?
         */
        glob.out.printf("%t%1%2Object__I(&%3Object__DESC__C, obj, %4, %5, TRUE"
            + ");\n", cs, (pol == DELETE_POLICY) ? "delete" : "destruct",
            glob.cname, fs, iInitErr ? "0" : "xdc_runtime_Core_NOSTATE");
        glob.out.printf("%-}\n");

        if (pol == DELETE_POLICY) {
            // Object__delete__S
            glob.genTitleD("Object__delete__S");
            glob.out.printf("xdc_Void %1Object__delete__S( xdc_Ptr instp ) \n{\n%+", glob.cname);
            if ((mask & L_LIFECYCLE) != 0) {
                glob.out.printf("%t%1write1(%1L_delete, (xdc_IArg)(*((%2Object**)instp)));\n", los, glob.cname);
            }
            glob.out.printf("%t%1deleteObject__I(&%2Object__DESC__C, *((%2Object**)instp), %3, %4, FALSE);\n",
                    cs, glob.cname, fs,
                    iInitErr ? "0" : "xdc_runtime_Core_NOSTATE");
            glob.out.printf("%t*((%1Handle*)instp) = NULL;\n", glob.cname);
            glob.out.printf("%-}\n");

            if (!isBeforeRom(mod)) {
                // delete
                glob.genTitleD("delete");
                glob.out.printf("void %1delete(%1Handle *instp)\n{\n%+",
                    glob.cname);
                glob.out.printf("%t%1Object__delete__S(instp);\n}\n%-", 
                    glob.cname);
            }
        }
        else { /* must be STATIC_POLICY or CREATE_POLICY */
            // Object__delete__S
            /* delete triggers an error */
            glob.genTitleD("Object__delete__S");
            glob.out.printf("xdc_Void %1Object__delete__S( xdc_Ptr instp ) \n{\n%+", glob.cname);
            genError(unit, "delete policy error");
            glob.out.printf("%-}\n");

            if (!isBeforeRom(mod)) {
                // delete
                glob.genTitleD("delete");
                glob.out.printf("void %1delete(%1Handle* instp)\n{\n%+",
                    glob.cname);
                genError(unit, "delete policy error");
                glob.out.printf("%-}\n", glob.cname);
            }
        }
    }

    /*
     *  ======== genSysInherits ========
     *
     *  This function generates virtual create and delete functions for
     *  interfaces inherited by the modules in a configuration. Only modules
     *  that are configured to have a function pointer table are taken into
     *  account.
     */
    private void genSysInherits(Value.Obj inter) {
        Unit unit = unitSpec(inter);

        if (isBeforeRom(inter) && unit.getCreator() != null) {
            glob.out.printf("#error The package %1 is not compatible with "
                + "XDCtools used in this build. Please rebuild the package with"
                + "XDCtools 3.50 or later.",
                inter.getv("$package").gets("$name"));
        }

        if (!unit.isInst() || isBeforeRom(inter)) {
            return;
        }
        glob.genTitle(unit.getQualName() + " VIRTUAL FUNCTIONS");
        /* __create in SysFxns2 has a signature of create__S. That signature
         * is common for all create__S functions so we can have one type
         * definition for that common part of the virtual table that includes
         * create() and delete(). To get rid of create__S, we would have to
         * generate Fxns types differently to accommodate different create()
         * signatures for different modules. That is not difficult to do, so if
         * we can stop using create__S for proxies, we could then get rid of
         * create__S functions completely.
         */
        glob.genTitleD("create");
        glob.out.printf("%1Handle %1create( %1Module mod", glob.cname);
        glob.genCreArgDecls(unit, Glob.Type$LCL, ", ");
        glob.out.printf("const %1Params *prms, %2 )\n{\n%+", glob.cname,
            Glob.ERRARG);
        if (unit.hasCreateArgs()) {
            glob.out.printf("%t%1Args__create args;\n", glob.cname);
            for (Decl.Arg arg : unit.getCreator().getArgs()) {
                glob.out.printf("%targs.%1 = %1;\n", arg.getName());
            }
        }
        glob.out.printf("%treturn (%1Handle) mod->__sysp->__create(",
            glob.cname);
        glob.out.printf("%1, ", unit.hasCreateArgs() ? "&args" : "0");
        glob.out.printf("(const xdc_UChar*)prms, sizeof (%1Params), eb);\n}\n%-", glob.cname);

        glob.genTitleD("delete");
        glob.out.printf("void %1delete( %1Handle *instp )\n{\n%+", glob.cname);
        glob.out.printf("%t(*instp)->__fxns->__sysp->__delete(instp);\n}\n%-",
            glob.cname);
    }

    /*
     *  ======== genTemplate ========
     */
    private void genTemplate(Value.Obj mod)
    {
        if (!(mod.has("TEMPLATE$", mod))) {
            return;
        }

        genTitle(mod, "TEMPLATE");

        String tplt = mod.gets("TEMPLATE$");
        String fname = tplt.startsWith("./") ?
            mod.getv("$package").gets("packageBase") + tplt : tplt;

        try {
            progGen.genList.addAll(    /* add recurs templates */
                Template.exec(fname, glob.out, mod, new Object[] {fname})
            );
        }
        catch (Exception e) {
            xdc.services.intern.xsr.Err.exit(e);
        }
    }

    /*
     *  ======== genTitle ========
     */
    private void genTitle(Value.Obj mod, String msg)
    {
        glob.genTitle(mod.getName() + ' ' + msg);
    }

    /*
     *  ======== genVal ========
     *
     *  This function generates the C representation of 'oval'. The prototype
     *  'proto' is needed to define what kind of value is being generated.
     *  The value 'oval' may be ambiguous, so the type is needed to separate,
     *  for example, between 'msg' as a name of a variable and 'msg' as a
     *  string.
     */
    private void genVal(Proto proto, Object oval, String cast)
    {
        String vs = "";
        if (proto instanceof Proto.Elm) {
            /* an element in an aggregate object */
            vs = oval((Proto.Elm)proto, oval);
        }
        else if (proto instanceof Proto.Map) {
            String errMsg = "Maps are not supported in the runtime code. They "
                + "must be declared as metaonly: "
                + ((Value.Map)oval).getName();
            xdc.services.intern.xsr.Err.exit(errMsg);
        }
        else if (proto instanceof Proto.Obj) {
            vs = valToStr((Value.Obj)oval);
        }
        else if (proto instanceof Proto.Str) {
            Value.Obj vobj = (Value.Obj)oval;
            if (vobj.geti("$hostonly") == 1) {      /// filtered in caller ???
                return;
            }
            genAgg(vobj);
            vs = "}";
        }
        else if (proto instanceof Proto.Arr) {
            vs = valToStr((Value.Arr)oval, cast);
        }
        else if (proto instanceof Proto.Enm) {
            vs = valToStr((xdc.services.intern.xsr.Enum)oval);
        }
        else if (proto instanceof Proto.Adr) {
            if (cast != null) {
                /* The only time cast is not null is when the call is from
                 * genModConfigs and cast is 'const CT__...'
                 */
                vs = "((" + cast + ')' + ptrToStr(oval, "") + ')';
            }
            else {
                String temp_sig =
                    Utils.setArgNames(((Proto.Adr)proto).getSig());
                vs = "((" + temp_sig + ')' + ptrToStr(oval, "") + ')';
            }
        }
        else if (proto instanceof Proto.Typedef) {
            /* encoded types */
            Proto.Typedef ptyp = (Proto.Typedef)proto;
            if (ptyp.getEncFxn() == null) {
                /* There is no encode function to call, so we are just using
                 * the declared metatype that might not even exist in the
                 * target code.
                 */
                genVal(ptyp.getBase(), oval, cast);
                return;
            }
            else {
                Object res = null;
                try {
                    Context cx = Context.getCurrentContext();
                    Scriptable scope = Global.getTopScope();
                    Scriptable thisObj = null;
                    res = ptyp.getEncFxn().call
                          (cx, scope, thisObj, new Object[] {oval});
                }
                catch (Exception e) {
                    xdc.services.intern.xsr.Err.exit(e);
                }
                if (res == null) {
                    vs = null;
                }
                else {
                    vs = res.toString();
                }
            }
        }
        glob.out.print(vs);
    }

    /*
     *  ======== getTargetObject ========
     */
    private Value.Obj getTargetObject(Value prog)
    {
        Scriptable build = (Scriptable)(prog.geto("build"));
        Value.Obj target = (Value.Obj)(build.get("target", build));
        return (target);
    }

    /*
     *  ======== getXdcVers ========
     */
    private String getXdcVers(Value.Obj mod)
    {
        String base = mod.getv("$package").gets("packageBase");
        String vers = pkgVers.get(base);

        if (vers == null) {
            /* call What to get version the package's xdc version */
            vers = Vers.getXdcString(base + "/package/package.defs.h");
            if (vers == null) {
                vers = Glob.xdctree();
                if (vers == null) {
                    System.out.println("warning: can't determine the version of"
                        + " xdc tools used to build " + base);
                    vers = "xdc-a00";
                }
            }

            /* cache the answer so we run Vers just once per package */
            pkgVers.put(base, vers);
        }

        return (vers);
    }

    /*
     *  ======== checkNogen ========
     *  checks for the value that means a target object for a config parameter
     *  should not be generated
     */
    private boolean checkNogen(Proto proto, Object fldval)
    {
        if (fldval == xdc.services.intern.xsr.Value.NOGEN) {
            return(true);
        }
        if (proto instanceof Proto.Typedef) {
            Proto.Typedef ptyp = (Proto.Typedef)proto;
            if (ptyp.getEncFxn() != null) {
                Object res = null;
                try {
                    Context cx = Context.getCurrentContext();
                    Scriptable scope = Global.getTopScope();
                    Scriptable thisObj = null;
                    res = ptyp.getEncFxn().call
                          (cx, scope, thisObj, new Object[] {fldval});
                }
                catch (Exception e) {
                    xdc.services.intern.xsr.Err.exit(e);
                }
                if (res == xdc.services.intern.xsr.Value.NOGEN) {
                    return(true);
                }
            }
        }
        return(false);
    }


    /*
     *  ======== isBeforeConst ========
     *  returns whether mod was build using a version of xdc before
     *  xdc-z00 (when Params__init__S signature changed).
     */
    private boolean isBeforeConst(Value.Obj mod)
    {
        String L = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        /* get the xdc version used to build mod's headers */
        String pvers = getXdcVers(mod);

        /* get a numeric value of the xdc series letter */
        int pi = L.indexOf(pvers.charAt(4));
        int vi = L.indexOf('z');

        /* compare the numeric values */
        return (pi < vi);
    }

    /*
     *  ======== isBeforeRom ========
     *  returns whether mod was built using a version of xdc before
     *  xdc-A00 (when we changed create generation).
     */
    private boolean isBeforeRom(Value.Obj mod)
    {
        String L = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        /* get the xdc version used to build mod's headers */
        String pvers = getXdcVers(mod);

        /* get a numeric value of the xdc series letter */
        int pi = L.indexOf(pvers.charAt(4));
        int vi = L.indexOf('A');

        /* compare the numeric values */
        return (pi < vi);
    }

    /*
     *  ======== isBefore350 ========
     *  returns whether mod was built using a version of xdc before
     *  xdc-D00.
     */
    private boolean isBefore350(Value.Obj mod)
    {
        String L = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        /* get the xdc version used to build mod's headers */
        String pvers = getXdcVers(mod);

        /* get a numeric value of the xdc series letter */
        int pi = L.indexOf(pvers.charAt(4));
        int vi = L.indexOf('D');

        /* compare the numeric values */
        return (pi < vi);
    }

    /*
     *  ======== val2mod ========
     */
    private String val2mod( Value val )
    {
        Value.Obj vobj = (Value.Obj)val.getRoot();
        String tn = vobj.getOrig().getProto().tname();

        return tn.substring(0, tn.lastIndexOf("."));
    }

    /*
     *  ======== aname ========
     */
    private String aname(Value.Arr arr)
    {
        String ns = arr.getPath().substring(0, arr.getPath().lastIndexOf('/')).replaceAll("[/#$.]", "_");
        String suf = arr.getProto().getDim() == -1 ? "__A" : "";     // redundant ???
        return ns + suf;
    }

    private boolean targetCheckDone = false;
    private boolean hasConstCustom = false;

    /*
     *  ======== clink ========
     */
    private void clink(String ns, String ts)
    {
        Object[] nameString = {ns};
        Object[] typeString = {ts};
        if (!targetCheckDone) {
            targetCheckDone = true;
            if (target.has("genConstCustom", target)) {
                hasConstCustom = true;
            }
        }

        if (hasConstCustom) {
            Context cx = Global.getContext();
            Scriptable gscope = Global.getTopScope();
            Scriptable constStr = cx.newArray(gscope, nameString);
            Scriptable typeStr = cx.newArray(gscope, typeString);
            Object res = target.invoke("genConstCustom", new Object[]
                         {constStr, typeStr});
            if (res != null) {
                glob.out.print(res.toString());
            }
        }
        else {
            /* This is for pre-3.21 targets. We don't even test any target
             * older than 3.25, so pre-3.21 targets should be rebuilt.
             */
            xdc.services.intern.xsr.Err.exit("The build target '"
                + target.get("name", target) + "' is too old. Please rebuild "
                + "the target package.");
        }
    }

    /*
     *  ======== commonGeti ========
     */
    private int commonGeti(Value mod, String name)
    {
        return ((Value.Obj)mod.getv("common$")).geti(name);
    }

    /*
     *  ======== commonGets ========
     */
    private String commonGets(Value mod, String name)
    {
        return ((Value.Obj)mod.getv("common$")).gets(name);
    }

    /*
     *  ======== commonGetv ========
     */
    private Value commonGetv(Value mod, String name)
    {
        return ((Value.Obj)mod.getv("common$")).getv(name);
    }

    /*
     *  ======== instArr ========
     */
    private String instArr(String cname)
    {
        return cname + "Object__table__V";
    }

    /*
     *  ======== iobjKind ========
     */
    private int iobjKind(Value.Obj mod)
    {
        if (mod.geti("PROXY$") == 1) {
            return IOBJ_PROXY;
        }
        else if (mod.geti("$$dlgflag") == 0) {
            return IOBJ_BASIC;
        }
        else if (mod.geti("$$sizeflag") == 1) {
            return IOBJ_DLGST;
        }
        else {
            return IOBJ_DLGPR;
        }
    }

    /*
     *  ======== needsDiagsMask ========
     */
    private boolean needsDiagsMask(Value.Obj mod)
    {
        int included = getDiagsIncluded(mod);
        if ((mod.get("Module__diagsEnabled", mod) ==
            xdc.services.intern.xsr.Value.NOGEN)) {
            return (included != 0);
        }
        return (included ^ mod.geti("Module__diagsEnabled")) != 0;
    }

    /*
     *  ======== getDiagsIncluded ========
     */
    private int getDiagsIncluded(Value.Obj mod)
    {
        if (mod.get("Module__diagsIncluded", mod) ==
            xdc.services.intern.xsr.Value.NOGEN) {
            return 0;
        }
        return (mod.geti("Module__diagsIncluded"));
    }

    /*
     *  ======== isasm ========
     */
    private boolean isasm(Value.Obj mod)
    {
        return asmset.contains(mod);
    }

    /*
     *  ======== modDeleg ========
     */
    private Value.Obj modDeleg(Value.Obj mod)
    {
        Value vobj = mod.getv("delegate$");
        return vobj == null ? mod : modDeleg(((Value.Obj)vobj).getOrig());
    }

    /*
     *  ======== otype ========
     */
    private int otype(String tname)
    {
        return (
            tname.endsWith(".Module") ? MOD :
            tname.endsWith(".Instance") ? INST :
            STRUCT
        );
    }

    /*
     *  ======== oval ========
     *
     *  This function is invoked for elements of aggregate objects
     */
    private String oval(Proto.Elm proto, Object elm)
    {
        if (elm == null) {
            return "0";
        }
        else if (elm instanceof CharSequence) {
            String s = elm.toString();
            if (!s.startsWith("\"")) {
                s = "\"" + s + "\"";
            }
            return s;
        }
        else if (elm instanceof Number) {
            String cast = (proto != null && proto.getCast() != null) ?
                proto.getCast() : "";
            String value = Proto.Elm.numval((Number)elm);
            String suffix = "";

            /* If we are casting constant to some of the unsigned integral
             * types, and it is a constant and not an expression, we add U
             * to satisfy MISRA requirements.
             * The content of cast is (xdc_<type>), that's why we start match
             * at the index 5.
             */
             if (!(cast.isEmpty())) {
                 if (cast.regionMatches(5, "UInt", 0, 4) ||
                    cast.regionMatches(5, "Bits", 0, 4)) {
                    suffix = "U";
                }
                else if (cast.regionMatches(5, "ULong", 0, 5)) {
                    suffix = "UL";
                }
                else if (cast.regionMatches(5, "ULLong", 0, 6)) {
                    suffix = "ULL";
                }
            }

            /* Value could be an expression enclosed in parentheses, or a
             * rational number, and in these case we don't want a suffix.
             */
            if (value.indexOf("(") != -1 || value.indexOf(".") != -1) {
                suffix = "";
            }
            if (value.toUpperCase().indexOf("0X") == 0) {
                if (value.toUpperCase().indexOf("P") != -1) {
                    suffix = "";
                }
            }
            else {
                if (value.toUpperCase().indexOf("E") != -1) {
                    suffix = "";
                }
            }

            return (cast + Proto.Elm.numval((Number)elm) + suffix);
        }
        else if (elm instanceof Boolean) {
            boolean bval = ((Boolean)elm).booleanValue();
            return (bval ? "1" : "0");
        }
        else {
            return "";
        }
    }

    /*
     *  ======== pkgSpec ========
     */
    private Pkg pkgSpec(Value.Obj pkg)
    {
        Object obj = pkg.get("$spec", null);

        if (obj == null) {
            xdc.services.intern.xsr.Err.exit("parser cannot find package '"
                + pkg.getName() + "' along the path '"
                + Path.curpath() + "'.");
        }

        if (obj instanceof Pkg) {
            return (Pkg)obj;
        }

        NativeJavaObject nobj = (NativeJavaObject)obj;
        return (Pkg)nobj.unwrap();
    }

    /*
     *  ======== ptrToStr ========
     *
     *  @params(qual)   it's always set to ""
     */
    private String ptrToStr(Object pobj, String qual)     // Proto.Adr
    {
        if (pobj == null) {
            return "NULL";
        }
        else if (pobj instanceof Extern) {
            Extern ext = (Extern)pobj;
            String ns = ext.isRaw() ? ext.getName() : ext.getName().replace('.', '_');  /// already has '_' ???
            if (ext.isFxnT()) {
                return "(" + ns + ")";
            }
            else if (ext.getSig() == null || (ext.getSig().endsWith("*") || ext.getSig().equals("xdc_Ptr"))) {
                return "((void*)&" + ns + ")";
            }
            else {
                return ns;
            }
        }
        else if (pobj instanceof Addr) {
            Addr addr = (Addr)pobj;
            return "((void*)0x" + Long.toHexString(addr.getVal()) + ")";
        }
        else if (pobj instanceof Value.Obj) {
            return "((void*)" + valToStr((Value.Obj)pobj) + ")";
        }
        else if (pobj instanceof Ptr) {
            return "(" + ptrToStr((Ptr)pobj) + ")";
            // was: return "((void*)" + ptrToStr((Ptr)pobj) + ")";
            // we removed the cast to eliminate klockwork warnings about Text
            // module state initialization which casts away the const charBase
            // and nodeBase tables; removing this cast should allow clients
            // to cast as necessary in their .xdc file (now that we have CPtr)
        }
        else if (pobj instanceof xdc.services.intern.xsr.Enum) {
            return "((void*)(int)" + valToStr((xdc.services.intern.xsr.Enum)pobj) + ")";
        }
        else if (pobj instanceof Number) {
            return "(" + valToStr(pobj) + ")";
        }
        else {
            // TODO -- deal with 'Arg'
            return "((void*)" + valToStr(pobj) + ")";
        }
    }

    /*
     *  ======== ptrToStr ========
     */
    private String ptrToStr(Ptr ptr)
    {
        if (ptr.getAgg() instanceof Value.Arr) {
            Value.Arr arr = (Value.Arr)ptr.getAgg();
            return("&" + aname(arr) + '[' + ((Number)ptr.getSel()).intValue()
                + ']');
        }

        Value.Obj vobj = ((Value.Obj)ptr.getAgg()).getOrig();
        String ts = ((Proto.Obj)vobj.getProto()).tname();
        int ot = otype(ts);

        if (ot == MOD) {
            String ns = glob.mkIname(ts.substring(0, ts.lastIndexOf('.')))
                + ptr.getSel().toString() + "__C" ;
            return "&" + ns;
        }

        if (ot == INST) {
            xdc.services.intern.xsr.Err.exit("bad pointer");
            return "0";
        }

        if (ot == STRUCT) {
            return valToStr(vobj) + '.' + ptr.getSel().toString();
        }

        return "0";
    }

    /*
     *  ======== skip ========
     */
    private void skip()
    {
        glob.out.printf("\n");
    }

    /*
     *  ======== unitSpec ========
     */
    private Unit unitSpec(Value.Obj mod)
    {
        Unit u;

        Object obj = mod.get("$spec", null);

        u = (Unit)(obj instanceof NativeJavaObject ? ((NativeJavaObject)obj).unwrap() : obj);

        if (glob.curUnit != u) {
            glob.curUnit = u;
            glob.cname = glob.mkCname(u.getQualName());
        }

        return u;
    }

    /*
     *  ======== undef ========
     */
    private boolean undef(Object oval)
    {
        return oval instanceof Undefined ||
            (oval instanceof Number && Double.isNaN(((Number)oval).doubleValue()));
    }

    /*
     *  ======== valToStr ========
     */
    private String valToStr(Value.Arr varr, String cast)    // Proto.Arr
    {
        Proto.Arr parr = varr.getProto();

        int len = varr.geti("length");
        String as = (len == 0) ? "0" : aname(varr);

        if (parr.getDim() > 0) {
            genArrVals(varr, "", "");
            glob.out.tab();
            return "}";
        }
        else if (parr.getLflag()) {
            if (len == 0) {
                return "{0, 0}";
            }
            else {
                String cts = varr.getCtype();
                /* For module config parameters of a vector type, any cast to
                 * the pointer to the undelying array must be qualified with
                 * 'const' because the definition of the type declares the
                 * pointer as an immutable pointer. Without 'const', MISRA will
                 * complain that the cast removes a qualifier. If 'cast' is not
                 * 'null' we know it's a module config parameter because they
                 * are the only ones that have 'cast' not 'null'.
                 */
                String qual = "";
                if (cast != null) {
                    qual = "const ";
                }
                return "{" + len + ", ((" + cts + " " + qual + " *)" + as
                   + ")}";
            }
        }
        else if (cast != null) {
            return "((" + cast + ')' + as + ')';
        }
        else {
            return "((void*)" + as + ')';
        }
    }

    /* This is invoked for an element in an aggregate structure */
    private String valToStr(Object elmval)     // Proto.Elm
    {
        return oval(null, elmval);
    }

    private String valToStr(Proto.Elm proto, Object elmval)     // Proto.Elm
    {
        return oval(proto, elmval);
    }

    private String valToStr(xdc.services.intern.xsr.Enum enmval) // Proto.Enm
    {
        return enmval.getName().replace('.', '_');
    }

    /* This function is invoked whenever a reference to a Module_State or to
     * an instance state is needed.
     */
    private String valToStr(Value.Obj vobj)    // Proto.Obj
    {
        if (vobj == null) {
            return "0";
        }

        String tname = vobj.getProto().tname();
        String otname = vobj.getOrig().getProto().tname();
        int ot = otype(otname);

        String cn =
            otname.substring(0, otname.lastIndexOf('.')).replace('.', '_');
        String an = tname.replace('.', '_');

        if (ot == MOD) {
            return "(" + an + ")&" + cn + "_Module__FXNS__C";
        }

        if (ot == INST) {
            Value.Obj dobj = (Value.Obj)vobj.getv("delegate$");
            if (dobj != null) {
                vobj = dobj;
                otname = vobj.getOrig().getProto().tname();
                cn = otname.substring(0, otname.lastIndexOf('.')).replace('.', '_');
            }
            an = tname.substring
                (0, tname.lastIndexOf('.')).replace('.', '_') + "_Handle";
            return "(" + an + ")&" + instArr(cn + '_') + "["
                + vobj.geti("$index") + "]";
        }

        int nxt;
        if ((nxt = vobj.getPath().indexOf('/')) == -1) {
            return "";
        }

        Stack pstk = new Stack();
        for (Value v = vobj; v != null; v = v.getPrnt()) {
            pstk.push(v);
        }

        String res = vobj.getPath().substring(0, nxt);
        boolean under = true;

        int k = res.indexOf("#");
        if (k == -1) {
            res = cn;
        }
        else {
            String pre = res.substring(0, k);
            int k2;
            if ((k2 = pre.indexOf(".Instance_State")) != -1) {
                cn = pre.substring(0, k2).replace('.', '_');
//                an = tname.substring(0, tname.lastIndexOf('.')).replace('.', '_') + "_Handle";
                res = instArr(cn + '_') + "[" + res.substring(k + 1) + "]";
//                res = "(*(" + an + ")&" + res + ")";
            }
            else if ((k2 = pre.indexOf(".Module_State")) != -1) {
                cn = pre.substring(0, k2).replace('.', '_');
///                res = cn + "__MODULE__";
                res = cn + "_Module__state__V";
            }
            else {
                res = res.replace('.', '_');
                res = res.replaceFirst("#(\\d+)", "_$1__T");
            }
            under = false;
        }

        for (int cur = nxt + 1; (nxt = vobj.getPath().indexOf('/', cur)) != -1;
             cur = nxt + 1) {
            Value v = (Value)pstk.pop();
            String sel = vobj.getPath().substring(cur, nxt);
            if (v instanceof Value.Arr) {
                Value.Arr arr = (Value.Arr)v;
                if (arr.getProto().getDim() > 0) {
                    res += "[" + sel + "]";
                }
                else {
                    res = aname((Value.Arr)v) + "[" + sel + "]";
                }
                continue;
            }
            res += (under ? '_' : '.')
                + (sel.equals("$delegate") ? "__deleg" : sel);
            if (under && !(sel.equals("$delegate"))) {
                res += "__C";
            }
            under = false;
        }

        if (tname.endsWith(".Instance")) {
            an = tname.substring
                (0, tname.lastIndexOf('.')).replace('.', '_') + "_Handle";
            return "(" + an + ")&" + res;
        }
        else {
            return "&" + res;
        }
    }
}
