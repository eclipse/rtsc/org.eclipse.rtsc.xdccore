/* --COPYRIGHT--,EPL
 *  Copyright (c) 2013 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== FileManager.java ========
 *  Manage output files during configuration
 */
package xdc.services.intern.gen;

import xdc.services.intern.xsr.Value;

import xdc.services.global.Out;

import java.io.File;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.BufferedReader;
import java.io.FileReader;

import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Map;
import java.util.HashMap;

/*
 *  ======== FileManager ========
 */
public class FileManager extends Out
{
    /* tmpDummy is here only to be a parameter for the superclass's
     * constructor. We do not write anything to this file.
     */
    private static ByteArrayOutputStream tmpDummy;

    static {
        tmpDummy = new ByteArrayOutputStream(4);
    }

    private class FileOutPair
    {
        File moduleFile;
        OutputStream moduleOut;

        FileOutPair(File mf, OutputStream mo) {
            moduleFile = mf;
            moduleOut = mo;
        }
    }

    public static final int COMMON_FILE = 0;
    public static final int SEPARATE_FILE = 1;
    public static final int NO_FILE = 2;

    private String cfgName;

    /* This flag signals that a printf command should write to every file. */
    private boolean allFlag = false;

    private HashMap<Value.Obj, FileOutPair> filemap =
        new HashMap<Value.Obj, FileOutPair>();
    private FileOutPair mainFile;

    /*
     *  ======== FileManager ========
     */
    FileManager(String cfgName, Value om)
        throws FileNotFoundException
    {
        super(tmpDummy);
        try {
            this.cfgName = cfgName;
            String cfgBaseDir =
                cfgName.substring(0, cfgName.lastIndexOf('/') + 1);
            String cfgBaseName = cfgName.substring(
                cfgName.lastIndexOf('/') + 1);
            File dir = new File(cfgBaseDir);
            File file = File.createTempFile(cfgBaseName, ".tmp", dir);
            file.deleteOnExit();

            OutputStream out = new BufferedOutputStream(
                               new FileOutputStream(file));
            mainFile = new FileOutPair(file, out);
            this.out = out;

            /* create temporary files for modules who are configured to have
             * separate C output files
             */
            Value modarr = om.getv("$modules");
            for (int i = 0; i < modarr.geti("length"); i++) {
                Value.Obj m = ((Value.Obj)(modarr.getv(i))).getOrig();
                if (m.geti("$used") == 0 || m.geti("$hostonly") == 1
                    || m.geti("$$nortsflag") != 0
                    || m.getv("$package").geti("$$genflg") != 1) {
                    continue;
                }

                int policy = ((Value.Obj)m.getv("common$")).geti("outPolicy");
                if (policy == SEPARATE_FILE) {
                    File mfile = File.createTempFile(m.gets("$name"),
                        ".tmp", dir);
                    OutputStream mout = new BufferedOutputStream(
                        new FileOutputStream(mfile));
                    FileOutPair fop = new FileOutPair(mfile, mout);
                    filemap.put(m, fop);
                    mfile.deleteOnExit();
                }
                else if (policy == NO_FILE) {
                    filemap.put(m, null);
                }
            }
        }
        catch (FileNotFoundException fnfe) {
            xdc.services.intern.xsr.Err.exit(fnfe);
        }
        catch (IOException e) {
            xdc.services.intern.xsr.Err.exit(e);
        }
    }

    /*
     *  ======== switchAll ========
     *
     *  Turn on printing to multiple files at once.
     */
    public void switchAll()
    {
        this.allFlag = true;
    }

    /*
     *  ======== switchCommon ========
     *
     *  The output stream is set to the main output file.
     */
    public void switchCommon()
    {
        this.out = mainFile.moduleOut;
        this.allFlag = false;
    }

    /*
     *  ======== switchMod ========
     *
     *  Changes the output stream to the one allocated to module 'mod' and
     *  returns the module's outPolicy.
     *
     *  For the modules with NO_FILE policy, the output stream is set to 'null'.
     */
    public int switchMod(Value.Obj mod)
    {
        this.allFlag = false;
        if (!filemap.containsKey(mod)) {
            this.out = mainFile.moduleOut;
            return (COMMON_FILE);
        }

        FileOutPair fop = filemap.get(mod);
        if (fop == null) {
            this.out = null;
            return (NO_FILE);
        }
        else {
            this.out = fop.moduleOut;
            return (SEPARATE_FILE);
        }
    }

    /*
     *  ======== switchString ========
     *  Prints into a new output stream supplied by the caller
     *
     *  FileManager doesn't keep track of 'temp' anywhere, so if the caller
     *  needs to print into 'temp' later again, the caller has to supply the
     *  reference.
     *
     *  The intended use for this method is to supply an output stream that can
     *  be easily turned into a string once the streaming is over.
     */
    public OutputStream switchString(ByteArrayOutputStream temp)
    {
        this.allFlag = false;
        OutputStream oldout = this.out;
        this.out = temp;
        return (oldout);
    }

    /*
     *  ======== isCurrentModule ========
     */
    public boolean isCurrentModule(Value.Obj mod)
    {
        if (filemap.containsKey(mod)) {
            FileOutPair fop = filemap.get(mod);
            if (fop != null) {
                return (fop.moduleOut == this.out);
            }
            else {
                return (this.out == null);
            }
        }
        return (false);
    }

    /*
     *  ======== printMod ========
     */
    public void printMod(String fmt, Value.Obj mod)
    {
        FileOutPair fop = filemap.get(mod);
        if (fop != null) {
            this.out = fop.moduleOut;
            doPrint(fmt, "", "", "", "", "");
        }
    }

/*
 *  ======== printAll ========
 */
    private void printAll(String fmt, Object obj1, Object obj2, Object obj3,
                          Object obj4, Object obj5)
    {
        this.out = mainFile.moduleOut;
        doPrint(fmt, obj1, obj2, obj3, obj4, obj5);
        for (Value.Obj m : filemap.keySet()) {
            FileOutPair fop = filemap.get(m);
            if (fop != null) {
                this.out = fop.moduleOut;
                doPrint(fmt, obj1, obj2, obj3, obj4, obj5);
            }
        }
    }

/*
 *  ======== printf ========
 */
    public void printf(String fmt) {
        if (this.out == null) {
            return;
        }
        if (!this.allFlag) {
            super.printf(fmt);
            return;
        }

        printAll(fmt, "", "", "", "", "");
    } 

    public void printf(String fmt, Object obj) {
        if (this.out == null) {
            return;
        }
        if (!this.allFlag) {
            super.printf(fmt, obj);
            return;
        }

        printAll(fmt, obj, "", "", "", "");
    }

    public void printf(String fmt, Object obj1, Object obj2) {
        if (this.out == null) {
            return;
        }
        if (!this.allFlag) {
            super.printf(fmt, obj1, obj2);
            return;
        }

        printAll(fmt, obj1, obj2, "", "", "");
    }

    public void printf(String fmt, Object obj1, Object obj2, Object obj3) {
        if (this.out == null) {
            return;
        }
        if (!this.allFlag) {
            super.printf(fmt, obj1, obj2, obj3);
            return;
        }

        printAll(fmt, obj1, obj2, obj3, "", "");
    }

    /*
     *  ======== closeAll ========
     */
    public void closeAll()
        throws IOException
    {
        this.close();
        mainFile.moduleOut.close();
        for (Value.Obj m : filemap.keySet()) {
            FileOutPair fop = filemap.get(m);
            if (fop != null) {
                fop.moduleOut.close();
            }
        }
        //tmpDummy.delete();
    }

    /*
     *  ======== moveFiles ========
     */
    public void moveFiles()
        throws IOException
    {
        this.out.close();

        /* always update big.c: the comparison can be expensive and
         * it ensures dependencies between <app>.cfg and <big>.c are
         * properly handled; i.e., if date of <big>.c is later than
         * <app>.cfg, there is no need to re-reun config.
         */
        File permFile = new File(cfgName + ".c");
        permFile.delete();
        mainFile.moduleOut.close();
        if (!mainFile.moduleFile.renameTo(permFile)) {
            throw new IOException("file rename failed: " + 
                                  mainFile.moduleFile.getAbsolutePath());
        }

        for (Value.Obj m : filemap.keySet()) {
            FileOutPair fop = filemap.get(m);
            if (fop != null) {
                permFile = new File(cfgName + "_"
                    + m.gets("$name").replace('.', '_') + "_config.c");
                if (!compareFiles(fop.moduleFile, permFile)) {
                    permFile.delete();
                    fop.moduleOut.close();
                    if (!fop.moduleFile.renameTo(permFile)) {
                        throw new IOException("file rename failed: " + 
                            fop.moduleFile.getAbsolutePath());
                    }
                }
            }
        }
    }

    /*
     *  ======== compareFiles ========
     *  Return true if file a and b are identical; otherwise return false.
     */
    public static boolean compareFiles(File a, File b)
    {
        if (a.length() != b.length()) {
            return (false);
        }

        BufferedReader f = null, g = null;
        try {
            f = new BufferedReader(new FileReader(a));
            g = new BufferedReader(new FileReader(b));

            String fline = f.readLine();
            String gline = g.readLine();
            while (fline != null && gline != null) {
                if (!fline.equals(gline)) {
                    return (false);
                }
                fline = f.readLine();
                gline = g.readLine();
            }

            if (fline != null || gline != null) {
                return (false);
            }
        }
        catch (Exception e) {
            return (false);
        }
        finally {
            if (f != null) {
                try {
                    f.close();
                }
                catch (java.io.IOException e) {
                    ;
                }
            }
            if (g != null) {
                try {
                    g.close();
                }
                catch (java.io.IOException e) {
                    ;
                }
            }
        }
        return (true);
    }
}
