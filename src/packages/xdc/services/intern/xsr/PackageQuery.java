/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== xdc.services.global.PackageQuery ========
 */

package xdc.services.intern.xsr;

import java.io.*;
import java.util.*;
import xdc.services.spec.*;

/*
 *  ======== PackageQuery ========
 */

/**
 *  A helper class that allows clients to query useful information
 *  regarding packages. 
 */

public class PackageQuery
{
   /**
     *  
     * @param  moduleName       Canonical name of the module 
     * @param  elemName         Name of the element in the module that needs
     *                          to be queried
     * @return                  Returns the Atom corresponding to the
     *                          elemName. If the element is not found 'null'
     *                          is returned.                            
     */
    public static Atom findAtom(String moduleName, String elemName)
    {
        BrowserSession ses = new BrowserSession();
        Atom a = null;    
            
        int index = moduleName.lastIndexOf(".");
        if (index == -1) {
            return (null);
        }
            
        Pkg pkg = ses.findPkg(moduleName.substring(0,index));
        if (pkg == null) {
            return (null);
        }

        Unit unit = ses.findUnit(pkg,moduleName.substring(index+1));
        if (unit == null) {
            return (null);
        }
        
        Object[] decls = unit.getDecls().toArray();
        for (int i = 0; i < decls.length; i++) {
            if (((Decl)decls[i]).getName().equals(elemName)) {
                a = ((Decl)decls[i]).getAtom();
                break;
            }
        }
        
        return (a);
    }
}
