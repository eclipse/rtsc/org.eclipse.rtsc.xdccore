/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

/**
 * Instance of this class is a value assigned to a config of an enumerated
 * type.
 */
public class Enum extends XScriptO
    implements AnyType
{
    Proto.Enm proto;
    String ename;
    Integer ival;
    
    static public Enum make( Proto.Enm proto, String ename, int val)
    {
        Integer ival = new Integer(val);
        Enum res = (Enum)proto.valmap.get(ival);
        
        if (res == null) {
            res = new Enum();
            res.proto = proto;
            res.ival = ival;
            res.ename = ename == null ? ("#" + res.ival) : ename;
            res.bindtab.put("$category", "EnumVal");
            res.bindtab.put("$name", res.ename);
            res.bindtab.put("$value", res.ival);
            proto.valmap.put(res.ival, res);
        }
        
        return res;
    }
    
    static public int intValue( Object e )
    {
        if (e instanceof Enum) {
            return ((Enum)e).ival;
        }
        else {
            return ((Number)e).intValue();
        }
    }
    
    public String toString()
    {
        int k = this.ename.lastIndexOf('.');
        return k == -1 ? this.ename : this.ename.substring(k + 1);
    }
    
    // OVERRIDE Scriptable
    
    public Object getDefaultValue( java.lang.Class hint )
    {
        return hint == java.lang.String.class ? (Object)this.ename : (Object)this.ival;
    }
}
