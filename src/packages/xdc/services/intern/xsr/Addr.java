/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/

package xdc.services.intern.xsr;

/**
 * Instances of this class are generated when the operator $addr is invoked.
 * While a constant address can be assigned to an array or a string, the
 * generated file inserts 0 in place of the assigned number for arrays, and
 * in case of strings an invalid C expression is being generated.
 * For pointers, the only difference between $addr and a constant number is in
 * the additional (void*) cast applied to the operand of $addr.
 */
public class Addr extends XScriptO
    implements AnyType
{
    long val;
    String name;
    
    public Addr( long val )
    {
        this.val = val;
        
        this.name = "0x";
        
        String valStr = Long.toHexString(val);
        if (valStr.length() < 8) {
        
            /* Make sure the address is 8 hex digits. */
            int pad = 8 - valStr.length();
            for (int i = 0; i < pad; i++) {
                this.name += "0";
            }
        }
        
        this.name += valStr;
        
        this.setName(this.name);
        this.bindtab.put("$addr", new Long(this.val));
        this.bindtab.put("$category", "Addr");
    }
    
    public final long getVal()
    {
        return (this.val);
    }
    
    public Object getDefaultValue( java.lang.Class hint )
    {
        return (hint == java.lang.String.class ?
            (Object)this.name : new Long(this.val));
    }
    
    public String toString()
    {
        return (this.name);
    }
}
