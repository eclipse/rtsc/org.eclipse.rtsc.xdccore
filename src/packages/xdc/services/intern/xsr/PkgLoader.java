/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.io.FileInputStream;
import java.io.File;

import java.net.URLClassLoader;
import java.net.URL;
import java.util.HashMap;

import org.mozilla.javascript.Scriptable;

import xdc.services.spec.BrowserSession;
import xdc.services.spec.Pkg;
import xdc.services.spec.Session;

/*
 *  ======== PkgLoader ========
 */
public class PkgLoader
{
    static private PkgClassLoader clsLoader = new PkgClassLoader();
    
    private Scriptable xdc;
    private BrowserSession ses;

    /*
     *  ======== PkgLoader ========
     */
    public PkgLoader(Scriptable xdc, BrowserSession ses)
    {
        this.xdc = xdc;
        this.ses = ses;
    }

    /*
     *  ======== exec ========
     */
    public void exec(String pkgName, File clsFile) 
        throws InvocationTargetException
    {      
        /* find/load the pkg's schema */
        Class cls = PkgLoader.clsLoader.findPkgClass(pkgName, clsFile);

        /* run the the pkg's exec() method to define its types, etc. */
        try {
            Method met = cls.getMethod("exec", Scriptable.class, Session.class);
            Object inst = cls.newInstance();
            met.invoke(inst, this.xdc, this.ses);

        }
        catch (NoSuchMethodException e) {
            Err.exit(e);
        }
        catch (InstantiationException e) {
            Err.exit(e);
        }
        catch (IllegalAccessException e) {
            Err.exit(e);
        }
//        catch (Exception e) {
//            e.printStackTrace();
//            if (e.getCause() != null) {
//                System.err.print("caused by: ");
//                e.getCause().printStackTrace();
//            }
//        }
    }
    
    /*
     *  ======== getVersion ========
     *  Get version of xdc used to generate the schema
     */
    public String getVersion(String pkgName) 
    {
        String vers;

        /* get the pkg's schema */
        Class cls = PkgLoader.clsLoader.findPkgClass(pkgName);

        /* read the VERS field */
        try {
            Field fld = cls.getDeclaredField("VERS");
            fld.setAccessible(true);

            /*
             *  The VERS field is a "what" string of the form:
             *     "@(#) xdc-xxx\n"
             *  So, to provide a cleaner return value, we extract just the
             *  version substring.
             */
            vers = ((String)fld.get(null)).substring(5, 12);
        }
        catch (Exception e) {
            vers = "xdc-a00";
        }

        return (vers);
    }

    /*
     *  ======== PkgClassLoader ========
     */
    static class PkgClassLoader extends URLClassLoader
    {
        private HashMap<String,Class> pkgmap;

        PkgClassLoader()
        {
            super(new URL[0], PkgLoader.class.getClassLoader());
            this.pkgmap = new HashMap(64);
        }
        
        /*
         *  ======== findPkgClass ========
         */
        Class findPkgClass(String pkgName)
        {
            String clsname = pkgName.replace('.', '_');

            return (pkgmap.get(clsname));
        }

        Class findPkgClass(String pkgName, File clsFile)
        {
            String clsname = pkgName.replace('.', '_');

            /* if the pkg schema is already loaded, there's nothing to do */
            Class cls = pkgmap.get(clsname);
            if (cls != null) {
                return (cls);
            }

            /* otherwise, load the class */
            byte[] bytes = null;
            try {
                FileInputStream fin = new FileInputStream(clsFile);
                bytes = new byte[fin.available()];
                fin.read(bytes);
                fin.close();
                cls = super.defineClass(clsname, bytes, 0, bytes.length);
            }
            catch (Exception e) {
                Err.exit(e);
            }

            /* and add it to the pkgmap so we can find it again later */
            pkgmap.put(clsname, cls);

            return (cls);
        }
    }
}
