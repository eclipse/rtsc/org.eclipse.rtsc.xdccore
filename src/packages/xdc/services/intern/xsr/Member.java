/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.Function;

public class Member
    implements Cloneable
{
    String name;
    Proto proto;
    Proto.Obj owner;
    Object defval;
    Function getter;
    Function setter;
    int idx;
    boolean hflag;
    boolean rflag;
    
    protected Object clone()
        throws CloneNotSupportedException
    {
        return super.clone();
    }
    
    public final String getName() { return this.name; }
    public final Proto getProto() { return this.proto; }
    
    public final boolean isMeta() { return this.hflag; }
    public final boolean isReadonly() { return this.rflag; }
}
