/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

import org.mozilla.javascript.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;


/*
 *  ======== Utils ========
 */
public class Utils
{
    /*
     *  ======== escapeHTML ========
     *
     *  @param input the string with special HTML characters
     *  @return new string with escaped special HTML characters
     */
    static public String escapeHTML(String input)
    {
        String output = input.replace("&", "&amp;");
        output = output.replace("<", "&lt;").replace(">", "&gt;");
        return (output);
    }

    /*
     *  ======== expandString ========
     *
     *  @param cmd the string to expand
     *  @param values hashtable of name-value pairs
     *  @return new expanded string
     */
    static public String expandString(String cmd, Scriptable values)
    {
	StringBuffer result = new StringBuffer(64);
	int recurse = 0;
	
	/* loop until no more $() tokens appear in cmd */
	int start;
	while ((start = cmd.indexOf("$(")) >= 0) {
	    /* locate the end of the token */
	    int end = cmd.indexOf(')', start + 2);
	    if (end < 0) {
		break;
	    }

	    /* key is the stuff between the '$(' and the first ')' */
	    String key = cmd.substring(start + 2, end);
	    /* find the inner-most $() pair */
	    int tmp;
	    if ((tmp = key.lastIndexOf("$(")) >= 0) {
		start += tmp + 2;	    /* we found an nested $( token */
		key = key.substring(tmp + 2);
		recurse += 1;		    /* so we need to run again */
	    }
	    /* look up the replacement for the token */
	    Object val = values.get(key, values);
	    if (val == Scriptable.NOT_FOUND) {
		/* leave the token in the string */
		result.append(cmd.substring(0, end + 1));
		recurse -= 1;
	    }
	    else if (val != Context.getUndefinedValue()) {
		/* replace the token in the string */
		String sval = val.toString();
		result.append(cmd.substring(0, start));
		if (sval.indexOf('$') >= 0) {
		    sval = expandString(sval, values);
		}
		result.append(sval);
	    }
	    else {
		/* remove the token from the string */
		result.append(cmd.substring(0, start));
		recurse -= 1;
	    }

	    /* advance to the part after the token */
	    cmd = cmd.substring(end + 1);
	}
	/* append remainder of cmd (after all $() tokens) to result */
	result.append(cmd); 
	return (recurse > 0? expandString(result.toString(), values) : result.toString());
    }

    /*
     *  ======== saveFile ========
     *  Save string to a file only if the string differs from what is
     *  already in the file.
     *
     *  @param content the contents of the file to save (in a string)
     *  @param fileName the name of the file to create/update (if necessary)
     *  @return true if the file was not updated; otherwise false
     */
    static public boolean saveFile(String content, String fileName)
    {
	File file = new File(fileName);
	if (file.exists()) {
            BufferedReader oldFile = null;
	    try {
		String old;
		oldFile = new BufferedReader(new FileReader(file));
		int start = 0;

		while ((old = oldFile.readLine()) != null) {
		    int len = old.length();
		    String ns = content.substring(start, start + len);
		    if (!old.equals(ns) || content.charAt(start+len) != '\n'){
			break;
		    }
		    start += len + 1;
		}
		if (old == null && content.length() <= start) {
		    return (true);
		}
	    }
	    catch (Exception e) {
		;
	    }
            finally {
                if (oldFile != null) {
                    try {
                        oldFile.close();
                    }
                    catch (IOException e) {
                        ;
                    }
                }
            }

	    /* if we get here, the file exists and differs from content */
	    file.delete();	/* force removal of old file */
	}
	else {
	    /* ensure the file's directories exist; so we can write it below */
	    File dir = file.getParentFile();
	    if (dir != null) {
	        if (!dir.exists() && !dir.mkdirs()) {
		    Err.exit("can't create directory " + dir.getName());
	        }
	        else if (dir.exists() && !dir.isDirectory()) {
	            Err.exit("Can't create directory " + dir.getName()
	                + ". Normal file with the same name already exists.");
	        }
	    }
	}

	/* if we get here the contents are different, create new file */
	try {
	    Writer out = new BufferedWriter(new FileWriter(file));
	    out.write(content);
	    out.close();
	}
	catch (Exception e) {
	    Err.exit(e);
	}

	return (false);
    }

    /*
     *  ======== setArgNames ========
     *  Add argument names to function signatures.
     *
     *  @param sig function signature Type[*](*)(Type1[,TypeI][,...]
     *  @return the same signature with arg names added
     *          Type[*](*)(Type0 f_arg0[,TypeN f_argNI][,...]
     */
    static public String setArgNames(String sig) {
        if (sig == null) {
            return (null);
        }

        // we only care about function type casts, and only about args part
        Pattern pat = Pattern.compile("([\\w]+\\*?\\(\\*\\))\\((.+)\\)");
        Matcher mat = pat.matcher(sig);
        if (!mat.find()) {
            return (sig);
        }
        StringBuilder res = new StringBuilder();
        /* The first group is Type[*](*), while the arguments are in the second
         * group.
         */
        res = res.append(mat.group(1)).append("(");
        String args = mat.group(2);
        StringBuilder type = new StringBuilder();
        int openBr = 0;  // keeps track of open parentheses
        int counter = 0;
        boolean simple = true;  // type that's not a function type

        for (int i = 0; i < args.length(); i++) {
            if (args.charAt(i) == ',' && openBr == 0) {
                if (!simple) {
                    res.append(setArgNames(type.toString()));
                    type = new StringBuilder();
                }
                else {
                    res.append(type.append(" f_arg" + counter++));
                }
                res.append(",");
                type = new StringBuilder();
            }
            else if (i == args.length() - 1) {
                type.append(args.charAt(i));
                if (!simple) {
                    res.append(setArgNames(type.toString()));
                }
                else if (type.toString().equals("xdc_Void")
                    || type.toString().equals("...")) {
                    res.append(type);
                }
                else {
                    res.append(type.append(" f_arg" + counter++));
                }
                type = new StringBuilder();
            }
            else if (args.charAt(i) == '(') {
                simple = false;
                openBr++;
                type.append(args.charAt(i));
            }
            else if (args.charAt(i) == ')') {
                openBr--;
                type.append(args.charAt(i));
            }
            else {
                type.append(args.charAt(i));
            }
        }
        return (res.append(")").toString());
    }

    /*
     *  ======== test1 ========
     *  Simple unit test of expandString
     */
    static public void test1(String [] args)
    {
    }

    /*
     *  ======== test2 ========
     *  Simple unit test of saveFile
     */
    static public void test2(String [] args)
    {
	int N = 1000;
	String prefix = "this is a ";
	for (int i = 0; i < 10; i++) {
	    prefix += prefix;
	}
	
	/* create the files */
	System.out.println("creating files ...");
	String content = prefix + "\n";
	for (int i = 0; i < N; i++) {
	    saveFile(content, "junk/foo" + i + ".txt");
	    content += i + " test\n";
	}
	
	/* create files in reverse order (all should fail) */
	System.out.println("creating different files ...");
	content = prefix + '\n';
	for (int i = 0; i < N; i++) {
	    if (saveFile(content, "junk/foo" + ((N - 1) - i) + ".txt")) {
		System.out.println("test failed!");
		System.exit(1);
	    }
	    content += i + " test\n";
	}
	
	/* create files in reverse order again (all should pass) */
	System.out.println("re-creating files ...");
	content = prefix + '\n';
	for (int i = 0; i < N; i++) {
	    if (!saveFile(content, "junk/foo" + ((N - 1) - i) + ".txt")) {
		System.out.println("test failed!");
		System.exit(1);
	    }
	    content += i + " test\n";
	}

	/* remove generated files */
	System.out.println("removing files ...");
	for (int i = 0; i < N; i++) {
	    File file = new File("junk/foo" + i + ".txt");
	    file.delete();
	}
	File dir = new File("junk");
	dir.delete();
	System.out.println("test passed.");
    }

}
