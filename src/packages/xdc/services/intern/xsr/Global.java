/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 *  ======== Global.java ========
 */
package xdc.services.intern.xsr;

import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.ObjArray;

import java.util.*;
import java.lang.reflect.*;

public class Global
{
    private static Scriptable global = null;

    /*
     *  ======== callFxn ========
     */
    public static Object callFxn(String fxnName, Scriptable thisObj, Object... fxnArgs )
    {
        Function fxn = (Function)thisObj.get(fxnName, null);
        return fxn.call(Context.getCurrentContext(), thisObj, thisObj, fxnArgs);
    }
    
    /*
     *  ======== eval ========
     */
    public static Object eval(String source)
    {
        return Context.getCurrentContext().evaluateString(global, source, "$internal", 1, null);
    }
    
    /*
     *  ======== get ========
     */
    public static Object get(String propName)
    {
        return global.get(propName, global);
    }
    
    /*
     *  ======== get ========
     */
    public static Object get(Scriptable obj, String propName)
    {
        Object res = obj.get(propName, obj);
        return res == Scriptable.NOT_FOUND ? null : res;
    }
    
    /*
     *  ======== put ========
     */
    public static void put(Scriptable obj, String propName, Object val)
    {
        obj.put(propName, obj, val);
    }
    
    /*
     *  ======== getContext ========
     */
    public static Context getContext()
    {
	return (Context.getCurrentContext());
    }

    /*
     *  ======== getTopScope ========
     */
    public static Scriptable getTopScope()
    {
	return (global);
    }

    /*
     *  ======== newArray ========
     */
    public static Scriptable newArray(Object... elems)
    {
        return Context.getCurrentContext().newArray(global, elems);
    }

    /*
     *  ======== newObject ========
     */
    public static Scriptable newObject(Object... props)
    {
        Scriptable sobj = Context.getCurrentContext().newObject(global);
        for (int i = 0; i < props.length; i++) {
            String p = (String)props[i++];
            sobj.put(p, sobj, props[i]);
        }
        return sobj;
    }

    /*
     *  ======== setTopScope ========
     */
    public static void setTopScope(Scriptable scope)
    {
	global = scope;
    }

    /*
     *  ======== findMethods ========
     */
    public static Method[] findMethods(Class<Object> clazz, String name) {
        return findMethods(getMethodList(clazz), name);
    }

    private static Method[] findMethods(Method[] methods, String name) {
        /* Usually we're just looking for a single method, so optimize
	 * for that case.
	 */
        ObjArray v = null;
        Method first = null;
        for (int i = 0; i < methods.length; i++) {
            if (methods[i] != null && methods[i].getName().equals(name)) {
                if (first == null) {
                    first = methods[i];
                }
		else {
                    if (v == null) {
                        v = new ObjArray();
                        v.add(first);
                    }
                    v.add(methods[i]);
                }
            }
        }
        if (v == null) {
            if (first == null) {
                return null;
	    }
            Method[] single = { first };
            return single;
        }
        Method[] result = new Method[v.size()];
        v.toArray(result);
        return result;
    }

    private static Method[] getMethodList(Class<Object> clazz) {
        Method[] cached = methodsCache; // get once to avoid synchronization
        if (cached != null && cached[0].getDeclaringClass() == clazz) {
//	    System.out.println("    returning cached class " + clazz);
            return cached;
	}

//	System.out.println("scanning methods of class " + clazz);
	Method[] methods = null;
        try {
            /* getDeclaredMethods may be rejected by the security manager
             * but getMethods is more expensive
	     */
            methods = clazz.getDeclaredMethods();
        }
	catch (SecurityException e) {	/* should never happen */
	    Err.exit("can't find methods from class " + clazz.toString());	
        }

	/* count number of public methods */
	int count = 0;
        for (int i = 0; i < methods.length; i++) {
            if (!Modifier.isPublic(methods[i].getModifiers())) {
                methods[i] = null;
            } 
	    else {
                count++;
            }
        }

	/* create new array of just public methods */
        Method[] result = new Method[count];
        int j = 0;
        for (int i = 0; i < methods.length; i++) {
            if (methods[i] != null) {
                result[j++] = methods[i];
	    }
        }
        if (result.length > 0) {    /* only cache useful results */
            methodsCache = result;
	}

	return result;
    }

    private static Method[] methodsCache = null;
}
