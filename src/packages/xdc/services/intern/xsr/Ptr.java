/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

public class Ptr extends XScriptO
    implements AnyType
{
    Value agg;
    Object sel;

    public Ptr( Value agg, Object sel )
    {
        this.agg = agg;
        this.sel = sel;
        
        String name = agg.path + sel.toString();
        
        this.setName(name);
        this.bindtab.put("$category", "Ptr");
    }
    
    public final Value getAgg() { return this.agg; }
    public final Object getSel() { return this.sel; }
}
