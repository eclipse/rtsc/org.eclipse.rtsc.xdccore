/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
package xdc.services.intern.xsr;

/**
 * Instances of this class are returned by $externPtr, $externFxn and
 * $externModFxn calls, and created by schemas whenever an extern is declared
 * in an XDCspec file.
 * Externs declared in an XDCspec file are only aliases for symbols they
 * reference, and they cannot be changed. Module header files contain defines
 * that replace XDCspec names with the original symbol names, and there are
 * also extern declarations for original symbol names.
 */
public class Extern extends XScriptO
    implements AnyType
{
    String name;
    String sig;
    boolean fxnT;

    /* This field specifies if this Extern is a function or a data pointer
     * not declared in a module. If that's the case, then the generated config
     * C file has to declare it as an extern. Also, if that's the case, the
     * name of this Extern is printed in the C file as it is. If 'raw' is
     * false, all '.' are replaced with '_' by the code that generates externs
     * in the config C file.
     */
    boolean raw;

    public Extern( String name, String sig, boolean fxnT )
    {
        this(name, sig, fxnT, false);
    }

    /** deprecated constructor, can be removed when we increment the schema
     *  version number from 170
     */
    public Extern(String name, String sig, boolean fxnT, boolean raw,
                  String specName)
    {
        this(name, sig, fxnT, raw);
    }

    public Extern(String name, String sig, boolean fxnT, boolean raw)
    {
        this.name = name;
        if (sig != null) {
            this.sig = Utils.setArgNames(sig).toString();
        }
        else {
            this.sig = null;
        }
        this.fxnT = fxnT;
        this.raw = raw;

        /* add arguments to sig */
        this.setName(name);

        this.bindtab.put("$sig", sig);
        this.bindtab.put("$raw", new Integer(raw ? 1 : 0));
        this.bindtab.put("$fxnT", new Integer(fxnT ? 1 : 0));
        this.bindtab.put("$category", "Extern");
    }

    public final String getName() { return this.name; }
    public final String getSig() { return this.sig; }

    public final boolean isFxnT() { return this.fxnT; }
    public final boolean isRaw() { return this.raw; }

    // OVERRIDE Scriptable

    public Object getDefaultValue( java.lang.Class hint )
    {
        if (Character.isDigit(this.name.charAt(0))) {
            return Long.decode(this.name);
        }
        else {
            return "&" + this.name;
        }
    }
}
