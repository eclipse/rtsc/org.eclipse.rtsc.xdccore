/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008-2019 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
#include <afx.h>

#include "StringUtils.h"

namespace StringUtils
{

size_t split(const CString input, const CString delimiter, CStringArray& results)
{
    int prevStart = 0;
    int delimSize = delimiter.GetLength();
    int inputSize = input.GetLength();

    /* stop if either argument is empty */
    if((inputSize == 0) || (delimSize == 0)) {
        return 0;
    }

    /* find the first delimiter */
    int nextStart = input.Find(delimiter, 0);

    /* keep looking for delimiters */
    while (nextStart >= prevStart)
    {
        /* found one, so accumulate up to that delimiter */
        int length = nextStart - prevStart;
        results.Add(input.Mid(prevStart, length));

        /* advance past the delimiter */
        prevStart = nextStart + delimSize;

        /* and look for the next match */
        nextStart = input.Find(delimiter, prevStart);
    }

    /* add the remainder of the string after the last delimiter */
    results.Add(input.Mid(prevStart));

    /* return the number of outputs found */
    return results.GetSize();
}

void join(const CStringArray& input, const CString delimiter, CString& result)
{
    result.Empty();
    for (int i = 0; i < input.GetSize(); i++) {
        /* if not at the beginning, separate by a delimiter */
        if (i != 0) {
            result = result + delimiter;
        }

        /* add the next element */
        result = result + input[i];
    }
}

}
