/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
#ifndef _Included_xdc_services_io_DosDll
#define _Included_xdc_services_io_DosDll

#include <afx.h>

_declspec (dllexport) CString xdc_services_io_Dos_getDOSPath( LPCTSTR path );

#endif
