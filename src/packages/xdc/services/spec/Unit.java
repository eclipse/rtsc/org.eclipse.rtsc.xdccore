/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008-2017 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
package xdc.services.spec;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Arrays;

public class Unit
    extends Node
{
    // Static Serialization UID
    static final long      serialVersionUID = -1475297028398889384L;

    List<Decl.Config>      cfgs;        // most specific override
    Decl.Fxn               creator;
    int                    curPass;
    boolean                custHdr;
    List<Decl>             decls;
    Sup                    dlg;
    List<Decl.Fxn>         fxns;   // all external fxns, most specific override
    int                    icfgCnt;
    private List<Decl.Fxn> ifxns;
    List<Import>           imps;
    boolean                inherits;
    boolean                iobjFlag;
    Sup                    isa;         // "is a" inheritance chain
    List<Decl.Fxn>         mbrs;        // target-fxns, order of decl; may
                                        // contain dups if override
    String                 pkgName;
    boolean                proxy;
    List<Unit>             prxs;
    boolean                rtsFlag;
    int                    scfgCnt;
    boolean                sizeFlag;
    Set<Unit>              uses;

    transient HashSet<String> facSet = new HashSet();

    static private HashSet<String> reservedAttrs =
        new HashSet(Arrays.asList(new String[] {
            Attr.A_CustomHeader,
            Attr.A_DirectCall,
            Attr.A_Facet,
            Attr.A_Gated,
            Attr.A_GlobalNames,
            Attr.A_HeaderName,
            Attr.A_InstanceInitError,
            Attr.A_InstanceInitStatic,
            Attr.A_InstanceFinalize,
            Attr.A_ModuleStartup,
            Attr.A_NoProxyCreate,
            Attr.A_NoRuntime,
            Attr.A_Prefix,
            Attr.A_Proxy,
            Attr.A_TargetHeader,
            Attr.A_Template,
        })
    );

    Unit(Atom name, String pkgName, EnumSet<Qual> quals, List<Decl> decls,
        List<Import> imps, Sup isa, Sup dlg)
    {
        this.name = name;
        this.pkgName = pkgName;
        this.quals = quals;
        this.decls = decls;
        this.imps = imps;
        this.isa = isa;
        this.dlg = dlg;

        this.cfgs = new ArrayList();
        this.creator = null;
        this.curPass = 0;
        this.custHdr = false;
        this.fxns = new ArrayList();
        this.icfgCnt = 0;
        this.ifxns = new ArrayList();
        this.inherits = false;
        this.iobjFlag = false;
        this.mbrs = new ArrayList();
        this.proxy = false;
        this.prxs = new ArrayList();
        this.rtsFlag = true;
        this.scfgCnt = 0;
        this.sizeFlag = true;
        this.uses = new LinkedHashSet();

        if (this.getQualName().equals("xdc.runtime.IInstance")) {
            return;
        }

        if (this.isa == null && !this.isMeta()
            && !this.getQualName().equals("xdc.runtime.IModule")) {

            Atom a = this.name.copy("xdc.runtime.IModule");
            this.isa = new Sup(a);
            // TODO: add to incvec???
        }
    }

    // addProxy
    private void addProxy(Decl.Proxy prx, boolean strict)
    {
        prx.sup.resolve(this);

        if (this.isInter()) {
            return;
        }

        if (this.isProxy()) {
            return;
        }

        EnumSet<Qual> qS = EnumSet.copyOf(prx.sup.unit.quals);
        qS.remove(Qual.INTERFACE);
        qS.add(Qual.MODULE);

        Unit pu;

        pu = new Unit(prx.name.copy(this.name.text + '_' + prx.name.text),
                      this.pkgName, qS, new ArrayList(), new ArrayList(),
                      prx.sup, null);

        pu.bindParent(this.getParent());
        ArrayList<Attr> aL = new ArrayList();
        /* The attribute NoProxyCreate has to be transferred from the proxy
         * config parameter to the new unit because the attribute will be
         * queried at the config time when it will be more difficult to get
         * from the proxy unit to the original config declaration.
         */
        if (prx.hasAttr(Attr.A_NoProxyCreate)) {
            aL.add(new Attr(new Atom(Attr.A_NoProxyCreate),
                            new Expr.Const(new Atom("true"))));
        }
        pu.bindAttrs(aL);
        pu.proxy = true;
        pu.pass1Check();
        pu.pass2Check(strict);

        this.prxs.add(pu);
        this.uses.add(pu);
    }

    // addUse
    void addUse(Ref ref)
    {
        // TODO: not needed ???
    }

    // checkOver
    private void checkOver(Decl d, boolean strict)
    {
        if (d.isInternal() || this.getSuper() == null) {
            return;
        }

        if (d.name.text.equals("create")) {
            return;
        }

        Decl over = this.lookupSup(d.name.text, this.getSuper());
        d.over = over;

        if (over != null) {

            if (!d.isOver()) {
                ses.msg.error(d.name, "multiple declaration in current scope");
                return;
            }

            if (over.isFinal()) {
                ses.msg.error(d.name, "can't override final declaration");
                return;
            }

            String osig = ((Decl.Signature) over).getTypeSig();
            String sig = ((Decl.Signature) d).getTypeSig();

            /* If a unit overrides a config, the type of the config does not
             * need to literaly match the original type. If the original type
             * is declared in an interface, all interfaces and modules
             * inheriting from that interface have the same type defined,
             * with the qualified type name being different in the inheriting
             * interfaces and modules. Those other qualified names are
             * also acceptable.
             */
            boolean inherited = false;
            Type otype = ((Decl.Signature)over).getType();
            Type dtype = ((Decl.Signature)d).getType();

            /* The type is null for JavaScript functions. */
            if (otype != null && dtype != null) {

	        String otypeCode = otype.tspec().code();
	        String dtypeCode = dtype.tspec().code();
	        /* Only for structs and enums we know that the type cannot be
	         * changed in XDCspec files. Modules, Instances and other types
	         * that can be extended in XDCspec can have any type along
	         * the inheritance chain be used interchangeably.
	         */
                if ((otypeCode == "S" || otypeCode == "E" || otypeCode == "e")
                    && (dtypeCode == "S" || dtypeCode == "E"
                        || dtypeCode == "e")) {

	            /* getNode will find the actual specification of the type
	             * in the unit where the type is declared. If both 'over'
	             * and 'd' types resolve to the same type specification,
	             * they are the same type.
	             */
	            Node otypeParent = otype.tspec().getRef().getNode();
	            Node dtypeParent = dtype.tspec().getRef().getNode();

                    if (otypeParent == dtypeParent) {
                        inherited = true;
                    }
                }
            }

            boolean compat = (osig.equals(sig) || inherited)
                && over.getClass() == d.getClass()
                && over.isStatic() == d.isStatic()
                && over.isMeta() == d.isMeta();

            if (over.isReadonly() != d.isReadonly()) {
                if (strict || d.isReadonly()) {
                    compat = false;
                }
                else {
                    ses.msg.warn(d.name, "can't remove readonly qualification");
                }
            }

            if (!compat) {
                ses.msg.error(d.name, "incompatible override");
                return;
            }
        }

        else if (d.isOver()) {
            ses.msg.error(d.name, "no definition to override");
            return;
        }
    }

    /**
     *  @deprecated replaced by {@link #getDecl(String)}
     */
    @Deprecated public final Decl decl(String name)
    {
        return this.parent.ses.findDecl(this.getQualName() + '.' + name);
    }

    // delegatesTo
    public final Unit delegatesTo()
    {
        return this.dlg == null ? null : this.dlg.unit;
    }

    // finalCheck
    void finalCheck()
    {
        //TODO: handle @Facet uniqueness when inheriting an interface
        List<Decl> cL = new ArrayList();
        List<Decl> eL = new ArrayList();

        super.finalCheck();
        for (Decl d : this.decls) {
            if (d.parent == this) {
                d.finalCheck();
            }
            if (d instanceof Decl.Fxn) {
                Decl.Fxn fxn = (Decl.Fxn) d;
                if (fxn.over != null) {
                    this.fxns.remove(fxn.over);
                }
                /* If this module does not have instances, instance related
                 * functions are removed.
                 */
                if (!isInst()) {
                    String fname = fxn.getName();
                    if (fname.equals("Handle__label")
                        || fname.equals("Object__create")
                        || fname.equals("Object__delete")
                        || fname.equals("Object__first")
                        || fname.equals("Object__destruct")
                        || fname.equals("Object__get")
                        || fname.equals("Object__next")
                        || fname.equals("Object__init")
                        || fname.equals("Params__init")) {
                        this.fxns.remove(fxn);
                        this.mbrs.remove(fxn);
                    }
                }
                if (!isProxy()) {
                    String fname = fxn.getName();
                    if (fname.equals("Proxy__abstract")
                        || fname.equals("Proxy__delegate")) {
                        this.fxns.remove(fxn);
                        this.mbrs.remove(fxn);
                    }
                }
            }
            else if (d instanceof Decl.Config) {
                Decl.Config cfg = (Decl.Config) d;
                if (cfg.over != null) {
                    this.cfgs.remove(cfg.over);
                }
            }
            else if (d instanceof Decl.Enum) {
                eL.addAll(d.getChildren());
            }
            else if (d instanceof Decl.Const && d.over != null) {
                cL.add(d.over);
            }
        }

        this.decls.addAll(eL);
        this.decls.removeAll(cL);
        
        if (this.isInter() || this.isMeta()) {
            return;
        }
        
        int maxAnnex = 0;
        
        for (Decl d: this.decls) {
            if (d instanceof Decl.Annexable) {
                int a = d.attrInt(Attr.A_Annex);
                if (a > maxAnnex) {
                    maxAnnex = a;
                }
            }
        }

        if (maxAnnex == 0) {
            return;
        }

        List<Decl> dL = new ArrayList();
        for (int i = 1; i <= maxAnnex; i++) {
            for (Decl d: this.decls) {
                if (!(d instanceof Decl.Annexable)
                    || d.attrInt(Attr.A_Annex) != i) {
                    continue;
                }
                dL.add(d);
                if (d instanceof Decl.Config) {
                    this.cfgs.remove(d);
                    this.cfgs.add((Decl.Config)d);
                }
                else if (d instanceof Decl.Fxn) {
                    this.fxns.remove(d);
                    this.mbrs.remove(d);
                    this.fxns.add((Decl.Fxn)d);
                    this.mbrs.add((Decl.Fxn)d);
                }
            }
        }

        this.decls.removeAll(dL);
        this.decls.addAll(dL);
    }

    // getAllFxns
    public final List<Decl.Fxn> getAllFxns()
    {
        return this.mbrs;
    }

    /**
     *  Get the instance creation function for this unit.
     *
     *  This method returns the most specific constructor for this unit.  If
     *  this unit does not support instances, null is returned.
     */
    public final Decl.Fxn getCreator()
    {
        return this.creator;
    }

    // getChildren
    public List<Decl> getChildren()
    {
        return this.decls;
    }

    // getConfigs
    public final List<Decl.Config> getConfigs()
    {
        return this.cfgs;
    }

    /**
     *  Get the named declaration from this unit.
     *
     *  This method returns the most specific declaration named by
     *  name for this unit.  If the named declaration does not exist, null is
     *  returned.
     */
    public final Decl getDecl(String name)
    {
        Decl result = null;
        for (Decl d: this.decls) {
            if (d.name.text.equals(name)) {
                /*
                 * decls are ordered least specific to most specific,
                 * so remember the final one found.
                 */
                result = d;
            }
        }
        return result;
    }

    /**
     *  Get all the declarations defined for this unit.
     *
     *  This method returns the most specific declarations defined for this
     *  unit.  Declarations include functions, config parameters, constants,
     *  etc.
     */
    public final List<Decl> getDecls()
    {
        return this.decls;
    }

    // getEmbeds
    public final Set<Unit> getEmbeds()
    {
        Set<Unit> res = new LinkedHashSet();
        for (String name : new String[] {"Module_State", "Instance_State"}) {
            Decl d = this.ses.findDecl(this, name);
            if (d == null) {
                continue;
            }
            for (Decl.Field fld : ((Decl.Struct)d).flds) {
                if (Decl.objKind(fld) != Decl.Signature.ObjKind.NONE) {
                    res.add((Unit)fld.getType().tspec().getRef().getNode());
                }
            }
        }

        return res;
    }

    // getFxns
    public final List<Decl.Fxn> getFxns()
    {
        return this.fxns;
    }

    // getInherits
    public final List<Unit> getInherits()
    {
        List<Unit> uL = new ArrayList();
        for (Unit iu = this.getSuper(); iu != null; iu = iu.getSuper()) {
            uL.add(iu);
        }

        return uL;
    }

    // getInternFxns
    public final List<Decl.Fxn> getInternFxns()
    {
        return this.ifxns;
    }

    // getProxies
    public final List<Unit> getProxies()
    {
        return this.prxs;
    }

    // getPkgName
    public final String getPkgName()
    {
        return this.pkgName;
    }

    // getQualName
    public final String getQualName()
    {
        return this.pkgName + '.' + this.name.text;
    }

    // getSuper
    public final Unit getSuper()
    {
        return
        // this.inherits ? this.isa.unit :
        this.dlg != null ?
            this.dlg.unit : this.isa != null ? this.isa.unit : null;
    }

    // getUses
    public final Set<Unit> getUses()
    {
        return this.uses;
    }

    // getXmlTag
    public final String getXmlTag()
    {
        return this.isMod() ? "module" : "interface";
    }

    // hasCustHdr
    public final boolean hasCustHdr()
    {
        return this.custHdr;
    }

    // hasCreateArgs
    public boolean hasCreateArgs()
    {
        return this.creator != null && this.creator.args.size() > 0;
    }

    // isForwarding
    public final boolean isForwarding()
    {
        return this.proxy || this.dlg != null;
    }

    // isHeir
    public final boolean isHeir()
    {
        return this.inherits;
    }

    // isProxy
    public final boolean isProxy()
    {
        return this.proxy;
    }

    // isSized
    public final boolean isSized()
    {
        return this.sizeFlag;
    }

    // hasInstObj
    public final boolean hasInstObj()
    {
        return this.iobjFlag;
    }

    // lookupSup
    private Decl lookupSup(String name, Unit su)
    {
        Decl cur = null;

        for (; su != null; su = su.getSuper()) {
            if ((cur = this.getSession().findDecl(su.getQualName() + '.' + name)) != null) {
                break;
            }
        }
        return cur;
    }

    // needsRuntime
    public final boolean needsRuntime()
    {
        return this.rtsFlag == true;
    }

    // parseDocs
    void parseDocs()
    {
        super.parseDocs();

        for (Decl d : this.decls) {
            if (!(d instanceof Decl.EnumVal)) {
                d.parseDocs();
            }
        }

        if (this.creator != null) {
            this.creator.parseDocs();
        }
    }

    // pass1Check
    void pass1Check()
    {
        if (this.curPass >= 1) {
            return;
        }
        else {
            this.curPass = 1;
        }

//        System.out.println("pass1: " + this.getQualName());

        /* check for duplicate conflicting import aliases */
        HashMap<String, String> imports = new HashMap();
        for (Import i : this.imps) {
            String value = imports.get(i.alias);
            if (value != null  && !value.equals(i.uid.text)) {
                ses.msg.error(this.name, "import alias '" + i.alias
                    + "' for " + i.uid.text
                    + " is already defined to be " + value);
            }
            imports.put(i.alias, i.uid.text);
        }
        imports = null;

        for (Decl d : this.decls) {

            String qn = this.getQualName() + '.' + d.getName();
            if (!this.getSession().enterNode(qn, d)) {
                ses.msg.error(d.name, "multiple declaration in current unit");
                continue;
            }

            d.bindParent(this);
            d.pass1Check();

            if (!d.isMeta() && d.name.text.indexOf('$') != -1) {
                ses.msg.error(d.name,
                    "only metaonly feature names can contain a '$'");
            }

            if (d.name.text.equals("Module_State")) {
                if (d instanceof Decl.Struct && d.isInternal()) {
                    d.quals.add(Qual.SYSTEM);
                }
                else {
                    ses.msg.error(d.name, "must be an internal struct");
                }
            }
            if (d.name.text.equals("Instance_State")) {
                if (d instanceof Decl.Struct && d.isInternal()) {
                    d.quals.add(Qual.SYSTEM);
                    this.iobjFlag = true;
                }
                else {
                    ses.msg.error(d.name, "must be an internal struct");
                }
            }
        }
    }

    // pass2Check
    void pass2Check(boolean strict)
    {
        if (this.curPass >= 2) {
            return;
        }
        else {
            this.curPass = 2;
        }

//        System.out.println("pass2: " + this.getQualName());

        ses.enterNode(this.getQualName(), this);

        for (Import i : this.imps) {
            i.resolve(this);
        }

        if (this.isa != null) {
            this.isa.pass2Check(this);
            if (this.isa.unit.isMod()) {
                ses.msg.error(this.name, "can't inherit from a module");
            }
            if (this.isMeta() != this.isa.unit.isMeta()) {
                ses.msg.error(this.name, "can't inherit an interface with "
                    + "a different metaonly qualifier");
            }
            this.uses.add(this.isa.unit);
            if (!this.isa.iid.text.equals("xdc.runtime.IModule")) {
                this.inherits = true;
            }
        }

        if (this.dlg != null) {
            this.dlg.pass2Check(this);
            if (this.dlg.unit.isInter()) {
                ses.msg.error(this.name, "can't delegate to an interface");
            }
            if (this.dlg.unit.isProxy()) {
                ses.msg.error(this.name, "can't delegate to a proxy");
            }
            if (this.isMeta() != this.dlg.unit.isMeta()) {
                ses.msg.error(this.name, "can't delegate to a module with "
                    + "a different metaonly qualifier");
            }
            boolean iflg = false;
            for (Sup sup = this.dlg.unit.isa; sup != null; sup = sup.unit.isa) {
                if ((iflg = (sup.unit == this.isa.unit))) {
                    break;
                }
            }
            if (!iflg) {
                ses.msg.error(this.name,
                    "delegate must inherit from this module's interface");
            }
            this.uses.add(this.dlg.unit);
            this.sizeFlag = this.dlg.unit.sizeFlag;
        }

        if (ses.msg.hasErrors()) {
            return;
        }

        Unit u2 = this.getSuper();

        if (u2 != null) {
            List<Decl> dL = new ArrayList();
            for (Decl d : u2.decls) {
                if (d.isExternal()) {
                    dL.add(d);
                }
            }
            dL.addAll(this.decls);
            this.decls = dL;
            if (u2.isInst()) {
                this.quals.add(Qual.INSTANCE);
            }
        }

        /* getSuper() returns a delegate if 'unit' is a proxy. In that case,
         * we don't want to apply delegate's attributes to this unit.
         */
        if (u2 != null && u2.isInter()) {
            List<Attr> aL = u2.attrs;
            if (aL != null) {
                aL = new ArrayList(aL);
                if (this.attrs != null) {
                    aL.addAll(this.attrs);
                }
                this.attrs = aL;
            }
        }

        this.enterAttrs(Unit.reservedAttrs);
        if (this.attrs != null) {
            for (Attr a : this.attrs) {
                if (a.val != null) {
                    a.val.resolve(this);
                }
            }
        }

        if (this.attrBool(Attr.A_Gated) && this.isMod() && !this.isProxy()) {
            Decl.Proxy prx = new Decl.Proxy(
                new Atom("Module_GateProxy"),
                EnumSet.of(Qual.INTERNAL),
                new Sup(new Atom("xdc.runtime.IGateProvider"))
            );
            this.decls.add(prx);
            prx.bindParent(this);
            prx.pass1Check();

        }

        /* If a module has instances, but a struct Instance_State was not
         * specified in the spec, we add an empty struct here. This has to
         * happen after the module's interface was checked for instances
         * and Qual.INSTANCE was added to 'quals' for this module.
         */
        if (this.isMod() && this.isInst() && !this.isProxy() && !this.isMeta()
            && this.iobjFlag == false) {
            EnumSet<Qual> quals = EnumSet.noneOf(Qual.class);
            quals.add(Qual.INTERNAL);
            quals.add(Qual.SYSTEM);

            ArrayList<Decl.Field> flds = new ArrayList(0);
            Decl.Struct s = new Decl.Struct(
                new Atom("Instance_State"), quals, flds, false);
            this.decls.add(s);
            s.bindParent(this);

            /* This flag is set to false, so we won't require Instance_init
             * function in modules that did not even declare Instance_State.
             */
            this.iobjFlag = false;
        }

        if (!this.isMeta()) {
            this.proxy |= this.attrBool(Attr.A_Proxy);
        }

        if (this.proxy && this.isInter()) {
            ses.msg.error(this.name, "only modules can serve as proxies");
        }

        if (this.hasAttr(Attr.A_Facet) && !this.isMeta()) {
            ses.msg.error(this.name, "@Facet units must be metaonly");
        }

        if (this.attrBool(Attr.A_NoRuntime)) {
            this.rtsFlag = false;
        }

        for (Decl d : this.decls) {

            if (d.parent != this && d instanceof Decl.AuxDef
                && !(d instanceof Decl.OverridableDef)) {

                String qn = this.getQualName() + '.' + d.getName();
                if (!this.getSession().enterNode(qn, d)) {
                    ses.msg.error(d.name,
                        "multiple declaration in current unit");
                    continue;
                }
            }

            if (d.parent == this) {
                d.resolve(this);
                this.checkOver(d, strict);
            }

            if (d instanceof Decl.Fxn) {
                Decl.Fxn fxn = (Decl.Fxn) d;
                if (fxn.type instanceof Type.Creator) {
                    this.creator = fxn;
                    continue;
                }
                if (fxn.isExternal()) {
                    this.fxns.add(fxn);
                    this.mbrs.add(fxn);
                }
                else {
                    this.ifxns.add(fxn);
                }
            }
            else if (d instanceof Decl.Config) {
                Decl.Config cfg = (Decl.Config) d;
                this.cfgs.add(cfg);
                if (cfg.isStatic()) {
                    this.scfgCnt++;
                }
                else {
                    this.icfgCnt++;
                }
            }
            else if (d instanceof Decl.Proxy && !this.proxy) {
                this.addProxy((Decl.Proxy) d, strict);
            }
        }

        Pkg curPkg = this.getSession().curPkg();

        if (curPkg.getName().equals(this.pkgName)) {
            String s = this.attrString(Attr.A_Template);
            if (s != null) {
                curPkg.miscSrcs.add(s);
            }

            if (this.hasAttr(Attr.A_CustomHeader)) {
                if (this.isMeta()) {
                    ses.msg.error(this.name,
                        "@CustomHeader can't be used in metaonly units");
                }
                else {
                    this.custHdr = true;
                }
            }

            /* custHdr may have been set by Decl.resolve() for @Macro */
            if (this.custHdr == true) {
                curPkg.miscSrcs.add("./" + this.getName() +"__prologue.h");
                curPkg.miscSrcs.add("./" + this.getName() +"__epilogue.h");
            }

            if (this.hasAttr(Attr.A_TargetHeader) && !this.isMeta()) {
                ses.msg.error(this.name,
                    "@TargetHeader must be used in metaonly units");
            }
        }

        if (this.creator != null) {
            this.decls.remove(this.creator);
        }
        else if (this.isa != null) {
            this.creator = this.isa.unit.creator;
        }

        this.iobjFlag |= this.isMeta();
        this.sizeFlag &= !this.proxy;
    }

    /**
     *  Query unit for the specified facet type
     *
     *  @return the name of the config parameter that corresponds to the
     *  type named by tName
     */
    public String queryFacet(String tName)
    {
        String res = null;

        for (Decl.Config cfg : this.cfgs) {
            if (cfg.hasAttr(Attr.A_Facet)
                && cfg.type.tspec().getRef().getNode().getQualName().equals(tName)) {
                res = cfg.getName();
                break;
            }
        }

        return res;
    }
}
