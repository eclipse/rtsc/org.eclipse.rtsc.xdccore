/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
//
// ======== xdc.services.spec.Expr ========
//

package xdc.services.spec;

import java.util.List;

public class Expr
    implements java.io.Serializable
{
    // Static Serialization UID
    static final long serialVersionUID = -6252677966391348100L;

    // Aggregate
    public interface Aggregate
    {
        boolean isEmpty();
    }

    // eText
    String eText( boolean norm )
    {
        return "";
    }
    
    // getTerm
    public Expr.Term getTerm()
    {
        return null;
    }
    
    // resolve
    void resolve( Unit uspec )
    {
    }
    
    // toText
    static final public String toText( Expr e )
    {
        return e == null ? "" : e.eText(true);
    }

    // toXml
    static final public String toXml( Expr e )
    {
        return e == null ? "" : e.eText(false);
    }

    // Array
    static public class Array
        extends Expr implements Aggregate
    {
    	static final long serialVersionUID = -4769465389054526623L;
    	
        List<Expr> elems;

        Array( List<Expr> elems )
        {
            this.elems = elems;
        }

        // eText
        String eText( boolean norm )
        {
            if (this.elems.size() == 0) {
                return "[ ]";
            }
            String res = norm ? "[" : "[@+";
            String sep = "";
            for (Expr e : elems) {
                res += sep + e.eText(norm);
                sep = norm ? ", " : ",@=";
            }
            return res + (norm ? "]" : "@-]");
        }
        
        // getElems
        public final List<Expr> getElems()
        {
            return this.elems;
        }
        
        // isEmpty
        public boolean isEmpty()
        {
            return this.elems.size() == 0;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            for (Expr e : this.elems) {
                e.resolve(uspec);
            }
        }
    }

    // Binary
    static public class Binary
        extends Expr
    {
    	static final long serialVersionUID = -8527302494988163510L;
    	
        Atom op;
        Expr left;
        Expr right;

        Binary( Atom op, Expr left, Expr right )
        {
            this.op = op;
            this.left = left;
            this.right = right;
        }

        // eText
        String eText( boolean norm )
        {
            return this.left.eText(norm) + ' ' + this.op.text + ' '
                    + this.right.eText(norm);
        }
        
        // getLeft
        public final Expr getLeft()
        {
            return this.left;
        }
        
        // getOp
        public final String getOp()
        {
            return this.op.text;
        }
        
        // getRight
        public final Expr getRight()
        {
            return this.right;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.left.resolve(uspec);
            this.right.resolve(uspec);
        }
    }

    // Cond
    static public class Cond
        extends Expr
    {
    	static final long serialVersionUID = -3016213789698060325L;
    	
        Expr cond;
        Expr left;
        Expr right;

        Cond( Expr cond, Expr left, Expr right )
        {
            this.cond = cond;
            this.left = left;
            this.right = right;
        }
        
        // eText
        String eText( boolean norm )
        {
            return this.cond.eText(norm) + " ? " + this.left.eText(norm)
                    + " : " + this.right.eText(norm);
        }
        
        // getCond
        public final Expr getCond()
        {
            return this.cond;
        }
        
        // getLeft
        public final Expr getLeft()
        {
            return this.left;
        }
        
        // getRight
        public final Expr getRight()
        {
            return this.right;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.cond.resolve(uspec);
            this.left.resolve(uspec);
            this.right.resolve(uspec);
        }
    }

    // Const
    static public class Const
        extends Expr
    {
    	static final long serialVersionUID = 3804334373597760702L;
    	
        Atom val;

        Const( Atom val )
        {
            this.val = val;
        }
        
        // eText
        String eText( boolean norm )
        {
            if (norm) {
                return this.val.text;
            }
            else if (Character.isLetter(this.val.text.charAt(0))) {
                return "$K" + this.val.text;
            }
            else {
                return this.val.text.replaceAll("@", "@@");
            }
        }
        
        // getVal
        public final String getVal()
        {
            return this.val.text;
        }
    }

    // Creat
    static public class Creat
        extends Expr
    {
    	static final long serialVersionUID = 5455736916581289951L;
    	
        Expr.Term mod;
        List<Expr> args;

        Creat( Expr.Term mod, List<Expr> args )
        {
            this.mod = mod;
            this.args = args;
        }
        
        // eText
        String eText( boolean norm )
        {
            return this.mod.eText(norm);
        }
        
        // getArgs
        public final List<Expr> getArgs()
        {
            return this.args;
        }
        
        // getMod
        public final Expr.Term getMod()
        {
            return this.mod;
        }
        
        // getTerm
        public Expr.Term getTerm()
        {
            return this.mod;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.mod.resolve(uspec);
            for (Expr e : this.args) {
                e.resolve(uspec);
            }
        }
    }

    // Error
    static class Error
        extends Expr
    {
    	static final long serialVersionUID = -7456683367635014801L;
    	
        Error()
        {
        }
    }

    // Hash
    static public class Hash
        extends Expr implements Aggregate
    {
        static final long serialVersionUID = 5610650247782362148L;

        List<Atom> ids;
        List<Expr> vals;

        Hash( List<Atom> ids, List<Expr> vals )
        {
            this.ids = ids;
            this.vals = vals;
        }
        
        // eText
        String eText( boolean norm ) {
            if (this.ids.size() == 0) {
                return "{ }";
            }
            String res = norm ? "{" : "{@+";
            String sep = "";
            for (int i = 0; i < ids.size(); i++) {
                res += sep + ids.get(i).text + ": " + vals.get(i).eText(norm);
                sep = norm ? ", " : ",@=";
            }
            return res + (norm ? "}" : "@-}");
        }
        
        // getIds
        public final List<Atom> getIds()
        {
            return this.ids;
        }
        
        // getVals
        public final List<Expr> getVals()
        {
            return this.vals;
        }
        
        // isEmpty
        public boolean isEmpty()
        {
            return this.vals.size() == 0;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            for (Expr e : this.vals) {
                e.resolve(uspec);
            }
        }
    }

    // Paren
    static public class Paren
        extends Expr
    {
    	static final long serialVersionUID = -8628875245839027888L;
    	
        Expr body;

        Paren( Expr body )
        {
            this.body = body;
        }
        
        // eText
        String eText( boolean norm )
        {
            return "(" + (this.body == null ? "" : this.body.eText(norm)) + ")";
        }
                
        // getBody
        public final Expr getBody()
        {
            return this.body;
        }
        
        // getTerm
        public Expr.Term getTerm()
        {
            return body.getTerm();
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.body.resolve(uspec);
        }
    }

/*    
    // Size
    static public class Size
        extends Expr
    {
        // TODO static final long serialVersionUID = -8628875245839027888L;
        
        Ref tref;

        Size( Ref tref )
        {
            this.tref = tref;
        }
        
        // eText
        String eText( boolean norm )
        {
            return "sizeof 0";
        }
                
        // getRef
        public final Ref getRef()
        {
            return this.tref;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.tref.resolve(uspec, true);
        }
    }
*/
    // Term
    static public class Term
        extends Expr
    {
    	static final long serialVersionUID = 484911847601585871L;
    	
        Ref eref;
        Expr val;

        Term( Ref eref )
        {
            this.eref = eref;
        }
        
        // eText
        String eText( boolean norm )
        {
            if (this.eref.isGlobal()) {
                return this.eref.id.text;
            }
            else if (norm) {
                return Expr.toText(this.val);
            }
            else {
                return "$!" + this.eref.scope.text + ":" + this.eref.id.text + "!";
            }
        }
        
        // getRef
        public final Ref getRef()
        {
            return this.eref;
        }
        
        // getTerm
        public Expr.Term getTerm()
        {
            return this;
        }
        
        // getVal
        public final Expr getVal()
        {
            return this.val;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            if (this.eref.isGlobal() || this.eref.node != null) {
                return;
            }
            
            this.eref.resolve(uspec, true);
            
            uspec.addUse(this.eref);

            /* get session for error reporting */
            Session ses = uspec.getSession();

            if (this.eref.id.text.equals("module")
                || this.eref.id.text.equals("create")) {
                this.val = null;
            }
            else if (this.eref.node instanceof Decl.Const) {
                this.val = ((Decl.Const)this.eref.node).init;
            }
            else if (this.eref.node instanceof Decl.EnumVal || this.eref.node instanceof Decl.Extern) {
                this.val = null;
            }
            else if (this.eref.node instanceof Decl.Fxn) {
                Decl.Fxn fxn = (Decl.Fxn)this.eref.node;
                if (!(fxn.getType() instanceof Type.Creator) && fxn.isMeta()) {
                    ses.msg.error(this.eref.toAtom(), "metaonly functions not allowed");
                }
                this.val = null;
            }
            else if (this.eref.node instanceof Decl.Config) {
                if ((this.val = ((Decl.Config)this.eref.node).init) == null) {
                    ses.msg.error(this.eref.toAtom(), "uninitialized config parameter");
                }
            }
            else {
                ses.msg.error(this.eref.toAtom(), "not a valid initializer");
            }
        }
    }

    // Unary
    static public class Unary
        extends Expr
    {
    	static final long serialVersionUID = -3418172231763962792L;
    	
        Atom op;
        Expr right;

        Unary( Atom op, Expr right )
        {
            this.op = op;
            this.right = right;
        }
        
        // eText
        String eText( boolean norm )
        {
            return this.op.text + this.right.eText(norm);
        }
        
        // getOp
        public final String getOp()
        {
            return this.op.text;
        }
        
        // getRight
        public final Expr getRight()
        {
            return this.right;
        }
        
        // resolve
        void resolve( Unit uspec )
        {
            this.right.resolve(uspec);
        }
    }
}
