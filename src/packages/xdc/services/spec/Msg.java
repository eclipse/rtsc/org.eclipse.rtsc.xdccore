/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
//
// ======== xdc.services.spec.Msg ========
//

package xdc.services.spec;

import java.io.PrintWriter;

class Msg
{
    /*
     *  ======== error ========
     */
    void error(String s)
    {
        error((Atom)null, s);
    }

    /*
     *  ======== error ========
     */
    void error(Atom a, String s)
    {
        output(a, s);
        errors = true;
    };

    /*
     *  ======== warn ========
     */
    void warn(String s)
    {
        warn((Atom)null, s);
    }

    /*
     *  ======== warn ========
     */
    void warn(Atom a, String s)
    {
        if (warnings) {
            output(a, "warning: " + s);
        }
    }

    /*
     *  ======== output ========
     */
    private void output(Atom a, String s)
    {
        if (a == null) {
            err.println(s);
        }
        else {
            err.println("\"" + a.file + "\", line " + a.line + ":  "
                + s + " (" + a.text + ")");
        }
    }

    /**************************************/

    /**
     * Forget that any fatal errors have been reported.
     */
    public void clearErrors()
    {
        this.errors = false;
    }

    /**
     * Report whether any fatal errors have been reported.
     * @return true iff an error has occured since the last time errors
     *         were cleared
     */
    public boolean hasErrors()
    {
        return (this.errors);
    }

    /**
     * Set whether or not to report warnings.
     * @return true iff warnings were enabled previously.
     */
    public boolean setWarnings(boolean warnings)
    {
        boolean old = this.warnings;
        this.warnings = warnings;
        return (old);
    }

    /**
     * Set the stream that receives warning and error reports.
     */
    public void setErr(PrintWriter err)
    {
        this.err = err;
    }

    /**************************************/

    private boolean errors = false;       /* an error has occured */
    private boolean warnings = false;     /* display warnings (or not) */

    /* by default, report to stderr with automatic flushing */
    private PrintWriter err = new PrintWriter(System.err, true);
}
