/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec.views;

import xdc.services.spec.*;
import java.util.*;

/**
 * Structured Views of the configuration parameters of a module or instance.
 *
 * This class allows module writers to logically group config
 * parameters, to simplify the view of a module or instance for the end user.
 * 
 * The logical grouping is under the control of a special config param in
 * the module which has a well-known name -- "declNameMap$".
 * 
 * Each RTSC interface that a module inherits can contribute to the
 * organization of the module. Each interface may define a "declNameMap$"
 * parameter that organizes the config params defined in the interface.
 */
public class Views {
    
    /**
     * Get the string value of a Const from the XDCspec file
     */
    private static String getConst(Expr e) {
        if (!(e instanceof Expr.Const)) {
            /* TODO: throw "string value required" */
            return null;
        }
        Expr.Const c = (Expr.Const)e;
        return c.getVal().replaceAll("^\"|\"$","");
    }

    private static boolean isViewType(Decl decl, String viewType) {
        if (decl.isInst() && !viewType.equals("instance"))  {
            return false;
        }
        if (!decl.isInst() && !viewType.equals("module")) {
            return false;
        }
        return true;
    }
    
    /**
     * A single ViewInfo record extracted from a unit.
     *
     * A ViewInfo record is a parsed version of one element in
     * the "configNameMap$" array.
     */
    static private class ViewInfo {

        public ViewInfo(Unit unit, String name, Expr.Hash e) {
            this.unit = unit;
            this.name = name;
            Iterator<Expr> vi = e.getVals().iterator();
            for (Atom n : e.getIds()) {
                String field = n.getText();
                Expr val = vi.next();
                if (field.equals("viewType")) {
                    viewType = getConst(val);
                }
                else if (field.equals("viewFxn")) {
                    viewFxn = getConst(val);
                }
                else if (field.equals("fields")) {
                    /* TODO: check is an array */
                    Expr.Array a = (Expr.Array)val;
                    for (Expr elem : a.getElems()) {
                        fields.add(getConst(elem));
                    }
                }
            }
        }

        public Unit unit;
        public String name;
        public String viewType;
        public String viewFxn = null;
        public List<String> fields = new ArrayList<String>();
    }

    /**
     * All the ViewInfo records from a single unit, without considering
     * inheritance.
     */
    static private class UnitViewInfo {
        public UnitViewInfo(Unit unit) {
            this.unit = unit;

            /* check has a decl with the right name */
            Decl d = unit.decl("configNameMap$");
            if (d == null) {
                return;
            }

            /* check is a config param */
            if (!(d instanceof Decl.Config)) {
                return;
            }
            Decl.Config d2 = (Decl.Config)d;
            
            /* check is a ViewMap */
            if (!d2.getTypeSig().equals("xdc_runtime_Types_ViewInfo[string]")) {
                /* TODO: throw a "not a ViewInfo map" exception */
                return;
            }
            Expr e = d2.getInit();
            
            /* check has an initial value */
            if (e == null) {
                /* TODO: throw a "no initializer" exception */
                return;
            }

            /* check initializer is an array */
            if (!(e instanceof Expr.Array)) {
                /* TODO: throw a "invalid initializer" exception */
                return;
            }
            Expr.Array a = (Expr.Array)e;

            /* record all the group definition contributions */
            for (Expr val : a.getElems()) {
                /* TODO: check is an array of two */
                Expr.Array a2 = (Expr.Array)val;
                Expr id = a2.getElems().get(0);
                /* TODO: check is a hash */
                Expr h = a2.getElems().get(1);
                viewInfo.add(new ViewInfo(unit, getConst(id), (Expr.Hash)h));
            }
        }

        public Unit unit;
        public List<ViewInfo> viewInfo = new ArrayList<ViewInfo>();
    }

    /**
     * The source for a single unit, including inheritance.
     * 
     * This is not the final form, because the result of this information
     * could be different if run on various descendants of the unit. For
     * example, config parameters can be overridden, or added.
     */
    private static class ViewSource {
        public ViewSource(Unit unit) {
            this.unit = unit;
            unitViewInfo.add(new UnitViewInfo(unit));
            for (Unit inh : unit.getInherits()) {
                unitViewInfo.add(new UnitViewInfo(inh));
            }

            /* aggregate all the unit ViewInfo, in order */
            for (UnitViewInfo u : unitViewInfo) {
                viewInfo.addAll(u.viewInfo);
            }
        }

        public Unit unit;
        /** all the units that contribute view information, in order */
        List<UnitViewInfo> unitViewInfo = new ArrayList<UnitViewInfo>();
        /** the aggregated ViewInfo for this unit */
        List<ViewInfo> viewInfo = new ArrayList<ViewInfo>();
    }

    /**
     * A group of config parameters, independent of grouping criteria.
     *
     * This interface gives clients uniform access to the results of the
     * various grouping algorithms implemented in this package.
     */
    public interface Group {
        /** get the name of this group */
        public abstract String getName();
        /** return all the decls named in this group */
        public abstract List<String> getDeclNames();
        /** return the view type -- "module" or "instance" */
        public abstract String getViewType();
    }

    public static class AbstractGroup implements Group {
        public AbstractGroup(String name, String viewType) {
            this.name = name;
            this.viewType = viewType;
        }
        
        public String getName() {
            return name;
        }

        public String getViewType() {
            return viewType;
        }

        public List<String> getDeclNames() {
            return declNames;
        }

        protected String name;
        protected String viewType;
        protected List<String> declNames = new ArrayList<String>();
    }

    /** A Group generated from explicit info in the units */
    public static class ExplicitGroup extends AbstractGroup {
        /** create a group */
        public ExplicitGroup(String name, String viewType, ViewSource source) {
            super(name, viewType);
            this.unit = source.unit;
            this.source = source;
            makeViewInfo();
            makeDeclNames();
        }

        /**
         * collect together all the ViewInfo records for a single group,
         * in order
         */
        private void makeViewInfo() {
            for (ViewInfo vi : source.viewInfo) {
                /* skip the contribution if it's the wrong name */
                if (!vi.name.equals(name)) {
                    continue;
                }
                /* skip the contribution if it's the wrong viewType */
                if (!vi.viewType.equals(viewType)) {
                    continue;
                }
                viewInfo.add(vi);
            }
        }

        /** find a decl that matches this name */
        private Decl resolve(Unit u, String name) {
            /* separate off the first component of the name */
            String head = name.replaceAll("\\..*$","");
            Decl ret = null;

            /* first look for a short name in the same unit */
            for (Decl decl : u.getDecls()) {
                if (decl.getName().equals(head)) {
                    ret = decl;
                }
            }

            /* otherwise, ask the spec package */
            if (ret == null) {
                Ref r = new Ref(name);
                r.resolve(u, true);
                ret = (Decl) r.getNode();
            }

            return ret;
        }

        /** find all the decls named in this group */
        private void makeDeclNames() {
            Set<String> fieldNames = new HashSet<String>();
            Set<String> declNamesSet = new HashSet<String>();

            /* get the fields with the given field names */
            for (ViewInfo vi : viewInfo) {
                for (String fieldName : vi.fields) {
                    /* only add the field name once */
                    if (fieldNames.contains(fieldName)) {
                        continue;
                    }
                    fieldNames.add(fieldName);

                    Decl decl = resolve(vi.unit, fieldName);
                    if (
                        (decl instanceof Decl.Config) ||
                        (decl instanceof Decl.Proxy)
                    ) {
                        /* check that it's of the right type */
                        if (!isViewType(decl, viewType))  {
                            /* TODO: throw "wrong decl type" error */
                            continue;
                        }

                        /* add the named config */
                        if (!declNamesSet.contains(fieldName)) {
                            declNamesSet.add(fieldName);
                            declNames.add(fieldName);
                        }
                    }
                    else if (decl instanceof Decl.IsType) {
                        /* find all the matching configs of the given type */
                        String tsig = decl.getQualName().replaceAll("\\.", "_");
                        for (Decl.Config config : unit.getConfigs()) {
                            /* check that it has the right type */
                            Type type = config.getType();
                            if (!type.tsig().equals(tsig)) {
                                continue;
                            }

                            /* check that it's of the right type */
                            if (!isViewType(config, viewType))  {
                                continue;
                            }

                            /* add the named config */
                            String name = config.getName();
                            if (!declNamesSet.contains(name)) {
                                declNamesSet.add(name);
                                declNames.add(name);
                            }
                        }
                    }
                    else {
                        /* TODO: report an error */
                    }
                }
            }
        }

        private Unit unit;
        private ViewSource source;
        private List<ViewInfo> viewInfo = new ArrayList<ViewInfo>();
    }

    /** a Group generated by applying a rule to the configs */
    static public class ImplicitGroup extends AbstractGroup {
        public ImplicitGroup(String name, String viewType, Unit unit, Set<String> except) {
            super(name, viewType);
            
            /* get all the unique config names */
            Set<String> uniqueNames = new HashSet<String>();
            for (Decl decl : unit.getDecls()) {
                /* if not either a decl or a proxy, skip it */
                if (
                    !(decl instanceof Decl.Config) &&
                    !(decl instanceof Decl.Proxy)
                ) {
                    continue;
                }

                String declName = decl.getName();

                /* if this decl gets overridden, pick it up later */
                Decl lastOne = null;
                for (Decl d2 : unit.getDecls()) {
                    if (d2.getName().equals(declName)) {
                        lastOne = d2;
                    }
                }
                if (decl != lastOne) {
                    continue;
                }
                
                /* only add the decl once */
                if (uniqueNames.contains(declName)) {
                    continue;
                }

                /* check that it's of the right type */
                if (!isViewType(decl, viewType))  {
                    /* TODO: throw "wrong config type" error */
                    continue;
                }

                /* don't add it if it's in the except list */
                if (except != null && except.contains(declName)) {
                    continue;
                }

                /* add the name */
                declNames.add(declName);
                uniqueNames.add(declName);
            }
        }
    }

    public Views(Unit unit, String type) {
        this.unit = unit;
        this.type = type;
        viewSource = new ViewSource(this.unit);
        Set<String> groupNames = new HashSet<String>();

        /* collect all the group definitions */
        for (ViewInfo vi : viewSource.viewInfo) {
            /* keep going if we've already built it */
            if (groupNames.contains(vi.name)) {
                continue;
            }
            groupNames.add(vi.name);

            /* create the group contents */
            ExplicitGroup g = new ExplicitGroup(vi.name, this.type, viewSource);
            
            /* ignore empty groups */
            if (g.getDeclNames().size() == 0) {
                continue;
            }

            groups.add(g);
        }

        /* find all the config names that have already been mentioned */
        Set<String> except = new HashSet<String>();
        for (Group g : groups) {
            for (String declName : g.getDeclNames()) {
                except.add(declName.replaceAll("\\..*$",""));
            }
        }
        /* generate the Basic group */
        groups.add(0, new ImplicitGroup("Basic", type, unit, except));
    }

    public List<Group> getGroups() {
        return groups;
    }

    /** the unit that these are the views for */
    private Unit unit;
    /** whether this is a view of a module or an instance */
    private String type;
    /** the aggregated view source information for this unit */
    private ViewSource viewSource;
    /** the groups the module configs are divided into */
    private List<Group> groups = new ArrayList<Group>();
}
