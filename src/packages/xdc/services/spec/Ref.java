/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
//
// ======== xdc.services.spec.Ref ========
//

package xdc.services.spec;

public class Ref
    implements java.io.Serializable
{
	
    // Static Serialization UID
    static final long serialVersionUID = 6391260877086618330L;
	
    static final Atom GBL = new Atom("$GBL");
    static final Decl.Error ERRDECL = new Decl.Error();
    
    Atom scope;
    Atom id;

    Node node = null;
    Decl.Proxy prx = null;

    private boolean local = false;
    
    Ref( Atom scope, Atom id )
    {
        this.scope = scope;
        this.id = id;
    }

    /**
     * External constructor.
     *
     * Simple constructor that provides no file or line number information
     * for the source of the reference.
     */
    public Ref(String name) {
        this(null, new Atom(name));
    }
    
    // getId
    public final String getId()
    {
        return this.id.text;
    }
    
    // getNode
    public final Node getNode()
    {
        return this.node;
    }
    
    // getProxy
    public final Decl.Proxy getProxy()
    {
        return this.prx;
    }
    
    // getScope
    public final String getScope()
    {
        return this.scope.text;
    }
    
    // isGlobal
    public final boolean isGlobal()
    {
        return this.scope != null && this.scope.text.equals(Ref.GBL.text);
    }
    
    // resolve
    /**
     * Resolve this reference.
     *
     * @param uspec  the module or interface making this reference.
     * @param local  if true, the reference is to a decl within the same unit
     *               making the reference. The text of the reference excludes
     *               the name of the unit.
     * @return       the unit that exports the referenced declaration. It is
     *               up to the caller to descend into the unit to find the
     *               actual declaration.
     * @throws SessionRuntimeException if can't be resolved.
     */
    public Unit resolve( Unit uspec, boolean local )
    {
        Unit res = null;
        Atom oldid = this.id;
        
//        System.out.println("entering resolve: " + this + ' ' + uspec.getName() + ' ' + this.id);
        /* first look for a decl in the referencing unit */
        res = this.resolveInUnit(uspec, this.id.text);
//        System.out.println("resolve: resolveInUnit returned " + res);

        /* if found, record that it is local to the referencing unit */
        if (res != null) {
            this.local = true;
        }

        /* if not found, check the imports of the referencing unit */
        int k = this.id.text.indexOf('.');
        String head = k < 0 ? this.id.text : this.id.text.substring(0, k);
        String tail = k < 0 ? null : this.id.text.substring(k + 1);
        for (Import imp : uspec.imps) {
            /* stop looking as soon as you've found one */
            if (res != null) {
                break;
            }

            /* if an import <unit>.*, check all its decls */
            if (imp.star) {
                res = this.resolveInUnit(imp.unit, this.id.text);
                continue;
            }

            /* if not the named unit, keep going */
            if (!head.equals(imp.alias)) {
                continue;
            }

            /*
             * check whether the reference is defined in the scope of an
             * imported unit
             */
//            System.out.println("trying " + imp.unit.getQualName());
            res = this.resolveInUnit(imp.unit, tail);
//            System.out.println("done: " + ((res!=null)?res.getQualName():res));
        }

        /* get the session */
        Session ses = uspec.getSession();

        /*
         * if not found, look outside the referencing unit, first to
         * the referencing unit's package, then on the package path.
         */
        if (res == null) {
            Unit unit = null;

            /* separate the name at the first ".", if any */
            k = this.id.text.indexOf('.');
            head = k < 0 ? this.id.text : this.id.text.substring(0, k);
            tail = k < 0 ? null : this.id.text.substring(k + 1);

            /* look for a unit in the same package as the reference */
            String qn = uspec.pkgName + '.' + head;
            String unitXdcFile = ses.getEnv().resolve(qn);

            /*
             * if not found and has at least one "." in the name, look for
             * a unit on the package path that matches this qname
             */
            if (unitXdcFile == null && tail != null) {
                /*
                 * search for where the unit name ends and the decl name,
                 * if any, begins. Start at the beginning of the name, and
                 * test each "." to see whether the name up to that point
                 * is a unit on the package path. Stop at the first match.
                 */
                qn = head;
                do {
                    k = tail.indexOf('.');
                    qn += "." + (k < 0 ? tail : tail.substring(0, k));
                    tail = k < 0 ? null : tail.substring(k + 1);
    
                    /* look for a unit with a matching name */
                    unitXdcFile = ses.getEnv().resolve(qn);
                }
                while (unitXdcFile == null && tail != null);
            }

            /* if we've found a unit, load it */
            if (unitXdcFile != null) {
                /* load the unit */
//                System.out.println("before loadunit2 " + this + ' ' + qn);
                unit = ses.loadUnit(qn);
//                System.out.println("after loadunit2 " + this + ' ' + qn);

                /* check that it loaded successfully */
                if (unit == null) {
                    ses.msg.error(this.toAtom(), "can't load unit from " +
                        unitXdcFile);
                    throw new SessionRuntimeException("parser failed");
                }

                /* add it to the "uses" list for the referencing unit */
                uspec.uses.add(unit);

                /* find the reference within the unit */
                res = this.resolveInUnit(unit, tail);
            }
        }

        /*
         * throw an exception if the unit hasn't been found either directly
         * or via an import statement
         */
        if (res == null) {
            ses.msg.error(this.toAtom(), "can't resolve name");
            throw new SessionRuntimeException("parser failed");
        }

        /* check that import <unit>.* has not created ambiguities */
        if (local && this.local && uspec.imps.size() > 0) {
            for (Import imp: uspec.imps) {
                if (!imp.star) {
                    continue;
                }
                String qn = imp.unit.getQualName() + '.' + oldid.text;
                Node node = uspec.getSession().lookup(qn);
                if (node != null && node != this.node) {
                    ses.msg.error(oldid, "multiple scope resolutions");
                    throw new SessionRuntimeException("parser failed");
                }
            }
        }

        if (this.node instanceof Decl.LocalUnit) {
            if (this.node instanceof Decl.Proxy) {
                this.prx = (Decl.Proxy)this.node;
            }
            Unit lu = ((Decl.LocalUnit)this.node).getUnit();
            k = this.id.text.indexOf('.');
            this.id.text = k < 0 ? "module" : this.id.text.substring(k + 1);
            res = this.resolve(lu, true);
        }
        
//        System.out.println("leaving resolve: " + this + ' ' + uspec.getName() + ' ' + this.id + " local: " + local + " scope: " + scope);
        return res;
    }

    // resolveInUnit
    /**
     * Try to resolve the reference within a given unit.
     * </p>
     * This private method resolves the reference to a declaration within
     * the given unit if possible, or else not at all. The name of the unit
     * is never included.
     * </p>
     * On successful exit, sets the fields node, scope, and id.text to
     * reflect the found node, the name of the unit it was found in, and
     * the name of the declaration within the unit.
     * </p>
     * @param  uspec the spec of the unit (module or interface) to look inside.
     * @param  declName the name of the declaration to look for inside the
     *         unit. If null, means the unit.
     * @return the unit that exports the referenced declaration, or null if
     *         not found.
     */
    private Unit resolveInUnit( Unit uspec, String declName )
    {
//      System.out.println("resolveInUnit: " + this + ' ' + uspec.getName() + ' ' + this.id );

        /* if reference is to the entire unit, we're done */
        if (declName == null) {
            this.node = uspec;
            this.scope = this.id.copy(uspec.getQualName());
            this.id.text = "module";
            return uspec;
        }

        /* if a reference to a generated decl, return the unit */
        if (declName.matches("Module|Instance|Handle|Object|Params|module|create")) {
            this.node = uspec;
            this.scope = this.id.copy(uspec.getQualName());
            this.id.text = declName;
            return uspec;
        }

        Session ses = uspec.getSession();

        /* extract the first element from the reference name */
        int k = declName.indexOf('.');
        String head = k < 0 ? declName : declName.substring(0, k);

        /* check whether it's a decl in the unit, though not an import itself */
        String qn = uspec.getQualName() + '.' + head;
        Node decl = ses.lookup(qn);
        if (decl != null && !(decl instanceof Decl.Imp)) {
            this.node = decl;
            this.scope = this.id.copy(uspec.getQualName());
            this.id.text = declName;
//            System.out.println("decl within unit: " + qn + " is " + this.node);
            return uspec;
        }

        /*
         * look up the inheritance chain to find a parent that defines
         * the reference, though not an import itself.
         */
        for (Unit u = uspec; u != null; u = u.getSuper()) {
            qn = u.getQualName() + '.' + head;
            Node n = ses.lookup(qn);
            if (n != null && !(n instanceof Decl.Imp)) {
//                System.out.println("found in parent: " + qn + " is " + n);
                if (declName.equals("create")) {
                    u = uspec;
                }
                this.scope = this.id.copy(u.getQualName());
                this.id.text = declName;
                this.node = n;
                return u;
            }
        }

        /* not found */
        this.node = Ref.ERRDECL;
        return null;
    }
    
    // toAtom
    Atom toAtom()
    {
        if (this.scope == null || this.isGlobal()) {
            return this.id;
        }
        else if (this.id.text.equals("module")) {
            return (this.scope);
        }
        else {
            return (this.id.copy(this.scope.text + '.' + this.id.text));
        }
    }
}
