/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

public class Sup
    implements java.io.Serializable
{
	
    // Static Serialization UID
    static final long serialVersionUID = -1373089623744501363L;
	
    Atom iid;
    
    Ref ref;
    Unit unit;
    
    Sup( Atom iid )
    {
        this.iid = iid;
        this.ref = new Ref(null, iid.copy());
    }
    
    // getUnit
    public final Unit getUnit()
    {
        return this.unit;
    }
    
    // pass2Check
    void pass2Check( Unit unit )
    {
        this.resolve(unit);
        
        if (this.unit == unit) {
            Session ses = unit.getSession();
            ses.msg.error(unit.name, "unit is inherited from itself");
            return;
        }

        if (this.unit.isa != null) {
            this.unit.isa.pass2Check(unit);
        }
    }
    

    // resolve
    void resolve( Unit unit )
    {
        if (this.unit != null) {
            return;
        }
        
        Unit u = this.ref.resolve(unit, false);
        if (!this.ref.id.text.equals("module")) {
            Session ses = unit.getSession();
            ses.msg.error(this.iid, "not a unit");
            throw new SessionRuntimeException("parser failed");
        }
        
        this.unit = u;
    }
}
