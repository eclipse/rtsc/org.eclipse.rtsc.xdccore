/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class DocComment
    implements java.io.Serializable
{
    // Static Serialization UID
    static final long serialVersionUID = 7216773336916227011L;

    String arg;
    Atom   body;
    String mode;
    int    pid;
    String tag;

    /*
     *  ======== DocComment ========
     */
    DocComment(Atom body)
    {
        this.body = body;

        this.arg = null;
        this.mode = null;
        this.pid = -1;
        this.tag = null;
    }
    
    /*
     *  ======== getArg ========
     */
    public final String getArg()
    {
        return this.arg;
    }
    
    /*
     *  ======== getBody ========
     */
    public final String getBody()
    {
        return this.body.text;
    }
    
    /*
     *  ======== getMode ========
     */
    public final String getMode()
    {
        return this.mode;
    }
    
    /*
     *  ======== getTag ========
     */
    public final String getTag()
    {
        return this.tag;
    }
    
    /*
     *  ======== getParId ========
     */
    public final int getParId()
    {
        return this.pid;
    }

    /* ======== isParsed ======== */
    /**
     * Identify whether the given list of comments has already been parsed.
     *
     * @param docs The list of comments to inspect.
     *
     * @return true if the list has already been parsed.
     */
    public static Boolean isParsed(List<DocComment> docs)
    {
        if (docs == null || docs.size() == 0) {
            return true;
        }

        /* Search comments for any signs of having been parsed.
         * Normally succeeds or fails during first iteration.
         * If unparsed, there's normally only one DocComment in the list.
         * If parsed, the first paragraph is normally marked as a summary.
         *
         * Some corner cases that have been found in use:
         *  - no summary paragraph exists
         *  - source file contains two or more doc comments in a row
         */
        for (DocComment doc : docs) {
            if (doc.getParId() != -1 ||
                doc.getTag() != null ||
                doc.getArg() != null ||
                doc.getMode() != null
            ) {
                return true;
            }
        }

        /* all DocComments have only default values */
        return false;
    }


    /*
     *  ======== getArgSummary ========
     *  
     *  Helper function to extract summary information for
     *  function arguments. 
     *  
     *  @param docs     The comment list for a function
     *  @param argName  Argument for which summary is required.
     *
     *  @returns        Summary string for the matching argument.
     *                  Returns null if no matches are found. 
     */

    static String getArgSummary(List<DocComment> docs, String argName) 
    {
        if ((docs == null) || (argName == null)) {
            return null;
        }
        for (DocComment d:docs) {
            String docTag = d.getTag();
            String docArg = d.getArg();
            if ((docTag != null) && docTag.equals("param")
                && (docArg != null) && docArg.equals(argName)) {
                String summary = d.getBody();
                return ((summary != null) ? summary.trim() : null);
            }
        }
        return null;
    }

    /*
     *  ======== parse ========
     */
    static ArrayList<DocComment> parse(List<DocComment> docs)
    {
        if (docs == null) {
            return null;
        }
        ArrayList<DocComment> res = new ArrayList();

        /* remove XDOC syntactic sugar from the body text */
        for (DocComment ad : docs) {
            /* remove the opening "/*!" or "//!" */
            if (ad.body.text.length() > 3) {
                ad.body.text = ad.body.text.substring(3);
            }

            /* remove DOS linefeed characters */
            String text = ad.body.text.replace('\r', ' ');

            /* process individual lines */
            String[] lines = text.split("\\n");
            for (int j = 0; j < lines.length; j++) {
                String ln = lines[j];

                /* remove banner lines -- lines with more than four
                 * asterisks or equals characters in a row
                 */
                if (ln.matches(".*[=*]{4,}.*")) {
                    continue;
                }

                /* remove the line leader comment characters */
                ln = ln.replaceFirst("^\\s*[!*]", "");

                /* append each line to the result as a separate DocComment */
                DocComment newad = new DocComment(ad.body.copy(ln));
                newad.body.line += j;
                res.add(newad);
            }
        }

        /*
         * regexp for parsing xdoc attribute syntax: @tag(mode) ...
         *  1: @ symbol
         *  2: tag 
         *  3: optional mode, including parens
         *  4: optional mode, excluding parens
         *  5: trailing text
         *
         *  Note: @ tags allow white space separated words to support tags
         *  like "@a(Source Files)".
         */
        Pattern p = Pattern.compile(
            "\\s*(@)(\\w+)(\\([ \\t]*([\\w \\t]+)[ \\t]*\\))?(.*)\\s*"
        );

        String curtag = "summary";
        String curarg = null;
        String curmode = null;
        int curpid = 0;
        boolean pre = true;

        for (Iterator<DocComment> i = res.iterator(); i.hasNext();) {
            DocComment ad = i.next();

            /* remove any comments that are completely blank */
            if (curmode == null && !ad.body.text.matches(".*\\S.*")) {
            	i.remove();
                if (!pre && curpid++ == 0) {
                    curtag = "details";
                }
                continue;
            }

            pre = false;

            Matcher m = p.matcher(ad.body.text);
            if (m.matches()) {

                if (m.group(2).equals("p")) {
                    curmode = m.group(4);
                    if (curpid++ == 0 && curtag.equals("summary")) {
                        curtag = "details";
                    }
                    continue;
                }

                curtag = m.group(2);
                curarg = m.group(4);
                curmode = null;
                ad.body.text = (m.group(5).length() == 0
                    || !m.group(5).matches(".*\\S.*")) ? null : m.group(5);
            }

            ad.tag = curtag;
            ad.arg = curarg;
            ad.mode = curmode;
            ad.pid = curpid;
        }

        return res;
    }
}
