/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
/*
 * ======== xdc.services.spec.Session ========
 */

package xdc.services.spec;

import java.io.PrintWriter;

import xdc.services.global.Env;
import xdc.services.global.Path;
import xdc.services.intern.cmd.Builder;

/*
 *  ======== Session ========
 */
abstract public class Session
{
    private   Cache cache;
    private   Env   env;
    protected Msg   msg;

    /*
     *  ======== curPkg ========
     */
    abstract Pkg curPkg();
    
    /*
     *  ======== loadUnit ========
     */
    abstract Unit loadUnit(String name);
    
    /*
     *  ======== enterNode ========
     *  Enter a node into the node cache, optionally replacing any existing
     *  node
     */
    boolean enterNode(String qn, Node n)
    {
        return this.enterNode(qn, n, false);
    }

    boolean enterNode(String qn, Node n, boolean force)
    {
        return cache.put(qn, n, force);
    }

    /*
     *  ======== enterPackage ========
     */
    boolean enterPackage(String qn, Pkg pkg)
    {
        if (!this.enterNode(qn, pkg)) {
            return false;
        }

        for (Unit u : pkg.getUnits()) {
            this.enterNode(u.getQualName(), u, true);
            for (Decl d : u.getDecls()) {
                if (d.getParent() == u) {
                    this.enterNode(d.getQualName(), d, true);
                }
            }
            /* enter create(), if the unit has one */
            Node creator = u.getCreator();
            if (creator != null) {
                this.enterNode(creator.getQualName(), creator, true);
            }
        }
        return true;
    }
    
    /*
     *  ======== Session ========
     */
    public Session() {
        this(Path.getGlobal());
    }

    public Session(Env env) {
        this(env, new Cache());
    }

    public Session(Env env, Cache cache) {
        this.env = env;
        this.cache = cache;
        this.msg = new Msg();
    }

    /*
     *  ======== setEnv ========
     */
    public void setEnv(Env env) {
        this.env = env;
    }

    /*
     *  ======== getEnv ========
     */
    public Env getEnv() {
        return this.env;
    }

    /*
     *  ======== setErr ========
     */
    public void setErr(PrintWriter err) {
        msg.setErr(err);
    }

    /*
     *  ======== setWarnings ========
     */
    public boolean setWarnings(boolean warnings)
    {
        return (msg.setWarnings(warnings));
    }
    
    /*
     *  ======== findDecl ========
     */
    public Decl findDecl(Unit unit, String n)
    {
        return this.findDecl(unit.getQualName() + '.' + n);
    }

    public Decl findDecl(String qn)
    {
        Node n = lookup(qn);
        return (n instanceof Decl) ? (Decl)n : null;
    }
    
    /*
     *  ======== findPkg ========
     */
    public Pkg findPkg(String qn)
    {
        Node n = lookup(qn);
        return (n instanceof Pkg) ? (Pkg)n : null;
    }
    
    /*
     *  ======== findUnit ========
     */
    public Unit findUnit(Pkg pkg, String n)
    {
        return this.findUnit(pkg.getQualName() + '.' + n);
    }

    public Unit findUnit(String qn)
    {
        Node n = lookup(qn);
        return (n instanceof Unit) ? (Unit)n : null;
    }
    
    /*
     *  ======== getVers ========
     */
    public int getVers()
    {
        return Builder.GREEN_VERS;
    }
    
    /*
     *  ======== lookup ========
     */
    Node lookup(String qn)
    {
        return cache.get(qn);
    }
}
