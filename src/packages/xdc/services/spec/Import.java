/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

public class Import
    implements java.io.Serializable
{
    // Static Serialization UID
    static final long serialVersionUID = 1L;
    
    boolean star;       /* importing all units from a package */
    Atom uid;           /* imported unit id */
    Unit unit;          /* the unit itself (after resolution) */
    String alias;       /* alias for the import */
    
    /*
     *  ======== Import ========
     */
    Import(Atom uid, String alias)
    {
        this.uid = uid;
        this.alias = alias;
        this.star = false;

        int k = this.uid.text.indexOf(".*");
        
        if (k != -1) {
            /* '.*' is used to import all decls within a unit _not_ all
             * units of a package.  So, uid is always a unit qname, without
             * a '*'.
             */
            this.uid = this.uid.copy(this.uid.text.substring(0, k));
            this.star = true;
        }
        
        if (alias == null) {
            k = this.uid.text.lastIndexOf('.');
            this.alias = (k == -1) ? this.uid.text : this.uid.text.substring(k + 1);
        }
    }
    
    /*
     *  ======== resolve ========
     *  Resolve this import which appears in the unit uspec
     */
    void resolve(Unit uspec)
    {
        /* get session of Unit we're adding the import to */
        Session ses = uspec.getSession();

        /* check that import has a package name */
        if (this.uid.text.indexOf('.') == -1) {
            ses.msg.error(this.uid, "import must be a fully qualified unit name");
            throw new SessionRuntimeException("parser failed");
        }

        /* look up the name of the import */
        if (ses.getEnv().resolve(this.uid.text) == null) {
            ses.msg.error(this.uid, "can't find imported unit: " + this.uid.text);
            throw new SessionRuntimeException("parser failed");
        }

        /* get the unit we'll be importing */
        Unit imp = ses.loadUnit(this.uid.text);

        /* lookup any object that this import might conflict with */
        String qn = uspec.pkgName + '.' + this.alias;
        Node hides = ses.lookup(qn);

        /* finish resolving the Import */
        this.unit = imp;

        /* check that the alias doesn't collide in the current package */
        if (hides != null) {
            /*
             * Import is a no-op if the unit is from the current package,
             * and is imported under its own name. However, we still add this
             * unit to uses because Ref.resolve() will check if a reference is
             * defined in an imported unit. If it is, Ref.java assumes that
             * the imported unit is already in 'uses' and it won't add it to
             * 'uses'. Since 'uses' is a java.util.Set, it doesn't hurt to
             * call add twice for a same element.
             */
            if (hides == imp) {
                uspec.uses.add(this.unit);
                return;
            }
            ses.msg.error(this.uid,
                "import conflict with a unit name in this package: "
                + this.alias);
            throw new SessionRuntimeException("parser failed");
        }

        /* TODO: decide whether to allow aliasing a unit from current pkg */

        /* form the qname of the import */
        qn = uspec.getQualName() + '.' + this.alias;

        /* check that the alias doesn't collide in the current unit */
        Node n = ses.lookup(qn);
        if (n != null) {
            /* import is a no-op if the unit has been imported twice */
            if ((n instanceof Decl.Imp) && ((Decl.Imp)n).getUnit() == imp) {
                return;
            }
            ses.msg.error(this.uid,
                "import conflict with a declaration in this unit: "
                + this.alias);
            throw new SessionRuntimeException("parser failed");
        }

        /* add the import to the unit */
        uspec.uses.add(this.unit);

        /* add the import to the session */
        ses.enterNode(qn, new Decl.Imp(this));
    }
}
