/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;

/**
 * Spec serialization and deserialization support.
 * 
 * On serialization, prunes references to spec objects in other packages
 * and fixes them up on deserialization.
 */
public class Persist {

    /**
     * Serialized form of a reference to a Node in another package.
     */
    static public class Fixup implements java.io.Serializable {
        // Static Serialization UID
        static final long serialVersionUID = 5710009880454690167L;

        private String pkg;
        private String decl;

        public Fixup(Node n) {
            pkg = n.getPkgName();
            if (n instanceof Pkg) {
                decl = null;
            }
            else {
                decl = n.getQualName().substring(pkg.length()+1);
            }
        }

        public String getPkgName() {
            return pkg;
        }

        public String getQualName() {
            return isPkg()? pkg : pkg + "." + decl;
        }

        public String getDeclName() {
            return decl;
        }

        public boolean isPkg() {
            return decl == null || decl.length() == 0;
        }
    }

    /**
     * ObjectOutputStream that restricts to the current package.
     *
     * Whenever this ObjectOutputStream sees a reference to a node
     * in another package, it replaces the referenced Node with
     * instructions to fix up that reference on deserialization.
     */
    static public class Output extends java.io.ObjectOutputStream
    {
        private String curPkg;
        private BrowserSession ses;

        public Output(OutputStream out, String curPkg)
            throws IOException
        {
            super(out);
            this.curPkg = curPkg;
            enableReplaceObject(true);
        }

        /**
         * Inspect each object before serialization.
         *
         * If the object is a reference to a node in a different
         * package, replace it with a Fixup.
         */
        protected Object replaceObject(Object obj)
            throws IOException
        {
            /* if it's not a Node, return in normal flow */
            if (!(obj instanceof Node)) {
                return obj;
            }
            Node n = (Node) obj;

            /* if it's not declared to be in any package, return */
            String pkg = n.getPkgName();
            if (pkg == null) {
                return obj;
            }

            /* if it's a Node in the current package, return */
            if (pkg.equals(curPkg)) {
                return obj;
            }

            /* return a fixup to the Node instead of the Node */
            return new Fixup(n);
        }
    }

    /**
     * ObjectInputStream that reconstructs references to foreign packages.
     */
    static public class Input extends java.io.ObjectInputStream
    {
        private BrowserSession ses;
        private boolean loadDocs = true;
        private String pkgName;

        public Input(String pkgName, InputStream in, BrowserSession ses)
            throws IOException
        {
            super(in);
            this.pkgName = pkgName;
            this.ses = ses;
            if (ses != null) {
                enableResolveObject(true);
                this.loadDocs = ses.getLoadDocs();
            }
        }

        /*
         * Inspect each deserialized object before returning it.
         *
         * If the object is not a Fixup, just return it. If it is a
         * Fixup, replace it with the Node object it references, loaded
         * from the current package path.
         */
        protected Object resolveObject(Object obj)
            throws IOException
        {
            /* if it's a fixup, look it up in the current session */
            if (obj instanceof Fixup) {
                Fixup fixup = (Fixup)obj;

                /* load the named package from the current package path */
                Pkg pkg = ses.findPkg(fixup.getPkgName());

                /* throw an error if the package is not found */
                if (pkg == null) {
                    throw new SessionRuntimeException(
                        pkgName + ": can't find referenced package " +
                            fixup.getPkgName()
                    );
                }

                /* return the named node */
                obj = ses.lookup(fixup.getQualName());

                /* throw an error if declaration is not found */
                if (obj == null) {
                    throw new SessionRuntimeException(
                        pkgName + ": can't resolve referenced name " +
                            fixup.getQualName()
                    );
                }
            }

            if (obj instanceof Node) {
                Node n = (Node)obj;

                /* delete the docs fields */
                n.docs = null;
                
                /* bind current session */
                n.ses = this.ses;
            }

            /* return the object */
            return obj;
        }
    }
}
