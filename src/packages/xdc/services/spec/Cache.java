/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.services.spec;

import java.util.Map;
import java.util.HashMap;

/**
 * Cache of spec objects for a session.
 *
 * This cache can be shared between related sessions having the same package
 * path. For example, each BrowserSession has an associated ParserSession to
 * handle backwards compatibility cases, and the Cache is shared in this case.
 */
public class Cache {
    private Map<String,Node> nodes = new HashMap();

    /**
     * Poll whether the cache contains an object.
     */
    public boolean contains(Node n) {
        return contains(n.getQualName());
    }

    /**
     * Poll whether the cache contains the named object.
     *
     * Useful if the Node may not yet be fully initialized, and it may not
     * yet be possible to call getQualName().
     */
    public boolean contains(String qn) {
        return nodes.containsKey(qn);
    }

    /**
     * Add an object to the cache.
     *
     * @return true if this cache did not already contain the object
     */
    public boolean put(Node n) {
        String qn = n.getQualName();
        return put(qn, n);
    }

    /**
     * Add a object to the cache with a specific qualified name.
     *
     * Useful if the Node is not yet fully initialized, and it may not
     * yet be possible to call getQualName(). If the name is known by
     * out-of-band means then it can still be added to the cache.
     *
     * @return true if this cache did not already contain the object
     */
    public boolean put(String qn, Node n) {
        return this.put(qn, n, false);
    }

    public boolean put(String qn, Node n, boolean force) {
        if (!force && contains(qn)) {
            return false;
        }

        nodes.put(qn, n);
        return true;
    }

    /**
     * Get the cached copy of an object.
     *
     * If the object is already in the cache, return it.
     *
     * @return the cached equivalent of this object, or null.
     */
    public Node get(String qn) {
        return nodes.get(qn);
    }
}
