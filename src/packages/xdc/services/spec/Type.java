/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008-2017 Texas Instruments Incorporated
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/
//
// ======== xdc.services.spec.Type ========
//

package xdc.services.spec;

import java.util.EnumSet;
import java.util.List;

public class Type
    implements java.io.Serializable
{
    // Static Serialization UID
    static final long serialVersionUID = 5646275176634845046L;

    static public final String ARG = "IArg";
    static public final String CHAR = "Char";
    static public final String DOUBLE = "Double";
    static public final String FLOAT = "Float";
    static public final String FXN = "Fxn";
    static public final String INT = "Int";
    static public final String INT8 = "Int8";
    static public final String INT16 = "Int16";
    static public final String INT32 = "Int32";
    static public final String INT40 = "Int40";
    static public final String INT64 = "Int64";
    static public final String LONG = "Long";
    static public final String LONGDOUBLE = "LDouble";
    static public final String LONGLONG = "LLong";
    static public final String PTR = "Ptr";
    static public final String SHORT = "Short";
    static public final String SIZE = "Size";

    static private final int NORM = 0;
    static private final int XML = 1;
    static private final int POINTER = 2;

    public enum Modifier { CONST, RESTRICT, VOLATILE };

    // code
    String code()
    {
        return "?";
    }

    // ptrsig
    public String ptrsig()
    {
        return this.tspec().sig() + this.sig(POINTER);
    }

    // raw
    public Type raw()
    {
        Type t = this;

        while (t instanceof Type.Declarator) {
            Node n = t.tspec().getRef().getNode();
            if (!(n instanceof Decl.Typedef)) {
                break;
            }
            t = ((Decl.Typedef)n).getType();
        }

        return t;
    }

    // resolve
    final void resolve( Unit uspec )
    {
        this.resolve(uspec, false);
    }

    // resolve
    void resolve( Unit uspec, boolean mixed )
    {
    }

    // root
    public Type root()
    {
        return null;
    }

    // sig
    String sig()
    {
        return this.sig(NORM);
    }

    // sig
    String sig( int kind )
    {
        return "";
    }

    // sizeof
    public void sizeof( List<String> sL )
    {
    }

    // tcode
    public final String tcode()
    {
        return this.code() + this.tspec().code();
    }

    // tsig
    public final String tsig()
    {
        return this.tspec().sig() + this.sig();
    }

    // term
    public Type term()
    {
        return this;
    }

    // tspec
    public Type.Spec tspec()
    {
        return null;
    }

    // xmlsig
    public String xmlsig()
    {
        return this.tspec().sig(XML) + ' ' + this.sig(XML);
    }

    // Type.Array
    static public class Array
        extends Type
    {
        static final long serialVersionUID = 8764930492419867714L;
        public enum Kind { CARR, MAP, VEC };

        Type base;
        Expr dim;
        Kind kind;
        /* constElem is only relevant for Vectors because that's where we need
         * to know in advance whether to make elements of the underlying array
         * mutable or not. There is no way to change that in C code.
         */
        boolean constElem;

        Array( Type base, Expr dim, Kind kind )
        {
            this.base = base;
            this.dim = dim;
            this.kind = kind;
            this.constElem = false;
        }

        // code
        String code()
        {
            char dom = this.kind == Kind.CARR ? 'A' : this.kind == Kind.MAP ? 'M' : 'V';
            return "" + dom + this.base.code();
        }

        // resolve
        void resolve(Unit uspec, boolean mixed)
        {
            this.base.resolve(uspec, mixed);
            if (this.dim != null) {
                this.dim.resolve(uspec);
            }
        }

        // getBase
        public final Type getBase()
        {
            return this.base;
        }

        /*
         *  ======== getConstElem ========
         */
        public final boolean getConstElem()
        {
            return this.constElem;
        }

        // getDim
        public final Expr getDim()
        {
            return this.dim;
        }

        // getKind
        public final Kind getKind()
        {
            return this.kind;
        }

        // isC
        public final boolean isC()
        {
            return this.kind == Kind.CARR;
        }

        // isMap
        public final boolean isMap()
        {
            return this.kind == Kind.MAP;
        }

        // isVec
        public final boolean isVec()
        {
            return this.kind == Kind.VEC;
        }

        // root
        public Type root()
        {
            return this;
        }

        /*
         *  ======== setConstElem ========
         */
        public final void setConstElem(boolean ce)
        {
            this.constElem = ce;
        }

        // sig
        public String sig( int kind )
        {
            String doms;

            switch (this.kind) {
            case MAP:
                doms = "string";
                break;
            case VEC:
                doms = "length";
                break;
            case CARR:
                doms = this.dim != null ? "<expr>" : "";
                break;
            default:
                doms = "";
            }

            if (kind == XML) {
                if (this.dim != null) {
                    doms = Expr.toXml(this.dim).replace('$', 'T');
                }
                else {
                    doms = "K!" + doms + "!";
                }
            }
            return this.base.sig(kind) + '[' + doms + ']';
        }

        // Array.sizeof
        public void sizeof( List<String> sL )
        {
            if (this.kind == Kind.VEC) {
                sL.add("Sxdc.runtime.Types;Vec");
                return;
            }

            if (this.kind != Kind.CARR) {
                return;
            }

            if (this.dim == null) {
                sL.add("U" + Type.PTR);
                return;
            }

            sL.add("A" + Expr.toText(this.dim));
            this.base.sizeof(sL);
        }

        // term
        public Type term()
        {
            return this.base.term();
        }

        // tspec
        public Type.Spec tspec()
        {
            return this.base.tspec();
        }
    }

    // Type.Creator
    static public class Creator
        extends Type
    {
        static final long serialVersionUID = -771646166657371561L;

        Creator()
        {
        }

        // code
        String code()
        {
            return "C";
        }

        // sig
        String sig( int kind )
        {
            return "Instance";
        }

        // xmlsig
        public String xmlsig()
        {
            return "K!create!";
        }
    }

    // Type.Declarator
    static public class Declarator
        extends Type
    {
        static final long serialVersionUID = -1024790156516858614L;

        Type.Spec tspec;
        Atom id;

        Declarator( Type.Spec tspec, Atom id )
        {
            this.tspec = tspec;
            this.id = id;
        }

        // code
        String code()
        {
            return "";
        }

        // getId
        public final String getId()
        {
            return this.id == null ? null : this.id.text;
        }

        // resolve
        void resolve( Unit uspec, boolean mixed )
        {
            this.tspec.resolve(uspec, mixed);
        }

        // sig
        String sig( int kind )
        {
            if (kind == XML && this.id != null) {
                return "@";
            }
            else if (kind == POINTER) {
                return ("(*)");
            }
            else {
                return "";
            }
        }

        // Declarator.sizeof
        public void sizeof( List<String> sL )
        {
            this.tspec.sizeof(sL);
        }

        // tspec
        public Type.Spec tspec()
        {
            return this.tspec;
        }
    }

    // Type.Error
    static class Error
        extends Type
    {
        static final long serialVersionUID = -7932488117104645741L;

        Error()
        {
        }
    }

    // Type.Fxn
    static public class Fxn
        extends Type
    {
        static final long serialVersionUID = 4714104815468307967L;

        Type base;
        List<Decl.Arg> args;
        boolean isVarg;

        Fxn( Type base, List<Decl.Arg> args, boolean isVarg )
        {
            this.base = base;
            this.args = args;
            this.isVarg = isVarg;
        }

        // code
        String code()
        {
            String bc = this.base.code();
            return this.base instanceof Type.Paren ? bc + "F" : "F" + bc;
        }

        // getArgs
        public final List<Decl.Arg> getArgs()
        {
            return this.args;
        }

        // getBase
        public final Type getBase()
        {
            return this.base;
        }

        // isVarg
        public final boolean isVarg()
        {
            return this.isVarg;
        }

        // resolve
        void resolve( Unit uspec, boolean mixed )
        {
            this.base.resolve(uspec, mixed);
            int nameCounter = 0;
            for (Decl.Arg a : this.args) {
                nameCounter++;
                /* This is for function types that don't have argument names
                 * in the spec.
                 */
                if (a.getAtom() == null) {
                    if (a.type.tspec().code().equals("v")) {
                        a.setAtom("");
                        break;
                    }
                    else {
                        a.setAtom("arg" + nameCounter);
                    }
                }
                a.resolve(uspec);
            }
        }

        // root
        public Type root()
        {
            return this;
        }

        // sig
        String sig( int kind )
        {
            String res = this.base.sig(kind) + "(";
            if (kind == POINTER) {
                kind = NORM;
            }

            if (this.args.size() == 0 && !this.isVarg) {
                return kind == XML ? res + ")" : res + "xdc_Void)";
            }

            String sp = "";
            for (Decl.Arg arg : this.args) {
                res += sp + arg.type.tspec().sig(kind) + arg.type.sig(kind);
                /* TODO: when we increment the schema number from 170, the
                 * commented line should be used to conform to our coding
                 * conventions. It has to be done in sync with a similar change
                 * in Decl.java.
                 */
                //sp = ", ";
                sp = ",";
            }
            if (this.isVarg) {
                res += ",...";
            }
            return res + ")";
        }

        // Fxn.sizeof
        public void sizeof( List<String> sL )
        {
            sL.add("U" + Type.FXN);
        }

        // tspec
        public Type.Spec tspec()
        {
            return this.base.tspec();
        }
    }

    // Type.Paren
    static public class Paren
        extends Type
    {
        static final long serialVersionUID = 2287194647505979584L;

        Type base;

        Paren( Type base )
        {
            this.base = base;
        }

        // code
        String code()
        {
            return this.base.code();
        }

        // getBase
        public final Type getBase()
        {
            return this.base;
        }

        // resolve
        void resolve( Unit uspec, boolean mixed )
        {
            this.base.resolve(uspec, mixed);
        }

        // root
        public Type root()
        {
            return this.base.root();
        }

        // sig
        String sig( int kind )
        {
            return "(" + this.base.sig(kind) + ")";
        }

        // Paren.sizeof
        public void sizeof( List<String> sL )
        {
            this.base.sizeof(sL);
        }

        // term
        public Type term()
        {
            return this.base.term();
        }

        // tspec
        public Type.Spec tspec()
        {
            return this.base.tspec();
        }
    }

    // Type.Ptr
    static public class Ptr
        extends Type
    {
        static final long serialVersionUID = 8071706686749097693L;

        Type base;
        EnumSet<Type.Modifier> mods;

        Ptr( Type base )
        {
            this(base, null);
        }

        Ptr( Type base, EnumSet<Type.Modifier>mods )
        {
            this.base = base;
            this.mods = mods;
        }

        // code
        String code()
        {
            return "P" + this.base.code();
        }

        // getBase
        public final Type getBase()
        {
            return this.base;
        }

        // getMods
        public final EnumSet<Type.Modifier> getMods()
        {
            return this.mods;
        }

        // resolve
        void resolve( Unit uspec, boolean mixed )
        {
            this.base.resolve(uspec, mixed);
        }

        // root
        public Type root()
        {
            return this;
        }

        // Ptr.sizeof
        public void sizeof( List<String> sL )
        {
            sL.add("U" + Type.PTR);
        }

        // sig
        String sig( int kind )
        {
            return "*" + this.base.sig(kind);
        }

        // tspec
        public Type.Spec tspec()
        {
            return this.base.tspec();
        }
    }

    // Type.Spec
    static public class Spec
        extends Type
    {
        static final long serialVersionUID = -1847725876187637108L;

        Ref tref;
        String std;
        boolean isUns;
        EnumSet<Type.Modifier> mods;

        Spec( Ref tref, String std, boolean isUns, EnumSet<Type.Modifier> mods )
        {
            this.tref = tref;
            this.std = std;
            this.isUns = isUns;
            this.mods = mods;
        }

        // code
        String code()
        {
            String tn = this.tref.id.text;

            if (this.tref.isGlobal()) {
                return
                    tn.equals("any") || tn.equals("Any") ? "a" :
                    tn.equals("void") || tn.equals("Void") ? "v" :
                    tn.equals("IArg") || tn.equals("UArg") || tn.equals("Ptr")
                        || tn.equals("CPtr") ? "Pv" :
                    tn.equals("Fxn") ? "Pf" :
                    tn.equals("string") || tn.equals("String")
                        || tn.equals("CString") ? "s" :
                    tn.equals("bool") || tn.equals("Bool") ? "b" :
                    "n";
            }

            if (tn.equals("Module")
                || tn.equals("Instance")
                || tn.equals("Handle")) {
                return "o";
            }

            if (tn.equals("Params")) {
                return "S";
            }

            if (tn.equals("Object")) {
                return "O";
            }

            if (this.tref.node instanceof Decl.Struct) {
                return ((Decl.Struct) this.tref.node).flds != null ? "S" : "E";
            }

            if (this.tref.node instanceof Decl.Enum) {
                return "e";
            }

            if (this.tref.node instanceof Decl.Typedef) {
                return ((Decl.Typedef) this.tref.node).getTypeCode();
            }

            return "?";
        }

        // getMods
        public final EnumSet<Type.Modifier> getMods()
        {
            return this.mods;
        }

        // getRef
        public final Ref getRef()
        {
            return this.tref;
        }

        // getStd
        public final String getStd()
        {
            return this.std;
        }

        // isUns
        public final boolean isUns()
        {
            return this.isUns;
        }

        // resolve
        void resolve( Unit uspec, boolean mixed )
        {
            if (this.tref.isGlobal() || this.tref.node != null) {
                return;
            }

            Unit u = this.tref.resolve(uspec, true);
            Session ses = uspec.getSession();

            if (!mixed && !uspec.isMeta() && u.isMeta()) {
                ses.msg.error(this.tref.toAtom(),
                    "the non-metaonly unit " + uspec.getQualName()
                    + " cannot use types from a metaonly unit");
                return;
            }

            if (this.tref.node instanceof Unit) {
                String tn = this.tref.id.text;
                if (tn.equals("Module")) {
                    return;
                }
                if (tn.equals("Instance") || tn.equals("Handle")
                    || tn.equals("Object") || tn.equals("Params")) {
                    Unit u2 = (Unit)this.tref.node;
                    if (!u2.isInst()) {
                        ses.msg.error(this.tref.scope,
                            "unit does not support per-instance types");
                    }
                    else if (tn.equals("Object") && u2.isInter()) {
                        ses.msg.error(this.tref.scope,
                             "interfaces do not support Object types");
                    }
                    return;
                }
            }

            if (!(this.tref.node instanceof Decl.IsType)) {
                ses.msg.error(this.tref.toAtom(), "not a type");
                return;
            }
        }

        // sig
        String sig( int kind )
        {
            if (this.tref.scope == null) {
                return kind == XML ? "?" : "**UNKNOWN TYPE**";
            }
            else if (this.tref.isGlobal()) {
                if (kind == XML) {
                    return "K!" + this.tref.id.text + "!";
                }
                else {
                    String s = this.tref.id.text;
                    return (Character.isUpperCase(s.charAt(0))
                        ? ("xdc_" + s) : s);
                }
            }
            else if (kind == XML) {
                return "T!" + this.tref.scope.text + ":" + this.tref.id.text
                    + "!";
            }
            else if (this.tref.node instanceof Decl.Typedef
                && !this.tref.node.hasAttr(Attr.A_Encoded)) {
                Decl.Typedef tdef = (Decl.Typedef) this.tref.node;
                return tdef.type.tspec().sig() + tdef.type.sig();
            }
            else {
                return (this.tref.scope.text.replace('.', '_') + '_'
                    + this.tref.id.text);    /// USED TO BE "::"
            }
        }

        // Spec.sizeof
        public void sizeof( List<String> sL )
        {
            if (this.std != null) {
                sL.add((this.isUns ? "U" : "T") + this.std);
            }
            else if (this.tref.node instanceof Decl.Sizeable) {
                ((Decl.Sizeable) this.tref.node).sizeof(sL);
            }
            else if (this.tref.id.text.equals("Object")) {
                sL.add("S" + this.tref.getScope() + ';' + "Instance_State");
            }
            else {
                sL.add("U" + Type.PTR);
            }
        }

        // xmlsig
        public String xmlsig()
        {
            return this.sig(XML);
        }
    }
}
