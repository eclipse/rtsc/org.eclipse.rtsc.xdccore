/* --COPYRIGHT--,EPL
 *  Copyright (c) 2014 Texas Instruments
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/

package xdc.services.mapping;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.Set;
import java.util.HashSet;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.regex.Matcher;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.IdScriptableObject;
import org.mozilla.javascript.ScriptRuntime;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.json.JsonParser;

/**
 * Reads target mapping entries from a JSON file and selects a target based on
 * a map of search terms.
 *
 * Instances of this class read a JSON file which contains target selection
 * objects and stores them into a map. Then, the instance accepts a map of
 * search terms and selects an object based on these terms.
 * Objects are matched against the search terms in a map in the order the 
 * objects are listed in a JSON file. As soon as a match is found, the search 
 * stops.
 * <p>
 * A match is found when all of the search properties in an object can be found
 * and have equal values as the corresponding search terms in the supplied map.
 * <p>
 * Currently, the following JSON object properties are used for matching: 
 * 'deviceFamily', 'device', 'deviceVariant', 'endianness' and 'elf'. However,
 * a JSON file can have other properties. The function 'getKeys' returns a list
 * of all properties found in the JSON file managed by an instance.
 * Objects also have two additional properties that are used to generate a
 * fully qualified target name. These properties are 'packageName' and 
 * 'baseTargetName'. The property 'packageName' is the name of a package that
 * contains targets, while 'baseTargetName' is a name of a target module in
 * that package.
 * <p>
 * Suppose, for example, that a JSON file contains the following object:
 * <pre>
 * {
 *      device:              "MSP430F5.*",
 *      deviceFamily:        "MSP430.*",
 *      packageName:         "ti.targets.msp430",
 *      baseTargetName:      "MSP430X"
 * }
 * </pre>
 * If a map of search terms contains "device" and the value is "MSP430F5529",
 * and the map also contains "deviceFamily" and the value is "MSP430", the
 * generated target name will be:
 * <pre>
 *      ti.targets.msp430.MSP430X
 * </pre>
 *  If the map also contains "elf" and the value is "true", the target will be:
 * <pre>
 *      ti.targets.msp430.elf.MSP430X
 * </pre>
 */
 public class TargetMapping {

    /* Each instance of TargetMapping keeps sets of all search properties and 
     * targets encountered in the corresponding JSON file. The sets are returned
     * by instance functions 'getKeys' and 'getAllTargets'.
     */
    protected Set<String> keys = new HashSet<String>();
    protected Set<String> targets = new HashSet<String>();

    protected List<TargetSearchPattern> tsp = new Vector<TargetSearchPattern>();

    /* Container class which holds target-device pairs read from entries in
     * a file.
     */
    static class TargetSearchPattern {
        Map<String, String> pattern;
        String target;

        TargetSearchPattern(Map<String, String> m, String t) {
            this.pattern = m;
            this.target = t;
        }
    }

    private static Logger logger = Logger.getLogger(
        "org.eclipse.rtsc.xdctools.ui.logger");

    /**
     *  ======== getAllTargets ========
     *
     *  Returns all targets found in a collection of JSON files.
     *
     *  @param path	collection of JSON files
     *  @return	set containing any target found in any of the JSON files
     */
    static public Set<String> getAllTargets(Collection<String> path) {
       Set<String> targs = new HashSet<String>();
       for (String jsonFile: path) {
            TargetMapping tm = new TargetMapping();
            tm.readFile(jsonFile);
            Set<String> res = tm.getAllTargets();
            for (String key: res) {
                targs.add(key);
            }
        }
        return (targs);
    }

    /**
     *  ======== getTarget ========
     *
     *  Searches for a target over a collection of JSON files.
     *
     *  @param path	collection of JSON files
     *  @param map	map with search terms
     *  @return	string containing a fully qualified target name or 'null' if
     *		no target matches 'map'
     */
    static public String getTarget(Collection<String> path,
                                   Map<String, String> map) {
        for (String jsonFile: path) {
            TargetMapping tm = new TargetMapping();
            tm.readFile(jsonFile);
            String res = tm.getTarget(map);
            if (res != null) {
                return (res);
            }
        }
        return (null);
    }

    /**
     *  ======== getKeys ========
     *
     *  Returns a set of poperties used in a collection of JSON files.
     *
     *  @param path	collection of JSON files
     *  @return	set containing any search property found in any of the JSON
     *          files
     */
    static public Set<String> getKeys(Collection<String> path) {
       Set<String> keys = new HashSet<String>();
       for (String jsonFile: path) {
            TargetMapping tm = new TargetMapping();
            tm.readFile(jsonFile);
            Set<String> res = tm.getKeys();
            for (String key: res) {
                keys.add(key);
            }
        }
        return (keys);
    }

    /* ======== instance functions ======== */

    /**
     *  ======== getTarget ========
     */
    public String getTarget(Map<String, String> map) {
        if (map.containsKey("deviceName")) {
            map.put("device", map.remove("deviceName"));
        }

        String mapLog = "Search Map: ";
        for (String mapProp: map.keySet()) {
            mapLog += mapProp + ": " + map.get(mapProp) + "   ";
        }
        logger.fine(mapLog);

        for (TargetSearchPattern jsonFileEntry: tsp) {
            boolean matchFound = true;
            logger.fine("New JSON entry");
            String logMessage = "";
            for (String searchProperty: jsonFileEntry.pattern.keySet()) {
                /* For a match to happen, the supplied map must have all search
                 * properties defined in a JSON entry, but not the other way
                 * around. In other words, a JSON entry can be less specific
                 * and the match will happen, but if a JSON entry is more
                 * specific than the supplied map, there is no match.
                 * The assumption is that supplied maps are always as specific
                 * as needed, because they are coming from the build properties,
                 * and we always know exactly if it's ELF, big endian, etc.
                 */
                if (map.get(searchProperty) == null) {
                    matchFound = false;
                    logger.fine("map does not contain " + searchProperty);
                    break;
                }
                try {
                    String jsonPattern
                        = jsonFileEntry.pattern.get(searchProperty);
                    String ccsinput = map.get(searchProperty);
                    Pattern pat = Pattern.compile(jsonPattern);
                    Matcher mat = pat.matcher(ccsinput);
                    if (!mat.find()) {
                        matchFound = false;
                        logger.fine("map does not match at " + searchProperty
                            + ": " + jsonPattern);
                        break;
                    }
                }
                catch (PatternSyntaxException pse) {
                    logger.warning("TargetMapping: invalid pattern "
                        + pse.getMessage());
                    System.out.println("TargetMapping: invalid pattern "
                        + pse.getMessage());
                    matchFound = false;
                    break;
                }
            }
            if (matchFound) {
                String target = jsonFileEntry.target;
                if (map.get("elf") == null || !map.get("elf").equals("true")) {
                    target = target.replace(".elf", "");
                }
                if (map.get("endianness") != null
                    && (map.get("endianness").toLowerCase().equals("big")
                    || map.get("endianness").toLowerCase().equals("be32")
                    || map.get("endianness").toLowerCase().equals("be8"))) {
                    target = target + "_big_endian";
                }
                logger.fine("Match found - target: " + target);
                return (target);
            }
        }
        logger.fine("No match found");
        return (null);
    }

    /**
     * ======== readFile ========
     *
     * Reads a JSON file and fills up tsp.
     */
    public void readFile(String jsonFile) {
        logger.fine("Reading " + jsonFile);
        NativeArray parsedObj;
        try {
            BufferedReader br = new BufferedReader(new FileReader(
                new File(jsonFile)));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            String fileContent = sb.toString();
            br.close();

            Context cx = Context.enter();
            JsonParser jp = new JsonParser(cx, ScriptRuntime.getGlobal(cx));
            parsedObj = (NativeArray)(jp.parseValue(fileContent));
        }
        catch (java.io.IOException ioe) {
            System.out.println("Error reading file " + jsonFile + ": "
                + ioe.getMessage());
            return;
        }
        catch (org.mozilla.javascript.json.JsonParser.ParseException pe) {
            System.out.println("Error parsing JSON file " + jsonFile + ": " 
                + pe.getMessage());
            return;
        }

        for (int i = 0; i < parsedObj.getLength(); i++) {
            IdScriptableObject o = (IdScriptableObject)(parsedObj.get(i));
            Object objs[] = ScriptableObject.getPropertyIds(o);

            /* This creates a map out of an object from the JSON file */
            Map<String, String> key = new HashMap<String, String>();

            String targetName = ".elf";
            String logMessage = "";
            for (int j = 0; j < objs.length; j++) {
                String prop = (String)objs[j];
                String propValue = (String)o.get(prop, o);

                /* If there is a 'Pattern' at the end, we remove it to make a
                 * match possible, since search keys do not have 'Pattern'.
                 */
                if (prop.matches(".*Pattern$")) {
                    prop = prop.substring(0, prop.lastIndexOf("Pattern"));
                }
                if (prop.equals("deviceName")) {
                   prop = "device";
                }
                if (prop.equals("baseTargetName")) {
                    targetName = targetName + "." + propValue;
                    continue;
                }
                if (prop.equals("packageName")) {
                    targetName = propValue + targetName;
                    continue;
                }
                if (prop.equals("elf") && propValue.equals("false")) {
                    targetName = targetName.replace(".elf", "");
                }
                
                this.keys.add(prop);
                key.put(prop, propValue);
                logMessage += "    " + prop + ": " + propValue + "   ";
            }

            /* "elf" is removed for non-TI targets because only TI targets
             * use such a directory name to differentiate between COFF and
             * ELF targets. 
             */
            if (!(targetName.startsWith("ti."))) {
                targetName = targetName.replace(".elf", "");
            }
            TargetSearchPattern pattern
                = new TargetSearchPattern(key, targetName);
            this.targets.add(targetName);
            logger.fine("Target: " + targetName);
            logger.fine(logMessage); 
            tsp.add(pattern);
        }
    }

    /**
     * ======== getAllTargets ========
     *
     * Return a set of all targets found in the JSON file.
     */
    public Set<String> getAllTargets() {
        return (this.targets);
    }

    /**
     * ======== getKeys ========
     *
     * Return a set of all keys found in the JSON file.
     */
    public Set<String> getKeys() {
        return (this.keys);
    }
}
