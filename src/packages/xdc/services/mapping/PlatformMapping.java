/* --COPYRIGHT--,EPL
 *  Copyright (c) 2014 Texas Instruments
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 *
 * --/COPYRIGHT--*/

package xdc.services.mapping;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.IdScriptableObject;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.ScriptRuntime;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.json.JsonParser;

/**
 * Reads platform mapping entries from a JSON file and builds a platform list
 * based on a map of search terms.
 *
 * This class reads a JSON file which contain platform selection objects. Then,
 * it accepts a map of search terms and selects objects based on these terms.
 * Objects are matched against the search terms in a map in the order the 
 * objects are listed in a JSON file. Each object that matches is added to the
 * platform list.
 * A match is found when all the properties that can be found in both, the
 * supplied search map and a platform JSON object, have equal values. The 
 * properties that exist only in the map or only in the object are ignored.
 *
 * Currently, the following JSON object properties are used for matching: 
 * `target` and `device`. Objects also have a property `platform` that contains 
 * the string added to the platform list if a match is found.
 */
public class PlatformMapping {
    /* LinkedHashMap keeps the order in which the entries are added */
    protected List<SearchPattern> platList = new Vector<SearchPattern>();

    /* Container class which holds entries from a JSON file.
     */
    static class SearchPattern {
        Map<String, String> pattern;
        String value;

        SearchPattern(Map<String, String> m, String v) {
            this.pattern = m;
            this.value = v;
        }
    }

    private  static List<String> genericPlatformArray =
            Arrays.asList("ti.platforms.stellaris",
                          "ti.platforms.tms320x28",
                          "ti.platforms.msp430",
                          "ti.platforms.tiva",
                          "ti.platforms.cc26xx",
                          "ti.platforms.concertoM3",
                          "ti.platforms.concertoC28",
                          "ti.platforms.simplelink",
                          "ti.platforms.tm0sxx",
                          "ti.platforms.msp432");

    /* Each instance of PlatformMapping keeps sets of all search properties and 
     * platforms encountered in the corresponding JSON file. The sets are
     * returned by instance functions 'getKeys' and 'getAllPlatforms'.
     */
    protected Set<String> keys = new HashSet<String>();
    protected Set<String> platforms = new HashSet<String>();

    private static Logger logger = Logger.getLogger(
        "org.eclipse.rtsc.xdctools.ui.logger");

    /**
     *  ======== getAllPlatforms ========
     *
     *  Returns all platforms found in a collection of JSON files.
     *
     *  @param path	collection of JSON files
     *  @return	collection containing any platform found in any of the JSON
     *          files
     */
    static public Collection<String> getAllPlatforms(Collection<String> path) {
       Set<String> plats = new HashSet<String>();
       for (String jsonFile: path) {
            PlatformMapping pm = new PlatformMapping();
            pm.readFile(jsonFile);
            Collection<String> res = pm.getAllPlatforms();
            for (String p: res) {
                plats.add(p);
            }
        }
        return (plats);
    }

    /**
     *  ======== getKeys ========
     *
     *  Returns a set of poperties used in a collection of JSON files.
     *
     *  @param path	collection of JSON files
     *  @return	collection containing any search property found in any of the
     *          JSON files
     */
    static public Collection<String> getKeys(Collection<String> path) {
       Set<String> keys = new HashSet<String>();
       for (String jsonFile: path) {
            PlatformMapping pm = new PlatformMapping();
            pm.readFile(jsonFile);
            Collection<String> res = pm.getKeys();
            for (String key: res) {
                keys.add(key);
            }
        }
        return (keys);
    }

    /**
     *  ======== getPlatforms ========
     *
     *  Searches for platforms over a collection of JSON files.
     *
     *  @param path	collection of JSON files
     *  @param map	map with search terms
     *  @return	collection of unique fully qualified platform names
     */
    static public Collection<String> getPlatforms(Collection<String> path,
                                                  Map<String, String> map) {

        /* Platforms must be returned in the order they are encountered in the
         * JSON files, and LinkedHashSet guarantees that iterators over it will
         * return elements in the order they are entered.
         */
        Set<String> platforms = new LinkedHashSet<String>();
        for (String jsonFile: path) {
            PlatformMapping pm = new PlatformMapping();
            pm.readFile(jsonFile);
            Collection<String> res = pm.getPlatforms(map);
            for (String platform: res) {
                platforms.add(platform);
            }
        }
        return (platforms);
    }

    /* ======== instance functions ======== */

    /**
     * ======== readFile ========
     *
     * Reads a JSON file and fills up platList.
     */
    public void readFile(String jsonFile) {
        logger.fine("Reading " + jsonFile);
        NativeArray parsedObj;
        try {
            BufferedReader br = new BufferedReader(new FileReader(
                new File(jsonFile)));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            String fileContent = sb.toString();
            br.close();

            Context cx = Context.enter();
            JsonParser jp = new JsonParser(cx, ScriptRuntime.getGlobal(cx));
            parsedObj = (NativeArray)(jp.parseValue(fileContent));
        }
        catch (java.io.IOException ioe) {
            System.out.println("Error reading file " + jsonFile + ": "
                + ioe.getMessage());
            return;
        }
        catch (org.mozilla.javascript.json.JsonParser.ParseException pe) {
            System.out.println("Error parsing JSON file " + jsonFile + ": " 
                + pe.getMessage());
            return;
        }

        for (int i = 0; i < parsedObj.getLength(); i++) {
            IdScriptableObject o = (IdScriptableObject)(parsedObj.get(i));
            Object objs[] = ScriptableObject.getPropertyIds(o);

            /* This creates a map out of an object from the JSON file */
            Map<String, String> key = new HashMap<String, String>();
            String value = null;
            String logMessage = "";
            for (int j = 0; j < objs.length; j++) {
                String prop = (String)objs[j];
                String propValue = (String)o.get(prop, o);
                if (prop.equals("platform")) {
                    value = propValue;
                    continue;
                }
                key.put(prop, propValue);
                this.keys.add(prop);
                logMessage += "    " + prop + ": " + propValue + "   ";
            }
            platList.add(new SearchPattern(key, value));
            logger.fine("Platform: " + value);
            logger.fine(logMessage);
            this.platforms.add(value);
        }
    }

    /**
     *
     *  ======== getPlatforms ========
     *
     */
    public Collection<String> getPlatforms(Map<String, String> map) {

        String mapLog = "Search Map: ";
        for (String mapProp: map.keySet()) {
            mapLog += mapProp + ": " + map.get(mapProp) + "   ";
        }
        logger.fine(mapLog);

        /* Platforms must be returned in the order they are encountered in the
         * JSON file, and LinkedHashSet guarantees that iterators over it will
         * return elements in the order they are entered.
         */
        Set<String> res = new LinkedHashSet<String>();
        if (map.get("target") == null) {
            logger.warning("PlatformMapping: 'target' is not supplied.");
            return (res);
        }

        for (SearchPattern jsonFileEntry: platList) {
            boolean matchFound = true;
            logger.fine("New JSON entry");
            for (String searchProperty: map.keySet()) {
                /* For a match to happen, the supplied map must match all of a
                 * JSON entry's properties that exist in the supplied map. If a
                 * map contains properties that do not exist in the entry, they
                 * are ignored. Same goes with properties that exist in the
                 * entry but not in the supplied map. To understand this logic,
                 * it is important first to note that only two properties are
                 * really expected in the supplied map - 'target' and 'device'.
                 * Any other property is in the map to save us the filtering
                 * work. If 'device' is not supplied then all platforms for
                 * 'target' should be returned, even those that specify
                 * 'device'. If 'device' is in the map, then generic platforms
                 * which usually do not specify the device are in the list.
                 * However, we'll keep this more general code in place until we
                 * are certain that we won't add any further properties to
                 * platform entries. At that point, the for loop that runs
                 * through map.keySet can be replaced with if statements.
                 */
                String jsonValue = jsonFileEntry.pattern.get(searchProperty);
                if (jsonValue != null && map.get(searchProperty) != null) {
                    Pattern pat = Pattern.compile(jsonValue);
                    Matcher mat = pat.matcher(map.get(searchProperty));
                    if (!mat.find()) {
                        matchFound = false;
                        logger.fine("map does not match at " + searchProperty);
                        break;
                    }
                }
            }
            if (matchFound) {
                String platform = jsonFileEntry.value;
                if (!res.contains(platform)) {
                    /* We are still checking 'genericPlatformArray' in case
                     * we have to deal with older platforms.json files which
                     * did not add $DeviceId$. As soon as we don't have to
                     * support SYS/BIOS 6.40, that array and related code can
                     * be removed.
                     */
                    if (genericPlatformArray.contains(platform)
                        && platform.indexOf(":$DeviceId$") == -1) {
                        platform = platform + ":$DeviceId$";
                    }
                    if (map.get("device") != null) {
                        platform = platform.replace("$DeviceId$",
                                                    map.get("device"));
                    }
                    res.add(platform);
                    logger.fine("Match found - platform: " + platform);
                }
            }
        }
        return (res);
    }

    /**
     *
     *  ======== getAllPlatforms ========
     *
     */
    public Collection<String> getAllPlatforms() {
        Set<String> res = new HashSet<String>();
        for (SearchPattern sp: platList) {
            String platform = sp.value;
            /* add $DeviceId$ for the generic platforms that don't have it. We
             * are still checking 'genericPlatformArray' in case we have to deal
             * with older platforms.json files which did not add $DeviceId$. As
             * soon as we don't have to support SYS/BIOS 6.40, we can just
             * simply return a list of unique platforms by using the set
             * 'platforms' generated while reading the JSON file.
             */
            if (genericPlatformArray.contains(platform)
                && platform.indexOf(":$DeviceId$") == -1) {
                res.add(platform + ":$DeviceId$");
            }
            else {
                res.add(platform);
            }
        }
        return (res);
    }

    /**
     * ======== getKeys ========
     *
     * Return a set of all keys found in the JSON file.
     */
    public Collection<String> getKeys() {
        return (this.keys);
    }
}

