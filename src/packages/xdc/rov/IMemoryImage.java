package xdc.rov;

/*!
 *  ======== IMemoryImage ========
 *  Memory reader interface.
 */
public interface IMemoryImage {
    /*!
     *  ======== readMaus ========
     *  Read an array of data from target memory. The size of each element in
     *  the array is a target MAU (minimal addressable unit). This is a byte on
     *  byte addressable targets and 2 bytes on 16-bit targets such as 28x and
     *  55x.
     *
     *  @param(buffer) Buffer to store the MAUs to be read.
     *  @param(addr)   Address of the data to fetch.
     *  @param(count)  Number of MAUs to read.
     *
     *  Returns whether or not the read was successful. Optionally may throw
     *  an exception to indicate failure.
     */
    public boolean readMaus(int[] buffer, long addr, int count) throws Exception;
    
    /*!
     *  ======== getSections ========
     *  Returns the Sections object representing the section map of the loaded
     *  application.
     */
    public Sections getSections();
}
