/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
package xdc.rov;

/**
 *  The call stack interface required by xdc.rov.server.
 *
 *  The xdc.rov.server relies on a Java "backend" to provide a "controller"
 *  which, in turn, provides up a "call stack" instance that enables the
 *  server to parse the call stack.
 *
 *  This interface should be moved into xdc.rov.server if we fix xdc.rov.Model
 *  to require RTSC instances for Model.start.  Otherwise, if we replace the
 *  RTSC instances with Java instance objects, we should keep this interface
 *  in xdc.rov - were it specifes the requirements for xdc.rov.Model.
 */
public interface ICallStack {

    /**
     *  Clear all local register values
     */
    void clearRegisters();

    /**
     *  Fetch the current values of the specified registers
     */
    void fetchRegisters(String names[]);

    /**
     *  Get the local value of the named register
     */
    long getRegister(String name);

    /**
     *  Set the local value of the named register
     */
    void setRegister(String name, long value);

    /**
     *  Return entire call stack as a string.
     *  
     *  Create a string representation of the current call stack based
     *  on the currently set register values.
     */
    String toText();
}
