/*
 *  ======== TargetEncoder.java ========
 */
package xdc.rov;

/*
 *  ======== TargetEncoder ========
 *  This class provides an API for encoding a value into an array of bytes
 *  meant for the target.
 */
public class TargetEncoder {

    private TargetType.Endianess endian;
    private int bitsPerChar;
    
    /*
     *  ======== TargetEncoder ========
     *  Constructor.
     *  
     *  endian - The endianness of the target.
     *  bitsPerChar - The target's minimum addressable unit (MAU) size in bits,
     *                i.e., the number of bits in a 'char', the target's
     *                 smallest type.
     */
    public TargetEncoder(TargetType.Endianess endian, int bitsPerChar)
    {
        this.endian = endian;
        this.bitsPerChar = bitsPerChar;
    }
    
    /*
     *  ======== encodeBytes ========
     *  Encodes the specified value into target bytes in the buffer.
     *  
     *  buffer - Buffer of target bytes where the encoded value will go.
     *  offset - Beginning index in 'buffer' where encoded value will go.
     *  value - Value to be encoded
     *  size - Size of the value in bytes (not MAUs) 
     */
    public void encodeBytes(byte buffer[], int offset, long value, int size)
    {
        /* Little endian */
        if (this.endian == TargetType.Endianess.LITTLE) {

            /* Copy the values in reverse order. */
            for (int i = 0; i < size; i++) {
                /* Casting to byte truncates the upper bits. */
                buffer[offset + i] = (byte) (value >> (i * 8));
            }
        }
        /* Big endian */
        else if (this.endian == TargetType.Endianess.BIG) {
            int shift = size - 1;
            for (int i = 0; i < size; i++) {
                /* Casting to byte truncates the upper bits. */
                buffer[offset + i] = (byte) (value >> (shift * 8));
                shift--;
            }
        }
    }
}
