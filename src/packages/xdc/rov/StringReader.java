package xdc.rov;

import java.io.IOException;

import xdc.rta.IOFReader;

/*
 *  ======== StringReader ========
 */
public class StringReader implements IOFReader {

    MemoryImage memReader;
    
    /*! In fetchString, number of characters to read at a time. */
    public int nChars = 64;
    
    /*! In fetchString, maximum number of characters to read. */
    public int maxChars = 128;
    
    /*
     *  ======== Constructor ========
     */
    public StringReader(MemoryImage mem)
    {
        this.memReader = mem;
    }
    
    /*
     *  ======== findString ========
     *  Retrieves a string from the given address, reading until it reaches a
     *  null char or until it has read 'maxChars'. 
     */    
    public String findString(long addr, boolean addrCheck)
    {
        /* String to return. */
        String res = "";
        
        /* 
         * Read characters, nChars at a time, until we reach a null character 
         * or maxChars.
         */
        for (long a = addr; a < addr + maxChars; a += nChars) {

            int[] buf = null;
            
            /* Read nChars */
            try {
                buf = memReader.readMaus(a, nChars, addrCheck);
            }
            catch (Exception e) {
                return (res);
            }
            
            /* Decode each char */
            for (int i = 0; i < nChars; i++) {
                int b = buf[i];
                if (b == 0) {
                    return (res);
                }
                res += (char)b; /* TODO: use StringBuffer not +=! */
            }
        }

        return (res);
    }

    /*
     *  ======== findString ========
     *  findString API to satisfy the IOFReader interface.
     *  This API simply calls findString with 'false' for the address check.
     */
    public String findString(long addr)
    {
        return (findString(addr, false));
    }
    
    /*
     * ======== parse ========
     * (non-Javadoc)
     * @see xdc.rta.IOFReader#parse(java.lang.String)
     */
    public String parse(String arg0) throws IOException 
    {
        return null;
    }
    
    /*
     * ======== close ========
     * (non-Javadoc)
     * @see xdc.rta.IOFReader#close()
     */
    public void close() throws IOException 
    {
    }

    public static IOFReader getOFReaderByName(String className)
        throws Exception
    {
        Class c = Class.forName(className);
        return ((IOFReader) c.newInstance());
    }
}
