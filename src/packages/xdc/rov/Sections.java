package xdc.rov;

import java.util.Vector;

/*!
 *  ======== Sections ========
 *  Represents the valid memory sections for a given application.
 */
public class Sections
{
    /*
     *  ======== Section ========
     */
    public class Section {
        public long base;
        public long length;
    }

    private Vector<Section> sections = null;

    /*!
     *  ======== Constructor ========
     */
    public Sections()
    {
        sections = new Vector<Section>();
    }
    
    /*!
     *  ======== addSection ========
     *  The section length should be specified in MAUs.
     */
    public void addSection(long base, long length)
    {
        Section sect = new Section();
        sect.base = base;
        sect.length = length;
        sections.add(sect);
    }

    /*!
     *  ======== getSectionArray ========
     *  Return array of all current sections
     */
    public Section[] getSectionArray()
    {
        return (sections.toArray(new Section[0]));
    }
    
    /*!
     *  ======== isValidDataRead ========
     *  Determines whether the given data memory read request (whose length
     *  is given in MAUs) falls within a valid data section.
     */
    public boolean isValidDataRead(long addr, int length)
    {
        /* 'sections' is an array of all the valid data sections. */
        for (int i = 0; i < sections.size(); i++) {
            Section sect = sections.get(i);

            /* If the read fits within this section, return true. */
            if ((addr >= sect.base) && 
                ((addr + length) <= (sect.base + sect.length))) {
                return (true);
            }   
        }
        
        /* If the read does not fit within any section, return false. */
        return (false);
    }
}
