## structure of the tree
SRC=../../..
EXPORTS=$(SRC)/../exports
IMPORTS=$(SRC)/../imports

## the manifest file for the plugin
MANIFEST=META-INF/MANIFEST.MF

## determine the structure of the plugin from the manifest
## determine the plugin name
NAME := $(shell sed -n '/^Bundle-SymbolicName:/{s/^[^:]*:[ \t]*//;s/;.*//;p}' $(MANIFEST))
## determine the plugin version
VERS := $(shell sed -n '/^Bundle-Version:/s/^.*:[ \t]*//p' $(MANIFEST))
## determine the jars to be included
JARS := $(shell sed -n '/\.jar/{s/\.jar.*/.jar/;s/^.*[: \t]//;p}' $(MANIFEST))

## choose the name and location of the plugin jar file
PLUGIN=$(EXPORTS)/$(NAME)_$(VERS)

## the default goal
all: $(PLUGIN)

## build the plugin
$(PLUGIN): $(MANIFEST) $(JARS)
	rm -rf $(PLUGIN)
	mkdir $(PLUGIN)
	cp -r --parents $(MANIFEST) $(JARS) $(PLUGIN)
	find $(PLUGIN) -name .svn -prune -exec rm -rf {} \;
	rm -f $(JARS)
	rm -rf packages

## usage: $(call jars-in,out-pat,in-pat)
## $(1)                      filename pattern in output location
## $(2)                      filename pattern in input location
## $(filter $(1),$(JARS))    get only the jars that match the output pattern
## $(wildcard $(patsubst $(1),$(2), ...)) get only the jars that exist at
##                           the input location
## $(patsubst $(2),$(1), ...)translate back to the output location
jars-in = $(patsubst $(2),$(1),$(foreach jar,$(filter $(1),$(JARS)),\
	$(wildcard $(patsubst $(1),$(2),$(jar)))))

## copy the jar files from the packages directory
$(call jars-in,%,$(SRC)/%): % : $(SRC)/%
	mkdir -p $(dir $@)
	cp -f $< $@

## clean up
clean:
	rm -rf $(PLUGIN)
	rm -f $(JARS)
	rm -rf packages
