#!/bin/ksh

srcs=${0%/*}
srcs=`(cd $srcs ; /bin/pwd)`

die () { echo "$@" >&2 ; exit 1 ; }

[ $# = 3 ] || die "usage: mksrc.sh plugin-dir src-dir out-dir"
plugindir=$1
srcdir=$2
outdir=$3

[ -d $plugindir ] || die "can't find plugin directory $plugindir"
[ -d $srcdir ] || die "can't find source directory $srcdir"
[ -d $outdir ] || die "can't find output directory $outdir"

## make the output directory absolute
outdir=`(cd $outdir; /bin/pwd)`

## add the name of the plugin to the output directory
pluginname=${plugindir##*/}
outdir=$outdir/$pluginname

[ ! -e $outdir ] || die "output plugin $outdir already exists"

## get a list of all the package.jar files we've included
jars=`(cd $plugindir ; find * -name "package.jar" -print)`

for jar in $jars; do
    dir="${jar%/java/package.jar}"
    subdir="${dir#packages/}"
    srcjava=`(cd $srcdir; echo $subdir/*.java)`

    if [ x"$srcjava" = x"$subdir/*.java" ]
    then
        ## no java files found, so don't make jar
        continue
    fi

    mkdir -p $outdir/$dir/java
    (cd $srcdir; zip -q $outdir/$dir/java/packagesrc.zip $srcjava)
done
