all: .local-imports 

.local-imports :
	@$(MSG) making $@ ...
	$(RM) $@
	@rm -rf ./examples/*
	@cp -rf ../../examples/* ./examples


clean::
	$(RM) exports/*
	@rm -rf ./examples/*
