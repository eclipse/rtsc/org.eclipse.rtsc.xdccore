
/*!
 *  ======== testexamples ========
 *  A collection of XDC examples used for regression tests.
 *
 *  This package is a "bundle" of all iliad examples exported
 *  for use in tests. The examples are added as raw source and
 *  are not built or released individually within this bundle.
 *  
 */
package testexamples [1, 0, 0]
{
}
