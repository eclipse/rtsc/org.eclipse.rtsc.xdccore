include ../../../imports.mak

all: .local_docs 

#
#  PLIST is defined in the generated makefile before including this file
#	 and is a list of all packages installed in the repository named 
#	 "packages"
#
.local_docs: $(patsubst %,package/.packages.%,$(PLIST))
	@$(MSG) generating bundle docs [XDCROOT = $(XDCROOT)] ...
	$(RMDIR) cdoc
	$(DOC_XDC)/xs xdc.tools.cdoc -s -od:cdoc -title "RTSC Examples Bundle" -P .
	@$(MSG) generating repository docs ...
	$(RMDIR) packages/cdoc
	$(DOC_XDC)/xs xdc.tools.cdoc -s -od:packages/cdoc -title "RTSC Examples" -Pr packages

clean::
	$(RMDIR) cdoc
