requires idioms.i101;
requires idioms.i102;
requires idioms.i103;
requires idioms.i104;
requires idioms.i105;
requires idioms.i106;
requires idioms.i107;
requires idioms.i201;
requires idioms.i202;
requires idioms.i203;
requires idioms.i204;
requires idioms.i205;
requires idioms.i301;
requires idioms.i302;
requires idioms.i303;
requires idioms.i304;
requires idioms.i305;
requires idioms.i306;

requires idioms.i401;
requires idioms.i402;
requires idioms.i403;
requires idioms.i404;
requires idioms.i405;

requires basic.hello;
requires legacy.hello;

/*!
 *  ======== xdcsamples ========
 *  A collection of XDC examples
 *
 *  This package is a "bundle" of selected examples that illustrate numerous
 *  techniques and concepts supported by the RTSC component model.
 *  
 *  The `idioms` packages are a graduated set of examples that should be
 *  visited in order; 101, 102, ...
 *
 *  In addition there are two examples of the "hello world" application:
 *  `basic.hello` and `legacy.hello`.  The `basic.hello` package
 *  illustrates a configurable application package that
 *  supports both debug and release variants.  The `legacy.hello` package
 *  illustrates how to encapsulate a portable legacy code base without making
 *  any changes to the sources.
 *
 *  Follow the link(s) below to learn more about each example.
 *
 *  @a(LINKS)
@p(html)
<U><A HREF="../../packages/cdoc/index.html" target="_top">Examples Documentation</A></U>
@p
 */
package xdcsamples [1, 0, 0]
{
}
