#!/bin/ksh
#
#  Get all native host support for the designated host OS
#
#  usage: get_host [-h os] output-dir
#
usage="usage $0 [-h os] [-j] destination_dir"

myerror() {
    echo "$0: Error: $1"
    exit 1
}

## check arguments
host="Windows"
while getopts :h: c ; do
    case $c in
	h) host="$OPTARG" ;;
	\?) echo "$usage"; exit 1;
    esac
done
shift `expr $OPTIND - 1`
if [ "$#" -ne 1 ]; then
    echo "$usage"
    myerror "You must give a destination directory."
fi

##  map host os name to OS and architecture
fullhost=$host
arch="`echo ${host}_ | cut -d_ -f 2`"
host="`echo ${host}_ | cut -d_ -f 1`"
if [ "$arch" = "" ]; then
    arch="x86"
fi

## find xdc commands to find/copy packages
if [ "$XDCROOT" = "" -o ! -d "$XDCROOT/bin" ]; then
    XDCROOT="$TOOLS/vendors/xdc/xdctools_3_25_04_88/Linux"
fi
if [ ! -d "$XDCROOT" ]; then
    myerror "invalid XDCROOT directory: ${XDCROOT}"
fi
XDCPKG=${XDCROOT}/bin/xdcpkg
XDCCPP=${XDCROOT}/bin/xdccpp

## find directories containing host packages (^/../../imports;^/../packages)
#DIRS="`cd ../../../imports; /bin/pwd`"
DIRS="${DIRS} `cd ../../packages/host; /bin/pwd`"

## compute an "exclude" filter based on $host and $arch
xfilter=test
if [ "$host" = "Linux" ]; then
    xfilter="$xfilter|/SUN|/microsoft|/ti|/macos"
elif [ "$host" = "Windows" ]; then
    xfilter="$xfilter|/SUN|/gnu|/ti|/macos"
elif [ "$host" = "MacOS" ]; then
    xfilter="$xfilter|/SUN|/microsoft|/ti|/PC"
fi

## get list of host packages minus those inappropriate for the specifed host
pkgs="`${XDCPKG} -a ${DIRS} | egrep 'targets|platforms' | egrep -v $xfilter`"

## copy host packages to destination directory
mkdir -p $1
for p in $pkgs; do
    ${XDCCPP} $p $1
done
