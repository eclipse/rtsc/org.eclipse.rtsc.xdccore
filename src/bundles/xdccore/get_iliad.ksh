#!/bin/ksh
#
#  This script installs iliad tools (and all necessary components necessary
#  for its operation) for execution on a PC or Linux workstation.
#
#  This script can only be run from Linux.
#
#  Usage: install [-h os] [-j] destination_dir
#
#  If destination_dir does not exist a new directory will be created.  This
#  directory will contain all files necessary to run iliad; including the
#  appropriate JRE (Java runtime environment).  The JRE is copied to
#  destination_dir/jre.
#
#  The JRE copied is the one specified by the $XDCTOOLS_JAVA_HOME environment
#  variable, if it is defined; otherwise, it's the one that's a part of the JDK
#  specified in etc/TOOLS file.
#
#  Options:
#	-j      - don't copy JRE to destination directory
#	-h os   - install support for specified host (default is Windows)
#                 host name of the form <name>_<arch> specify a specific
#                 host architecture: Linux_arm refers to arm architecture
#                 Linux, for example.
#
#! Revision History
#! ================
#! 09-Apr-2004 dr: 	created

usage="usage $0 [-h os] [-j] destination_dir"

myerror() {
    echo "$0: Error: $1"
    exit 1
}

host="Windows"
if [ "$XDCTOOLS_JAVA_HOME" = "" ]; then
    XDCTOOLS_JAVA_HOME="$TOOLS"
fi

#
# Check arguments
#
jflag="false"
while getopts :jnh: c ; do
    case $c in
        h) host="$OPTARG" ;;
        j) jflag=true ;;
        \?) echo "$usage"; exit 1;
    esac
done
shift `expr $OPTIND - 1`
if [ "$#" -ne 1 ]; then
    echo "$usage"
    myerror "You must give a destination directory."
fi

#
#  Find other install scripts
#
dir=`dirname ${0#*:}`
drive="${0%%:*}:"
if [ "$drive" = "$0:" ]; then
    drive=""
fi
ROOT="$drive$dir"
for f in $ROOT $ROOT/../../packages/tisb/xdc/utils; do
    if [ -r $f/install_init.ksh ]; then
        UTILS_BIN=$f
    fi
done
if [ "$UTILS_BIN" = "" ]; then 
    myerror "Can't find install_init.ksh script"
fi
for f in $ROOT $ROOT/../../packages/tisb/xdc/utils $ROOT/../../../src; do
    if [ -r $f/install_iliad_core.ksh ]; then
        BIN=$f
    fi
done
if [ "$BIN" = "" ]; then 
    myerror "Can't find install_iliad_core.ksh script"
fi

#
#  map rational host os name to underlying cryptic flags for old scripts
#
fullhost=$host
arch="`echo ${host}_ | cut -d_ -f 2`"
host="`echo ${host}_ | cut -d_ -f 1`"
if [ "$arch" = "" ]; then
    arch="x86"
fi

if [ "$host" = "Windows" -o "$host" = "windows" ]; then
    osopt="";
    if [ "$arch" = "arm" ]; then
	myerror "Windows ARM architecture is not supported (yet)"
    fi
elif [ "$host" = "Linux" -o "$host" = "linux" ]; then
    osopt="-l ";
    if [ "$arch" = "arm" ]; then
	osopt="-l -a ";
    fi
elif [ "$host" = "MacOS" -o "$host" = "macos" ]; then
    osopt="-m ";
else
    echo "$usage"
    myerror "Unknown host os: $host"
fi

#
#  Source install_init to compute SRC, DST, TOP, and VERS
#
. $UTILS_BIN/install_init.ksh $0 $1

#
#  Start installation ...
#
echo "installing iliad (version $VERS) from $TOP to $DST ..."; echo

find $DST -type d -exec chmod +w {} \;

#
#  Install iliad ...
#
$BIN/install_iliad_core.ksh $osopt $TOP $DST

#
#  Install xs ...
#
$BIN/install_xs_core.ksh $osopt $TOP $DST

#
#  Install JRE (optionally)
#
if [ "$jflag" = "false" -a "$arch" = "x86" ]; then
    rm -rf $DST/jre
    $UTILS_BIN/install_jre.ksh $osopt $XDCTOOLS_JAVA_HOME $DST/jre
else
    echo "no JRE install for $fullhost"
fi

# Adding 32-bit JRE for IAR IDE
if [ "$host" = "Windows" -o "$host" = "windows" ]; then
    rm -rf $DST/jre32
    $UTILS_BIN/install_jre.ksh $XDCTOOLS_JAVA_HOME/../../Windows32/jre $DST/jre32
# removing large optional files
    rm -rf $DST/jre32/bin/server
fi

echo "installation done."

exit 0
