EXPORT_DIR   = ../../../exports
IMPORT_DIR   = ../../../imports

PACKAGE_NAME = xdccore
EXPORTS      = xdccore-Windows.zip xdccore-Linux.zip xdccore-Linux_arm.zip xdccore-MacOS.zip \
	       xdchost-Windows.zip xdchost-Linux.zip xdchost-Linux_arm.zip xdchost-MacOS.zip

JRE_Linux    = $(XDCTOOLS_JAVA_HOME)
JRE_Windows  = $(subst Linux,Windows,$(JRE_Linux))
JRE_Linux_arm =
JRE_MacOS    = $(subst Linux,MacOS,$(JRE_Linux))

SWT_Linux    = $(IMPORT_DIR)/xdc/tools/sg/swt_linux
SWT_Windows  = $(subst linux,win64,$(SWT_Linux))
SWT_Linux_arm =
SWT_MacOS    = $(subst linux,macos,$(SWT_Linux))

release: $(foreach d,$(EXPORTS),$(EXPORT_DIR)/$(d))

.SECONDARY: Windows/.install Linux/.install Linux_arm/.install MacOS/.install

## create OS-specific xdctools product image (minus ./packages)
%/.install:
	@$(MSG) installing host-specific tools into $*/$(PACKAGE_NAME) ...
	$(SH) XDCTOOLS_JAVA_HOME="$(JRE_$*)" ./get_iliad.ksh -h $* $*/$(PACKAGE_NAME)
	@$(if $(SWT_$*),$(MSG) "    adding $(SWT_$*) ...")
	$(if $(SWT_$*),mkdir $*/$(PACKAGE_NAME)/packages; $(XDCCPP) $(SWT_$*) $*/$(PACKAGE_NAME)/packages)
	$(SH) ./get_host.ksh -h $* $*/xdchost/local
	$(RM) $@; $(TOUCH) $@

## create and export OS-specific zips to exports
$(EXPORT_DIR)/xdccore-%.zip: $(PACKAGE_NAME).zip %/.install
	@$(MSG) creating $@ ...
	$(RM) $@; $(CP) $< $@
	if [ -r $*/xdccore/jre ]; then chmod a-w $*/xdccore/jre; fi
	cd $*; $(ZIP) ../$@ -q -r -u $(PACKAGE_NAME)
	if [ -r $*/xdccore/jre ]; then chmod u+w $*/xdccore/jre; fi

$(EXPORT_DIR)/xdchost-%.zip: %/.install
	@$(MSG) creating $@ ...
	$(RM) $@;
	cd $*; $(ZIP) ../$@ -q -r xdchost

clean::
	$(RMDIR) Windows Linux Linux_arm MacOS
	$(MKDIR) Windows Linux Linux_arm MacOS
